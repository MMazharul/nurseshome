<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'front_home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
// $route['doctorscont'] = 'Doctorscont';


# ======================== Nurse profile routes ========================= #

$route['nurse-profile'] = 'nurse/edit_profile/$1';
$route['home'] = 'home';


#============================================================#

$route['nurse-bio-add'] = 'nurse/add_bio/$1';
$route['nurse-bio-list'] = 'nurse/list_bio/$1';
$route['nurse-bio-update'] = 'nurse/edit_bio/$1';

#============================================================#

$route['nurse-assessment-add'] = 'nurse/add_assessment/$1';
$route['nurse-assessment-list'] = 'nurse/list_assessment/$1';
$route['nurse-assessment-update'] = 'nurse/edit_assessment/$1';

#============================================================#

$route['nurse-additional-certificate-add'] = 'nurse/add_additional_certificate/$1';
$route['nurse-additional-certificate-edit'] = 'nurse/edit_additional_certificate/$1';
$route['nurse-additional-certificate-list'] = 'nurse/list_additional_certificate/$1';

#============================================================#

$route['nurse-education-qualification-add'] = 'nurse/add_education/$1';
$route['nurse-education-qualification-list'] = 'nurse/list_education/$1';
$route['nurse-education-qualification-update'] = 'nurse/edit_education/$1';

#============================================================#

$route['nurse-experience-add'] = 'nurse/add_experience/$1';
$route['nurse-experience-list'] = 'nurse/list_experience/$1';
$route['nurse-experience-update/:num'] = 'nurse/edit_experience';

#============================================================#

$route['nurse-notes-add'] = 'nurse/add_notes/$1';
$route['nurse-notes-list'] = 'nurse/list_notes/$1';
$route['nurse-notes-update'] = 'nurse/edit_notes/$1';
#============================================================#

$route['nurse-reviews-add'] = 'nurse/add_reviews/$1';
$route['nurse-reviews-list'] = 'nurse/list_reviews/$1';
$route['nurse-reviews-update'] = 'nurse/edit_reviews/$1';
#============================================================#

$route['nurse-schedule-add'] = 'nurse/add_schedule/$1';
$route['nurse-schedule-add_admin/(:num)'] = 'nurse/add_schedule_admin';
$route['nurse-schedule-monthly'] = 'nurse/add_schedule_monthly/$1';
$route['nurse-schedule-list'] = 'nurse/list_schedule/$1';
$route['nurse-schedule-update'] = 'nurse/edit_schedule/$1';
#============================================================#

$route['nurse-competency-add'] = 'nurse/add_competency/$1';
$route['nurse-competency-list'] = 'nurse/list_competency/$1';
$route['nurse-competency-update'] = 'nurse/update_competency/$1';
# ======================== Nurse profile routes ========================= #patient-profile

#============================================================#

$route['nurse_patient_list'] = 'nurse/nurse_patient_list/$1';
$route['nurse-profile-view'] = 'nurse/nurse_profile_view/$1';

$route['patient-profile/(:num)'] = 'nurse/profile';

$route['nurse-edit-profile/(:num)'] = 'nurse/nurse_edit_profile/$1';

$route['patient-edit-profile/(:num)'] = 'patient/patient_edit_profile/$1';
$route['patient-profile/(:num)'] = 'patient/patient_profile_update/$1';
# ======================== Nurse Booking routes ========================= #

$route['patient-book-nurse/(:num)/(:num)'] = 'patient/book_nurse';

$route['patient-bookings-for-nurse'] = 'nurse/bookings';

$route['nurse-bookings-for-patient'] = 'patient/bookings';

$route['profile'] = 'patient/profile';

$route['condition'] = 'patient/condition';

$route['edit-condition'] = 'patient/edit_condition';

$route['assessment'] = 'patient/assessment';

$route['booking-details/(:num)'] = 'nurse/booking_details';

$route['change-password'] = 'auth/change_password_view';

$route['notes-list/(:num)'] = 'nurse/list_notes/$1';

$route['add-review/(:num)'] = 'patient/review/$1';




$route['care-patient/(:num)'] = 'patient/care_for_patient/$1';


$route['add_nurse_schedule/(:num)'] = 'nurse/schedule_add/$1';


$route['profile_update'] = 'ho/add_experience/$1';


$route['booking_transaction-for-patient'] = 'patient/transaction';




$route['booking_details/(:num)'] = 'booking/booking_details/$1';


//===============================Addons Route============================================// 

$route['add-addons'] = 'setting/add_addons';

$route['list-addons'] = 'setting/addons_list';

$route['view-addon/(:num)'] = 'setting/view_addon/$1';