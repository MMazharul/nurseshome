<?php $this->load->view('backend/head_link');?>

	<body class="app sidebar-mini rtl">

		<!--Global-Loader-->
		<!-- <div id="global-loader">
			<img src="back_assets/images/icons/loader.svg" alt="loader">
		</div> -->

		<div class="page">
			<div class="page-main">
				<!--app-header-->
                
                
                <?php $this->load->view('backend/header');?>



                <!-- app-content-->
				<div class="container content-area">
					<div class="side-app">


						<div class="row">
							<div class="col-12">
								<div class="card">
									 <?php if($this->session->flashdata('msg')){ ?>
                                <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <div class="alert-message">
                                        <span><?=$this->session->flashdata('msg');?></span>
                                    </div>
                                </div>
                            <?php } ?> 
									<div class="card-header ">
										<h3 class="card-title ">State List</h3>
										<div class="card-options">
											<a href="location/add_state"><button id="add__new__list" type="button" class="btn btn-sm btn-primary " data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fa fa-plus"></i> Add a new State</button></a>
										</div>
									</div>
									<div class="table-responsive">
										<table class="table card-table table-striped table-vcenter table-outline table-bordered text-nowrap ">
											<thead>
												<tr>
													<th scope="col" class="border-top-0">Sl</th>
													<th scope="col" class="border-top-0">State Title</th>
													
													<th scope="col" class="border-top-0">Action </th>
													
												</tr>
											</thead>
											<tbody>
												<?php
												if($cnt==0)
												{
													echo "No Data Found";
												}

												$sl=0;
foreach ($state as $key => $value) 
{
	$id=$value['id'];
	$sl+=1;
	?><tr>
													<th scope="row"><?php echo $sl?></th>
													<td><?=$value['state_name']?></td>
													
													<td>
														<a class="btn btn-sm btn-primary" href="location/edit_location/<?php echo $id?>"><i class="fa fa-edit"></i> Edit</a>
														<!-- <a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a> -->
													</td>
													
												</tr>
	<?php
}

												?>
												
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>


					</div><!--End side app-->
 <?php $this->load->view('backend/right_sidebar');?>

                    <?php $this->load->view('backend/footer');?>

				</div>
				<!-- End app-content-->
			</div>
		</div>
		<!-- End Page -->

		<!-- Back to top -->
		<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

	<?php $this->load->view('backend/footer_link');?>
	</body>
</html>