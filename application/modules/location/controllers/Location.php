<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Location extends MX_Controller {

    //public $counter=0;
    function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->model('auth/auth_model');
        $this->load->model('Location_model');
        // $this->load->model('product/product_model');
        // $this->load->helper('text');
        // $this->load->helper(array('form', 'url'));
        // $this->load->helper('inflector');
        //$this->load->library('encrypt');
        // $this->load->model('home_model');
        // $this->load->model('admin/admin_model');
        // $this->load->model('product/product_model');
        
        if(!$this->session->userdata('loginid'))
        {
           redirect('auth/index');
        }
    }
    public function state_list()
    {
       $data['state']= $this->Location_model->select_all('states');
       $data['cnt']=count($data['state']);
        $this->load->view('state/state_list',$data);
        
    }
    public function add_state()
    {
  
        $this->load->view('state/add_state');
        
    }
    public function add_location($value='')
    {
        $data['state_name']=$this->input->post('state_name');
        $this->Location_model->insert('states',$data);
        $this->session->set_flashdata('msg', 'Location Added  Successfully done.');
        redirect('location/state_list','refresh');
    }
      public function area_list()
    {
  
        $this->load->view('area/area_list');
        
    }
        public function edit_location($locationid)
    {
       
        $data['state']= $this->Location_model->select_with_where('*',"id=".$locationid,'states');
        $this->load->view('state/edit_state',$data);
    }
    public function edit_location_post($value='')
    {
        $stid=$this->input->post('stid');
        $data['state_name']=$this->input->post('state_name');
        $this->Location_model->update_function('id',$stid,'states',$data);
        $this->session->set_flashdata('msg', 'Location Updated  Successfully done.');
        redirect('location/state_list','refresh');
    }



    


}

?>
