<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Front_home extends MX_Controller {

    //public $counter=0;
    function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->model('front_home_model');
    }

    public function index()
    {

      $nurselist['state']=$this->front_home_model->select_all('states');
      $this->load->view('index',$nurselist);

    }
public function search_nurse($value='')
{

        $charge=$this->input->post('charge');
        //echo $charge;
        $location=$this->input->post('location');
        $nurse_type=$this->input->post('nurse_type');
        $Experience=$this->input->post('Experience');
        $Availability=$this->input->post('Availability');
        $langauge = $this->input->post('langauge');
        $nurselist['state']=$this->front_home_model->select_all('states');



     $nurselist['nurselist']=$this->front_home_model->custom_search_column($charge,$location,$nurse_type,$Experience,$langauge,$Availability);


$this->load->view('list',$nurselist);

}

}

?>
