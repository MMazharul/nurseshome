<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Front_home_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function check_login($email, $pass) {
        $this->db->select('*');
        $this->db->from('login');
        $this->db->where('email', $email);
        $this->db->where('password', $pass);
        $result = $this->db->get();
        return $result->result_array();
    }
     public function password_change($email,$data)
    {
        $this->db->where('email',$email);
        $this->db->update('login',$data);
    }
    public function select_with_where($selector, $condition, $tablename)
    {
    	$this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result = $this->db->get();
        return $result->result_array();
    }
    public function select_condition_random_with_limit($table_name,$condition,$limit)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by('created_at','RANDOM');
        $this->db->limit($limit);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function custom_search_column($charge,$location,$nurse_type,$Experience,$language,$Availability)
    {
      $this->db->select('*');
      $this->db->from('users');
        if(!empty($charge))
        {
          $this->db->where('charge',$charge);
        }
        if(!empty($location))
        {
            $this->db->where('location',$location);
        }
        if(!empty($nurse_type))
        {
          $this->db->where('nurse_work_type',$nurse_type);
        }
        if(!empty($Experience))
        {
            $this->db->where('total_experience',$Experience);
        }
        if(!empty($language))
        {
            $this->db->where('langauge',$language);
        }

        if(!empty($Availability))
        {
            // $this->db->where('loginid in (select distinct nurse_id from schedule_day where schedule_day=',$Availability);
            $this->db->where("loginid in (select distinct nurse_id from schedule_day where schedule_day= '$Availability')");
        }
        $this->db->where('user_role',3);

        $result = $this->db->get();
        return $result->result_array();

    }
      public function select_condition_decending_with_limit($table_name,$condition,$limit)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by('created_at','DESC');
        $this->db->limit($limit);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }


    public function select_all_acending($table_name,$col_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by($col_name,'ASC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function select_all($table_name)
    {
    	$this->db->select('*');
		$this->db->from($table_name);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
    }

       public function update_function($columnName, $columnVal, $tableName, $data)
    {
        $this->db->where($columnName, $columnVal);
        $this->db->update($tableName, $data);
    }

    // ==== Search Model Query Began =====//

    public function category_id_like($search_content)
    {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->like('name', $search_content, 'both');
        $result=$this->db->get();
        return $result->result_array();
    }

    public function get_search_item($condition,$table_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by($table_name.'.created_at','DESC');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->limit(5);
        $result=$this->db->get();
        return $result->result_array();
    }
      public function get_search_input_item($table_name,$condition,$search_content)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->like('p_name', $search_content,'after');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->limit(5);
        $result=$this->db->get();
        return $result->result_array();
    }

      public function fetch_item($query)
    {
        $this->db->select('*');
        $this->db->from('product');
        if($query!=''){
            $this->db->like('p_name',$query);
        }
        $this->db->order_by('created_at','DESC');
        // $where = '(' . $condition . ')';
        // $this->db->where($where);
        // $this->db->limit(10);
        return $this->db->get();

    }

    public function select_two_join($selector,$table_name,$join_table,$join_condition,$join_table2,$join_condition2)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition);
        $this->db->join($join_table2,$join_condition2);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }

     public function select_two_join_serialize($selector,$table_name,$join_table,$join_condition,$join_table2,$join_condition2,$order_col,$order_action)
    {
        $this->db->select($selector);
        $this->db->from($table_name);

        $this->db->join($join_table,$join_condition);
        $this->db->join($join_table2,$join_condition2);
        $this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }
     public function select_two_join_serialize_where($selector,$table_name,$join_table,$join_condition,$join_table2,$join_condition2,$order_col,$order_action,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
         $where = '(' . $condition . ')';
        $this->db->join($join_table,$join_condition);
        $this->db->join($join_table2,$join_condition2);
        $this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }
  public function custom_search_charge($charge)
     {

    $query="select * from users where user_role=3 and charge<=$charge";
    $result = $this->db->query($query) or die ("Schema Query Failed");
    $result=$result->result_array();
    return $result;

     }
    public function custom_search_charge_location($charge,$location)
     {

    $query="select * from users where user_role=3 and charge<=$charge and location='$location' ";
    $result = $this->db->query($query) or die ("Schema Query Failed");
    $result=$result->result_array();
    return $result;

     }
    public function custom_search_charge_location_nurse_type($charge,$location,$nurse_type)
     {

    $query="select * from users where user_role=3 and charge<=$charge and location='$location'  and nurse_work_type=$nurse_type";
    $result = $this->db->query($query) or die ("Schema Query Failed");
    $result=$result->result_array();
    return $result;

     }
   public function custom_search_charge_location_nurse_type_exp($charge,$location,$nurse_type,$exp)
     {

    $query="select * from users where user_role=3 and charge<=$charge and location='$location' and nurse_work_type=$nurse_type and total_experience>=$exp";
    $result = $this->db->query($query) or die ("Schema Query Failed");
    $result=$result->result_array();
    return $result;

     }

    public function custom_search_charge_location_nurse_type_exp_avail($charge,$location,$nurse_type,$exp,$Availability)
     {

    $query="select * from users where user_role=3 and charge<=$charge and location='$location' and nurse_work_type=$nurse_type and total_experience>=$exp and loginid in (select distinct nurse_id from schedule_day where schedule_day='$Availability')";
    $result = $this->db->query($query) or die ("Schema Query Failed");
    $result=$result->result_array();
    return $result;

     }

    public function custom_search_charge_location_nurse_type_exp_avail_lan($charge,$location,$nurse_type,$exp,$Availability,$language)
     {

    $query="select * from users where user_role=3 and charge<=$charge and location='$location' and nurse_work_type=$nurse_type and total_experience>=$exp and loginid in (select distinct nurse_id from schedule_day where schedule_day='$Availability') and langauge in ($language)";
    $result = $this->db->query($query) or die ("Schema Query Failed");
    $result=$result->result_array();
    return $result;

     }
    public function custom_search_charge_nurse($charge,$nurse_type)
     {

    $query="select * from users where user_role=3 and charge<=$charge and nurse_work_type=$nurse_type";
    $result = $this->db->query($query) or die ("Schema Query Failed");
    $result=$result->result_array();
    return $result;

     }
    // ====  Search  Model Query End=== ///
}
