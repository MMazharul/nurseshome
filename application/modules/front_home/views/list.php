<?php $this->load->view('frontend/head_link');?>

<body>

	<div id="preloader" class="Fixed">
		<div data-loader="circle-side"></div>
	</div>
	<!-- /Preload-->

	<div id="page">
		       <?php $this->load->view('frontend/header');?>


           <main class="theia-exception">



   <div class="container margin_60_35">

     <div class="row">
       <div class="col-lg-12">
                         <form action="<?=base_url()?>front_home/search_nurse" method="post" autocomplete="off">


                    <div class="row" style="margin-left: 0px !important;margin-bottom: 10px;background: #FFF;padding: 5px;border-radius: 5px;margin-right: 0px !important;">
                        <div class="col-lg-1 col-md-1 col-sm-12 p-0">
                            <select class="form-control search-slt" name="charge" id="exampleFormControlSelect1">
															<option disabled selected>Charge</option>
														 <option value="25" <?php if(set_value('charge')==25) { echo "selected"; }  ?>>RM 25</option>
														 <option value="50" <?php if(set_value('charge')==50) { echo "selected"; }  ?>>RM 50</option>
														 <option value="100" <?php if(set_value('charge')==100) { echo "selected"; }  ?>>RM 100</option>
														 <option value="150" <?php if(set_value('charge')==150) { echo "selected"; }  ?>>RM 150</option>
														 <option value="200" <?php if(set_value('charge')==200) { echo "selected"; }  ?>>RM 200</option>
														 <option value="250" <?php if(set_value('charge')==250) { echo "selected"; }  ?>>RM 250</option>
															<option value="300" <?php if(set_value('charge')==300) { echo "selected"; }  ?>>RM 300</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 p-0">
                            <select class="form-control search-slt" name="location" id="exampleFormControlSelect1">
                                        <option value="0">Location--</option>
                               <?php

foreach ($state as $key => $value)
{
    // if($value==$nurse_details[0]['location'])
    $r=$value['state_name'];
    ?>
    <option value='<?php echo $r?>' <?php if(set_value('location')==$r) { echo "selected"; } ?>><?php echo $r?></option>

    <?php
}


                               ?>
                            </select>
                        </div>
                          <div class="col-lg-2 col-md-2 col-sm-12 p-0">
                             <select class="form-control search-slt" name="nurse_type" id="exampleFormControlSelect1">
                                <option disabled selected>Type of Nurse</option>
																<option value="1" <?php if(set_value('nurse_type')==1) { echo "selected"; }  ?>>Full Time</option>
																<option value="2" <?php if(set_value('nurse_type')==2) { echo "selected"; }  ?>>Part Time</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 p-0">
                             <select class="form-control search-slt" name="Experience" id="exampleFormControlSelect1">
															 <option disabled selected>Select Experience</option>

															 <option value="1" <?php if(set_value('Experience')==1) { echo "selected"; }  ?>>1 Year</option>
															 <option value="2" <?php if(set_value('Experience')==2) { echo "selected"; }  ?>>2 Year</option>
															 <option value="3" <?php if(set_value('Experience')==3) { echo "selected"; }  ?>>3 Year</option>
															 <option value="4" <?php if(set_value('Experience')==4) { echo "selected"; }  ?>>4 Year</option>
															 <option value="5" <?php if(set_value('Experience')==5) { echo "selected"; }  ?>>5 Year</option>
															 <option value="6" <?php if(set_value('Experience')==6) { echo "selected"; }  ?>>6 Year</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 p-0">
                            <select class="form-control search-slt"  name="Availability" id="exampleFormControlSelect1">
																<option disabled selected>Availability</option>
 														 		<option value="sun" <?php if(set_value('Availability')=="sun") { echo "selected"; }  ?>>Sunday </option>
 														 		<option value="mon" <?php if(set_value('Availability')=="mon") { echo "selected"; }  ?>>Monday</option>
 														 		<option value="tue" <?php if(set_value('Availability')=="tue") { echo "selected"; }  ?>>Tuesday</option>
 														 		<option value="wed" <?php if(set_value('Availability')=="wed") { echo "selected"; }  ?>>Wednesday</option>
 														 		<option value="thu" <?php if(set_value('Availability')=="thu") { echo "selected"; }  ?>>Thursday</option>
 														 		<option value="fri" <?php if(set_value('Availability')=="fri") { echo "selected"; }  ?>>Friday</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 p-0">
              <select class="form-control select2"   name="langauge"  id="language" data-placeholder="Language">
								<option disabled selected>language</option>
								<option value="1" <?php if(set_value('langauge')==1) { echo "selected"; }  ?>>English</option>
 							 <option value="2" <?php if(set_value('langauge')==2) { echo "selected"; }  ?>>Bahasa Malaysia</option>
 							 <option value="3" <?php if(set_value('langauge')==3) { echo "selected"; }  ?>>Mandarin</option>
 							 <option value="4" <?php if(set_value('langauge')==4) { echo "selected"; }  ?>>Hindi</option>
 							 <option value="5" <?php if(set_value('langauge')==5) { echo "selected"; }  ?>>Cantonese</option>
 							 <option value="6" <?php if(set_value('langauge')==6) { echo "selected"; }  ?>>Tamil</option>
                      </select>

                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-12 p-0">
                            <button type="submit" class="btn btn-danger wrn-btn">Search</button>
                        </div>
                    </div>



        </form>



             <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="card Relatedpost">
                 <div class="card-header">
                   <span class="card-title" style="text-align:justifiy;font-weight:500;color:#000">
										 <?php if(count($nurselist)>0){?>
										 Search Result
									 <?php } else {?>
										 We are sorry, we couldn't find a match for your search. Please try again or contact our customer service department on WhatsApp at +6016-387 0749 or call us at 1300-22-NURSE(6877) between 9.00am to 5.00pm (Monday to Fridays)
									 <?php } ?>
								 </span>
                 </div>
                 <div class="card-body">
                <?php
                //echo "this is ok....";
foreach ($nurselist as $key => $value)
{
  $country_code=$value['country_code'];
  $contact_no=$value['contact_no'];
  $first_name=$value['first_name'];
  $last_name=$value['last_name'];
  $email=$value['email'];
  $registered_date=$value['registered_date'];
  $charge=$value['charge'];
  $profile_pic=$value['profile_pic'];
  $nurse_id=$value['loginid'];
  $point=$value['point'];
  $total_served_patient=$value['total_served_patient']






                ?>
                   <div class="row">
<div class="strip_list wow fadeIn" style="width: 100%;">
            <span class="wish_bt" style="float: right;font-weight: 700;color: #e8598b;font-size: 20px;"><span class="m-right mr-2">RM</span><?php echo $charge?></span>
            <figure>
             <img src="<?=base_url()?>uploads/<?php echo $profile_pic?>" alt="<?php echo $first_name?><?php echo $last_name?>">
            </figure>
            <h3><?php echo $first_name?> <?php echo $last_name?></h3>
             <small>Competencies</small>
            <p><?php
$sql ="select competencies.compet_ttile from competencies inner join
competencies_details on competencies.compe_id=competencies_details.compe_id where  nurse_id='$nurse_id' order by rand() limit 1 ";
//echo $sql;
$query = $this->db->query($sql);
    foreach ($query->result() as $row)
      {
     $compet_ttile=$row->compet_ttile;
echo " <span class='badge badge-default mr-1 mb-1 mt-1'>$compet_ttile</span> ";
     }
?></p>
            <span class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><small>(145)</small></span>
            <ul>


              <li><a href="<?=base_url('index.php/auth')?>">Book now</a></li>
            </ul>
          </div>
                   </div>
                   <?php

}
                   ?>
                 </div>
               </div>
                 </div>
               </div>


<!--          <nav aria-label="" class="add_top_20">
           <ul class="pagination pagination-sm">
             <li class="page-item disabled">
               <a class="page-link" href="#" tabindex="-1">Previous</a>
             </li>
             <li class="page-item active"><a class="page-link" href="#">1</a></li>
             <li class="page-item"><a class="page-link" href="#">2</a></li>
             <li class="page-item"><a class="page-link" href="#">3</a></li>
             <li class="page-item">
               <a class="page-link" href="#">Next</a>
             </li>
           </ul>
         </nav> -->
         <!-- /pagination -->
       </div>


     </div>
     <!-- /row -->
   </div>
   <!-- /container -->
 </main>
 <!-- /main -->


               <?php $this->load->view('frontend/footer');?>
               <?php $this->load->view('frontend/footer_link');?>


           	</body>
           </html>
