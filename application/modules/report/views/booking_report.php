<?php include 'report_head_link.php';?>

<style>
   @media (max-width: 480px) {
   .nav-wrapper {
   display: block;
   /*overflow: hidden;*/
   height: calc(1.5rem + 1rem + 2px); /** 1.5 is font-size, 1 is padding top and bottom, 2 is border width top and bottom */
   position: relative;
   z-index: 1;
   margin-bottom: -1px;
   }
   .nav-pills {
   overflow-x: auto;
   flex-wrap: nowrap;
   border-bottom: 0;
   }
   .nav-item {
   margin-bottom: 0;
   min-width: 12em !important;
   }
   /* .nav-item :first-child {
   padding-left: 15px;
   }
   .nav-item :last-child {
   padding-right: 15px;
   }*/
   .nav-link {
   white-space: nowrap;
   /*min-width: 5em !important;*/
   }
   .dragscroll:active,
   .dragscroll:active a {
   cursor: -webkit-grabbing;
   }
   }
</style>

<body class="app sidebar-mini rtl">
   <!--Global-Loader-->
   <!-- <div id="global-loader">
      <img src="back_assets/images/icons/loader.svg" alt="loader">
      </div> -->
   <div class="page">
      <div class="page-main">
         <!--app-header-->
         <?php $this->load->view('backend/header');?>
         <!-- app-content-->
         <div class="container content-patient">
            <div class="side-app">
               <!-- page-header -->
               <!-- <div class="page-header">
                  <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
                  </ol>
                  <div class="ml-auto">
                  <div class="input-group">
                      <a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
                          <span>
                              <i class="fa fa-calendar"></i> Events Settings
                          </span>
                          <i class="fa fa-caret-down"></i>
                      </a>
                      <a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                          <span>
                              <i class="fa fa-star"></i>
                          </span>
                      </a>
                  </div>
                  </div>
                  </div> -->
               <!-- End page-header -->
               <div class="card">
                  <div class="card-body">
                    <form action="<?php if($uri!='booking_report') echo base_url('report/nurse_wise_report'); else echo  base_url('report/booking_report')?>" method="get">
                    <div class="row">

                        <div class="col-md-3">
							<label for="from_date">From Date</label>
                            <input  id="from_date" type="date" class="form-control" name="from_date">
                        </div>

                        <div class="col-md-3">
							                   <label for="to_date">To Date</label>
                            <input type="date" id="to_date" class="form-control" name="to_date">
                        </div>
                        <?php if($uri!='booking_report') {?>

                        <div class="col-md-3">
                                 <label for="to_date">Nurse</label>
                            <select name="nurse_id" class="form-control">
                                <option disabled selected>Select Nurse</option>
                                <?php foreach ($nurse_list as $key => $value) { ?>
                                  <option value="<?=$value['loginid']?>"><?=$value['first_name'].' '.$value['last_name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                      <?php } ?>

                        <div class="col-md-3">




							<button style="margin-top:27px" class="btn btn-success" type="submit" name="save" id="save_contact">Search</button>
                        </div>

                    </div>
                  </form>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-12 col-lg-12">
                     <div class="card">
                        <div class="card-header">
                           <div class="col-md-10">
                              <div class="card-title"><?=$form_title?></div>
                           </div>
                           <!-- <div class="col-md-2">
                              <a href="nurse/nurse_list" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
                                  <span> Nurse List
                                  </span>
                              </a>
                              </div> -->
                        </div>
                        <div class="card-body">

                           <form action="nurse/register_user_as_nurse" method="post">
                              <div class="row">
                                 <?php if($this->session->flashdata('msg')){ ?>
                                 <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <div class="alert-message">
                                       <span><?=$this->session->flashdata('msg');?></span>
                                    </div>
                                 </div>
                                 <?php } ?>
                                 <div class="col-md-12">
                                    <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="card">
                                       <div class="card-body">
                                          <div class="table-responsive">
                                             <?php if($this->session->flashdata('msg')){ ?>
                                             <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                <div class="alert-message">
                                                   <span><?=$this->session->flashdata('msg');?></span>
                                                </div>
                                             </div>
                                             <?php } ?>
                                             <table id="booking_report" class="display nowrap" style="width:100%">
                                                  <thead>
             <tr>
               <th>Nurse Name</th>
               <th>Patient Name</th>
               <th>Dates</th>
               <th>Hours/day</th>
               <th>Total Hours</th>
               <th>Status</th>
               <th>Total Amount </th>
             </tr>
         </thead>

         <tbody>
           <?php foreach ($bookings as $key => $value) {
             $book_status=$value['book_status'];
             if($book_status==1)
             {
                 //$bt="New";
                 $bt="<div style='color:#09c1f6;font-weight:bold'>New</div>";
             }
             elseif($book_status==2)
             {

                  $bt="<div style='color:#02fd90;font-weight:bold'>Approved (Nurse)</div>";
             }
             elseif($book_status==3)
             {

                 $bt="<div style='color:#2eeb09;font-weight:bold'>Completed (Nurse)</div>";
             }
             elseif($book_status==4)
             {

                  $bt="<div style='color:red;font-weight:bold'>Cancelled</div>";
             }
             elseif($book_status==5)
             {
                 $bt="<div style='color:#987;font-weight:bold'>Finish</div>";
             }
             elseif($book_status==6)
             {
                 $bt="<div style='color:green;font-weight:bold'>Completed</div>";
             }



           ?>
            <tr>
              <td><?=$value['nurse_name']?></td>
              <td><?=$value['patient_name']?></td>
              <td><?=$value['fromtime']?> to <?=$value['totime']?></td>
              <td><?=$value['time_in_hours']?></td>
              <td><?=$value['total_hour']?></td>
              <!--  <td><?=$value['notes'];?></td> -->
              <td><?php echo $bt?></td>
              <td>RM <?=$value['book_charge'];?></td>

            </tr>
          <?php }?>

          </tbody>


                                              </table>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                        </div>
                        <!-- <div class="col-md-12">
                           <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a large text here ..."></textarea>
                           </div> -->
                        </form>
                        </fieldset>
                        <!-- table-wrapper -->
                     </div>
                     <!-- section-wrapper -->
                  </div>
               </div>
            </div>
            <!--End side app-->
            <!-- Right-sidebar-->
            <?php $this->load->view('backend/right_sidebar');?>
            <!-- End Rightsidebar-->
            <?php $this->load->view('backend/footer');?>
         </div>
         <!-- End app-content-->
      </div>
   </div>
   <!-- End Page -->
   <!-- Back to top -->
   <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>
  <?php include 'report_footer_link.php';?>




   <script type="text/javascript">
   $(document).ready(function() {
   $('#booking_report').DataTable( {
       dom: 'Bfrtip',
       buttons: [
           'copy', 'csv', 'excel',
            {
             extend:'print',
             title:'Booking Report',
           },
           {
             extend:'pdf',
             title:'Booking Report',
           }
           
       ]
   } );
} );

 </script>
</body>
</html>
