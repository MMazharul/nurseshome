<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function select_with_where($selector, $condition, $tablename)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result = $this->db->get();
        return $result->result_array();
    }
    
      public function select_where_left_three_join($selector,$table_name,$join_table,$join_condition,$join_table2,$join_condition2,$join_table3,$join_condition3,$condition)
  {
      $this->db->select($selector);
      $this->db->from($table_name);
      $this->db->join($join_table,$join_condition,'left');
      $this->db->join($join_table2,$join_condition2,'left');
      $this->db->join($join_table3,$join_condition3,'left');
      $where = '(' . $condition . ')';
      $this->db->where($where);
      //$this->db->order_by($order_col,$order_action);
      $result=$this->db->get();
      return $result->result_array();
  }

 public function join_query_with_date($today_date)
    {
  $query="select time_in_hours,((datediff(totime,fromtime)+1)*nurse_booking.time_in_hours) as total_hour,book_charge,fromtime,totime,adddate,book_charge,patient_id,addons_one,addons_two,addons_three,notes,book_status,booking_random_code,
  patient_bookid,a.loginid as nurse_id,concat(a.first_name,' ',a.last_name)as nurse_name,a.charge,a1.price as a1price,a2.price as a2price,a3.price as a3price,
  b.loginid as patient_id,concat(b.first_name,' ',b.last_name)as patient_name
  from nurse_booking inner join users a on a.loginid=nurse_booking.nurse_id
  left join addons a1 on a1.id=nurse_booking.addons_one left join addons as a2 on a2.id=nurse_booking.addons_two left join addons as a3 on a3.id=nurse_booking.addons_three
  inner join users b on b.loginid=nurse_booking.patient_id where date(`fromtime`) BETWEEN  '$today_date' and '$today_date' and book_status=5";
        $result = $this->db->query($query) or die ("Schema Query Failed");
        $result=$result->result_array();
        return $result;
    }


    public function join_with_between($condition)
  {
   		 $query="select time_in_hours,((datediff(totime,fromtime)+1)*nurse_booking.time_in_hours) as total_hour,book_charge,fromtime,totime,adddate,book_charge,patient_id,addons_one,addons_two,addons_three,notes,book_status,booking_random_code,
    	patient_bookid,a.loginid as nurse_id,concat(a.first_name,' ',a.last_name)as nurse_name,a.charge,a1.price as a1price,a2.price as a2price,a3.price as a3price,
    	b.loginid as patient_id,concat(b.first_name,' ',b.last_name)as patient_name
    	from nurse_booking inner join users a on a.loginid=nurse_booking.nurse_id
    	left join addons a1 on a1.id=nurse_booking.addons_one left join addons as a2 on a2.id=nurse_booking.addons_two left join addons as a3 on a3.id=nurse_booking.addons_three
    	inner join users b on b.loginid=nurse_booking.patient_id $condition";
          $result = $this->db->query($query) or die ("Schema Query Failed");
          $result=$result->result_array();
          return $result;
  }

    // ====  Search  Model Query End=== ///
}
