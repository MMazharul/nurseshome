<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report extends MX_Controller {

    //public $counter=0;
    function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->helper('date');
        $this->load->model('report_model');

    }

    public function summary_report()
    {
      $data['form_title'] = "Daily Summary Report";
      $format = "%Y-%m-%d";
      $today = mdate($format);

      $data['bookings'] = $this->report_model->join_query_with_date($today);
       $this->load->view('daily_report',$data);
    }
    public function booking_report()
    {
        $data['uri'] = $this->uri->segment(2, 0);

        $from_date = $this->input->get('from_date');
        $to_date = $this->input->get('to_date');
        $nurse_id = $this->input->get('nurse_id');

        $data['form_title'] = "Booking Report";
        $data['bookings'] = $this->report_model->join_with_between("where fromtime BETWEEN '$from_date' and '$to_date' and book_status=5");
        $this->load->view('booking_report',$data);
    }
    public function nurse_wise_report()
    {
        $data['uri'] = $this->uri->segment(2, 0);
        if($data['uri']=='nurse_wise_report')
        {
          $data['nurse_list'] = $this->report_model->select_with_where('first_name,last_name,loginid','user_role=3','users');
        }
        $from_date = $this->input->get('from_date');
        $to_date = $this->input->get('to_date');
        $nurse_id = $this->input->get('nurse_id');

        $data['form_title'] = "Nurse Wise Report";
        $data['bookings'] = $this->report_model->join_with_between("where fromtime BETWEEN '$from_date' and '$to_date' and book_status=5 and nurse_id='$nurse_id'");
        $this->load->view('booking_report',$data);
    }
    
      public function transaction_report()
    {

      $data['form_title'] = "Transaction Report";
      $from_date = $this->input->get('from_date');
      $to_date = $this->input->get('to_date');
      $data['all_transaction'] = $this->report_model->select_where_left_three_join('pay_status,time_in_hours,payment_type,paid,((datediff(totime,fromtime)+1)*nurse_booking.time_in_hours) as total_hour,fromtime,totime,notes,booking_random_code,concat(b.first_name," ",b.last_name)as patient_name,CONCAT(a.first_name," ",a.last_name) as nurse_name, b.loginid as patient_id','transaction','users a',' a.loginid=transaction.nurse_id','nurse_booking','nurse_booking.patient_bookid=transaction.booking_id','users b','b.loginid=transaction.patient_id',"date(`updated_at`) BETWEEN '$from_date' and  '$to_date' and `pay_status`=1");

      $this->load->view('transaction_report',$data);
    }

  }
