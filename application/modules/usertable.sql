/*
SQLyog Community Edition- MySQL GUI v6.15
MySQL - 5.5.5-10.4.6-MariaDB : Database - nursing_app
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

create database if not exists `nursing_app`;

USE `nursing_app`;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `loginid` bigint(200) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(250) DEFAULT NULL,
  `contact_no` bigint(250) DEFAULT NULL,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `gender` tinyint(4) NOT NULL COMMENT '1=male,2=female',
  `verify_status` int(10) DEFAULT 0 COMMENT '1=Verified,0=Not Verified..This one will be done by Email Verification',
  `user_role` int(10) DEFAULT NULL COMMENT '1=Superadmin,2=admin,3=Nurse.4=Patient',
  `user_type` int(10) DEFAULT 1 COMMENT '1=Normal,2=Corporate',
  `point` decimal(10,2) DEFAULT 0.00,
  `refferal` varchar(250) DEFAULT NULL,
  `registered_date` date DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `last_login_date` datetime DEFAULT NULL,
  `last_login_ip` varchar(250) DEFAULT NULL,
  `user_act_st` int(11) DEFAULT 1 COMMENT '1=Active,2=Inactive,3=Banned',
  `charge` decimal(10,2) DEFAULT NULL,
  `profile_pic` varchar(250) DEFAULT NULL,
  `total_served_patient` decimal(10,2) DEFAULT NULL,
  `profile_update` int(11) DEFAULT 1,
  `langauge` varchar(300) DEFAULT NULL,
  `location` varchar(300) DEFAULT NULL,
  `total_experience` bigint(100) DEFAULT 0,
  `nurse_work_type` bigint(11) DEFAULT 1,
  PRIMARY KEY (`loginid`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`loginid`,`country_code`,`contact_no`,`first_name`,`last_name`,`email`,`password`,`gender`,`verify_status`,`user_role`,`user_type`,`point`,`refferal`,`registered_date`,`updated_date`,`last_login_date`,`last_login_ip`,`user_act_st`,`charge`,`profile_pic`,`total_served_patient`,`profile_update`,`langauge`,`location`,`total_experience`,`nurse_work_type`) values (1,'+60',1234567890,NULL,NULL,'admin@gmail.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',1,1,1,NULL,'0.00',NULL,'0000-00-00','0000-00-00 00:00:00','0000-00-00 00:00:00','',1,NULL,'4_20191206062653_nurse_photo.png',NULL,1,NULL,NULL,0,1),(2,'+60',8888888,'fazley','rabby','fazley111@gmail.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',1,1,4,1,'0.00',NULL,'2019-11-05',NULL,NULL,'::1',1,NULL,'nursepic.jpg',NULL,1,NULL,NULL,0,1),(3,'+60',33333,'neww','patient','patient@gmail.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',1,1,4,NULL,'0.00',NULL,'2019-11-07',NULL,NULL,'::1',1,NULL,'3_20191213083147_patient_photo.png',NULL,1,NULL,NULL,0,1),(4,'+60',123132,'naomi','gomes','nurse@gmail.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',2,1,3,NULL,'0.00',NULL,'2019-11-12',NULL,NULL,'::1',1,'250.00','4_20191208153916_nurse_photo.jpg',NULL,1,'1','xAXa',6,1),(6,'+60',123456789,'Samanthasd','Lim','samantha.lim@gmail.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',2,1,3,NULL,'0.00',NULL,'2019-11-20',NULL,NULL,'113.210.50.189',1,NULL,'nursepic.jpg',NULL,1,NULL,NULL,0,1),(8,'+60',127894567,'Jayabalan','Nair','jayabalan@gmail.com','Q2U1TGRpMGI4N2tERXJya0pWM09RUT09',0,1,4,NULL,'0.00',NULL,'2019-12-01',NULL,NULL,'113.210.58.161',1,NULL,NULL,NULL,1,NULL,NULL,0,1),(9,'+60',1212121,'mohammad','kader','patient2@gmail.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',0,1,4,NULL,'0.00',NULL,'2019-12-01',NULL,NULL,'103.214.201.54',1,NULL,NULL,NULL,1,NULL,NULL,0,1),(11,'+60',1123123,'jibon','kaimr','pn@gmail.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',0,1,4,NULL,'0.00',NULL,'2019-12-01',NULL,NULL,'103.214.201.54',1,NULL,NULL,NULL,1,NULL,NULL,0,1),(13,'+60',12121,'sunhan','chy','suhan@gmail.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',0,1,3,NULL,'0.00',NULL,'2019-12-06',NULL,NULL,'::1',1,NULL,NULL,NULL,1,NULL,NULL,0,1),(14,'+60',0,'jibon','kqrim','ji@gmail.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',0,1,4,NULL,'0.00',NULL,'2019-12-06',NULL,NULL,'::1',1,NULL,NULL,NULL,1,NULL,NULL,0,1),(15,'+60',1973222183,'','','indi@gmail.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',0,1,7,2,'0.00',NULL,'2019-12-07',NULL,NULL,'::1',1,NULL,NULL,NULL,1,NULL,NULL,0,1),(16,'+60',123456,'akram hossain','khondorar','akara@gmail.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',0,1,4,1,'0.00',NULL,'2019-12-07',NULL,NULL,'::1',1,NULL,NULL,NULL,1,NULL,NULL,0,1),(17,'+60',52142,'jibon','daskarim','jin@gmail.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',0,1,3,1,'0.00',NULL,'2019-12-07',NULL,NULL,'::1',1,NULL,'avatar.png',NULL,1,NULL,NULL,0,1),(18,'+60',12222,'mona','mia','mon@gmail.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',0,1,4,1,'0.00',NULL,'2019-12-08',NULL,NULL,'::1',1,NULL,NULL,NULL,0,NULL,NULL,0,1),(19,'+60',1121,'rah','bad','rah@yahoo.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',0,1,4,1,'0.00',NULL,'2019-12-08',NULL,NULL,'::1',1,NULL,NULL,NULL,0,NULL,NULL,0,1),(20,'+60',1122,'k','k','k@yamco.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',0,1,4,1,'0.00',NULL,'2019-12-08',NULL,NULL,'::1',1,NULL,NULL,NULL,0,NULL,NULL,0,1),(21,'+60',1111,'arju','him','r@y.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',0,1,3,1,'0.00',NULL,'2019-12-08',NULL,NULL,'::1',1,NULL,'avatar.png',NULL,0,NULL,NULL,0,1),(22,'+60',1223,'aneam','karim','an@gmail.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',1,1,3,1,'0.00',NULL,'2019-12-08',NULL,NULL,'::1',1,NULL,'avatar.png',NULL,0,NULL,NULL,0,1),(23,'+60',5221,'ARJ','KK','KK@GMAIL.COM','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',1,1,4,1,'0.00',NULL,'2019-12-08',NULL,NULL,'::1',1,NULL,NULL,NULL,0,NULL,NULL,0,1),(24,'+60',654321,'REHENA','KARIM','KARIM@GMAIL.COM','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',1,1,4,1,'0.00',NULL,'2019-12-08',NULL,NULL,'::1',1,NULL,NULL,NULL,0,NULL,NULL,0,1),(25,'+60',1212,'k','k','k@gmail.com','bVlYZE03YXZScnZaZnBhaDJPQml0Zz09',0,1,3,1,'0.00',NULL,'2019-12-09',NULL,NULL,'::1',1,NULL,'avatar.png',NULL,1,NULL,NULL,0,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
