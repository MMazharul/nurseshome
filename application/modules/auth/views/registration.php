<?php $this->load->view('backend/head_link');?>
	<body>
		<style type="text/css">
			body{background-color:#e6190f}
		.header-brand-img-cutom{
			    height: 40px!important;
    line-height: 30px!important;
    vertical-align: middle;
    margin-right: .5rem;
    width: auto;
    margin: 0 auto;
    margin-top: -18px!important;
    margin-bottom: -15px!important;
		}
		.horizontalMenucontainer {


    background-attachment: fixed;
    background-position: 50% 0;
    background-repeat: no-repeat;
}

.field-icon {
      float: right;
      margin-left: -18px;
      margin-top: 12px;
      position: relative;
      z-index: 2;
      padding-right: 19px;
}

	</style>
	    <!-- page -->
		<div class="page">

			<!-- page-content -->
			<div class="page-content">
				<div class="container text-center text-dark">
					<div class="row">
						<div class="col-lg-5 d-block mx-auto">
							<div class="row">
								<div class="col-xl-12 col-md-12 col-md-12">
									<div style="margin-top: 30px;" class="card">
										<div class="card-body">
											<div class="text-center mb-6">
												<!-- <img src="" class="" alt=""> -->
												<img src="back_assets/images/logo.png" class="header-brand-img main-logo header-brand-img-cutom" alt="logo">
											</div>
											<hr>
											<!-- <h3>Login</h3> -->
											<p style="margin-top: -22px;margin-bottom: 7px;" class="">Create A New Account</p>
			<div class="panel panel-primary">
				<div style="padding: 8px; border: 0px solid rgba(107,122,144,0.1); border-bottom: 0;" class="tab-menu-heading">
					<div class="tabs-menu ">
						<!-- Tabs -->
						<ul class="nav panel-tabs">
							<li class=""><a style="padding: 6px 12px; font-size: 14px" href="#tab11" class="active" data-toggle="tab">Individual</a></li>
							<li><a style="padding: 6px 12px; font-size: 14px" href="#tab31" data-toggle="tab">Nurse</a></li>
							<li><a style="padding: 6px 12px; font-size: 14px" href="#tab21" data-toggle="tab">Corporate</a></li>

						</ul>
					</div>
				</div>
				<div class="panel-body tabs-menu-body">
					<div class="tab-content">
						<div class="tab-pane active " id="tab11">
							<form id='form' onsubmit="return  password_validation();" method="post" action="auth/register" autocomplete="off">
									<input name="user_type" value="1" type="hidden" class="form-control">

											<!-- registration/register -->
											<?php if($this->session->flashdata('msg')){ ?>
												<div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
													<button type="button" class="close" data-dismiss="alert">&times;</button>
													<!-- <div class="alert-icon">
														<i class="icon-info"></i>
													</div> -->
													<div class="alert-message">
														<span><?=$this->session->flashdata('msg');?></span>
													</div>
												</div>
											<?php } ?>
											<div id="alert_pass"></div>


								<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-unlock-alt"></i></span>
												<select class='form-control' required name="role" id="role_select">
													<option disabled selected>Select User Type</option>

													<option value="4">Patient Registration</option>
													<option value="5">Patient Next of Kin/Relative Registration</option>
												</select>

											</div>

						<div class="input-group mb-3" id="work_time" style="display: none;">
												<span class="input-group-addon bg-white"><i class="fa fa-unlock-alt"></i></span>
												<select class='form-control'  name="nurse_work_type" id="nurse_work_type" required>
													<option disabled selected>Type of Nurse</option>
													<option value="1">Part Time</option>
													<option value="2">Full Time</option>

												</select>

										</div>
									<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-user"></i></span>
												<input name="firstname" type="text" class="form-control" required placeholder="First Name">
											</div>
										<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-user"></i></span>
												<input name="lastname" type="text" class="form-control" required placeholder="Last Name">
											</div>
											<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-user"></i></span>
												<input name="email" type="email" class="form-control" required placeholder="Email">
											</div>

											<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-phone"></i></span>
												<input style='width:2rem;text-align:center;background:#ccc;color:#111; outline:0;border:0' type="text" name="country_code" readonly value="+60">
												<input name="contact" type="text" class="form-control" required placeholder="Contact Number">
											</div>


											<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-unlock-alt"></i></span>
												<input name="password" type="password" class="form-control" id='pass' placeholder="Type Password">
												<span toggle="#pass" class="fa fa-fw fa-eye field-icon toggle-password"></span>
											</div>
											<div class="input-group mb-4">
												<span class="input-group-addon bg-white"><i class="fa fa-unlock-alt"></i></span>
												<input name="passconf" id='passconf' type="password" class="form-control" placeholder="Retype Password">
												<span toggle="#passconf" class="fa fa-fw fa-eye field-icon toggle-password"></span>
											</div>
											<div class="row">
												<div class="col-12">
													<button id="submitBtnkuser" type="submit" class="btn btn-primary btn-block">Sign Up</button>
												</div>

											</div>
											</form>
						</div>

						<div class="tab-pane active " id="tab31">
							<form id='form' onsubmit="return  password_validation();" method="post" action="auth/register" autocomplete="off">
					<input name="user_type" value="1" type="hidden" class="form-control">
					<input name="role" value="3" type="hidden" class="form-control">

											<!-- registration/register -->
											<?php if($this->session->flashdata('msg')){ ?>
												<div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
													<button type="button" class="close" data-dismiss="alert">&times;</button>
													<!-- <div class="alert-icon">
														<i class="icon-info"></i>
													</div> -->
													<div class="alert-message">
														<span><?=$this->session->flashdata('msg');?></span>
													</div>
												</div>
											<?php } ?>
											<div id="alert_pass"></div>
						<div class="input-group mb-3" id="work_time" >
												<span class="input-group-addon bg-white"><i class="fa fa-unlock-alt"></i></span>
												<select class='form-control'  name="nurse_work_type" id="nurse_work_type" required>
													<option disabled selected>Type of Nurse</option>
													<option value="1">Part Time</option>
													<option value="2">Full Time</option>

												</select>

										</div>
									<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-user"></i></span>
												<input name="firstname" type="text" class="form-control" required placeholder="First Name">
											</div>
										<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-user"></i></span>
												<input name="lastname" type="text" class="form-control" required placeholder="Last Name">
											</div>
											<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-user"></i></span>
												<input name="email" type="email" class="form-control" required placeholder="Email">
											</div>

											<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-phone"></i></span>
												<input style='width:2rem;text-align:center;background:#ccc;color:#111; outline:0;border:0' type="text" name="country_code" readonly value="+60">
												<input name="contact" type="text" class="form-control" required placeholder="Contact Number">
											</div>


											<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-unlock-alt"></i></span>
												<input name="password" type="password" class="form-control" id='pass' placeholder="Type Password">
												<span toggle="#pass" class="fa fa-fw fa-eye field-icon toggle-password"></span>
											</div>
											<div class="input-group mb-4">
												<span class="input-group-addon bg-white"><i class="fa fa-unlock-alt"></i></span>
												<input name="passconf" id='passconf' type="password" class="form-control" placeholder="Retype Password">
												<span toggle="#passconf" class="fa fa-fw fa-eye field-icon toggle-password"></span>
											</div>
											<div class="row">
												<div class="col-12">
													<button id="submitBtnkuser" type="submit" class="btn btn-primary btn-block">Sign Up</button>
												</div>

											</div>
											</form>
						</div>

						<div class="tab-pane  " id="tab21">
							<form id='form' onsubmit="return false password_validation();" method="post" action="auth/register">
								<input name="user_type" value="2" type="hidden" class="form-control">
											<!-- registration/register -->
											<?php if($this->session->flashdata('msg')){ ?>
												<div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
													<button type="button" class="close" data-dismiss="alert">&times;</button>
													<!-- <div class="alert-icon">
														<i class="icon-info"></i>
													</div> -->
													<div class="alert-message">
														<span><?=$this->session->flashdata('msg');?></span>
													</div>
												</div>
											<?php } ?>
											<div id="alert_pass"></div>


								<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-unlock-alt"></i></span>

						<select class='form-control' required name="role" id="Individual_corporate">
							<option value="0">Select User Type</option>
													<option value="6">Hospital or Clinic</option>
													<option value="7">Industrial Nurse</option>
													<option value="8">Wellness Program</option>
													<option value="9">Others-FH</option>
												</select>
											</div>

									<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-user"></i></span>
												<input name="firstname" type="text" class="form-control" required placeholder="First Name">
											</div>
										<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-user"></i></span>
												<input name="lastname" type="text" class="form-control" required placeholder="Last Name">
											</div>

											<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-user"></i></span>
												<input name="email" type="email" class="form-control" required placeholder="Email">
											</div>

											<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-phone"></i></span>
												<input style='width:2rem;text-align:center;background:#ccc;color:#111; outline:0;border:0' type="text" name="country_code" readonly value="+60">
												<input name="contact" type="text" class="form-control" required placeholder="Contact Number">
											</div>


											<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-unlock-alt"></i></span>
												<input name="password" type="password" class="form-control" id='pass' placeholder="Password">
												 <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
											</div>
											<div class="input-group mb-4">
												<span class="input-group-addon bg-white"><i class="fa fa-unlock-alt"></i></span>
												<input name="passconf" id='passconf' type="password" class="form-control" placeholder="Re-enter Password">
												 <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
											</div>
											<div class="row">
												<div class="col-12">
													<button id="submitBtn" type="submit" class="btn btn-primary btn-block">Sign Up</button>
												</div>

											</div>
											</form>
						</div>

					</div>
				</div>
			</div>


											<br>
											<div class="row">
												<div class="col-12">
												Already Have An Account?
												<a href="auth" class="btn btn-link box-shadow-0 px-0">Login
													here </a>

												</div>
												<!-- <div class="col-6">
												<a href="forgot-password.html" class="btn btn-link box-shadow-0 px-0">Forgot password?
													</a>
												</div> -->
												</div>

											<!-- <div class="mt-6 btn-list">
												<button type="button" class="btn btn-icon btn-facebook"><i class="fa fa-facebook"></i></button>
												<button type="button" class="btn btn-icon btn-google"><i class="fa fa-google"></i></button>
												<button type="button" class="btn btn-icon btn-twitter"><i class="fa fa-twitter"></i></button>
												<button type="button" class="btn btn-icon btn-dribbble"><i class="fa fa-dribbble"></i></button>
											</div> -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
			<!-- page-content end -->
		</div>
		<!-- page End-->

		<?php $this->load->view('backend/footer_link');?>
<script type="text/javascript">


// toggle password visibility
$(".toggle-password").click(function() {

	$(this).toggleClass("fa-eye fa-eye-slash");
	var input = $($(this).attr("toggle"));

	if (input.attr("type") == "password") {
		input.attr("type", "text");
	} else {
		input.attr("type", "password");
	}
});


$(document).ready(function(){
    $('#role_select').on('change', function() {
      if ( this.value == 3)
      {

        $("#work_time").show();
      }
      else
      {
        $("#work_time").hide();
      }
    });
});

</script>
<script type="text/javascript">
function usertypecheck() {
    if (document.getElementById('Individual').checked) {
        document.getElementById('Individual_div').style.visibility = 'visible';
         document.getElementById('Individual_corporate').style.visibility = 'hidden';
    }
    else if (document.getElementById('Corporate').checked) {
        document.getElementById('Individual_corporate').style.visibility = 'visible';
        document.getElementById('Individual_div').style.visibility = 'hidden';
    }
    else
    {
     document.getElementById('Individual_div').style.visibility = 'hidden';
      document.getElementById('Individual_corporate').style.visibility = 'hidden';
    }

}

</script>
	 <script>
	 	let pass = document.querySelector('#pass');
	 	let pass2 = document.querySelector('#passconf');

		pass2.onkeyup = () => {
			if (pass.value != pass2.value ) {
				document.getElementById("alert_pass").innerHTML =
				`<div class="alert alert-danger alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button><div class="alert-message">
					<span>Password Don't Match!</span>
				</div>
				</div>`;

				document.getElementById("submitBtn").disabled = true;
			}
			else{
				document.getElementById("alert_pass").innerHTML = '';
				document.getElementById("submitBtn").disabled = false;
			}
		}

	 	// let form = document.querySelector('form');

		// function password_validation(e){
		// 	if (pass != pass2) {
		// 		document.getElementById("alert_pass").innerHTML = "Password Don't Match";
		// 	}
		// 	else {
		// 		document.getElementById("alert_pass").innerHTML = "";
		// 		return true;
       	// 	}
		// }



	 </script>

	</body>
</html>
