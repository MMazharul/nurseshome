<?php $this->load->view('backend/head_link');?>
		<body>
		<style type="text/css">
			body{background-color:#FF0000}
		.header-brand-img-cutom{
			    height: 40px!important;
    line-height: 30px!important;
    vertical-align: middle;
    margin-right: .5rem;
    width: auto;
    margin: 0 auto;
    margin-top: -18px!important;
    margin-bottom: -15px!important;
		}
		.horizontalMenucontainer {
   
  
    background-attachment: fixed;
    background-position: 50% 0;
    background-repeat: no-repeat;
}
	</style>
	    <!-- page -->
		<div class="page">

			<!-- page-content -->
			<div class="page-content">
				<div class="container text-center text-dark">
					<div class="row">
						<div class="col-lg-4 d-block mx-auto">
							<div class="row">
								<div class="col-xl-12 col-md-12 col-md-12">
									<div class="card">
										<div class="card-body">
											<div class="text-center mb-6">
												<!-- <img src="" class="" alt=""> -->
												<img src="back_assets/images/logo.png" class="header-brand-img main-logo" alt="logo">
											</div>
											<hr>	
											<!-- <h3>Login</h3> -->
											<p class="">Please Contact Me as Follows : Contact Details</p>	
											<form method="post" action="auth/contact_us_post">
											<?php if($this->session->flashdata('msg')){ ?>
												<div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
													<button type="button" class="close" data-dismiss="alert">&times;</button>
													<!-- <div class="alert-icon">
														<i class="icon-info"></i>
													</div> -->
													<div class="alert-message">
														<span><?=$this->session->flashdata('msg');?></span>
													</div>
												</div>
											<?php } ?> 
											<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-user"></i></span>
												<input name="firstname" type="text" class="form-control" placeholder="First Name">
											</div>
								<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-user"></i></span>
												<input name="lastname" type="text" class="form-control" placeholder="Last Name">
											</div>
											<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-phone"></i></span>
												<input name="contact" type="text" class="form-control" placeholder="Mobile Number">
											</div>
											<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-envelope"></i></span>
												<input name="emailaddress" type="text" class="form-control" placeholder="Email Address">
											</div>
											<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-home"></i></span>
												<input name="company" type="text" class="form-control" placeholder="Company Name">
											</div>
											<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-pencil"></i></span>
												<input name="msg_title" type="text" class="form-control" placeholder="Title">
											</div>
										<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-pencil"></i></span>
							<textarea class="form-control" name="message" placeholder="Please Enter Description" rows="3"></textarea>
												
											</div>
											<div class="row">
												<div class="col-12">
													<button type="submit" class="btn btn-primary btn-block">Send Message</button>
												</div>
												
											</div>
											</form>
											<br>
											<div class="row">
												<div class="col-xl-6 col-sm-12">
												<a href="auth/registration" class="btn btn-link box-shadow-0 px-0">Sign Up
													</a>
												</div>
												<div class="col-xl-6 col-sm-12">
												<a href="auth" class="btn btn-link box-shadow-0 px-0">Login
													</a>
												</div>
												</div>
												
										<!-- 	<div class="mt-6 btn-list">
												
												<button type="button" class="btn btn-icon b">Contact Us</button>
											</div> -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
			<!-- page-content end -->
		</div>
		<!-- page End-->

		<?php $this->load->view('backend/footer_link');?>

	</body>
</html>