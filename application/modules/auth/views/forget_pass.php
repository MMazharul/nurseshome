<?php $this->load->view('backend/head_link');?>
	<body class="bg-account">
	    <!-- page -->
		<div class="page">

			<!-- page-content -->
			<div class="page-content">
				<div class="container text-center text-dark">
					<div class="row">
						<div class="col-lg-4 d-block mx-auto">
							<div class="row">
								<div class="col-xl-12 col-md-12 col-md-12">
									<div class="card">
										<div class="card-body">
											<div class="text-center mb-6">
												<!-- <img src="" class="" alt=""> -->
												<img src="back_assets/images/logo.png" class="header-brand-img main-logo" alt="logo">
											</div>
											<hr>
											<!-- <h3>Login</h3> -->
											<p class="">Forget your Password</p>
											<form method="post" action="<?=base_url('auth/forget_password_send')?>">
											<?php if($this->session->flashdata('msg')){ ?>
												<div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
													<button type="button" class="close" data-dismiss="alert">&times;</button>
													<!-- <div class="alert-icon">
														<i class="icon-info"></i>
													</div> -->
													<div class="alert-message">
														<span><?=$this->session->flashdata('msg');?></span>
													</div>
												</div>
											<?php } ?>
											<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-user"></i></span>
												<input name="email" type="text" class="form-control" placeholder="Type Email Address">
											</div>

											<div class="row">
												<div class="col-12">
													<button type="submit" class="btn btn-primary btn-block">Submit</button>
												</div>

											</div>


											</form>
											<br>
											<div class="row">
												<div class="col-xl-6 col-sm-12">
												<a href="auth/registration" class="btn btn-link box-shadow-0 px-0">Sign Up
													</a>
												</div>
												<div class="col-xl-6 col-sm-12">
												<a href="auth/index" class="btn btn-link box-shadow-0 px-0">Login
													</a>
												</div>
												</div>

											<!-- <div class="mt-6 btn-list">
												<button type="button" class="btn btn-icon btn-facebook"><i class="fa fa-facebook"></i></button>
												<button type="button" class="btn btn-icon btn-google"><i class="fa fa-google"></i></button>
												<button type="button" class="btn btn-icon btn-twitter"><i class="fa fa-twitter"></i></button>
												<button type="button" class="btn btn-icon btn-dribbble"><i class="fa fa-dribbble"></i></button>
											</div> -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
			<!-- page-content end -->
		</div>
		<!-- page End-->

		<?php $this->load->view('backend/footer_link');?>

	</body>
</html>
