<?php $this->load->view('backend/head_link');?>
<style type="text/css">
	th,td{text-align: center !important; }

</style>
<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
	<img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
	<div class="page-main">
		<!--app-header-->


		<?php $this->load->view('backend/header');?>



		<!-- app-content-->
		<div class="container content-nurse">
			<div class="side-app">



				<div class="row">
					<div class="col-md-12 col-lg-12">
					<div class="card">
						<div class="card-header">
							<div class="col-md-10">
							<div class="card-title">Nurse List</div>
							</div>
							<div class="col-md-2">
							<a href="nurse" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
								<span> Add Nurse
								</span>

							</a>
							</div>
						</div>
						<div class="card-body">
							<div class="table-responsive">
							<?php if($this->session->flashdata('msg')){ ?>
								<div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<div class="alert-message">
										<span><?=$this->session->flashdata('msg');?></span>
									</div>
								</div>
							<?php } ?>
								<table id="example" class="table table-striped table-bordered text-nowrap w-100">
									<thead>
										<tr>
											<!-- Name | Gender | Date Registered | Status | Rate (RM) | Contact No | Email Address -->
									<!-- SL | Name | Rate (RM) | Contact No | Email | Date Registered | Action	 -->
											<th class="wd-15p">Sl</th>
											<th>Name</th>
											<th>Rate (RM)</th>
											<th>Contact No</th>
											<th>Email</th>
											<!-- <th>Gender</th> -->
											<th>Date Registered</th>
											<!-- <th>Status</th> -->

											<!-- <th>Experience (YR)</th> -->

											<!-- <th class="wd-15p">Profile</th> -->
											<th class="wd-20p" style="text-align: center;">Action</th>
											<!-- <th class="wd-15p">#</th>
											<th class="wd-15p">Name</th>
											<th class="wd-15p">Gender</th>
											<th class="wd-15p">Charge</th>
											<th class="wd-15p">Contact Number</th>
											<th class="wd-15p">R</th>
											<th class="wd-15p">Status</th>
											<th class="wd-15p">Email</th>
											 -->
										</tr>
									</thead>
									<tbody>
									<?php
										$i = 1;
										foreach ($nurse_details as $key => $value) { ?>
										<tr>
											<!-- SL | Name | Rate (RM) | Contact No | Email | Date Registered | Action	 -->
											<td><?=$i;?></td>
											<td><?=$value['first_name']." ".$value['last_name']?></td>
											<td><?=$value['charge'];
											?></td>
											<td><?=$value['country_code']." ".$value['contact_no']?></td>

											<td><?=$value['email']?></td>
											<!-- <td><?=$value['gender'] == '1' ? 'Male' : 'Female';
											?></td> -->
											<td><?=$value['registered_date']?></td>

											<!-- <td><?=$value['verify_status'] == '1' ? 'Active' : 'Inactive';
											?></td> -->

											<!-- <td><?=$value['total_experience'];
											?></td> -->


											<td>
												<a href="nurse-edit-profile/<?=$value['loginid']?>"  class="btn btn-primary text-white mr-2"  id="">
													<span>
														Profile
													</span>
												</a>
												<a href="nurse/add_charge/<?=$value['loginid']?>" class="btn btn-success text-white mr-2"  id="">
												<span>
														Charge
												</span>
											</a>
											<a href="add_nurse_schedule/<?=$value['loginid']?>" class="btn btn-success text-white mr-2"  id="">
												<span>
														Schedule
												</span>
											</a>
													<a data-toggle="modal" data-target="#smallModal_<?=$i?>"  class="btn btn-danger text-white mr-2"  id="">
												<span>
														Delete
												</span>
											</a>
											<a href="nurse/verify/<?=$value['loginid']?>" class="btn btn-danger text-white mr-2"  id="">
										<span>
											<?=$value['status'] == 1 ? 'Verified' : 'Not Verified';
											?>
										</span>
									</a>


											</td>


										</tr>


										<div id="smallModal_<?=$i?>" class="modal fade">
											<div class="modal-dialog modal-sm" role="document">
												<div class="modal-content">
													<div class="modal-body bg-danger">
														<h4>Do you want to delete?</h4>
													</div><!-- modal-body -->
													<div class="modal-footer">
														<a href="nurse/delete_nurse/<?=$value['loginid']?>" type="" class="btn btn-danger">YES</a>
														<button type="button" class="btn btn-primary" data-dismiss="modal">NO</button>
													</div>
												</div>
											</div>
										</div>

										<?php $i++; } ?>


									</tbody>
								</table>
							</div>
						</div>
						<!-- table-wrapper -->
					</div>
					<!-- section-wrapper -->
					</div>
				</div>

			</div><!--End side app-->

			<!-- Right-sidebar-->
			<?php $this->load->view('backend/right_sidebar');?>
			<!-- End Rightsidebar-->

			<?php $this->load->view('backend/footer');?>

		</div>
		<!-- End app-content-->
	</div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>

</body>
</html>
