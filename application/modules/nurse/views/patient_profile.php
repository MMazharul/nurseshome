<?php $this->load->view('backend/head_link');?>


<style>
    @media (max-width: 480px) {
    .nav-wrapper {
    display: block;
    /*overflow: hidden;*/
    height: calc(1.5rem + 1rem + 2px); /** 1.5 is font-size, 1 is padding top and bottom, 2 is border width top and bottom */
    position: relative;
    z-index: 1;
    margin-bottom: -1px;
    }
    .nav-pills {
        overflow-x: auto;
        flex-wrap: nowrap;
        border-bottom: 0;
    }
     .nav-item {
        margin-bottom: 0;
        min-width: 12em !important;
    }
   /* .nav-item :first-child {
        padding-left: 15px;
    }
    .nav-item :last-child {
        padding-right: 15px;
    }*/
    .nav-link {
        white-space: nowrap;
        /*min-width: 5em !important;*/
    }
    .dragscroll:active,
    .dragscroll:active a {
        cursor: -webkit-grabbing;
    }

    }

</style>

<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
<img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
<div class="page-main">
<!--app-header-->
<?php $this->load->view('backend/header');?>
<!-- app-content-->
<div class="container content-patient">
<div class="side-app">
<!-- page-header -->
<!-- <div class="page-header">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
</ol>
<div class="ml-auto">
<div class="input-group">
    <a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
        <span>
            <i class="fa fa-calendar"></i> Events Settings
        </span>
        <i class="fa fa-caret-down"></i>
    </a>
    <a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
        <span>
            <i class="fa fa-star"></i>
        </span>
    </a>
</div>
</div>
</div> -->
<!-- End page-header -->

<div class="row">
<div class="col-md-12 col-lg-12">
<div class="card">
<div class="card-header">
    <div class="col-md-10">
    <div class="card-title"><?=$form_title?></div>
    </div>
    <!-- <div class="col-md-2">
    <a href="nurse/nurse_list" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
        <span> Nurse List
        </span>
    </a>
    </div> -->
</div>
<div class="card-body">
    <form action="nurse/register_user_as_nurse" method="post">
    <div class="row">
    <?php if($this->session->flashdata('msg')){ ?>
        <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert-message">
                <span><?=$this->session->flashdata('msg');?></span>
            </div>
        </div>
    <?php } ?> 
    <div class="col-md-12">
        <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-profile  overflow-hidden">
                <div class="card-body text-center profile-bg">
                    <div class=" card-profile">
                        <div class="row justify-content-center">
                            <div class="">
                                <div class="">
                                    <a href="#">
                                        <img src="back_assets/images/users/female/33.png" class="avatar-xxl rounded-circle" alt="profile">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3 class="mt-3 text-white"><?=$patient_details[0]['first_name']." ".$patient_details[0]['last_name'];?></h3>
                    <p class="mb-2 text-white"><?=$patient_details[0]['user_role']== 3 ? 'Nurse' : ($patient_details[0]['user_role'] == 4 ? 'Patient' : '') ?></p>
                    <div class="text-center mb-4">
                        <span><i class="fa fa-star text-warning"></i></span>
                        <span><i class="fa fa-star text-warning"></i></span>
                        <span><i class="fa fa-star text-warning"></i></span>
                        <span><i class="fa fa-star-half-o text-warning"></i></span>
                        <span><i class="fa fa-star-o text-warning"></i></span>
                    </div>
                 
                    <!-- <a href="nurse-profile" class="btn btn-info btn-sm"><i class="fas fa-pencil-alt" aria-hidden="true"></i>Edit profile</a> -->
                </div>
                <div class="card-body">
                    <div class="nav-wrapper p-0">
                        <ul class="nav nav-pills dragscroll horizontal nav-fill flex-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0 active show" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="fa fa-home mr-2"></i>Profile</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="fa fa-user mr-2"></i>Patient Condition</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i class="fa fa-picture-o mr-2"></i>Assessment History</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body pb-0">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">

                            <div class="table-responsive mb-3">
                                <table class="table row table-borderless w-100 m-0 border">
                              <h4>Contact Details </h4>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>First Name :</strong> <?=ucwords($patient_details[0]['first_name'])?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Email :</strong> <?=$patient_details[0]['email']?></td>
                                        </tr>
                                        
                                        <tr>
                                            <td><strong>Gender :</strong> <?=$patient_details[0]['gender']==1 ? 'Male' : 'Female' ?></td>
                                        </tr>
                                    </tbody>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Last Name :</strong> <?=ucwords($patient_details[0]['last_name'])?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Contact :</strong> <?=$patient_details[0]['country_code']." ".$patient_details[0]['contact_no']?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                                <table class="table row table-borderless w-100 m-0 border">
                                    <h4>Bio Data</h4>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>ID :</strong>111</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Age :</strong> 25 </td>
                                        </tr>
                                        
                                        <tr>
                                            <td><strong>Height :</strong> 5feet </td>
                                        </tr>
                                    </tbody>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Weight :</strong> 50kg </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Photo :</strong> <span class="border d-block"><img src="back_assets/images/users/female/33.png" alt=""></span> </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                                <table class="table row table-borderless w-100 m-0 border">
                                    <h4>Location Details </h4>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Address :</strong> Kuala lampur </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Type Resident :</strong> Landed House </td>
                                        </tr>
                                        
                                        <tr>
                                            <td><strong>Type Security :</strong> Gated </td>
                                        </tr>
                                    </tbody>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Living Status :</strong> Family </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Current Care Status :</strong> Self </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Pets in House :</strong> Dog </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                        <div aria-labelledby="tabs-icons-text-2-tab" class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel">
                             <div class="table-responsive mb-3">
                                <table class="table row table-borderless w-100 m-0 border">
                                    <h4>Patient Health Status </h4>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Independent :</strong> Able to move</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Mental State :</strong> Awake and reponsive </td>
                                        </tr>
                                        
                                        <tr>
                                            <td><strong>Feeding :</strong> Feeds own self </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Allergies :</strong> None </td>
                                        </tr>
                                    </tbody>
                                   
                                </table>
                                <br>
                                <table class="table row table-borderless w-100 m-0 border">
                                    <h4>Items available at patients residence </h4>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td>A. <strong>Normal bed or Hospital Bed :</strong> No</td>
                                        </tr>
                                        <tr>
                                            <td>B. <strong>Ripple Matress or air matress :</strong> No</td>
                                        </tr>
                                        <tr>
                                            <td>C. <strong>Wheel Chair :</strong> No</td>
                                        </tr>
                                        <tr>
                                            <td>D. <strong>Walkers :</strong> No</td>
                                        </tr>
                                        <tr>
                                            <td>E. <strong>Commodes :</strong> Yes</td>
                                        </tr>
                                        <tr>
                                            <td>F. <strong>Oxygen Tank/Concentrator :</strong> No</td>
                                        </tr>
                                    </tbody>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td>G. <strong>Suction Machine :</strong> No</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>H. <strong>Drip Stand :</strong> Yes</td>
                                        </tr>

                                        <tr>
                                            <td>I. <strong>Bed Pan :</strong> Yes</td>
                                        </tr>

                                        <tr>
                                            <td>J. <strong>Bp Set :</strong> No</td>
                                        </tr>
                                        <tr>
                                            <td>K. <strong>Thermometer :</strong> No</td>
                                        </tr>

                                        <tr>
                                            <td>L. <strong>Spo2 Meter :</strong> No</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>M. <strong>Glucometer :</strong> No</td>
                                        </tr>
                                    </tbody>
                                </table>
                               
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
                            
                            <div class="table-responsive">
                                        <table id="example" class="table table-striped table-bordered text-nowrap w-100">
                                            <thead>
                                                <tr>
                                                    <th class="wd-15p">Booking ID</th>
                                                    <th class="wd-15p">Assessment Date</th>
                                                    <!-- <th class="wd-20p">Nurse Name</th> -->
                                                    <th class="wd-15p">View Assessment</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $i = 0;
                                                    foreach ($assessment_details as $value) { ?>
                                                <tr>   
                                                    <td><?=++$i?></td>
                                                    <td><?= date_format(date_create($value['created_at']),'d-m-Y'); ?></td>
                                                    <td><a href="nurse/assessment_details/<?=$value['id']?>" class="btn btn-primary">View</a></td>

                                                </tr>
                                                  <?php }  
                                                    ?>
                                            </tbody>
                                        </table>
                                    </div>
                        </div>
                        <!-- <div class="tab-pane fade" id="tabs-icons-text-4" role="tabpanel" aria-labelledby="tabs-icons-text-4-tab">
                            <table class="table row table-borderless w-100 m-0 border">
                        <h4>Experience</h4>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Organisation :</strong> -- </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Start Date :</strong> -- </td>
                                        </tr>
                                        
                                        <tr>
                                            <td><strong>Title :</strong> -- </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Reference :</strong> -- </td>
                                        </tr>
                                    </tbody>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>End Date :</strong> -- </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Description :</strong> -- </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Trainings Attented :</strong> </td>
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                        <div class="tab-pane fade" id="tabs-icons-text-5" role="tabpanel" aria-labelledby="tabs-icons-text-5-tab">
                            <h4>Competency </h4>
                            <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <td>Name of competency</td>
                                            <td>Description Here</td>
                                        </tr>
                                    </thead>
                                    <tbody class="col-lg-12 p-0">
                                        <tr>
                                            <td>Basic personal care - basic care for cleaning, feeding, medication etc.</td>
                                            <td>Trained but low experience and not very confident</td>
                                        </tr>
                                        <tr>
                                            <td> Tracheostomy Care</td>
                                            <td>Trained and very confident (More than 3 years experience)</td>
                                        </tr>
                                        <tr>
                                            <td>Central use and care</td>
                                            <td>Trained but no experience</td>
                                        </tr>
                                    </tbody>
                                   
                                </table>
                        </div>

                        <div class="tab-pane fade" id="tabs-icons-text-6" role="tabpanel" aria-labelledby="tabs-icons-text-6-tab">
                            <h4>Schedule</h4>
                            <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <td>Date</td>
                                            <td>Day</td>
                                            <td>Time</td>
                                        </tr>
                                    </thead>
                                    <tbody class="col-lg-12 p-0">
                                        <tr>
                                             <td>31st Noveberf,2019</td>
                                            <td>Sunday</td>
                                            <td><span class="bg-primary p-3 rounded">9:00 - 12:00</span></td>
                                        </tr>
                                        <tr>
                                              <td>31st Noveberf,2019</td>
                                            <td>Monday</td>
                                            <td><span class="bg-primary p-3 rounded">9:00 - 12:00</span></td>
                                        </tr>
                                        <tr>
                                              <td>31st Noveberf,2019</td>
                                            <td>Tuesday</td>
                                            <td><span class="bg-primary p-3 rounded">9:00 - 12:00</span></td>
                                        </tr>
                                        <tr>
                                              <td>31st Noveberf,2019</td>
                                            <td>Wednesday</td>
                                            <td><span class="bg-primary p-3 rounded">9:00 - 12:00</span></td>
                                        </tr>
                                        <tr>
                                              <td>31st Noveberf,2019</td>
                                            <td>Thursday</td>
                                            <td><span class="bg-danger p-3 rounded">None</span></td>
                                        </tr>
                                        <tr>
                                              <td>31st Noveberf,2019</td>
                                            <td>Firday</td>
                                            <td><span class="bg-primary p-3 rounded">9:00 - 12:00</span>
                                                
                                              <span class="bg-primary p-3 rounded">3:00 - 5:00</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                   
                                </table>
                        </div>

                        <div class="tab-pane fade" id="tabs-icons-text-7" role="tabpanel" aria-labelledby="tabs-icons-text-7-tab">
                            <h4>Patients</h4>
                            <table class="table table-bordered new_table w-100">
                                    <thead>
                                        <tr>
                                            <td>ID</td>
                                            <td>Name</td>
                                            <td>Contact No.</td>
                                            <td>Review</td>
                                            <td>Notes</td>
                                            <td>Assesment</td>
                                        </tr>
                                    </thead>
                                    <tbody class="col-lg-12 p-0">
                                        <tr>
                                            <td>#098123</td>
                                            <td>Arju</td>
                                            <td>0198233333</td>
                                            <td>
                                            <div class="btn-list">
                                                <a href="nurse-reviews-add" class="btn btn-outline-default">Add Review</a>
                                                <a href="nurse-reviews-list" class="btn btn-outline-primary">View Review</a>
                                            </div>
                                            </td>
                                            <td>
                                            <div class="btn-list">
                                            <a href="nurse-notes-add" class="btn btn-outline-default">Add Notes</a>
                                            <a href="nurse-notes-list" class="btn btn-outline-primary">View Notes</a>
                                            </div>
                                            </td>
                                            <td>
                                            <div class="btn-list">
                                            <a href="nurse-assessment-add" class="btn btn-outline-default">Add Assesment</a>
                                            <a href="nurse-assessment-list" class="btn btn-outline-primary">View Assesment</a>   
                                            </div>
                                            </td>

                                        </tr>
                                      
                                     
                                    </tbody>
                                   
                                </table>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    

            
            <!-- <div class="col-md-12">
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a large text here ..."></textarea>
            </div> -->
    
    </form>
</fieldset>
<!-- table-wrapper -->
</div>
<!-- section-wrapper -->
</div>
</div>

</div><!--End side app-->

<!-- Right-sidebar-->
<?php $this->load->view('backend/right_sidebar');?>
<!-- End Rightsidebar-->

<?php $this->load->view('backend/footer');?>
</div>
<!-- End app-content-->
</div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>
  <script type="text/javascript">
    let dataTable = $(".new_table").dataTable(
      {  "bLengthChange" : false,
           "responsive": true,
          "oLanguage": 
        {
            "sSearch": ""
        }
      });
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust()
           .responsive.recalc();
        });    
      
</script>

</body>
</html>