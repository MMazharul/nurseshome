<?php $this->load->view('backend/head_link');?>


<style>
    @media (max-width: 480px) {
    .nav-wrapper {
    display: block;
    /*overflow: hidden;*/
    height: calc(1.5rem + 1rem + 2px); /** 1.5 is font-size, 1 is padding top and bottom, 2 is border width top and bottom */
    position: relative;
    z-index: 1;
    margin-bottom: -1px;
    }
    .nav-pills {
        overflow-x: auto;
        flex-wrap: nowrap;
        border-bottom: 0;
    }
     .nav-item {
        margin-bottom: 0;
        min-width: 12em !important;
    }
   /* .nav-item :first-child {
        padding-left: 15px;
    }
    .nav-item :last-child {
        padding-right: 15px;
    }*/
    .nav-link {
        white-space: nowrap;
        /*min-width: 5em !important;*/
    }
    .dragscroll:active,
    .dragscroll:active a {
        cursor: -webkit-grabbing;
    }

    }

</style>

<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
<img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
<div class="page-main">
<!--app-header-->
<?php $this->load->view('backend/header');?>
<!-- app-content-->
<div class="container content-patient">
<div class="side-app">
<!-- page-header -->
<!-- <div class="page-header">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
</ol>
<div class="ml-auto">
<div class="input-group">
    <a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
        <span>
            <i class="fa fa-calendar"></i> Events Settings
        </span>
        <i class="fa fa-caret-down"></i>
    </a>
    <a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
        <span>
            <i class="fa fa-star"></i>
        </span>
    </a>
</div>
</div>
</div> -->
<!-- End page-header -->

<div class="row">
<div class="col-md-12 col-lg-12">
<div class="card">
<div class="card-header">
    <div class="col-md-10">
    <div class="card-title"><?=$form_title?></div>
    </div>
    <!-- <div class="col-md-2">
    <a href="nurse/nurse_list" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
        <span> Nurse List
        </span>
    </a>
    </div> -->
</div>
<div class="card-body">
    <form action="nurse/register_user_as_nurse" method="post">
    <div class="row">
    <?php if($this->session->flashdata('msg')){ ?>
        <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert-message">
                <span><?=$this->session->flashdata('msg');?></span>
            </div>
        </div>
    <?php } ?>
    <div class="col-md-12">
        <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-profile  overflow-hidden">
                <div class="card-body text-center profile-bg">
                    <div class=" card-profile">
                        <div class="row justify-content-center">
                            <div class="">
                                <div class="">
                                    <a href="#">
                                        <img src="uploads/<?=$nurse_details[0]['profile_pic'];?>" class="avatar-xxl rounded-circle" alt="profile">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3 class="mt-3 text-white"><?=$nurse_details[0]['first_name']." ".$nurse_details[0]['last_name'];?></h3>
                    <p class="mb-2 text-white"><?=$nurse_details[0]['user_role']== 3 ? 'Nurse' : ($nurse_details[0]['user_role'] == 4 ? 'Patient' : '') ?></p>
                    <!-- <div class="text-center mb-4">
                        <span><i class="fa fa-star text-warning"></i></span>
                        <span><i class="fa fa-star text-warning"></i></span>
                        <span><i class="fa fa-star text-warning"></i></span>
                        <span><i class="fa fa-star-half-o text-warning"></i></span>
                        <span><i class="fa fa-star-o text-warning"></i></span>
                    </div> -->


                    <a href="nurse-profile" class="btn btn-pink btn-sm"><i class="fas fa-pencil-alt" aria-hidden="true"></i>Edit profile</a>
                </div>
                <div class="card-body">
                    <div class="nav-wrapper p-0">
                        <ul class="nav nav-pills dragscroll horizontal nav-fill flex-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0 active show" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="fa fa-home mr-2"></i>Account</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="fa fa-user mr-2"></i>Bio</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i class="fa fa-picture-o mr-2"></i>Qualification</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-4-tab" data-toggle="tab" href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-4" aria-selected="false"><i class="fa fa-newspaper-o mr-2 mt-1"></i>Experience</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-0 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-5-tab" data-toggle="tab" href="#tabs-icons-text-5" role="tab" aria-controls="tabs-icons-text-5" aria-selected="false"><i class="fa fa-cog mr-2"></i>Competency</a>
                            </li>
                           <!--  <li class="nav-item">
                                <a class="nav-link mb-sm-0 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-6-tab" data-toggle="tab" href="#tabs-icons-text-6" role="tab" aria-controls="tabs-icons-text-6" aria-selected="false"><i class="fa fa-cog mr-2"></i>Schedule</a>
                            </li> -->
                        <!--     <li class="nav-item">
                                <a class="nav-link mb-sm-0 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-7-tab" data-toggle="tab" href="#tabs-icons-text-7" role="tab" aria-controls="tabs-icons-text-7" aria-selected="false"><i class="fa fa-cog mr-2"></i>Patient</a>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body pb-0">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">

                            <div class="table-responsive mb-3">
                              <pre>
                                <table class="table row table-borderless w-100 m-0 border">
                              <h4>Basic Contact Information
                                <!-- <a class="btn-primary btn ml-2" href="nurse-profile">Add (+)</a> -->
                            </h4>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>First Name  : </strong><?=ucwords($nurse_details[0]['first_name'])?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Email       : </strong><?=$nurse_details[0]['email']?></td>
                                        </tr>

                                        <tr>
                                            <td><strong>Gender      : </strong><?=$nurse_details[0]['gender']==1 ? 'Male' : 'Female' ?></td>
                                        </tr>
                                    </tbody>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Last Name   : </strong><?=ucwords($nurse_details[0]['last_name'])?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Contact     : </strong><?=$nurse_details[0]['country_code']." ".$nurse_details[0]['contact_no']?></td>
                                        </tr>
                                    </tbody>
                                </table>
                              </pre>
                            </div>
                        </div>
                        <div aria-labelledby="tabs-icons-text-2-tab" class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel">
                             <div class="table-responsive mb-3">
                                <pre>
                                <table class="table row table-borderless w-100 m-0 border">
                                    <h4>Bio
                                    </h4>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>ID     :</strong><?=!empty($nurse_bio[0]['nric_passport_id']) ? $nurse_bio[0]['nric_passport_id'] : '';?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Age    :</strong> <?=!empty($nurse_bio[0]['age']) ? $nurse_bio[0]['age'] : '';?> </td>
                                        </tr>

                                        <tr>
                                            <td><strong>Height :</strong> <?=!empty($nurse_bio[0]['height']) ? $nurse_bio[0]['height'] : '';?> </td>
                                        </tr>
                                    </tbody>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Weight :</strong> <?=!empty($nurse_bio[0]['weight']) ? $nurse_bio[0]['weight'] : '';?> </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Photo  :</strong> <span class="border d-block"><img src="uploads/<?=!empty($nurse_bio[0]['bio_file']) ? $nurse_bio[0]['bio_file'] : '';?>" alt=""></span> </td>
                                        </tr>
                                    </tbody>
                                </table>
                               </pre>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
                            <pre>
                            <table class="table row table-borderless w-100 m-0 border">
                                    <h4>Qualification
                                        <!-- <a class="btn-primary btn ml-2" href="nurse-education-qualification-add">Add (+)</a> -->
                                    </h4>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Post Basic  :</strong> <?=!empty($nurse_education_certificate[0]['post_basic']) ? $nurse_education_certificate[0]['post_basic'] : '' ?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Degree      :</strong> <?=!empty($nurse_education_certificate[0]['degree']) ? $nurse_education_certificate[0]['degree'] : '' ?> </td>
                                        </tr>

                                        <tr>
                                            <td><strong>Diploma     :</strong> <?=!empty($nurse_education_certificate[0]['diploma']) ? $nurse_education_certificate[0]['diploma'] : '' ?></td>
                                        </tr>
                                    </tbody>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Certificate :</strong> <?=!empty($nurse_education_certificate[0]['certificate']) ? $nurse_education_certificate[0]['certificate'] : '' ?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Lim Reg. no :</strong> <?=!empty($nurse_education_certificate[0]['reg_number']) ? $nurse_education_certificate[0]['reg_number'] : '' ?> </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Date Reg.   :</strong> <?=!empty($nurse_education_certificate[0]['reg_date']) ? $nurse_education_certificate[0]['reg_date'] : '' ?> </td>
                                        </tr>
                                    </tbody>
                                </table>
                              </pre>
                                <h4 class="m-2">Additional Certificate
                                    <!-- <a class="btn-primary btn ml-2" href="nurse-additional-certificate-add">Add (+)</a> -->
                                </h4>
                                <pre>
                                <table class="table row table-borderless w-100 m-0 border">
                                  <!--   <span class="d-block float-right mb-2"></span> -->
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>BLS         :</strong> <?=!empty($nurse_additional_certificate[0]['bls']) ? $nurse_additional_certificate[0]['bls'] : '' ?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Cannulation :</strong> <?=!empty($nurse_additional_certificate[0]['cannulation']) ? $nurse_additional_certificate[0]['cannulation'] : '' ?> </td>
                                        </tr>

                                        <tr>
                                            <td><strong>Palliative  :</strong> <?=!empty($nurse_additional_certificate[0]['palliative']) ? $nurse_additional_certificate[0]['palliative'] : '' ?> </td>
                                        </tr>

                                         <tr>
                                            <td><strong>Others      :</strong> <?=!empty($nurse_additional_certificate[0]['others']) ? $nurse_additional_certificate[0]['others'] : '' ?> </td>
                                        </tr>
                                    </tbody>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Current Valid APC/Retention :</strong> <?=!empty($nurse_additional_certificate[0]['retention']) ? $nurse_additional_certificate[0]['retention'] : '' ?> </td>
                                        </tr>
                                        <tr>
                                            <td><strong>LIM Reg. Certificate        :</strong><br> <img src="uploads/<?=!empty($nurse_additional_certificate[0]['lm_reg_certificate']) ? $nurse_additional_certificate[0]['lm_reg_certificate'] : '' ?>" alt=""> </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Driver License              :</strong> <?=!empty($nurse_additional_certificate[0]['driver_license']) ? $nurse_additional_certificate[0]['driver_license'] : '' ?> </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Own Transport               :</strong> <?=!empty($nurse_additional_certificate[0]['have_own_transport']) ? $nurse_additional_certificate[0]['have_own_transport'] : '' ?> </td>
                                        </tr>
                                    </tbody>
                                </table>
                              </pre>
                        </div>
                        <div class="tab-pane fade" id="tabs-icons-text-4" role="tabpanel" aria-labelledby="tabs-icons-text-4-tab">

                            <?php $serial = 1;
                            foreach ($nurse_experience as $key => $value) : ?>
                                <pre>
                                 <table class="table row table-borderless w-100 m-0 border">
                                    <?php echo $serial > 1 ? '<br>' : ''?>

                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Organisation :</strong> <?=$value['organisation']?> </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Start Date   :</strong> <?=$value['start_date']?> </td>
                                        </tr>

                                        <tr>
                                            <td><strong>Title        :</strong> <?=$value['title']?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Reference    :</strong> <?=$value['ref']?> </td>
                                        </tr>
                                    </tbody>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>End Date           :</strong> <?=$value['end_date']?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Description        :</strong> <?=$value['duty_description']?> </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Trainings Attented :</strong> <?=$value['training_attended']?> </td>
                                        </tr>
                                    </tbody>
                                    </table>
                                 </pre>

                                <?php $serial++; endforeach;?>

                        </div>
                        <div class="tab-pane fade" id="tabs-icons-text-5" role="tabpanel" aria-labelledby="tabs-icons-text-5-tab">
                            <h4>Competency
                                <!-- <a class="btn-primary btn ml-2" href="nurse-competency-add">Add (+)</a> -->
                            </h4>

                            <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <td>Name of competency</td>
                                            <td>Description Here</td>
                                        </tr>
                                    </thead>
                                    <tbody class="col-lg-12 p-0">

                                        <?php
                                            foreach ($competencies as $key => $value) { ?>
                                               <tr>
                                                    <td><?=!empty($value['title']) ? $value['title'] : '' ?></td>
                                                    <td><?=!empty($value['com_value']) ? $value['com_value'] : '' ?></td>
                                                </tr>
                                            <?php }
                                        ?>

                                    </tbody>

                                </table>
                      
                        </div>

                        <div class="tab-pane fade" id="tabs-icons-text-6" role="tabpanel" aria-labelledby="tabs-icons-text-6-tab">
                            <h4>Schedule
                                <!-- <a class="btn-primary btn ml-2" href="nurse-schedule-monthly">Add (+)</a> -->
                            </h4>
                            <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <td>Date</td>
                                            <td>Day</td>
                                            <td>Time</td>
                                        </tr>
                                    </thead>
                                    <tbody class="col-lg-12 p-0">
                                        <tr>
                                             <td>31st Noveberf,2019</td>
                                            <td>Sunday</td>
                                            <td><span class="bg-primary p-3 rounded">9:00 - 12:00</span></td>
                                        </tr>
                                        <tr>
                                              <td>31st Noveberf,2019</td>
                                            <td>Monday</td>
                                            <td><span class="bg-primary p-3 rounded">9:00 - 12:00</span></td>
                                        </tr>
                                        <tr>
                                              <td>31st Noveberf,2019</td>
                                            <td>Tuesday</td>
                                            <td><span class="bg-primary p-3 rounded">9:00 - 12:00</span></td>
                                        </tr>
                                        <tr>
                                              <td>31st Noveberf,2019</td>
                                            <td>Wednesday</td>
                                            <td><span class="bg-primary p-3 rounded">9:00 - 12:00</span></td>
                                        </tr>
                                        <tr>
                                              <td>31st Noveberf,2019</td>
                                            <td>Thursday</td>
                                            <td><span class="bg-danger p-3 rounded">None</span></td>
                                        </tr>
                                        <tr>
                                              <td>31st Noveberf,2019</td>
                                            <td>Firday</td>
                                            <td><span class="bg-primary p-3 rounded">9:00 - 12:00</span>

                                              <span class="bg-primary p-3 rounded">3:00 - 5:00</span>
                                            </td>
                                        </tr>
                                    </tbody>

                                </table>
                        </div>

                        <div class="tab-pane fade" id="tabs-icons-text-7" role="tabpanel" aria-labelledby="tabs-icons-text-7-tab">
                            <h4>Patients</h4>
                            <table class="table table-bordered new_table w-100">
                                    <thead>
                                        <tr>
                                            <td>ID</td>
                                            <td>Name</td>
                                            <td>Contact No.</td>
                                            <td>Review</td>
                                             <td>Notes</td>
                                              <td>Assesment</td>
                                        </tr>
                                    </thead>
                                    <tbody class="col-lg-12 p-0">
                                        <tr>
                                            <td>#098123</td>
                                            <td>Arju</td>
                                            <td>0198233333</td>
                                            <td>
<div class="btn-list">
                                                <a href="nurse-reviews-add" class="btn btn-outline-default">Add Review</a>
<a href="nurse-reviews-list" class="btn btn-outline-primary">View Review</a>
</div>
                                            </td>
                                              <td>
<div class="btn-list">
                                                <a href="nurse-notes-add" class="btn btn-outline-default">Add Notes</a>
<a href="nurse-notes-list" class="btn btn-outline-primary">View Notes</a>
</div>
                                                 </td>
       <td>
        <div class="btn-list">
                 <a href="nurse-assessment-add" class="btn btn-outline-default">Add Assesment</a>
        <a href="nurse-assessment-list" class="btn btn-outline-primary">View Assesment</a>
        </div>

    </td>

                                        </tr>


                                    </tbody>

                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



            <!-- <div class="col-md-12">
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a large text here ..."></textarea>
            </div> -->

    </form>
</fieldset>
<!-- table-wrapper -->
</div>
<!-- section-wrapper -->
</div>
</div>

</div><!--End side app-->

<!-- Right-sidebar-->
<?php $this->load->view('backend/right_sidebar');?>
<!-- End Rightsidebar-->

<?php $this->load->view('backend/footer');?>
</div>
<!-- End app-content-->
</div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>
  <script type="text/javascript">
    let dataTable = $(".new_table").dataTable(
      {  "bLengthChange" : false,
           "responsive": true,
          "oLanguage":
        {
            "sSearch": ""
        }
      });
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust()
           .responsive.recalc();
        });

</script>

</body>
</html>
