<?php $this->load->view('backend/head_link');?>

<body class="app sidebar-mini rtl">
<style>
  .select-box{
    display: none;
  }
  td:first-child{
    width: 50%;
  }
</style>
<!--Global-Loader-->
<!-- <div id="global-loader">
    <img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
    <div class="page-main">
        <!--app-header-->
        <?php $this->load->view('backend/header');?>
        <!-- app-content-->
        <div class="container content-patient">
            <div class="side-app">
                <!-- page-header -->
                <!-- <div class="page-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
                    </ol>
                    <div class="ml-auto">
                        <div class="input-group">
                            <a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
                                <span>
                                    <i class="fa fa-calendar"></i> Events Settings
                                </span>
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                                <span>
                                    <i class="fa fa-star"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div> -->
                <!-- End page-header -->
                <?php if($this->session->flashdata('msg')){ ?>
                                <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <div class="alert-message">
                                        <span><?=$this->session->flashdata('msg');?></span>
                                    </div>
                                </div>
                            <?php } ?> 

           <div class="row">
        <div class="col-md-12">
            <div class="card card-profile  overflow-hidden">
           
            <div class="card-body">
                    <div class="nav-wrapper p-0">
                        <ul class="nav nav-pills dragscroll horizontal nav-fill flex-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0 active show" id="profile" data-toggle="tab" href="#tabs-profile" role="tab" aria-controls="profile" aria-selected="true"><i class="fa fa-home mr-2"></i>Profile</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="bio" data-toggle="tab" href="#tabs-bio" role="tab" aria-controls="bio" aria-selected="false"><i class="fa fa-user mr-2"></i>Bio</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="qualification" data-toggle="tab" href="#tabs-qualification" role="tab" aria-controls="tabs-qualification" aria-selected="false"><i class="fa fa-picture-o mr-2"></i>Qualification</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="experience" data-toggle="tab" href="#tabs-experience" role="tab" aria-controls="experience" aria-selected="false"><i class="fa fa-newspaper-o mr-2 mt-1"></i>Experience</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-0 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="competency" data-toggle="tab" href="#tabs-competency" role="tab" aria-controls="competency" aria-selected="false"><i class="fa fa-cog mr-2"></i>Competency</a>
                            </li>
                        </ul>
                    </div>
            </div>
            </div>
            <div class="card">
                <div class="card-body pb-0">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show" id="tabs-profile" role="tabpanel" aria-labelledby="profile">
                        <div class="row">
                            <div class="col-md-12">
                                <form action="nurse/update_profile/<?=$nurse_details[0]['loginid']?>" method="post">
                            <div class="row">
                            
                            <div id="alert_pass"></div>
                            <fieldset class="w-100">
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="firstname">First Name</label>
                                                </div>
                                                <div class="col-md-9">
                                                <input name="firstname" value="<?=$nurse_details[0]['first_name']?>" type="text" class="form-control" required placeholder="First Name">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                        <div class="col-md-3 text-right">
                                            <label for="lastname">Last Name</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input name="lastname" value="<?=$nurse_details[0]['last_name']?>" type="text" class="form-control" required placeholder="Last Name">
                                        </div>
                                        </div> 
                                    </div>
                                </div>
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="email">E-mail</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="email" value="<?=$nurse_details[0]['email']?>" class="form-control" id="email" name="email" placeholder="Email" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="row">
                                            <div class="col-md-3 text-right">
                                                <label for="contact">Contact</label>
                                            </div>
                                            <div class="col-md-9">
                                            <div class="input-group mb-3">
                                              <input style='width:2rem;text-align:center;background:#ccc;color:#111; outline:0;border:0' type="text" name="country_code" readonly value="+60">
                                              <input name="contact" value="<?=$nurse_details[0]['contact_no']?>" type="text" class="form-control" required placeholder="Contact no">
                                            </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    <div class="form-group">
                                            <div class="row">
                                                    <div class="col-md-3 text-right">
                                                        <label for="id">Gender</label>
                                                    </div>
                                                    <div class="col-md-9">
                                                    <select name="gender" class="form-control" id="gender">
                                                        <option value="1" <?=$nurse_details[0]['gender'] == 1 ? 'selected' : '' ;?>  >Male</option>
                                                        <option value="2" <?=$nurse_details[0]['gender'] == 2 ? 'selected' : '' ;?> >Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="password">Password</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="password" class="form-control" id="password" name="password"  placeholder="Password" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="row">
                                            <div class="col-md-3 text-right">
                                                <label for="re_password">Re-enter Password</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="password" class="form-control" id="passconf" name="passconf" placeholder="Re-enter Password" value="">
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </fieldset>
                            
                          

                                    

                                <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                    
                                    <div class="">
                                        <button class="btn btn-primary" type="submit" name="save" id="save_contact">Submit</button>
                                    </div>
                                </div>
                            </div>
                            
                            </form>



                            </div>
                        </div>
                        </div>
                        <div aria-labelledby="" class="tab-pane fade" id="tabs-bio" role="tabpanel">
                            <form action="" method="post">
                            <fieldset class="biodata">
                          
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="id">ID</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="id" name="id" placeholder="NRIC/Passport" value="">
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="age">Age</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="number" class="form-control" id="age" name="age" placeholder="Age" value="">
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="age">Height</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="number" class="form-control" id="height" name="height" placeholder="height" value="">
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="age">Weight</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="number" class="form-control" id="weight" name="weight" placeholder="Weight" value="">
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                
                                <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="id">Photo</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="file" name="photo" id="">
                                            </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            </fieldset>
                            <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                <div class="">
                                    <button class="btn btn-primary" type="submit" name="save" id="">Submit</button>
                                </div>
                            </div>
                            </form>
                          
                        </div>
                        <div class="tab-pane fade" id="tabs-experience" role="tabpanel" aria-labelledby="tabs-experience">
                           <form action="" method="post">
                            
                            
                          
                            <fieldset class="nursing_experiece">
                            <div class="row">
                            <div class="col-md-12">
                                <h4 class="" style="line-height:2.7em">
                                Nursing Experience
                                <span class="float-right btn btn-primary add_exp">Add+</span>
                            </h4>
                            </div>
                            </div>
                        
                            <div class="main_exp">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="organisation">Organisation</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="organisation" name="organisation" placeholder="Organisation" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="start_period">Start Date</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input class="form-control fc-datepicker hasDatepicker" placeholder="MM/DD/YYYY" type="text" id="dp1573642265589">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="start_period">End Date</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input class="form-control fc-datepicker hasDatepicker" placeholder="MM/DD/YYYY" type="text" id="dp1573642265590">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="title">Title</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="desc_of_duties">Description of duties</label>
                                            </div>
                                            <div class="col-md-9">
                                            <textarea class="form-control" id="desc_of_duties" name="desc_of_duties" cols="30" rows="1">Description of duties
                                            </textarea>
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="references">References</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input type="text" class="form-control" id="references" name="references" placeholder="References" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="desc_of_duties">Trainings Attended</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input type="text" class="form-control" id="training_attended" name="training_attended" cols="30" rows="1" placeholder="Trainings Attended">
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div>

                            <div id="additional_exp">
                                    
                            </div>

                        
                            </fieldset>
                                    

                            <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                <div class="">
                                    <button class="btn btn-primary" type="submit" name="save" id="">Submit</button>
                                </div>
                            </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="tabs-qualification" role="tabpanel" aria-labelledby="tabs-qualification">
                          <form action="" method="post">
                            
                            <fieldset>                         
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="age">Post basic</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="postbasic" name="postbasic" placeholder="Post Basic" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="degree">Degree</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="degree" name="degree" placeholder="Degree" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="age">Diploma</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="diploma" name="diploma" placeholder="Diploma" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="certificate">Certificate</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="certificate" name="certificate" placeholder="Certificate" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="lim_reg_number">LIM Reg. Number</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="lim_reg_number" name="lim_reg_number" placeholder="LIM Registration Number" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="certificate">Date Registered</label>
                                                </div>
                                                <div class="col-md-9">
                                                <input class="form-control fc-datepicker hasDatepicker" placeholder="MM/DD/YYYY" type="text" id="dp1573642265588">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>

                            <fieldset>
                            <div class="row">
                            <div class="col-md-12"><h4 class="" style="line-height:2.7em">Additional Certificate</h4></div>
                            </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="bls">BLS</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="bls" name="bls" placeholder="BLS" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="cannulation">Cannulation</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="cannulation" name="cannulation" placeholder="Cannulation" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="palliative">Palliative</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="palliative" name="palliative" placeholder="Palliative" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="Others">Others</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="Others" name="Others" placeholder="Others" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="valid_apc_retention">Current Valid APC/Retention</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="valid_apc_retention" name="valid_apc_retention" placeholder="Current Valid APC/Retention" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="upload_lim_reg_cert">Upload LIM REG. Certificate</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="file" class="form-control" id="upload_lim_reg_cert" name="upload_lim_reg_cert" placeholder="Upload LIM REG. Certificate" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="driver_license">Driver License</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="driver_license" name="driver_license" placeholder="CAR/MOTORCYCLE" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="have_own_transport">Have Own Transport</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="have_own_transport" name="have_own_transport" placeholder="CAR/MOTORCYCLE" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                    <div class="">
                                        <button class="btn btn-primary btn-full-width" type="submit" name="save" id="save_contact">Save</button>
                                    </div>
                                </div> -->
                                </div>
                                <div class="row">
                            </div>
                            </fieldset>
                          
                          

                                    

                            <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                <div class="">
                                    <button class="btn btn-primary" type="submit" name="save" id="">Submit</button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tabs-competency" role="tabpanel" aria-labelledby="tabs-competency">
                           <form action="" method="post">
                    <fieldset class="competencies">
                      <div class="row">
                        <div class="col-md-6">
                          <h4 class="" style="line-height:2.7em">List of Competencies</h4>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-12 text-left">
                                <label for="nursing_competency_desc[]">Select one of following description for each
                                competency below : </label>
                              </div>
                              <div class="col-md-12">

                                <table class="table-bordered" cellpadding="8">
                                    <thead>
                                     
                                    </thead>
                                    <tbody>


<?php
//print_r($competencies);
foreach ($competencies as $key => $value) {
    ?>
                                          <tr>
                                        <td><label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input checkbox" name="nursing_competency[]" value="<?=$value['compe_id'];?>">
                                            <span class="custom-control-label"><?=$value['compet_ttile']?></span>
                                          </label></td>
                                        <td>
                                          <select name="nursing_competency_desc[]" class="select-box form-control" id="">
                                            <option value="1">Trained and very confident (More than 3 years experience)</option>
                                            <option value="2">Trained but low experience and not very confident</option>
                                            <option value="3">Trained but no experience</option>
                                            <option value="4">Not trained</option>
                                          </select>
                                        </td>
                                      </tr>
    <?php
    # code...
}


?>
                                 
                                    </tbody>

                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                      </div>
                    
                    </fieldset>


                    <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                      <div class="">
                        <button class="btn btn-primary" type="submit" name="save" id="">Submit</button>
                      </div>
                    </div>
      
                  </form>
                </div>

                     

                       
                    </div>
                </div>
            </div>
        </div>
    </div>

            <!-- Right-sidebar-->
            <?php $this->load->view('backend/right_sidebar');?>
            <!-- End Rightsidebar-->

            <?php $this->load->view('backend/footer');?>
        </div>
        <!-- End app-content-->
    </div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>



<template>
    <div class="row-target">
                            <h4 class="" style="line-height:2.7em">
                                Nursing Experience
                                <span class="add_exp float-right btn btn-primary">Add+</span>
                                <span  data-row-target='.row-target' class="remove_exp float-right btn btn-danger">Remove-</span>
                            </h4>  
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="organisation">Organisation</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="" name="organisation[]" placeholder="Organisation" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="start_period">Start Date</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input class="form-control fc-datepicker hasDatepicker" placeholder="MM/DD/YYYY" type="text" id="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="start_period">End Date</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input class="form-control fc-datepicker hasDatepicker" placeholder="MM/DD/YYYY" type="text" id="dp1573642265590">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="title">Title</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="desc_of_duties">Description of duties</label>
                                            </div>
                                            <div class="col-md-9">
                                            <textarea class="form-control" id="desc_of_duties" name="desc_of_duties" cols="30" rows="1">Description of duties
                                            </textarea>
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="references">References</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input type="text" class="form-control" id="references" name="references" placeholder="References" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="desc_of_duties">Trainings Attended</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input type="text" class="form-control" id="training_attended" name="training_attended" cols="30" rows="1" placeholder="Trainings Attended">
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
    </div>
</template>
<?php $this->load->view('backend/footer_link');?>


<script>
let pass = document.querySelector('#password');
let pass2 = document.querySelector('#passconf');

pass2.onkeyup = () => {
    if (pass.value != pass2.value ) {
        document.getElementById("alert_pass").innerHTML = 
        `<div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button><div class="alert-message">
            <span>Password Don't Match!</span>
        </div>
        </div>`;
        
        document.getElementById("submitBtn").disabled = true;	
    } 
    else{
        document.getElementById("alert_pass").innerHTML = '';
        document.getElementById("submitBtn").disabled = false;	
    }
}
    // $('.checkbox').change(function(){
    //     alert('hi');
    // });

</script>


<script>
    let template = document.querySelectorAll('template')[0];
    let additional_exp = document.getElementById('additional_exp');

    removeEvent(document, 'click', '.remove_exp',removeItem);
    addEvent(document, 'click', '.add_exp', addItem);
    
    let count = 1;

    function addEvent(element, event, selector,callback){
       element.addEventListener(event, e =>{
        if (e.target.matches(selector)) {
            if (count < 10) {
               count++; 
               addItem(count);  
            }
            else{
                alert('Sorry can\'t add more!');
            } 
            }
       })
    }

    function removeEvent(element, event, selector, callback){
      element.addEventListener(event, e => {
        if (e.target.matches(selector)) {
          removeItem(e)
        }
      })
    }

    function addItem(count){
        additional_exp.append(template.content.cloneNode(true));  
    } 

    function removeItem(params) {
      let buttonClicked = params.target
      buttonClicked.closest(buttonClicked.dataset.rowTarget).remove()
      count--; 
    }
    
    
</script>


  <script>
    let otherComp = document.querySelector('#others_comp');
    let otherCompBox = document.getElementById('other_comp_box');

    otherComp.addEventListener('change', () => {
      if(otherComp.checked){
        otherCompBox.style.display = 'block';
      }else{
        otherCompBox.style.display = 'none';
      }
    });

    const checkbox = document.querySelectorAll('.checkbox');
    const selectBox = document.querySelectorAll('.select-box');

    for (let i = 0; i < checkbox.length; i++) {
      checkbox[i].addEventListener('change', () => {
      if(checkbox[i].checked){
        selectBox[i].style.display = 'block';
      }else{
        selectBox[i].style.display = 'none';
      }
    });
    }
    
  </script>
</body>
</html>