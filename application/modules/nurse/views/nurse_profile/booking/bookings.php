<?php $this->load->view('backend/head_link');?>


<style>
    @media (max-width: 480px) {
    .nav-wrapper {
    display: block;
    /*overflow: hidden;*/
    height: calc(1.5rem + 1rem + 2px); /** 1.5 is font-size, 1 is padding top and bottom, 2 is border width top and bottom */
    position: relative;
    z-index: 1;
    margin-bottom: -1px;
    }
    .nav-pills {
        overflow-x: auto;
        flex-wrap: nowrap;
        border-bottom: 0;
    }
     .nav-item {
        margin-bottom: 0;
        min-width: 12em !important;
    }
   /* .nav-item :first-child {
        padding-left: 15px;
    }
    .nav-item :last-child {
        padding-right: 15px;
    }*/
    .nav-link {
        white-space: nowrap;
        /*min-width: 5em !important;*/
    }
    .dragscroll:active,
    .dragscroll:active a {
        cursor: -webkit-grabbing;
    }

    }

</style>

<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
<img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
<div class="page-main">
<!--app-header-->
<?php $this->load->view('backend/header');?>
<!-- app-content-->
<div class="container content-patient">
<div class="side-app">
<!-- page-header -->
<!-- <div class="page-header">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
</ol>
<div class="ml-auto">
<div class="input-group">
    <a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
        <span>
            <i class="fa fa-calendar"></i> Events Settings
        </span>
        <i class="fa fa-caret-down"></i>
    </a>
    <a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
        <span>
            <i class="fa fa-star"></i>
        </span>
    </a>
</div>
</div>
</div> -->
<!-- End page-header -->

<div class="row">
<div class="col-md-12 col-lg-12">
<div class="card">
<div class="card-header">
    <div class="col-md-10">
    <div class="card-title"><?=$form_title?></div>
    </div>
    <!-- <div class="col-md-2">
    <a href="nurse/nurse_list" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
        <span> Nurse List
        </span>
    </a>
    </div> -->
</div>
<div class="card-body">
    <form action="nurse/register_user_as_nurse" method="post">
    <div class="row">
    <?php if($this->session->flashdata('msg')){ ?>
        <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert-message">
                <span><?=$this->session->flashdata('msg');?></span>
            </div>
        </div>
    <?php } ?>
    <div class="col-md-12">
        <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-profile  overflow-hidden">

                <div class="card-body">
                    <div class="nav-wrapper p-0">
                        <ul class="nav nav-pills dragscroll horizontal nav-fill flex-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0 active show" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="fa fa-home mr-2"></i>All (<?php echo $all_count?>)</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="fa fa-user mr-2"></i>Active (<?php echo $active_count?>)</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i class="fa fa-picture-o mr-2"></i>Completed (<?php echo $complete_count?>)</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-4-tab" data-toggle="tab" href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-4" aria-selected="false"><i class="fa fa-newspaper-o mr-2 mt-1"></i>Cancelled (<?php echo $cancel_count?>)</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body pb-0">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                            <div class="table-responsive mb-3">
                                <table class="table table-bordered new_table w-100">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th width="3%">Booking ID</th>
                                            <th>Patient Name</th>
                                            <th width="3%">Contact No</th>
                                            <th>Booking Period</th>
                                            <th>Booking Date</th>
                                            <th width="3%">Hours/day</th>
                                            <th width="3%">Total Hours</th>
                                            <th>Total Charge</th>
                                            <th>Status</th>
                                            <th>View Booking</th>
                                        </tr>
                                    </thead>
                                    <tbody class="col-lg-12 p-0">
                                        <?php
                                        $i =0;
                                        foreach ($book_details_all as $key => $value)
                                         {
                                            $id=$value['book_id'];
                                            $bookingid=$value['patient_bookid'];
                                            $patientname=$value['first_name'].' '.$value['last_name'];
                                            $patientcontact=$value['contact_no'];
                                            $patientpic=$value['profile_pic'];
                                            $bookperiod=$value['fromtime']." To <br>".$value['totime'];
                                            $bookhour=$value['time_in_hours'];
                                            $bookdate=$value['adddate'];
                                            $notes=$value['notes'];
                                            $book_status=$value['book_status'];
                                            $book_charge=$value['book_charge'];

                                            $st = strtotime($value['fromtime']);
                                            $end = strtotime($value['totime']);
                                            $datediff = ($end - $st);

                                            $total_days = round($datediff / (60 * 60 * 24))+1;

                                            $addon_price = $value['a1price']+$value['a2price']+$value['a3price'];

                                            $total_hours = ($value['time_in_hours'])*$total_days;

                                            // $total_price = ($value['book_charge']*$total_hours)+$addon_price;


                                                if($book_status==1)
                                                {
                                                    $bt="New";
                                                }
                                                elseif($book_status==2)
                                                {
                                                    $bt="Approved";
                                                }
                                                elseif($book_status==3)
                                                {
                                                    $bt="Completed";
                                                }
                                                elseif($book_status==4)
                                                {
                                                    $bt="Review";
                                                }
                                                elseif($book_status==5)
                                                {
                                                    $bt="Finish";
                                                }
                                                elseif($book_status==6)
                                                {
                                                    $bt="Complete";
                                                }

                                            ?>
                                        <tr>
                                            <td><?=++$i?></td>
                                            <td><?=$id?></td>
                                            <td><span><img src="uploads/<?php echo $patientpic?>" width="30px" height="30px"/></span> <?php echo $patientname?></td>
                                            <td><?php echo $patientcontact?></td>

                                            <td><?php echo $bookperiod?>
                                                </br>
                                            </td>
                                            <td><?php echo $bookdate?></td>
                                            <td><?php echo $bookhour?></td>
                                            <td><?php echo $total_days*$bookhour?></td>
                                            <td>RM <?php echo $value['book_charge']?></td>
                                            <td>
                                                <?=$bt?>
                                            </td>
                                            <td><a href="booking-details/<?php echo $bookingid?>" class="btn bg-pink">View</a></td>

                                        </tr>
                                            <?php
                                        }

                                        ?>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div aria-labelledby="tabs-icons-text-2-tab" class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel">
                             <div class="table-responsive mb-3">
                                                               <table class="table table-bordered new_table w-100">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th width="3%">Booking ID</th>
                                            <th>Patient Name</th>
                                            <th width="3%">Contact No</th>
                                            <th>Booking Period</th>
                                            <th>Booking Date</th>
                                            <th width="3%">Hours/day</th>
                                            <th width="3%">Total Hours</th>
                                            <th>Total Charge</th>
                                            <th>View Booking</th>
                                        </tr>
                                    </thead>
                                    <tbody class="col-lg-12 p-0">
                                        <?php
                                        $i =0;
                                        foreach ($book_details_active as $key => $value)
                                         {
                                            $id=$value['book_id'];
                                            $bookingid=$value['patient_bookid'];
                                            $patientname=$value['first_name'].' '.$value['last_name'];
                                            $patientcontact=$value['contact_no'];
                                            $patientpic=$value['profile_pic'];
                                            $bookperiod=$value['fromtime']." To <br>".$value['totime'];
                                            $bookhour=$value['time_in_hours'];
                                            $bookdate=$value['adddate'];
                                            $notes=$value['notes'];
                                            $book_status=$value['book_status'];
                                            $book_charge=$value['book_charge'];

                                            $st = strtotime($value['fromtime']);
                                            $end = strtotime($value['totime']);
                                            $datediff = ($end - $st);

                                            $total_days = round($datediff / (60 * 60 * 24))+1;

                                            $addon_price = $value['a1price']+$value['a2price']+$value['a3price'];

                                            $total_hours = ($value['time_in_hours'])*$total_days;

                                            $total_price = ($value['book_charge']*$total_hours)+$addon_price;
                                            ?>
                                        <tr>
                                            <td><a href="booking-details"><?=$bookingid?></a></td>
                                            <td><span><img src="uploads/<?php echo $patientpic?>" width="30px" height="30px"/></span> <?php echo $patientname?></td>
                                            <td><?php echo $patientcontact?></td>

                                            <td><?php echo $bookperiod?></br>
                                            </td>
                                            <td><?php echo $bookdate?></td>
                                            <td><?php echo $bookhour?></td>
                                            <td><?php echo $total_days*$bookhour?></td>
                                            <td>RM <?php echo $value['book_charge']?></td>
                                            <td><a href="booking-details/<?php echo $bookingid?>" class="btn bg-pink">View</a>
                                            </td>
                                        </tr>
                                            <?php
                                        }

                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
                                                               <table class="table table-bordered new_table w-100">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th width="3%">Booking ID</th>
                                            <th>Patient Name</th>
                                            <th width="3%">Contact No</th>
                                            <th>Booking Period</th>
                                            <th>Booking Date</th>
                                            <th width="3%">Hours/day</th>
                                            <th width="3%">Total Hours</th>
                                            <th>Charge</th>
                                            <th>View Booking</th>
                                        </tr>
                                    </thead>
                                    <tbody class="col-lg-12 p-0">
                                        <?php
                                        foreach ($book_details_complete as $key => $value)
                                         {
                                            $id=$value['book_id'];
                                            $bookingid=$value['patient_bookid'];
                                            $patientname=$value['first_name'].' '.$value['last_name'];
                                            $patientcontact=$value['contact_no'];
                                            $patientpic=$value['profile_pic'];
                                            $bookperiod=$value['fromtime']." To ".$value['totime'];
                                            $bookhour=$value['time_in_hours'];
                                            $bookdate=$value['adddate'];
                                            $notes=$value['notes'];
                                            $book_status=$value['book_status'];
                                            $book_charge=$value['book_charge'];

                                            $st = strtotime($value['fromtime']);
                                            $end = strtotime($value['totime']);
                                            $datediff = ($end - $st);

                                            $total_days = round($datediff / (60 * 60 * 24))+1;

                                            $addon_price = $value['a1price']+$value['a2price']+$value['a3price'];

                                            $total_hours = ($value['time_in_hours'])*$total_days;

                                            $total_price = ($value['book_charge']*$total_hours)+$addon_price;
                                            ?>
                                            <tr>
                                            <td><a href="booking-details"><?=$bookingid?></a></td>
                                            <td><a ><?=$id?></a></td>
                                            <td><span><img src="uploads/<?php echo $patientpic?>" width="30px" height="30px"/></span> <?php echo $patientname?></td>
                                            <td><?php echo $patientcontact?></td>

                                            <td><?php echo $bookperiod?>
                                                </br>
                                            </td>
                                            <td><?php echo $bookdate?></td>
                                            <td><?php echo $bookhour?></td>
                                            <td><?php echo $total_days*$bookhour?></td>
                                            <td>RM <?php echo $value['book_charge']?></td>
                                            <td><a href="booking-details/<?php echo $bookingid?>" class="btn bg-pink">View</a>
                                            </td>

                                        </tr>
                                            <?php
                                        }

                                        ?>

                                    </tbody>
                                </table>

                        </div>
                        <div class="tab-pane fade" id="tabs-icons-text-4" role="tabpanel" aria-labelledby="tabs-icons-text-4-tab">
                                                             <table class="table table-bordered new_table w-100">
                                    <thead>
                                        <tr>
                                           <th>ID</th>
                                           <th width="3%">Booking ID</th>
                                            <th>Patient Name</th>
                                            <th width="3%">Contact No</th>
                                            <th>Booking Period</th>
                                            <th>Booking Date</th>
                                            <th width="3%">Hours/day</th>
                                            <th width="3%">Total Hours</th>
                                            <th>Charge</th>
                                            <th>View Booking</th>
                                        </tr>
                                    </thead>
                                    <tbody class="col-lg-12 p-0">
                                        <?php
                                        $i = 0;
                                        foreach ($book_details_cancel as $key => $value)
                                         {
                                            $bookingid=$value['patient_bookid'];
                                            $id=$value['book_id'];
                                            $patientname=$value['first_name'].' '.$value['last_name'];
                                            $patientcontact=$value['contact_no'];
                                            $patientpic=$value['profile_pic'];
                                            $bookperiod=$value['fromtime']." To ".$value['totime'];
                                            $bookhour=$value['time_in_hours'];
                                            $bookdate=$value['adddate'];
                                            $notes=$value['notes'];
                                            $book_status=$value['book_status'];
                                            $book_charge=$value['book_charge'];

                                            $st = strtotime($value['fromtime']);
                                            $end = strtotime($value['totime']);
                                            $datediff = ($end - $st);

                                            $total_days = round($datediff / (60 * 60 * 24))+1;

                                            $addon_price = $value['a1price']+$value['a2price']+$value['a3price'];

                                            $total_hours = ($value['time_in_hours'])*$total_days;

                                            $total_price = ($value['book_charge']*$total_hours)+$addon_price;
                                            ?>
                                        <tr>

                                            <td><?=$id?></td>
                                            <td><span><img src="uploads/<?php echo $patientpic?>" width="30px" height="30px"/></span> <?php echo $patientname?></td>
                                            <td><?php echo $patientcontact?></td>

                                            <td><?php echo $bookperiod?>
                                                </br>
                                            </td>
                                            <td><?php echo $bookdate?></td>
                                            <td><?php echo $bookhour?></td>
                                            <td><?php echo $total_days*$bookhour?></td>
                                            <td>RM <?php echo $value['book_charge']?></td>
                                            <td><a href="booking-details/<?php echo $bookingid?>" class="btn bg-pink">View</a>
                                            </td>

                                        </tr>
                                            <?php
                                        }

                                        ?>

                                    </tbody>
                                </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>



            <!-- <div class="col-md-12">
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a large text here ..."></textarea>
            </div> -->

    </form>
</fieldset>
<!-- table-wrapper -->
</div>
<!-- section-wrapper -->
</div>
</div>

</div><!--End side app-->

<!-- Right-sidebar-->
<?php $this->load->view('backend/right_sidebar');?>
<!-- End Rightsidebar-->

<?php $this->load->view('backend/footer');?>
</div>
<!-- End app-content-->
</div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>
  <script type="text/javascript">
    let dataTable = $(".new_table").dataTable(
      {  "bLengthChange" : false,
           "responsive": true,
          "oLanguage":
        {
            "sSearch": ""
        }
      });
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust()
           .responsive.recalc();
        });

</script>

</body>
</html>
