<?php $this->load->view('backend/head_link');?>

<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
    <img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->
<style>
td{
    border-bottom:1px solid #ccc;
    /* padding-left:10px; */
    padding: 8px 20px 0 15px;
}
td:last-child{
    padding: 0 ;
}
td:last-child button{
    line-height: 1;
}
.input-group{
    /* display:inline; */
}
@media only screen and (max-width: 600px) {
  #day, #timerange{
      width: 0;
  }
}
</style>
<div class="page">
    <div class="page-main">
        <!--app-header-->
        <?php $this->load->view('backend/header');?>
        <!-- app-content-->
        <div class="container content-patient">
            <div class="side-app">
                <!-- page-header -->
                <!-- <div class="page-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
                    </ol>
                    <div class="ml-auto">
                        <div class="input-group">
                            <a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
                                <span>
                                    <i class="fa fa-calendar"></i> Events Settings
                                </span>
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                                <span>
                                    <i class="fa fa-star"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div> -->
                <!-- End page-header -->

                <div class="row">
                    <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-10">
                            <div class="card-title"><?=$form_title?></div>
                            </div>
                            <div class="col-md-2">
                            <a href="nurse-shedule-list" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
                                <span> Shedule List
                                </span>
                            </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="" method="post">
                            <div class="row">
                            <?php if($this->session->flashdata('msg')){ ?>
                                <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <div class="alert-message">
                                        <span><?=$this->session->flashdata('msg');?></span>
                                    </div>
                                </div>
                            <?php } ?> 
                            <div id="alert_pass"></div>
                            <div class="col-md-12">
                                <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
                            </div>
                            </div>
                            <form action="" method="post">
                            
                            
                            <fieldset class="add_shedule">

                            <table style="table-layout:fixed" cellpadding=8>
                                <tbody>
                                    <thead>
                                        <th id="day" width="50%">Day</th>
                                        <th id="timerange" width="40%">Time-Range</th>
                                        <th>Add</th>
                                    </thead>
                                <tr>
                                    <td colspan=1>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="" value="">
                                                    <h4 class="custom-control-label">Sunday</h4>
                                                </label>
                                            </div>
                                        </div>    
                                        </div>
                                        </td>
                                        <td>
                                        <div class="form-group ">
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <input class="form-control" data-time="range"  placeholder="Start-time" type="text">
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                        <div class="input-group">
                                            <input class="form-control" data-time="range" placeholder="End-time" type="text">
                                        </div>
                                        </div>
                                        </div>
                                        <div class="custom_time_range">

                                        </div>
                                        
                                        </div>
                                    </td>
                                    <td>
                                        <span class="addCustomRange btn btn-primary">+</span>
                                        <div class="customBtnDelete"></div>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan=1>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="" value="">
                                                    <h4 class="custom-control-label">Monday</h4>
                                                </label>
                                            </div>
                                        </div>    
                                        </div>
                                        </td>
                                        <td>
                                        <div class="form-group ">
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <input class="form-control" data-time="range"  placeholder="Start-time" type="text">
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                        <div class="input-group">
                                            <input class="form-control" data-time="range" placeholder="End-time" type="text">
                                        </div>
                                        </div>
                                        </div>
                                        <div class="custom_time_range">
                                        </div>
                                        
                                        </div>
                                    </td>
                                    <td>
                                    <span class="addCustomRange btn btn-primary">+</span>
                                    <div class="customBtnDelete"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=1>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="" value="">
                                                    <h4 class="custom-control-label">Tuesday</h4>
                                                </label>
                                            </div>
                                        </div>    
                                        </div>
                                        </td>
                                        <td>
                                        <div class="form-group ">
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <input class="form-control" data-time="range"  placeholder="Start-time" type="text">
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                        <div class="input-group">
                                            <input class="form-control" data-time="range" placeholder="End-time" type="text">
                                        </div>
                                        </div>
                                        </div>
                                        <div class="custom_time_range">
                                        </div>
                                        
                                        </div>
                                    </td>
                                    <td>
                                    <span class="addCustomRange btn btn-primary">+</span>
                                    <div class="customBtnDelete"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=1>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="" value="">
                                                    <h4 class="custom-control-label">Wednesday</h4>
                                                </label>
                                            </div>
                                        </div>    
                                        </div>
                                        </td>
                                        <td>
                                        <div class="form-group ">
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <input class="form-control" data-time="range"  placeholder="Start-time" type="text">
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                        <div class="input-group">
                                            <input class="form-control" data-time="range" placeholder="End-time" type="text">
                                        </div>
                                        </div>
                                        </div>
                                        <div class="custom_time_range">
                                        </div>
                                        
                                        </div>
                                    </td>
                                    <td>
                                    <span class="addCustomRange btn btn-primary">+</span>
                                    <div class="customBtnDelete"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=1>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="" value="">
                                                    <h4 class="custom-control-label">Thursday</h4>
                                                </label>
                                            </div>
                                        </div>    
                                        </div>
                                        </td>
                                        <td>
                                        <div class="form-group ">
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <input class="form-control" data-time="range"  placeholder="Start-time" type="text">
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                        <div class="input-group">
                                            <input class="form-control" data-time="range" placeholder="End-time" type="text">
                                        </div>
                                        </div>
                                        </div>
                                        <div class="custom_time_range">
                                        </div>
                                        
                                        </div>
                                    </td>
                                    <td>
                                    <span class="addCustomRange btn btn-primary">+</span>
                                    <div class="customBtnDelete"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=1>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="" value="">
                                                    <h4 class="custom-control-label">Friday</h4>
                                                </label>
                                            </div>
                                        </div>    
                                        </div>
                                        </td>
                                        <td>
                                        <div class="form-group ">
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <input class="form-control" data-time="range"  placeholder="Start-time" type="text">
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                        <div class="input-group">
                                            <input class="form-control" data-time="range" placeholder="End-time" type="text">
                                        </div>
                                        </div>
                                        </div>
                                        <div class="custom_time_range">
                                        </div>
                                        
                                        </div>
                                    </td>
                                    <td>
                                    <span class="addCustomRange btn btn-primary">+</span>
                                    <div class="customBtnDelete"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=1>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="" value="">
                                                    <h4 class="custom-control-label">Saturday</h4>
                                                </label>
                                            </div>
                                        </div>    
                                        </div>
                                        </td>
                                        <td>
                                        <div class="form-group ">
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <input class="form-control" data-time="range"  placeholder="Start-time" type="text">
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                        <div class="input-group">
                                            <input class="form-control" data-time="range" placeholder="End-time" type="text">
                                        </div>
                                        </div>
                                        </div>
                                        <div class="custom_time_range"></div>
                                        </div>
                                    </td>
                                    <td>
                                    <span class="addCustomRange btn btn-primary">+</span>
                                    <div class="customBtnDelete"></div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                           
                            </fieldset>

                                    

                            <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                <div class="">
                                    <button class="btn btn-primary" type="submit" name="save" id="">Submit</button>
                                </div>
                            </div>
                                    
                                    <!-- <div class="col-md-12">
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a large text here ..."></textarea>
                                    </div> -->
                            
                            </form>
                       
                        <!-- table-wrapper -->
                    </div>
                    <!-- section-wrapper -->
                    </div>
                </div>

            </div><!--End side app-->

            <!-- Right-sidebar-->
            <?php $this->load->view('backend/right_sidebar');?>
            <!-- End Rightsidebar-->

            <?php $this->load->view('backend/footer');?>
        </div>
        <!-- End app-content-->
    </div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>

<script>
 $("[data-time='range']").timepicker();
 
 const addCustomRange = document.querySelectorAll('.addCustomRange');
 const custom_time_range = document.querySelectorAll('.custom_time_range');
 const customBtnDelete = document.querySelectorAll('.customBtnDelete');
 const customTime = document.querySelectorAll("[data-time='range']");
 const dltBtn = `<span class="deletebtn btn btn-danger pt-2">x</span>`;
    

 for (let i = 0; i < addCustomRange.length; i++) {
    addCustomRange[i].onclick = () => {
        custom_time_range[i].innerHTML = `<div class="row pt-2">
                    <div class="col-sm-6">
                        <div class="input-group">
                            <input class="form-control" data-time="range"  placeholder="Start-time" name="start_time[]" type="text">
                        </div>
                    </div>  
                    <div class="col-md-6">
                        <div class="input-group">
                            <input class="form-control" data-time="range" placeholder="End-time" name="end_time[]" type="text">
                        </div>
                    </div>
                </div>
                `; 
        customBtnDelete[i].innerHTML = dltBtn;
        $("[data-time='range']").timepicker();
    }
     
 }
 for (let i = 0; i < customBtnDelete.length; i++) {
    customBtnDelete[i].onclick = () => {
        custom_time_range[i].innerHTML = ''; 
        customBtnDelete[i].innerHTML = '';
        $("[data-time='range']").timepicker();
    }  
 }

</script>


</body>
</html>