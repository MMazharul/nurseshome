<?php $this->load->view('backend/head_link');?>

    <body class="app sidebar-mini rtl">

        <!--Global-Loader-->
        <!-- <div id="global-loader">
            <img src="back_assets/images/icons/loader.svg" alt="loader">
        </div> -->

        <div class="page">
            <div class="page-main">
                <!--app-header-->


       <?php $this->load->view('backend/header');?>



        <!-- app-content-->
                <div class="container content-area">
                    <div class="side-app">

<style type="text/css">
            .c-concierge-card__pulse {
                background: #01a400;
                margin-right: 10px;
            }

            .c-concierge-card__pulse, .u-pulse {
                margin: 4px 20px 0 10px;
                    margin-right: 8px;
                width: 10px;
                height: 10px;
                border-radius: 50%;
                background: #14bef0;
                cursor: pointer;
                -webkit-box-shadow: 0 0 0 rgba(40,190,240,.4);
                box-shadow: 0 0 0 rgba(40,190,240,.4);
                -webkit-animation: pulse 1.2s infinite;
                animation: pulse 1.2s infinite;
                display: inline-block;
            }

                .m-right{
                    margin-right: 7px;
                }
                .u-green-text {
                    color: #01a400;
                }

                .modal_mou{
                    text-decoration: underline!important;
                    font-size: 11px!important;
                }


                /*search box css start here*/
                .search-sec{
                    padding: 2rem;
                }
                .search-slt{
                    display: block;
                    width: 100%;
                    font-size: 11px!important;
                    line-height: 1.5;
                    color: #55595c;
                    background-color: #fff;
                    background-image: none;
                    border: 1px solid #f3ecec;
                    height: calc(3rem + 2px) !important;
                    border-radius:0;
                }
                .wrn-btn{
                    width: 100%;
                    font-size: 16px;
                    font-weight: 400;
                    text-transform: capitalize;
                    height: calc(3rem + 2px) !important;
                    border-radius:0;
                }
                @media (min-width: 992px){
                    .search-sec{
                        position: relative;
                        top: -114px;
                        background: rgba(26, 70, 104, 0.51);
                    }
                }

                @media (max-width: 992px){
                    .search-sec{
                        background: #1A4668;
                    }
                }
    </style>

    <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">

                     <?php if($this->session->flashdata('msg')){ ?>
                        <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <div class="alert-message">
                                <span><?=$this->session->flashdata('msg');?></span>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card Relatedpost nested-media">
                        <!-- <div class="card-header">
                            <h4 class="card-title">Search for Care</h4>
                        </div> -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <div class="media media-lg mt-0">
                                        <div class="media-body">
                                            <h2 class="mt-0">Booking ID : <?=!empty($booking_details[0]['booking_random_code']) ? $booking_details[0]['booking_random_code'] : '';?>
                                            </h2>
                                            <div style="margin-top: -10px;margin-bottom: 13px;"><span class="u-green-text"></span><span class="u-green-text" data-reactid="211">

                                            Nurse Name:</span><span>

                                            <?php
                                            if(!empty($nurse_details[0]['first_name'])){
                                                echo ucwords($nurse_details[0]['first_name']);
                                            }
                                            echo " ";
                                            if (!empty($nurse_details[0]['last_name'])) {
                                                echo ucwords($nurse_details[0]['last_name']);
                                            }?>
                                            <br>
                                            <span class="u-green-text" data-reactid="211">

                                            Patient Name:</span>
                                            <?php
                                            if(!empty($booking_details[0]['first_name'])){
                                                echo ucwords($booking_details[0]['first_name']);
                                            }
                                            echo " ";
                                            if (!empty($booking_details[0]['last_name'])) {
                                                echo ucwords($booking_details[0]['last_name']);
                                            }?>
                                             | <?=$book_day?></span></div>



                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <div class="media media-lg mt-0">
                                        <div class="media-body">
                                                <h3 class="text-right"><span class="u-green-text">
                                                 <?=!empty($booking_details[0]['book_charge']) ? ($booking_details[0]['book_charge'] == 0 ? '25.00' : $booking_details[0]['book_charge']) : ''?>
                                                </span></h3>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <hr class="m-0 mb-4">

                            <p><?=$booking_details[0]['notes']?></p>

                            <br>


                            <table class="table-bordered" cellpadding="8">
                                <thead class="bg-light text-dark">
                                    <tr>
                                        <th>Time (Hours)</th>
                                        <th>Time (Total Hours)</th>
                                        <th>Charge</th>
                                        <th>Days</th>
                                        <th>Duration (Days)</th>
                                        <th class="text-right">Amount (RM)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-right">
                                        <td><?=!empty($booking_details[0]['time_in_hours']) ? $booking_details[0]['time_in_hours'] : ''?></td>
                                        <td><?=!empty($booking_details[0]['time_in_hours']) ? ($booking_details[0]['time_in_hours'] * $datediff) : ''?></td>
                                        <td>RM <?=!empty($nurse_details[0]['charge']) ? $nurse_details[0]['charge'] : '25'?></td>
                                        <td><?=!empty($from_date) ? $from_date : ''?> - <?=!empty($to_date) ? $to_date : ''?></td>
                                        <td><?=!empty($datediff) ? $datediff : ''?></td>
                                        <td>
                                            <?=!empty($nurse_details[0]['charge']) ? $nurse_details[0]['charge'] * $datediff * $booking_details[0]['time_in_hours'] : '25'?>
                                        </td>
                                    </tr>
                                    <?php if ($booking_details[0]['a1price'] != '') :?>

                                            <tr class="text-right">
                                                <td colspan="5"><?=$booking_details[0]['a1title']?></td>
                                                <td colspan="1"> <?=$booking_details[0]['a1price']?></td>
                                            </tr>

                                        <?php endif;?>
                                        <?php if ($booking_details[0]['a2price'] != '') :?>

                                            <tr class="text-right">
                                                <td colspan="5"><?=$booking_details[0]['a2title']?></td>
                                                <td colspan="1"><?=$booking_details[0]['a2price']?></td>
                                            </tr>

                                        <?php endif;?>
                                        <?php if ($booking_details[0]['a2price'] != '') :?>

                                            <tr class="text-right">
                                                <td colspan="5"><?=$booking_details[0]['a3title']?></td>
                                                <td colspan="1"> <?=$booking_details[0]['a3price']?></td>
                                            </tr>

                                        <?php endif;?>
                                        <tr class="text-right">
                                            <td colspan="5"><strong>Total </strong></td>
                                            <td> <?=$total_charge?>

                                            </td>
                                        </tr>
                                        <?php if(count($transaction_log_history)>0) :
                                          foreach ($transaction_log_history as $key => $value) {
                                          ?>

                                          <tr class="text-right">
                                              <td colspan="4">Payment Date  :  <?=date("d-m-Y", strtotime($value['payment_date']))?> </td>

                                              <td colspan="1">Paid Amount </td>
                                              <td colspan="1"><?=$value['paid_amount']?></td>
                                          </tr>
                                        <?php }?>
                                        <?php endif;?>
                                        <?php
                                            if (count($transaction) > 0) : ?>
                                            <tr class="text-right">
                                                <td colspan="5"><strong>Total Paid</td>
                                                <td colspan="6"><strong> <?=$transaction[0]['paid']?></strong> </td>

                                            </tr>
                                            <tr class="text-right">
                                              <td colspan="5"><strong>Due</strong></td>
                                              <td colspan="6"><?=$transaction[0]['due']?></td>
                                            <tr>
                                            <?php endif; ?>
                                        </tbody>
                            </table>
                            <br>
                            <p class="text-right"><strong>Total : <?=$total_charge?></strong></p>
                            <div class="row">

                                <input type="hidden" name="total_amount" id="total_charge" value="<?=$total_charge?>">

                                <div class="col-md-6 text-left">
                                   <?php
                                   if(isset($booking_details[0]['payment_method']) && $booking_details[0]['payment_method'] == 0)
                                   {

                                        if(isset($transaction[0]['due']) && $transaction[0]['due'] != 0) { ?>


                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#due_payment" class="btn btn-pink">
                                        <i class="fa fa-plus"></i>
                                            Add Payment
                                        </a>
                                      <?php } else if(count($transaction) == 0){?>

                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#payment" class="btn btn-pink">
                                        <i class="fa fa-plus"></i>
                                            Add Payment
                                        </a>
                                      <?php }  }?>
                                </div>


                            </div>

                        </div>
                    </div>
                    </div>
                    </div>

                    <?php if ($booking_details[0]['book_status'] >= 3) : ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <h4 class="card-title">Search for Care</h4>
                        </div> -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


                                        <div class="text-center d-block">
                                            <i class="fa fa-rocket text-info" style="font-size: 40px"></i>
                                            <!-- <br> -->
                                            <!-- <span style="font-size: 20px; text-transform: uppercase;"><strong>Service Started</strong></span> -->
                                            <br>
                                            <span>
                                            Assessment Done By <strong><?php
                                            if(!empty($nurse_details[0]['first_name'])){
                                                echo ucwords($nurse_details[0]['first_name']);
                                            }
                                            echo " ";
                                            if (!empty($nurse_details[0]['last_name'])) {
                                                echo ucwords($nurse_details[0]['last_name']);
                                            }?></strong>
                                            </span>
                                            <hr>
                                        </div>

                        <?php

                        if ($row_count > 0) { ?>

                        <fieldset class="biodata">
                        <!-- <h4>Nutrition</h4> -->
                            <table class="table table-bordered">
                              <tr>
                                <th><strong>Eating Disorder</strong></th>
                                <th><?php
                                  if (!empty($assessment_details[0]['eating_disorder'])) {
                                    if ($assessment_details[0]['eating_disorder'] == 1) {
                                       echo "Nil";
                                    }elseif ($assessment_details[0]['eating_disorder'] == 2) {
                                      echo "Malnourished";
                                    }
                                    elseif ($assessment_details[0]['eating_disorder'] == 3) {
                                      echo "Obese";
                                    }
                                  };
                                  ?></th>
                              </tr>

                              <tr>
                                <th><strong>Diet Type</strong></th>
                                <th><?php
                                  if (!empty($assessment_details[0]['diet_type'])) {
                                    if ($assessment_details[0]['diet_type'] == 1) {
                                       echo "Regular";
                                    }elseif ($assessment_details[0]['diet_type'] == 2) {
                                      echo "Theraputic";
                                    }
                                    elseif ($assessment_details[0]['diet_type'] == 3) {
                                      echo "Supplement (Endsure etc)";
                                    }
                                    elseif ($assessment_details[0]['diet_type'] == 4) {
                                      echo "Parental (TPN)";
                                    }
                                  };
                                  ?></th>
                              </tr>

                              <tr>
                                <th><strong>Musculosketel</strong></th>
                                <th><?php
                                  if (!empty($assessment_details[0]['musculosketel'])) {
                                    if ($assessment_details[0]['musculosketel'] == 1) {
                                       echo "Normal";
                                    }elseif ($assessment_details[0]['musculosketel'] == 2) {
                                      echo "Paralysis-Free hand notes";
                                    }
                                    elseif ($assessment_details[0]['musculosketel'] == 3) {
                                      echo "Joint Swelling-Free hand notes";
                                    }
                                    elseif ($assessment_details[0]['musculosketel'] == 4) {
                                      echo "Pain-Free hand notes";
                                    }
                                    elseif ($assessment_details[0]['musculosketel'] == 5) {
                                      echo "Others-Free hand notes";
                                    }
                                  };
                                  ?></th>
                              </tr>

                               <tr>
                                <th><strong>Genitallia</strong></th>
                                <th><?php
                                  if (!empty($assessment_details[0]['genitallia'])) {
                                    if ($assessment_details[0]['genitallia'] == 1) {
                                       echo "Normal";
                                    }elseif ($assessment_details[0]['genitallia'] == 2) {
                                      echo "Others-FHN";
                                    }
                                  };
                                  ?></th>
                              </tr>

                               <tr>
                                <th><strong>Vascular Access</strong></th>
                                <th><?php
                                  if (!empty($assessment_details[0]['vascular_access'])) {
                                    if ($assessment_details[0]['vascular_access'] == 1) {
                                       echo "Normal";
                                    }elseif ($assessment_details[0]['vascular_access'] == 2) {
                                      echo "Others-FHN";
                                    }
                                  };
                                  ?></th>
                              </tr>
                               <tr>
                                <th><strong>Fall Risk</strong></th>
                                <th><?php
                                  if (!empty($assessment_details[0]['fall_risk'])) {
                                    if ($assessment_details[0]['fall_risk'] == 1) {
                                       echo "No Risk";
                                    }elseif ($assessment_details[0]['fall_risk'] == 2) {
                                      echo "At Risk";
                                    }elseif ($assessment_details[0]['fall_risk'] == 3) {
                                      echo "High Risk";
                                    }
                                  };
                                  ?></th>
                              </tr>

                               <tr>
                                <th><strong>Fall Risk</strong></th>
                                <th><?=!empty($assessment_details[0]['fall_history']) ? $assessment_details[0]['fall_history']  : ''?></th>
                              </tr>


                               <tr>
                                <th><strong>Walking Device</strong></th>
                                <th><?=!empty($assessment_details[0]['walking_device']) ? $assessment_details[0]['walking_device'] : ''?></th>
                              </tr>

                               <tr>
                                <th><strong>Urine Colour</strong></th>
                                <th><?php
                                  if (!empty($assessment_details[0]['urine_colour'])) {
                                    if ($assessment_details[0]['urine_colour'] == 1) {
                                       echo "Clear";
                                    }elseif ($assessment_details[0]['urine_colour'] == '2_1') {
                                      echo "Cloudy";
                                      if (!empty($assessment_details[0]['cloudy_sub'])) {
                                          if ($assessment_details[0]['cloudy_sub'] ==1) {
                                            echo "<small>\n\r(Dark)</small>";
                                          }elseif ($assessment_details[0]['cloudy_sub'] ==2) {
                                            echo "<small>\n\r(Hemanturia)</small>";

                                          }
                                      }
                                    }elseif ($assessment_details[0]['urine_colour'] == 3) {
                                      echo "Others (Endsure etc)";
                                    }elseif ($assessment_details[0]['urine_colour'] == 4) {
                                      echo "Normal Voiding";
                                    }
                                  };
                                  ?></th>
                              </tr>

                              <tr>
                                <th><strong>Incontinent/Dysuria/Catheter</strong></th>
                                <th><?php
                                  if (!empty($assessment_details[0]['incontinent_dysuria_catheter'])) {
                                    if ($assessment_details[0]['incontinent_dysuria_catheter'] == 1) {
                                       echo "FHN";
                                    }elseif ($assessment_details[0]['incontinent_dysuria_catheter'] == 2) {
                                      echo "Dialysis";
                                    }elseif ($assessment_details[0]['incontinent_dysuria_catheter'] == 3) {
                                      echo "Others-FHN";
                                    }
                                  };
                                  ?></th>
                              </tr>

                              <tr>
                                <th><strong>Home Assessment</strong></th>
                                <th><?=!empty($assessment_details[0]['home_assessment'])? trim($assessment_details[0]['home_assessment']): ''?></th>
                              </tr>
                            </table>
                            <div class="row">
                            <div class="col-md-12">



                                </div>

                            </div>
                     <!--        <h4>Genitourinary</h4> -->


                        </fieldset>


                        <?php } ?>


                            </div>
                            </div>



                            <?php if($booking_details[0]['book_status'] == 3) : ?>
                             <div class="text-center d-block mt-5">
                                  <i class="fa fa-check-square-o text-info" style="font-size: 40px"></i>
                                  <br>
                                  <span style="font-size: 20px; text-transform: uppercase;">
                                      <strong>service has been marked as completed by Nurse</strong>

                                    </span>

                                  <br>
                              </div>
                           <hr>
                          <?php elseif($booking_details[0]['book_status'] == 5):?>

                           <!--  <div class="text-center d-block mt-5">
                                <i class="fa fa-comment-o text-info" style="font-size: 30px"></i>
                                <br>
                                <span style="font-size: 20px; text-transform: uppercase;"><strong>Review for Nurse</strong></span>
                                <br>
                            </div> -->
                            <hr>

                            <h5>Review By Nurse</h5>

                            <?php if(isset($review_by_nurse) && count($review_by_nurse) > 0):?>
                            <div class="row">
                                <div class="col-md-2">
                                    <img class="rounded-circle" width=70% src="uploads/<?=$review_by_nurse[0]['profile_pic']?>" alt="">
                                </div>
                                <div class="col-md-10 pt-4">
                                    <h4><?=$review_by_nurse[0]['first_name']." ".$review_by_nurse[0]['last_name']?>
                                        <span>
                                            <i class="fa fa-star text-warning"></i> <strong class="text-warning"><?=$review_by_nurse[0]['rating']?></strong>
                                        </span>
                                    </h4>
                                    <p style="font-size: 14px"><?=$review_by_nurse[0]['comment']?></p>
                                </div>
                            </div>
                            <?php else: ?>

                            <div class="row">
                                <div class="col-md-2">
                                    <img class="rounded-circle" width=70% src="uploads/<?=$nurse_details[0]['profile_pic']?>" alt="">
                                </div>
                                <div class="col-md-10 pt-4">
                                    <h4><?=$nurse_details[0]['first_name']." ".$nurse_details[0]['last_name']?>
                                    </h4>

                                    <p class="text-danger">-Hasn't Reviewed Yet-</p>

                                </div>
                            </div>



                            <?php endif; ?>
                            <hr>
                            <h5>Review By Patient</h5>

                             <?php if(isset($review_by_patient) && count($review_by_patient) > 0):?>
                            <div class="row">
                                <div class="col-md-2">
                                    <img class="rounded-circle" width=70% src="uploads/<?=$review_by_patient[0]['profile_pic']?>" alt="">
                                </div>
                                <div class="col-md-10 pt-4">
                                    <h4><?=$review_by_patient[0]['first_name']." ".$review_by_patient[0]['last_name']?>
                                        <span>
                                            <i class="fa fa-star text-warning"></i> <strong class="text-warning"><?=$review_by_patient[0]['rating']?></strong>
                                        </span>
                                    </h4>
                                    <p style="font-size: 14px"><?=$review_by_patient[0]['comment']?></p>
                                </div>
                            </div>
                            <?php else: ?>

                            <div class="row">
                                <div class="col-md-2">
                                    <img class="rounded-circle" width=70% src="uploads/<?=$booking_details[0]['profile_pic']?>" alt="">
                                </div>
                                <div class="col-md-10 pt-4">
                                    <h4><?=$booking_details[0]['first_name']." ".$booking_details[0]['last_name']?>
                                    </h4>

                                    <p class="text-danger">-Hasn't Reviewed Yet-</p>

                                </div>
                            </div>



                            <?php endif; ?>

                            <?php endif; ?>

                        </div>
                    </div>
                    </div>

                    </div>


                    <?php endif; ?>
                </div>
                <div class="col-md-3">

                    <div class="row">

                    <?php
                        if (isset($nurse_care_note_all) && count($nurse_care_note_all)>0) : ?>
                            <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                   <h5 class="card-title"> Previous notes </h5>
                                </div>

                            <div class="card-body">

                                 <table>
                                    <thead>
                                        <tr>
                                            <td>Sl.</td>
                                            <td>Date</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach($nurse_care_note_all as $values):?>
                                        <tr>
                                            <td><?=$i ?></td>
                                            <td><a class="fetch_nurse_notes" href="javascript:void(0)" data-id="<?=$values['id']?>" data-toggle="modal" data-target="#exampleModal2">
                                                    <?php
                                                  $date=date_create($values['date']);
                                                  echo date_format($date,"d-M-Y");?>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php $i++; endforeach; ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                      <?php   endif; ?>



                    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Summary</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-label mb-2 text-center" id="note_date"> </div>
                                        <div class="form-label d-block row">



                                        <div class="col-12">
                                             <h5> Completed Care: </h5>
                                           <table class="table table-bordered">
                                           <thead>
                                               <tr>
                                                    <td>Title</td>
                                                    <td>Note</td>
                                               </tr>
                                           </thead>
                                               <tbody id="care_details">
                                               </tbody>
                                           </table>

                                        </div>

                                        <div class="col-12">
                                        <h5>Basic Vital Signs</h5>
                                        <table class="table table-bordered">
                                           <tbody id="note_summary">

                                           </tbody>
                                          </table>
                                        </div>

                                        </div>
                                </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    </div>
                </div>

    </div>

                </div><!--End side app-->
                <div class="modal fade" id="payment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                         <div class="modal-dialog modal-lg" role="document">
                         <div class="modal-content">
                             <div class="modal-header">
                                 <h5 class="modal-title" id="">Payment</h5>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                     <span aria-hidden="true">×</span>
                                 </button>
                             </div>
                             <div class="modal-body">
                                 <form action="nurse/cash_transaction" method="post">
                                     <input type="hidden" value="<?=$booking_details[0]['patient_bookid']?>" name="booking_id">
                                     <input type="hidden" value="<?=$booking_details[0]['patient_id']?>" name="patient_id">
                                     <input type="hidden" value="<?=$booking_details[0]['nurse_id']?>" name="nurse_id">

                                     <input type="hidden" name="amount" id="" value="<?=$total_charge?>">

                                     <div class="row mb-4">
                                     <label class="custom-control col-md-4 text-right">
                                         Amount
                                     </label>
                                     <div class="col-md-8">
                                         <input min="1" id="check_amount" type="number" max="<?=$total_charge?>" class="form-control" name="paid" value="<?=$total_charge?>">
                                     </div>
                                     </div>
                                     <div class="row mb-4">
                                         <label class="custom-control col-md-4 text-right">
                                         Due
                                     </label>
                                     <div class="col-md-8">
                                         <!-- <span id="due"></span> -->
                                         <input type="text" readonly name="due" id="due" class="form-control" value="0">
                                     </div>
                                     </div>
                                     <div class="modal-footer">
                                         <button id="submit_price" type="submit" class="btn bg-primary">Add</button>
                                     </div>
                                 </form>
                             </div>

                             </div>

                         </div>
                         </div>

                     <div class="modal fade" id="due_payment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                         <div class="modal-dialog modal-lg" role="document">
                         <div class="modal-content">
                             <div class="modal-header">
                                 <h5 class="modal-title" id="">Due Payment</h5>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                     <span aria-hidden="true">×</span>
                                 </button>
                             </div>
                             <div class="modal-body">
                                 <form action="nurse/cash_transaction/<?=$transaction[0]['id']?>/due" method="post">

                                     <input type="hidden" value="<?=$booking_details[0]['patient_bookid']?>" name="booking_id">
                                     <input type="hidden" value="<?=$booking_details[0]['patient_id']?>" name="patient_id">
                                     <input type="hidden" value="<?=$booking_details[0]['nurse_id']?>" name="nurse_id">

                                     <input type="hidden" name="amount" value="<?=$total_charge?>">

                                     <div class="row mb-4">

                                     <label class="custom-control col-md-4 text-right">
                                         Amount
                                     </label>

                                     <div class="col-md-8">
                                         <input min=1 id="due_check_amount" type="number" max="<?=$transaction[0]['due']?>" class="form-control" name="paid" value="<?=$transaction[0]['due']?>">
                                     </div>

                                     </div>
                                     <div class="row mb-4">
                                         <label class="custom-control col-md-4 text-right">
                                         Due
                                     </label>
                                     <div class="col-md-8">
                                         <!-- <span id="due"></span> -->
                                         <input type="text" readonly name="due" id="due_pay" class="form-control" value="0">
                                     </div>
                                     </div>

                                     <div class="modal-footer">
                                         <button id="due_submit_price" type="submit" class="btn bg-primary">Add</button>
                                     </div>
                                 </form>
                             </div>

                             </div>

                         </div>

                         </div>

                    <!-- Right-sidebar-->
                <?php $this->load->view('backend/right_sidebar');?>

                    <?php $this->load->view('backend/footer');?>

                </div>
                <!-- End app-content-->
            </div>
        </div>
        <!-- End Page -->

        <!-- Back to top -->
        <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

    <?php $this->load->view('backend/footer_link');?>
    <script>
        var total = parseFloat($("#total_charge").val());

        var due_total = parseFloat($("#due_check_amount").val());


        $("#check_amount").on("keypress keyup blur", function(event) {
            var float_number = parseFloat($(this).val());
            var $float = parseFloat($(this).val);
            $(this).val($(this).val().replace($float));
            if (float_number > total) {
                $("#submit_price").prop("disabled", true);
            }
            else{
                var due = total-float_number;
                if($(this).val() == ''){
                  due = total;
                }
                $("#due").val(due);
                $("#submit_price").prop("disabled", false);
            }
        });

        // $("#edit_check_amount").on("keypress keyup blur", function(event) {
        //     var float_number = parseFloat($(this).val());
        //     var $float = parseFloat($(this).val);
        //     $(this).val($(this).val().replace($float));
        //
        //     if (float_number > total) {
        //         $("#edit_submit_price").prop("disabled", true);
        //     }
        //     else{
        //         var due = total-float_number;
        //         if($(this).val() == ''){
        //           due = total;
        //         }
        //
        //         $("#edit_due").val(due);
        //
        //         $("#edit_submit_price").prop("disabled", false);
        //     }
        //    });



        $("#due_check_amount").on("keypress keyup blur", function(event) {
          
            var float_number = parseFloat($(this).val());
            var $float = parseFloat($(this).val);
            $(this).val($(this).val().replace($float));
            if (float_number > due_total) {
                $("#due_submit_price").prop("disabled", true);
            }
            else{
                var due = due_total-float_number;
                if($(this).val() == ''){
                  due = total;
                }
                $("#due_pay").val(due);
                $("#due_submit_price").prop("disabled", false);
            }
           });

    </script>

<script>
    let allDate = document.querySelectorAll('.fetch_nurse_notes');

    allDate.forEach(function (eachDate) {
        eachDate.addEventListener('click', function(){
            let id = this.dataset.id
            let note_summary = document.getElementById('note_summary');
            let care_details = document.getElementById('care_details');
            let noteDate = document.getElementById('note_date');

            $.ajax({
            url: "<?php echo base_url();?>nurse/fetch_note_summary_by_id",
            type: 'POST',
            datatype: 'JSON',
            data: {id: id},
            success: function(data) {
            var result = JSON.parse(data);
            // console.log(result.nurse_note_details[0].details);
            care_details.innerHTML = '';

                noteDate.innerHTML = `Note Date: ${result.nurse_note.date}`;

                result.nurse_note_details.forEach(function (val){
                care_details.innerHTML += `<tr>
                                            <th width=60%>${val.details}</th>
                                            <th>${val.care_note}</th>
                                          </tr>`;
                })

                note_summary.innerHTML = `<tr> <td>Temp</td><td>${result.nurse_note.temp}</td> </tr>
                                          <tr> <td>Bp</td> <td>${result.nurse_note.bp}</td> </tr>
                                          <tr> <td>Pulse</td> <td>${result.nurse_note.pulse}</td> </tr>
                                          <tr> <td>Pain Sore</td><td>${result.nurse_note.pain_sore}</td> </tr>
                                          <tr> <td>Respiration</td> <td>${result.nurse_note.respiration}</td> </tr>
                                          <tr> <td>Spo2</td> <td>${result.nurse_note.spo2}</td> </tr>
                                          <tr> <td>Etc</td> <td>${result.nurse_note.etc}</td> </tr>
                                          <tr> <td>Photo Wounds</td> <td> <img src="uploads/${result.nurse_note.photo_wounds}" alt="" /> </td> </tr>
                                        <tr> <td>Summary</td> <td>${result.nurse_note.summary}</td> </tr>
                                          `;



            },
            error: function() {
            alert('Something is wrong');
            }
        });
        })
    });

    // fetch.addEventListener('click', (e)=>{



    // })


    // $('.fetch_nurse_notes').click(function()
    // {
    //     $.each( this, function( value ) {
    //       alert($(this).data('id'))
    //     });
    //     // alert('clicked');


    //    // var class_id = $(this).val();
    //    // html2 = html = '';

    // });

</script>
<script>
const cloudy = document.querySelectorAll('.urine_colour');
const cloudySubSelect = document.getElementById('cloudy_sub_select');
// const existCloudySubSelect = document.getElementById('exist_sub_select');

// if (existCloudySubSelect == "2_1") {
//     cloudySubSelect.style.display = 'block';
// }
// else{
//     cloudySubSelect.style.display = 'none';
// }

for (let i = 0; i < cloudy.length; i++) {
    cloudy[i].onchange = () => {
    if(cloudy[i].value == "2_1"){
        cloudySubSelect.style.display = 'block';
    }
    else {
        cloudySubSelect.style.display = 'none';
    }
}
}

</script>
<script type="text/javascript">

    const getStarted = document.getElementById('getStarted');


    getStarted.addEventListener('click', function(e){
    e.preventDefault();
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        cancelButtonColor: '#F27474',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
        },
         function(isConfirm){
           if (isConfirm){
             // swal("Shortlisted!", "Candidates are successfully shortlisted!", "success");
             window.location.href = getStarted.getAttribute('href');
            } else {
              swal("Cancelled", "Your imaginary file is safe :)", "error");
                 e.preventDefault();
            }
        })
    });

</script>
<script>
    const serviceAccepted = document.getElementById('serviceAccepted');
    const serviceRejected = document.getElementById('serviceRejected');
    serviceAccepted.addEventListener('click', function(e){
    e.preventDefault();
    swal({
        title: "Are you sure?",
        // text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        cancelButtonColor: '#F27474',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
        },
         function(isConfirm){
           if (isConfirm){
             window.location.href = serviceAccepted.getAttribute('href');
            } else {
              swal("Cancelled", "", "error");
                 e.preventDefault();
            }
        })
    });

    serviceRejected.addEventListener('click', function(e){
    e.preventDefault();
    swal({
        title: "Are you sure?",
        // text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        cancelButtonColor: '#F27474',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
        },
         function(isConfirm){
           if (isConfirm){
             window.location.href = serviceRejected.getAttribute('href');
            } else {
              swal("Cancelled", "", "error");
                 e.preventDefault();
            }
        })
    });

</script>
    </body>
</html>
