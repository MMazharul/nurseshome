<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Booking extends MX_Controller {

    //public $counter=0;
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('booking_model');
        // $this->load->model('home_model');
        $this->load->model('nurse/nurse_model');
        // $this->load->helper('text');
        // $this->load->helper(array('form', 'url'));
        // $this->load->helper('inflector');
        //$this->load->library('encrypt');
        // $this->load->model('home_model');
        // $this->load->model('admin/admin_model');
        // $this->load->model('product/product_model');
        if(!$this->session->userdata('loginid'))
        {
           redirect('auth/index');
        }
    }
    public function index()
    {

                // $data['css_to_load'] = array('datepicker.css');
        // $data['js_to_load'] = array('datepicker.all.js','datepicker.en.js');
        // $data['loginid']=$this->session->userdata('loginid');

        $data['booking_details_new']=$this->booking_model->special_join_query_all_admin();

        $data['new_count']=count($data['booking_details_new']);

        $data['booking_details_active']=$this->booking_model->special_join_query_booking_admin('2');

        $data['active_count']=count($data['booking_details_active']);
        $data['booking_details_cancel']=$this->booking_model->special_join_query_booking_admin('4');
        $data['cancel_count']=count($data['booking_details_cancel']);
        $data['booking_details_completed']=$this->booking_model->special_join_query_booking_admin('5');
        $data['completed_count']=count($data['booking_details_completed']);

        $data['form_title'] = 'All Bookings';

        // echo $data['completed_count'];
        // print_r($data['booking_details_active']);
        // die();

        $this->load->view('index', $data);
    }

    public function all_transaction($value='')
    {
        $data['form_title'] = 'All Transaction';
        // $data['Transaction']=$this->booking_model->select_all('nurse_booking');
        $data['Transaction']=$this->booking_model->select_all('transaction');

        $this->load->view('all_transaction', $data);
    }
    public function review($type)
    {
        if($type==1)
        {
        $data['form_title'] = "Review To Nurse";
        $data['userlist'] = $this->booking_model->select_where_left_join('*','users','review','users.loginid=review.user_id','review.user_role=3');
        }
        else
        {
        $data['form_title'] = "Review To Patient";
        $data['userlist'] = $this->booking_model->select_where_left_join('*','users','review','users.loginid=review.user_id','review.user_role=4');
        }
        $this->load->view('review', $data);

    }
    public function review_details($loginid, $role, $role_to_check)
    {
       $data['form_title'] = "Review Details";
       $data['role'] = $role_to_check;

       $data['review_details'] = $this->booking_model->select_where_left_three_join('users.first_name,users.last_name,review.*, nurse_details.first_name as nurse_first_name,nurse_details.last_name as nurse_last_name, nurse_booking.booking_random_code as booking_random_code',
        'review',
        'users',
        'review.user_id=users.loginid',
        'users as nurse_details',
        'nurse_details.loginid='.$loginid,
        'nurse_booking',
        'review.patient_bookid=nurse_booking.patient_bookid',
        'review.patient_bookid in (select patient_bookid from review where review.user_id = '.$loginid.' and review.user_role = '.$role.') and review.user_role = '.$role_to_check.'');

        // print_r($data['review_details']);
        // exit;

       // $data['review_details'] = $this->booking_model->select_with_where_desc('*','patient_bookid in (select patient_bookid from review where user_id = '.$loginid.' and user_role = '.$role.') and user_role = '.$role_to_check.'' ,'review','created_at','Desc');


       $this->load->view('review_details', $data);
    }
    public function booking_details($id)
    {
        // $data['form_title'] = 'Review';

        $booking_id = $this->uri->segment(2);

      $data['booking_details']=$this->nurse_model->booking_with_addons('user_role, loginid,patient_bookid,
        first_name,
        last_name,
        contact_no,
        profile_pic,
        email,
        time_in_hours,
        fromtime,
        totime,
        adddate,
        notes,
        book_status,
        book_charge,
        nurse_id,
        users.charge as charge,
        booking_random_code,
        addon1.price as a1price,
        addon1.title as a1title,
        addon2.price as a2price,
        addon2.title as a2title,
        addon3.price as a3price,
        addon3.title as a3title,
        sum(addon1.price+addon2.price+addon3.price) as total_addons,
        patient_id,
        nurse_id,
        payment_method',
        'nurse_booking','users','users.loginid=nurse_booking.patient_id',
        'addons as addon1','nurse_booking.addons_one=addon1.id',
        'addons as addon2','nurse_booking.addons_two=addon2.id',
        'addons as addon3','nurse_booking.addons_three=addon3.id',
        "patient_bookid=".$booking_id);


        // $data['payment_details'] = $this->nurse_model->select_with_where('*','booking_id= '.$booking_id  ,' transaction');

        $data['transaction'] = $this->nurse_model->select_with_where('*','transaction.booking_id= '.$booking_id, 'transaction');


        $data['transaction_log_history'] = $this->nurse_model->select_with_where('*','transaction_history.booking_id= '.$booking_id, 'transaction_history');

        $data['transaction_history'] = $this->nurse_model->select_where_left_join('transaction.*','transaction','transaction_history','transaction_history.id=transaction_history.transaction_log_id','transaction.booking_id= '.$booking_id);

        $data['price_witout_addon'] = $data['booking_details'][0]['book_charge']-$data['booking_details'][0]['total_addons'];


        $data['assessment_details'] = $this->nurse_model->select_with_where('*','patient_book_id= '.$booking_id  ,' patient_assessment_by_nurse');

        $data['review_by_nurse'] = $this->nurse_model->select_where_left_join('user_id,review.user_role,first_name,review.patient_bookid,
            last_name,profile_pic,comment,rating',
            'users','review','users.loginid=review.user_id',
            'patient_bookid='.$booking_id.' and user_id='.$data['booking_details'][0]['nurse_id']);

        $data['review_by_patient'] = $this->nurse_model->select_where_left_join('user_id,review.user_role,first_name,review.patient_bookid,
            last_name,profile_pic,comment,rating',
            'users','review','users.loginid=review.user_id',
            'patient_bookid='.$booking_id.' and user_id='.$data['booking_details'][0]['patient_id']);

        // $data['patient_details'] = $this->nurse_model->select_with_where('*','loginid= '.$data['booking_details'][0]['patient_id']  ,' users');

        $data['nurse_details'] = $this->nurse_model->select_with_where('*','loginid= '.$data['booking_details'][0]['nurse_id']  ,' users');

        // print_r($data['assessment_details'][0]);
        // exit;
        $date = new DateTime($data['booking_details'][0]['adddate']);
        $from_date = new DateTime($data['booking_details'][0]['fromtime']);
        $to_date = new DateTime($data['booking_details'][0]['totime']);

        $data['total_charge'] = $data['booking_details'][0]['book_charge'];
        // * $data['booking_details'][0]['time_in_hours'];

        $data['row_count'] = count($data['assessment_details']);

        if ($data['total_charge'] == 0) {

            $data['total_charge'] = "25.00";

        }

        $data['book_day'] = $date->format('M j');

        $data['from_date'] = $from_date->format('M j ,Y');

        $data['to_date'] = $to_date->format('M j ,Y');

        $st = strtotime($data['booking_details'][0]['fromtime']);

        $end = strtotime($data['booking_details'][0]['totime']);

        $data['datediff'] = round(($end - $st) / (60 * 60 * 24)+1);

        $data['com_book_id'] = "#".$booking_id."".$data['booking_details'][0]['time_in_hours']."".$date->format('Ymd');

        $data['care_for_patient'] = $this->nurse_model->select_where_left_join('care_for_patient_other_details.id as cof_id,
             care_for_patient_other_details.details as cof_details',
            'care_for_patient_other_details',
            'care_for_patient_others',
            'care_for_patient_other_details.id=care_for_patient_others.other_type',
            'care_for_patient_others.patient_id='.$data['booking_details'][0]['patient_id']);


        $data['nurse_care_note'] = $this->nurse_model->select_with_where('id,temp,bp,pulse,pain_sore, respiration, spo2, etc, summary, photo_wounds',"patient_book_id=".$data['booking_details'][0]['patient_bookid']." and date=curdate()",'nurse_note_for_patient');


        $data['nurse_care_note_all'] = $this->nurse_model->select_with_where('id,patient_book_id, date',"patient_book_id=".$data['booking_details'][0]['patient_bookid'],'nurse_note_for_patient');


        if (count($data['nurse_care_note']) > 0) {

            $data['nurse_care_note_details'] = $nurse_care_note_details = $this->nurse_model->select_with_where('care_for_patient_id, care_note',"nurse_note_id=".$data['nurse_care_note'][0]['id'],'nurse_note_care_details');

            $data['care_note_id'] = $note_id = array_column($nurse_care_note_details, 'care_for_patient_id');

            $care_note = array_column($nurse_care_note_details, 'care_note');
              $new_arr = array();

            foreach ($note_id as $key => $value)
            {
                $data['care_note'][$value] = $care_note[$key];

            }
        }

        $this->load->view('booking_details', $data);
    }



}

?>
