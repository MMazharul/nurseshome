<?php
    $id=$this->session->userdata('loginid');
    $sql ="select * from users  where loginid='".$id."'";
    $query = $this->db->query($sql);
    foreach ($query->result() as $row)
    {
     $prfpic=$row->profile_pic;
     $st=$row->profile_update;
     $id=$row->loginid;
     $name=$row->first_name.' '.$row->last_name;
    //echo $upgrade_st;
    }
?>

<style type="text/css">
  th{
        text-transform: none !important;
  }
</style>
<div class="row">
<div class="col-md-12">
    <div class="card-body">
     <h5>Welcome : <span style="color:#e6190f;font-weight: bold"><?php echo $name?></span></h5>
    </div>



 </div>
</div>
<div class="row">
                            <div class="col-md-12">
                                    <div class="card-header" style="background: #FFF;">
                                       <h2 class="card-title w-90">
                    My Monthly Summary
                </h2>
                <h4><span class="text-right"><?php echo date('M Y'); ?></span><h2>
                                 </div>



                                <div class="card card-bgimg br-7">
                                    <div class="row">
                                        <div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
                                            <div class="card-body text-center">
                                                <h5 class="text-white">Active Bookings</h5>
                                                <h3 class="mb-2 mt-3 text-white mainvalue"><?=$active_booking?></h3>
                                                 <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
                                            <div class="card-body text-center">
                                                <h5 class="text-white">Completed Bookings</h5>
                                                <h3 class="mb-2 mt-3 text-white mainvalue"><?=$complete_booking?></h3>
                                                 <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
                                            <div class="card-body text-center">
                                                <h5 class="text-white">Total Bookings</h5>
                                                <h3 class="mb-2 mt-3 text-white mainvalue"><?=$total_booking?></h3>
                                                 <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
                                            <div class="card-body text-center">
                                                <h5 class="text-white">Total Patients</h5>
                                                <h3 class="mb-2 mt-3 text-white mainvalue"><?=$total_patient?></h3>
                                                 <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
                                            <div class="card-body text-center">
                                                <h5 class="text-white">Total Hours</h5>
                                                <h3 class="mb-2 mt-3 text-white mainvalue"><?php if($nurse_total_hour>0) echo $nurse_total_hour; else echo 0; ?></h3>
                                                 <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
                                            <div class="card-body text-center">
                                                <h5 class="text-white">Total Earnings</h5>
                                                <h3 class="mb-2 mt-3 text-white mainvalue">RM <?php if($nurse_total_charge>0) echo $nurse_total_charge; else echo 0; ?></h3>
                                                 <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

  <div class="row">
        <div class="col-md-12">
            <!-- <div class="card card-profile  overflow-hidden">
                <div class="card-header">
                <div class="card-title">My Latest Booking</div>
                    </div>
                <div class="card-body">
                    <div class="nav-wrapper p-0">
                        <ul class="nav nav-pills dragscroll horizontal nav-fill flex-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0 active show" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="fa fa-home mr-2"></i>All (0)</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="fa fa-user mr-2"></i>Active (0)</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i class="fa fa-picture-o mr-2"></i>Completed (0)</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-4-tab" data-toggle="tab" href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-4" aria-selected="false"><i class="fa fa-newspaper-o mr-2 mt-1"></i>Cancelled (0)</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div> -->
            <div class="card">
                <div class="card-body pb-0">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                            <div class="table-responsive mb-3">
                                 <div class="card-title">My Recent Booking Summary </div>

                                <table id="example" class="table table-striped table-bordered text-nowrap w-100 text-nowrap mb-0">
                                              <thead>

                                              <tr>


                  <th>SL</th>
                  <th>Book ID</th>
                  <th>Booked On</th>
                  <th>Patient Name</th>
                  <th>Patient Contact</th>
                  <th>Dates</th>
                  <th>Time</th>
                  <th>Hours <br>(Per Day)</th>
                  <th>Total <br> Hours</th>
                  <th>Amount</th>
                  <th>Status</th>
                  <th>Action</th>

            <!--  <th>Booking Date</th>
                  <th>Hours Booked</th>
                  <th>Total Hours</th>
                  <th>Patient Name</th>
                  <th>Contact No</th>
                  <th>Action</th>    -->
            <!--  <th>Charge</th> -->
            <!--  <th>Patient Pic</th> -->


          </tr>
          </thead>
          <tbody>
          <?php
          $i = 0;
          foreach ($nurselist as $key => $value)
           {

            $nursename=$value['nfmae'].' '.$value['nlname'];
            $pcontactno=$value['pcontactno'];
            $book_id=$value['book_id'];
            $nursepic=$value['nursepic'];
            $patientname=$value['bfname'].' '.$value['blname'];
            $patientpic=$value['patient_pic'];
            $nurseid=$value['nurseid'];

            $st = strtotime($value['fromtime']);
            $end = strtotime($value['totime']);
            $datediff = ($end - $st);

            $total_days = round($datediff / (60 * 60 * 24))+1;

            $addon_price = $value['a1price']+$value['a2price']+$value['a3price'];

            $total_hours = ($value['time_in_hours'])*$total_days;

            $total_price = ($value['charge']*$total_hours)+$addon_price;

            $book_status = $value['book_status'];
            if($book_status==1)
            {
                $bt="New";
            }
            elseif($book_status==2)
            {
                $bt="Approved";
            }
            elseif($book_status==3)
            {
                $bt="Completed";
            }
            elseif($book_status==4)
            {
                $bt="Review";
            }
            elseif($book_status==5)
            {
                $bt="Finish";
            }
            elseif($book_status==6)
            {
                $bt="Complete";
            }

            ?>
            <tr>
              <td><?=++$i?></td>
              <td><?=$book_id?></td>

              <td><?=$value['adddate']?></td>
              <td><?=$patientname?></td>
              <td><?=$pcontactno?></td>
              <td><?=$value['fromtime']?> to <br><?=$value['totime']?></td>
              <td><?=$value['start_time']?> to <br><?=$value['end_time']?></td>
              <td><?=$value['time_in_hours']?></td>
              <td><?=$total_hours?></td>
              <td>RM <?=$total_price?></td>
              <td><?=$bt?></td>
              <td><a href="booking-details/<?=$value['p_book_id']?>" class="btn btn-danger">View Details</a></td>
              </tr>
            <?php
           // }
         }

                    ?>


                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
