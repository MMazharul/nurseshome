<!-- 		<div class="row">
			<div class="col-md-12">
				<div class="owl-carousel owl-carousel2 owl-theme mb-5">
					<div class="item">
						<div class="card mb-0">
							<div class="row">
								<div class="col-4">
									<div class="feature">
										<div class="fa-stack fa-lg fa-2x icon bg-primary-transparent">
											<i class="si si-briefcase  fa-stack-1x text-primary"></i>
										</div>
									</div>
								</div>
								<div class="col-8">
									<div class="card-body p-3  d-flex">
										<div>
											<p class="text-muted mb-1">Total Patient</p>
											<h2 class="mb-0 text-dark">18,367K</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="card mb-0">
							<div class="row">
								<div class="col-4">
									<div class="feature">
										<div class="fa-stack fa-lg fa-2x icon bg-success-transparent">
											<i class="si si-drawer fa-stack-1x text-success"></i>
										</div>
									</div>
								</div>
								<div class="col-8">
									<div class="card-body p-3  d-flex">
										<div>
											<p class="text-muted mb-1">Total Hours</p>
											<h2 class="mb-0 text-dark">700 Hrs</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="card mb-0">
							<div class="row">
								<div class="col-4">
									<div class="feature">
										<div class="fa-stack fa-lg fa-2x icon bg-pink-transparent">
											<i class="si si-layers fa-stack-1x text-pink"></i>
										</div>
									</div>
								</div>
								<div class="col-8">
									<div class="card-body p-3  d-flex">
										<div>
											<p class="text-muted mb-1">Total Revenue</p>
											<h2 class="mb-0 text-dark">RM 3,548K </h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="card mb-0">
							<div class="row">
								<div class="col-4">
									<div class="feature">
										<div class="fa-stack fa-lg fa-2x icon bg-warning-transparent">
											<i class="si si-chart fa-stack-1x text-warning"></i>
										</div>
									</div>
								</div>
								<div class="col-8">
									<div class="card-body p-3  d-flex">
										<div>
											<p class="text-muted mb-1">Review</p>
											<h2 class="mb-0 text-dark">4.5 Star</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div> -->

						<div class="row">
							<div class="col-md-12">
								<div class="card card-bgimg br-7">
									<div class="row">
										<div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
											<div class="card-body text-center">
												<h5 class="text-white">Active Bookings</h5>
		<h2 class="mb-2 mt-3 fs-2 text-white mainvalue"><?php echo $active_bookings?></h2>
									             <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
									        </div>
										</div>
										<div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
											<div class="card-body text-center">
												<h5 class="text-white">Completed Bookings</h5>
									            <h2 class="mb-2 mt-3 fs-2 text-white mainvalue"><?php echo $completed_bookings?></h2>
									             <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
									        </div>
										</div>
										<div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
											<div class="card-body text-center">
												<h5 class="text-white">Total Bookings</h5>
									            <h2 class="mb-2 mt-3 fs-2 text-white mainvalue"><?php echo $all_bookings?></h2>
									             <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
									        </div>
										</div>
										<div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
											<div class="card-body text-center">
												<h5 class="text-white">Total Patients</h5>
									            <h2 class="mb-2 mt-3 fs-2 text-white mainvalue"><?php echo $total_patient?></h2>
									             <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
									        </div>
										</div>
										<div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
											<div class="card-body text-center">
												<h5 class="text-white">Total Hours</h5>
									            <h2 class="mb-2 mt-3 fs-2 text-white mainvalue"><?php  if($total_hour>0) echo $total_hour; else echo 0; ?></h2>
									             <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
									        </div>
										</div>
										<div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
											<div class="card-body text-center">
												<h5 class="text-white">Total Earnings</h5>
									            <h2 class="mb-2 mt-3 fs-2 text-white mainvalue">RM <?php if($total_charge>0) echo $total_charge; else echo 0; ?> </h2>
									             <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
									        </div>
										</div>
									</div>
								</div>
							</div>
						</div>	

						<div class="row">
							<div class="col-xl-12 col-lg-12 col-md-12">
								<div class="card">
														<div class="card-header">
									<div class="card-title">Latest Booking</div>
								</div>
									<div class="card-body">
										<div class="table-responsive">
						 <table id="example" class="table table-striped table-bordered text-nowrap w-100 text-nowrap mb-0">
						                      <thead>
						                      <tr>
						                       
					
						  <th width="20%">Nurse Name</th>
						  <th>Nurse<br>Contact</th>
							<!-- <th>Nurse Pic</th>  -->
              <th>Patient Name</th>
              <th>Patient<br>Contact</th>
            <!--  <th>Patient Pic</th> -->
              <th>Booking<br>Date</th>
              <th width="5%">Hour<br>(day)</th>
              <th >Hour<br>(total)</th>
              <th>Total<br>Charge</th>
              <th>View</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
     
          foreach ($nurselist as $key => $value)
           {

            $nursename=$value['nfmae'].' '.$value['nlname'];
            $nurse_conatct=$value['ncontactno'];
            $patient_contact=$value['pcontactno'];
            $nursepic=$value['nursepic'];
            $patientname=$value['bfname'].' '.$value['blname'];
            $patientpic=$value['patient_pic'];
            $total_hour=$value['total_hour'];
            $book_charge=$value['book_charge'];
            //$bookdate=date('Y-m-d H:i:s',$value['adddate']);
            ?>
                                  <tr>
                                   
             <td><img src="uploads/<?php echo $nursepic?>" width="30px" height="30px"/> <?php echo $nursename?></td>
             	<td><?php echo $nurse_conatct?></td>
             	<!-- <td></td>      -->      
             	<td><img src="uploads/<?php echo $patientpic?>" width="30px" height="30px"/> <?php echo $patientname?></td>
             	<td><?php echo $patient_contact?></td>
            	<!--  <td></td> -->
              <td><?=$value['adddate']?></td>
              <td><?=$value['time_in_hours']?></td>
              <td><?=$total_hour?></td>
              <td>RM <?=$book_charge?></td>
               <td><a href="booking_details/<?=$value['p_book_id']?>" class="btn btn-pink">View</a></td>
                      </tr>  
            <?php
           }

                    ?>

  
                    </tbody>
                    </table>	
											
					</div>
				</div>
			</div>
		</div>
	</div>
				