<?php
   $id=$this->session->userdata('loginid');
   $sql ="select * from users  where loginid='".$id."'";
   $query = $this->db->query($sql);
   foreach ($query->result() as $row)
       {

        $prfpic=$row->profile_pic;
        $st=$row->profile_update;
        $id=$row->loginid;
        $name=$row->first_name.' '.$row->last_name;
       //echo $upgrade_st;
       }
   ?>
<style type="text/css">
   .competency tr td:first-child{
   width: 20%;
   }
   h5{
   font-size: 13.28px;
   }
   .price{
   /*color: #f35e90;*/
   font-size: 17px;
   font-weight: 700;
   margin-bottom: 0px;
   }
   .c-concierge-card__pulse {
   background: #01a400;
   margin-right: 10px;
   }
   .c-concierge-card__pulse, .u-pulse {
   margin: 4px 20px 0 10px;
   margin-right: 8px;
   width: 10px;
   height: 10px;
   border-radius: 50%;
   background: #14bef0;
   cursor: pointer;
   -webkit-box-shadow: 0 0 0 rgba(40,190,240,.4);
   box-shadow: 0 0 0 rgba(40,190,240,.4);
   -webkit-animation: pulse 1.2s infinite;
   animation: pulse 1.2s infinite;
   display: inline-block;
   }
   .m-right{
   margin-right: 7px;
   }
   .u-green-text {
   color: #01a400;
   }
   .modal_mou{
   text-decoration: underline!important;
   font-size: 11px!important;
   }
   /*search box css start here*/
   .search-sec{
   padding: 2rem;
   }
   .search-slt{
   display: block;
   width: 100%;
   font-size: 13px!important;
   line-height: 1.5;
   color: #55595c;
   background-color: #fff;
   background-image: none;
   border: 1px solid #f3ecec;
   height: calc(3rem + 2px) !important;
   border-radius:0;
   }
   .wrn-btn{
   width: 100%;
   font-size: 16px;
   font-weight: 400;
   text-transform: capitalize;
   height: calc(3rem + 2px) !important;
   border-radius:0;
   }
   @media (min-width: 992px){
   .search-sec{
   position: relative;
   top: -114px;
   background: rgba(26, 70, 104, 0.51);
   }
   }
   @media (max-width: 992px){
   .search-sec{
   background: #1A4668;
   }
   }
   .select2-container--default .select2-search--inline .select2-search__field{height: 37px}
</style>
<div class="row">
   <div class="col-md-12">
      <div class="card-body">
         <h5>Welcome : <span style="color:#e6190f;font-weight: bold"><?php echo $name?></span></h5>
      </div>
   </div>
</div>
<div class="row">
<div class="col-md-12">
   <div class="card">
      <div class="card-header" style="background: #FFF;">
         <h2 class="card-title w-90">
            Patient Monthly Summary
         </h2>
         <h4>
         <span class="text-right"><?php echo date('M Y'); ?></span>
         <h2>
      </div>
      <div class="card-body" style="padding:10px">
         <div class="row" style="margin-top: 15px">
            <div class="col-sm-12 col-lg-6 col-xl-3">
               <div class="card">
                  <div class="row">
                     <div class="col-4">
                        <div class="feature">
                           <div class="fa-stack fa-lg fa-2x icon bg-purple">
                              <i class="fa fa-bed fa-stack-1x text-white"></i>
                           </div>
                        </div>
                     </div>
                     <div class="col-8">
                        <div class="card-body p-3  d-flex">
                           <div>
                              <p class="text-muted mb-1">Total Bookings</p>
                              <h3 class="mb-0 text-dark"><?php if($patient_booking>0) {echo $patient_booking;} else {echo 0;}?></h3>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- col end -->
            <div class="col-sm-12 col-lg-6 col-xl-3">
               <div class="card">
                  <div class="row">
                     <div class="col-4">
                        <div class="feature">
                           <div class="fa-stack fa-lg fa-2x icon bg-green">
                              <i class="fa fa-user-md fa-stack-1x text-white"></i>
                           </div>
                        </div>
                     </div>
                     <div class="col-8">
                        <div class="card-body p-3  d-flex">
                           <div>
                              <p class="text-muted mb-1">Last Booking</p>
                              <h3 class="mb-0 text-dark"><?php if($last_book_date) {echo $last_book_date;} else {echo 'None';}?></h3>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- col end -->
            <div class="col-sm-12 col-lg-6 col-xl-3">
               <div class="card">
                  <div class="row">
                     <div class="col-4">
                        <div class="feature">
                           <div class="fa-stack fa-lg fa-2x icon bg-orange">
                              <i class="fa fa-hospital-o fa-stack-1x text-white"></i>
                           </div>
                        </div>
                     </div>
                     <div class="col-8">
                        <div class="card-body p-3  d-flex">
                           <div>
                              <p class="text-muted mb-1">Amount Spent</p>
                              <h3 class="mb-0 text-dark"><?php if($total_charge>0) {echo 'RM '. $total_charge;} else {echo 'RM 0';}?></h3>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- col end -->
            <div class="col-sm-12 col-lg-6 col-xl-3">
               <div class="card">
                  <div class="row">
                     <div class="col-4">
                        <div class="feature">
                           <div class="fa-stack fa-lg fa-2x icon bg-yellow">
                              <i class="fa fa-flask fa-stack-1x text-white"></i>
                           </div>
                        </div>
                     </div>
                     <div class="col-8">
                        <div class="card-body p-3  d-flex">
                           <div>
                              <p class="text-muted mb-1">Nurses Engaged</p>
                              <h3 class="mb-0 text-dark"><?php if($total_nurse>0) {echo $total_nurse;} else {echo 0;}?></h3>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- col end -->
         </div>
      </div>
   </div>
   <div class="card">
      <div class="card-header" style="background: #FFF;">
         <h2 class="card-title w-90">
            Active Bookings
         </h2>
         <h4>
         <span class="text-right"><?php echo date('M Y'); ?></span>
         <h2>
      </div>
      <div class="card-body pb-0">
         <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade active show" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
               <div class="table-responsive mb-3">
                  <table class="table table-bordered new_table w-100">
                     <thead>
                        <tr>
                           <th>SL</th>
                           <th>Booking ID</th>
                           <th>Nurse Name</th>
                           <th>Dates</th>
                           <th>Time</th>
                           <th>Hours (Per Day)</th>
                           <th>Total Hours</th>
                           <th>Total Amount</th>
                           <th>Status</th>
                           <th>Action</th>
                           <!-- <th>Status</th> -->
                        </tr>
                     </thead>
                     <tbody class="col-lg-12 p-0">
                        <?php
                           if(isset($booking_details_active))
                           {
                           $ol=0;
                           foreach ($booking_details_active as $key => $value) {
                           $book_status=$value['book_status'];
                           if($book_status==1)
                           {
                           $bt="New";
                           }
                           elseif($book_status==2)
                           {
                           $bt="Approved (Nurse)";
                           }
                           elseif($book_status==3)
                           {
                           $bt="Completed (Nurse)";
                           }
                           elseif($book_status==4)
                           {
                           $bt="Patient Review";
                           }
                           elseif($book_status==5)
                           {
                           $bt="Finish";
                           }
                           elseif($book_status==6)
                           {
                           $bt="Complete";
                           }
                           $ol+=1;

                           $st = strtotime($value['fromtime']);
                           $end = strtotime($value['totime']);
                           $datediff = ($end - $st);
                           $total_days = round($datediff / (60 * 60 * 24))+1;
                           $addon_price = $value['a1price']+$value['a2price']+$value['a3price'];

                           $total_hours = ($value['time_in_hours'])*$total_days;

                           ?>
                        <tr>
                           <td>#<?php echo $ol?></td>
                           <td><?=$value['booking_random_code']?></td>
                           <td><?=$value['nurse_name']?></td>
                           <td><?=$value['fromtime']?> to <?=$value['totime']?></td>
                           <td><?=$value['start_time']?> to <?=$value['end_time']?></td>
                           <td><?=$value['time_in_hours']?></td>
                           <td><?=$total_hours?></td>
                           <td>RM <?php
                              echo ($value['charge']*$total_hours)+$addon_price;


                              ?></td>
                           <td>
                              <div class="text-success">
                                 <?php echo $bt?>
                           </td>
                           <td>
                           <a href="add-review/<?=$value['patient_bookid']?>" class="btn btn-pink">View Booking</a>
                           </div>
                           </td>
                        </tr>
                        <?php
                           }
                           }

                           ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
