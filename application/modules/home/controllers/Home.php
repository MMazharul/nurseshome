<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends MX_Controller {

    //public $counter=0;
    function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->model('auth/auth_model');
        $this->load->model('home_model');
      
        $this->load->model('patient/patient_model');
        
        $health_status = $this->patient_model->select_with_where('*',"patient_id=".$this->session->userdata('loginid'),'patient_health_status');
        $patient_details = $this->patient_model->select_with_where('id',"patient_id=".$this->session->userdata('loginid'),'patient_details');
        if(count($health_status)==0 && $this->session->userdata('user_role')!=1)
        {
          $this->session->set_flashdata('type', 'danger');
          $this->session->set_flashdata('msg', 'Your Profile is not Completed yet! Please Update Your Profile Info');
          redirect('patient-profile/'.$this->session->userdata('loginid'));
        }
        else if(count($patient_details)==0 && $this->session->userdata('user_role')!=1)
        {
          $this->session->set_flashdata('type', 'danger');
          $this->session->set_flashdata('msg', 'Your Profile is not Completed yet! Please Update Your Profile Info');
          redirect('patient-profile/'.$this->session->userdata('loginid'));
        }
        
        if(!$this->session->userdata('loginid'))
        {
           redirect('auth/index');
        }
    }
    public function index()
    {

        $nurselist['nurse_total_charge']='';
        $role=$this->session->userdata('user_role');

        $nurselist['page_title']="Active Nurses in System";
        $nurselist['loginid']=$this->session->userdata('loginid');
        $loginid=$this->session->userdata('loginid');
        $id=$this->session->userdata('loginid');

        $nurselist['active_bookings']=count($this->home_model->select_with_where('*','book_status=2','nurse_booking'));
        $nurselist['completed_bookings']=count($this->home_model->select_with_where('*','book_status=5','nurse_booking'));
        $nurselist['all_bookings']=count($this->home_model->select_all('nurse_booking'));
        $nurselist['total_patient']=count($this->home_model->select_with_where('*','user_role=4','users'));
       $nurselist['hr']=$this->home_model->select_with_where('sum((TIMEDIFF(totime,fromtime)+1)) as a','book_status=5','nurse_booking');
       // SELECT HOUR(TIMEDIFF(TIME(totime),TIME(fromtime))) as Total_time FROM `nurse_booking`

        $nurselist['total_hour']=$nurselist['hr'][0]['a'];
        //$nurselist['b']=$this->home_model->select_columnwise('(sum(book_charge)) as a','nurse_booking');



      $nurselist['total_nurse'] = $nurselist['patient_booking'] = $nurselist['last_book_date'] = '' ;

      // exit;
        if($role == 4){


          $nurselist['total_charge']='';

            $nurselist['b']=$this->home_model->custom_grupby("patient_id = $id AND pay_status=1 GROUP BY patient_id");

            if(count($nurselist['b'])>0)
            {
            $nurselist['total_charge']=$nurselist['b'][0]['total'];
            }
          //SELECT patient_id,SUM(paid) FROM transaction WHERE patient_id=3 AND pay_status=1 GROUP BY patient_id

          $nurselist['total_nurse'] = count($this->home_model->select_with_where('DISTINCT(nurse_id)','patient_id='.$loginid,'nurse_booking'));
          $nurselist['patient_booking'] = count($this->home_model->select_with_where('*','patient_id='.$loginid,'nurse_booking'));

          if($nurselist['patient_booking']==0)
          {
              $nurselist['last_book_date']="";
          }
          //$selector, $condition, $tablename,$col,$style,$limit
          else
          {
            $nurselist['book_details_last']=$this->home_model->select_with_where_limit_one('*','patient_id='.$loginid,'nurse_booking','adddate','DESC',1);
            $nurselist['last_book_date']=$nurselist['book_details_last'][0]['adddate'];
          }
        }


      //upper for patient

        if($role==1)
        {

          $nurselist['total_charge']='';

          $nurselist['b']=$this->home_model->custom_grupby("pay_status=1 GROUP BY id");
          if(count($nurselist['b'])>0)
          {
            $nurselist['total_charge']=$nurselist['b'][0]['total'];
          }
         $nurselist['nurselist']=$this->home_model->select_with_join_serialize('a.first_name as nfmae,a.last_name as nlname,a.contact_no as ncontactno,a.profile_pic as nursepic,a.email as nursemail,a.charge as charge,
          b.first_name as bfname,b.last_name as blname,b.contact_no as pcontactno,b.profile_pic as patient_pic,b.email as pmail,a1.price as a1price,a2.price as a2price,a3.price as a3price,
          nurse_booking.fromtime,nurse_booking.totime,nurse_booking.adddate,nurse_booking.book_status,a.loginid as nurseid,((datediff(totime,fromtime)+1)*nurse_booking.time_in_hours) as total_hour,book_charge,
          nurse_booking.notes,nurse_booking.time_in_hours,nurse_booking.patient_id,nurse_booking.patient_bookid as p_book_id','nurse_booking','users a','a.loginid=nurse_booking.nurse_id','users b','b.loginid=nurse_booking.patient_id','addons a1','a1.id=nurse_booking.addons_one','addons a2','a2.id=nurse_booking.addons_two','addons a3', 'a3.id=nurse_booking.addons_three','adddate','desc');

        }
        elseif($role==3)
        {
          $nurselist['total_patient'] = count($this->home_model->select_with_where('DISTINCT(patient_id)','nurse_id='.$loginid,'nurse_booking'));


          $nurselist['total_booking'] = count($this->home_model->select_with_where('*','nurse_id='.$loginid.' and year(adddate) = year(curdate())
          and month(adddate) = month(curdate())
          and adddate <= curdate() + interval 30 day','nurse_booking'));


        $nurselist['active_booking'] = count($this->home_model->select_with_where('*','nurse_id='.$loginid.' and book_status=2 and year(adddate) = year(curdate())
        and month(adddate) = month(curdate())
        and adddate <= curdate() + interval 30 day','nurse_booking'));

        $nurselist['complete_booking'] = count($this->home_model->select_with_where('*','nurse_id='.$loginid.' and book_status=5 and year(adddate) = year(curdate())
        and month(adddate) = month(curdate())
        and adddate <= curdate() + interval 30 day','nurse_booking'));

        // $nurselist['nurselist']=$this->home_model->select_with_join_serialize_where('a.first_name as nfmae,a.last_name as nlname,a.contact_no as ncontactno,a.profile_pic as nursepic,a.email as nursemail,a.charge as charge,
        // b.first_name as bfname,b.last_name as blname,b.contact_no as pcontactno,b.profile_pic as patient_pic,b.email as pmail,a1.price as a1price,a2.price as a2price,a3.price as a3price,
        // nurse_booking.fromtime,nurse_booking.totime,nurse_booking.adddate,nurse_booking.book_status,a.loginid as nurseid,
        // nurse_booking.notes,nurse_booking.time_in_hours,nurse_booking.patient_id,nurse_booking.patient_bookid as p_book_id','nurse_booking',' users a ','a.loginid=nurse_booking.nurse_id','users b','b.loginid=nurse_booking.patient_id','addons a1','a1.id=nurse_booking.addons_one','addons a2','a2.id=nurse_booking.addons_two','addons a3', 'a3.id=nurse_booking.addons_two','adddate','desc','a.loginid='.$id);

        $nurselist['nurselist'] =  $this->home_model->custom_query($id);


        $nurselist['nurse_hr']=$this->home_model->select_with_where('sum((datediff(totime,fromtime)+1) * time_in_hours) as a','book_status=5 AND nurse_id='.$id,'nurse_booking');
        $nurselist['nurse_total_hour']=$nurselist['nurse_hr'][0]['a'];

          //$nurselist['nurse_charge']=$this->home_model->select_with_where('(sum(book_charge)) as a','book_status=5 AND nurse_id='.$id,'nurse_booking');
          //$nurselist['nurse_total_charge'] = $nurselist['nurse_charge'][0]['a'];
        $nurselist['nurse_charge']=$this->home_model->custom_grupby("nurse_id = $id AND pay_status=1 GROUP BY nurse_id");
          if(count($nurselist['nurse_charge'])>0)
          {
            $nurselist['nurse_total_charge']=$nurselist['nurse_charge'][0]['total'];
          }

        }

        else
        {
        $nurselist['nurselist']=$this->home_model->active_booking_patient($id);
          if(count($nurselist['nurselist'])==0)
        {
          $nurselist['page_title']="No yet booking in system";
        }
        $nurselist['booking_details_active'] = $this->patient_model->special_join_query_booking($loginid,2);


        $nurselist['state']=$this->home_model->select_all('states');
        }

        // echo "<pre>";
        // echo json_encode($nurselist);
        // die;
        $this->load->view('index',$nurselist);


    }

    public function search()
    {
      $this->load->view('list.php');
    }

public function search_nurse($value='')
{
         $nurselist['page_title']="Active Nurses in System";

        $role=$this->session->userdata('user_role');
        $nurselist['loginid']=$this->session->userdata('loginid');
        $loginid=$this->session->userdata('loginid');
        $id=$this->session->userdata('loginid');
        $nurselist['active_bookings']=count($this->home_model->select_with_where('*','book_status=2','nurse_booking'));
        $nurselist['completed_bookings']=count($this->home_model->select_with_where('*','book_status=5','nurse_booking'));
        $nurselist['all_bookings']=count($this->home_model->select_all('nurse_booking'));
        $nurselist['total_patient']=count($this->home_model->select_with_where('*','user_role=4','users'));
      $nurselist['hr']=$this->home_model->select_with_where('sum(datediff(totime,fromtime)*time_in_hours) as a','book_status=5','nurse_booking');
      $nurselist['total_hour']=$nurselist['hr'][0]['a'];
      $nurselist['b']=$this->home_model->select_columnwise('(sum(book_charge)) as a','nurse_booking');
      $nurselist['total_charge']=$nurselist['b'][0]['a'];

     $nurselist['total_nurse'] = $nurselist['patient_booking'] = $nurselist['last_book_date'] = '' ;
      // print_r($nurselist);
      // exit;
        if($role == 4){
          $nurselist['total_nurse'] = count($this->home_model->select_with_where('DISTINCT(nurse_id)','patient_id='.$loginid,'nurse_booking'));
          $nurselist['patient_booking'] = count($this->home_model->select_with_where('*','patient_id='.$loginid,'nurse_booking'));
          if($nurselist['patient_booking']==0)
          {
              $nurselist['last_book_date']="";
          }
          //$selector, $condition, $tablename,$col,$style,$limit
          else
          {
            $nurselist['book_details_last']=$this->home_model->select_with_where_limit_one('*','patient_id='.$loginid,'nurse_booking','adddate','DESC',1);
            $nurselist['last_book_date']=$nurselist['book_details_last'][0]['adddate'];
          }
        }
    $nurselist['loginid']=$this->session->userdata('loginid');
        
        $charge=$this->input->post('charge');
        $location=$this->input->post('location');
        $nurse_type=$this->input->post('nurse_type');
        $Experience=$this->input->post('Experience');
        $Availability=$this->input->post('Availability');
        $language=$this->input->post('language');
        $nurselist['state']=$this->home_model->select_all('states');
        $nurselist['nurselist']=$this->home_model->custom_search_column($charge,$location,$nurse_type,$Experience,$language,$Availability);
        $this->load->view('index',$nurselist);

}


}

?>
