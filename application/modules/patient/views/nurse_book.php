<?php $this->load->view('backend/head_link');?>


  <style>
  .competency tr td:first-child{
  width: 20%;
  }
  h5{
  font-size: 13.28px;
  }
  .price{
  /*color: #f35e90;*/
  font-size: 17px;
  font-weight: 700;
  margin-bottom: 0px;
  }
  .c-concierge-card__pulse {
  background: #01a400;
  margin-right: 10px;
  }
  .c-concierge-card__pulse, .u-pulse {
  margin: 4px 20px 0 10px;
  margin-right: 8px;
  width: 10px;
  height: 10px;
  border-radius: 50%;
  background: #14bef0;
  cursor: pointer;
  -webkit-box-shadow: 0 0 0 rgba(40,190,240,.4);
  box-shadow: 0 0 0 rgba(40,190,240,.4);
  -webkit-animation: pulse 1.2s infinite;
  animation: pulse 1.2s infinite;
  display: inline-block;
  }
  .m-right{
  margin-right: 7px;
  }
  .u-green-text {
  color: #01a400;
  }
  .modal_mou{
  text-decoration: underline!important;
  font-size: 11px!important;
  }
  /*search box css start here*/
  .search-sec{
  padding: 2rem;
  }
  .search-slt{
  display: block;
  width: 100%;
  font-size: 13px!important;
  line-height: 1.5;
  color: #55595c;
  background-color: #fff;
  background-image: none;
  border: 1px solid #f3ecec;
  height: calc(3rem + 2px) !important;
  border-radius:0;
  }
  .wrn-btn{
  width: 100%;
  font-size: 16px;
  font-weight: 400;
  text-transform: capitalize;
  height: calc(3rem + 2px) !important;
  border-radius:0;
  }
  @media (min-width: 992px){
  .search-sec{
  position: relative;
  top: -114px;
  background: rgba(26, 70, 104, 0.51);
  }
  }
  @media (max-width: 992px){
  .search-sec{
  background: #1A4668;
  }
  }
  .select2-container--default .select2-search--inline .select2-search__field{height: 37px}
  </style>
  <body class="app sidebar-mini rtl">

  <div class="page">
  <div class="page-main">
  <!--app-header-->
  <?php $this->load->view('backend/header');?>
  <!-- app-content-->
  <div class="container content-patient">
  <div class="side-app">

  <div class="row">
  <div class="col-md-12 col-lg-12">
  <div class="card">
  <div class="card-header">
      <div class="col-md-10">
      <div class="card-title"><?=$form_title?></div>
      </div>
  </div>
  <div class="card-body">
    <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <h4>Find a best nurse for you</h4>
          <form method="post" action="home/search_nurse">
             <div class="row  m-1 mb-3">
                <div class="col-lg-12">
                   <div class="row">
                      <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 p-0">
                         <select class="form-control search-slt" name="charge" id="exampleFormControlSelect1">
                            <option disabled selected>Charge</option>
                            <option value="25" <?php if(set_value('charge')==25) { echo "selected"; }  ?>>RM 25</option>
                            <option value="50" <?php if(set_value('charge')==50) { echo "selected"; }  ?>>RM 50</option>
                            <option value="100" <?php if(set_value('charge')==100) { echo "selected"; }  ?>>RM 100</option>
                            <option value="150" <?php if(set_value('charge')==150) { echo "selected"; }  ?>>RM 150</option>
                            <option value="200" <?php if(set_value('charge')==200) { echo "selected"; }  ?>>RM 200</option>
                            <option value="250" <?php if(set_value('charge')==250) { echo "selected"; }  ?>>RM 250</option>
                            <option value="300" <?php if(set_value('charge')==300) { echo "selected"; }  ?>>RM 300</option>
                         </select>
                      </div>
                      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 p-0">
                         <select class="form-control search-slt" name="location" id="exampleFormControlSelect1">
                            <option disabled selected>Location--</option>
                            <?php
                               foreach ($state as $key => $value)
                               {
                                   // if($value==$nurse_details[0]['location'])
                                   $r=$value['state_name'];
                                   ?>
                            <option value='<?php echo $r?>' <?php if(set_value('location')==$r) { echo "selected"; } ?>><?php echo $r?></option>
                            <?php
                               }


                                                              ?>
                         </select>
                      </div>
                      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 p-0">
                         <select class="form-control search-slt" name="nurse_type"  id="exampleFormControlSelect1">
                            <option disabled selected>Type of Nurse</option>
                            <option value="1" <?php if(set_value('nurse_type')==1) { echo "selected"; }  ?>>Full Time</option>
                            <option value="2" <?php if(set_value('nurse_type')==2) { echo "selected"; }  ?>>Part Time</option>
                         </select>
                      </div>
                      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 p-0">
                         <select class="form-control search-slt" name="Experience" id="exampleFormControlSelect1">
                            <option disabled selected>Select Experience</option>
                            <option value="1" <?php if(set_value('Experience')==1) { echo "selected"; }  ?>>1 Year</option>
                            <option value="2" <?php if(set_value('Experience')==2) { echo "selected"; }  ?>>2 Year</option>
                            <option value="3" <?php if(set_value('Experience')==3) { echo "selected"; }  ?>>3 Year</option>
                            <option value="4" <?php if(set_value('Experience')==4) { echo "selected"; }  ?>>4 Year</option>
                            <option value="5" <?php if(set_value('Experience')==5) { echo "selected"; }  ?>>5 Year</option>
                            <option value="6" <?php if(set_value('Experience')==6) { echo "selected"; }  ?>>6 Year</option>
                         </select>
                      </div>
                      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 p-0">
                         <select class="form-control search-slt" name="Availability" id="exampleFormControlSelect1">
                            <option disabled selected>Availability</option>
                            <option value="sun" <?php if(set_value('Availability')=="sun") { echo "selected"; }  ?>>Sunday </option>
                            <option value="mon" <?php if(set_value('Availability')=="mon") { echo "selected"; }  ?>>Monday</option>
                            <option value="tue" <?php if(set_value('Availability')=="tue") { echo "selected"; }  ?>>Tuesday</option>
                            <option value="wed" <?php if(set_value('Availability')=="wed") { echo "selected"; }  ?>>Wednesday</option>
                            <option value="thu" <?php if(set_value('Availability')=="thu") { echo "selected"; }  ?>>Thursday</option>
                            <option value="fri" <?php if(set_value('Availability')=="fri") { echo "selected"; }  ?>>Friday</option>
                         </select>
                      </div>
                      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 p-0">
                         <div class="form-group">
                            <select style="background: #FFF !important;height: 37px;" class="form-control select2" name="langauge" data-placeholder="Language" multiple>
                               <option value="1" <?php if(set_value('langauge')==1) { echo "selected"; }  ?>>English</option>
                               <option value="2" <?php if(set_value('langauge')==2) { echo "selected"; }  ?>>Bahasa Malaysia</option>
                               <option value="3" <?php if(set_value('langauge')==3) { echo "selected"; }  ?>>Mandarin</option>
                               <option value="4" <?php if(set_value('langauge')==4) { echo "selected"; }  ?>>Hindi</option>
                               <option value="5" <?php if(set_value('langauge')==5) { echo "selected"; }  ?>>Cantonese</option>
                               <option value="6" <?php if(set_value('langauge')==6) { echo "selected"; }  ?>>Tamil</option>
                            </select>
                         </div>

                      </div>
                      <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 p-0">
                         <button type="submit" class="btn btn-danger wrn-btn"><i class="fa fa-search" aria-hidden="true"></i></button>
                      </div>
                   </div>
                </div>
             </div>
          </form>
          <div class="row">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card Relatedpost nested-media">
                   <div class="card-header">
                      <h4 class="card-title"><?=$page_title?></h4>
                   </div>
                   <div class="card-body">
                      <?php if(count($nurselist)>0)
                         {
                         foreach ($nurselist as $key => $value)
                         {
                         $country_code=$value['country_code'];
                         $contact_no=$value['contact_no'];
                         $first_name=$value['first_name'];
                         $last_name=$value['last_name'];
                         $email=$value['email'];
                         $registered_date=$value['registered_date'];
                         $charge=$value['charge'];
                         $profile_pic=$value['profile_pic'];
                         $nurse_id=$value['loginid'];
                         $point=$value['point'];
                         $total_served_patient=$value['total_served_patient']


                         ?>
                      <div class="row border-bottom mb-4">
                         <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 mb-4">
                            <div class="media media-lg mt-0">
                               <img class="mr-3" src="uploads/<?php echo $profile_pic?>" alt="Generic placeholder image">
                               <div class="media-body">
                                  <h4 class="mt-0"><strong><?php echo $first_name." ".$last_name?></strong></h4>
                                  <!-- 	<p>Practo Surgery Guide</p> -->
                                  <table>
                                     <tbody class='competency'>
                                        <tr>
                                           <td>Experience</td>
                                           <td>: <?=$value['total_experience']?></td>
                                        </tr>
                                        <tr>
                                           <td>Language Spoken</td>
                                           <td>: English</td>
                                        </tr>
                                        <tr>
                                           <td>Competencies</td>
                                           <td><?php
                                              $sql ="select competencies.compet_ttile from competencies inner join
                                              competencies_details on competencies.compe_id=competencies_details.compe_id where nurse_id='$nurse_id'and competencies_details.com_value = 'Trained and very confident (More than 3 years experience)' limit 3";

                                              $query = $this->db->query($sql);

                                              if (count($query->result()) > 0) {
                                                foreach ($query->result() as $row)
                                                 {

                                                   $compet_ttile=$row->compet_ttile;
                                                   echo "<span class='badge badge-default mr-1 mb-1 mt-1'>$compet_ttile</span> ";
                                                    }

                                                  }
                                                  else{
                                                    echo ":";
                                                   }
                                                   ?></td>
                                        </tr>
                                        <tr>
                                           <td>Location</td>
                                           <td>: <?=$value['location']?></td>
                                        </tr>
                                        <tr>
                                           <td>Gender</td>
                                           <td>: <?php if($value['gender']==1) echo "Male"; else echo "Female"; ?></td>
                                        </tr>
                                        <tr>
                                           <td>Availability</td>
                                           <td>: <span style="color:lightgreen">Available</span></td>
                                        </tr>
                                     </tbody>
                                  </table>
                                  <!-- 	<h5>
                                     <strong>Competencies</strong>
                                     </h5> -->
                               </div>
                            </div>
                         </div>
                         <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mb-4">
                            <div class="media media-lg mt-0">
                               <div class="media-body">
                                  <p class="price"><span class="m-right">RM </span><?php echo $charge?> /Hour</p>
                                  <br>
                                  <br>
                                  <a href="patient-book-nurse/<?php echo $nurse_id?>/<?php echo $loginid?>" class="btn btn-pink"></i>Book Now</a>
                               </div>
                            </div>
                         </div>
                      </div>
                      <?php
                         }
                              }
                              else {

                             ?>
                      <h4>
                      <span>
                      We are sorry, we couldn't find a match for your search. Please try again or contact our customer service department on WhatsApp at +6016-387 0749 or call us at 1300-22-NURSE(6877) between 9.00am to 5.00pm (Monday to Fridays)
                      </span>
                      </h3>
                      <?php
                         }
                         ?>
                   </div>
                </div>
             </div>
          </div>
          </div>
      </div>
  </div>
  <!-- section-wrapper -->
  </div>
  </div>

  </div><!--End side app-->

  <!-- Right-sidebar-->
  <?php $this->load->view('backend/right_sidebar');?>
  <!-- End Rightsidebar-->

  <?php $this->load->view('backend/footer');?>
  </div>
  <!-- End app-content-->
  </div>
  </div>
  <!-- End Page -->

  <!-- Back to top -->
  <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

  <?php $this->load->view('backend/footer_link');?>
    <script type="text/javascript">
      let dataTable = $(".new_table").dataTable(
        {  "bLengthChange" : false,
             "responsive": true,
            "oLanguage":
          {
              "sSearch": ""
          }
        });
       $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
          $($.fn.dataTable.tables(true)).DataTable()
             .columns.adjust()
             .responsive.recalc();
          });

  </script>

  </body>
  </html>
