<?php $this->load->view('backend/head_link');?>

<body class="app sidebar-mini rtl">
<style>
.select-box{
/*display: none;*/
}
.competency td:first-child{
width: 50%;
}
span:first-letter {
    text-transform:capitalize;
}
</style>
<!--Global-Loader-->
<!-- <div id="global-loader">
<img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
<div class="page-main">
<!--app-header-->
<?php $this->load->view('backend/header');?>
<!-- app-content-->
<div class="container content-patient">
    <div class="side-app">

        <?php if($this->session->flashdata('msg')){ ?>
                        <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <div class="alert-message">
                                <span><?=$this->session->flashdata('msg');?></span>
                            </div>
                        </div>
                    <?php } ?>

    <div class="row">
<div class="col-md-12">
    <?php
$fname=$patient_details[0]['first_name'];
$lname=$patient_details[0]['last_name'];
$name=$fname." ".$lname;
    ?>
    <div class="card card-profile  overflow-hidden">
                    <div class="card-body text-center profile-bg">
                    <div class=" card-profile">
                        <div class="row justify-content-center">
                            <div class="">
                                <div class="">
                                    <a href="#">
                          <img src="uploads/<?=$patient_details[0]['profile_pic'];?>" class="avatar-xxl rounded-circle" alt="profile">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3 class="mt-3 text-white"><?=$patient_details[0]['first_name']." ".$patient_details[0]['last_name'];?></h3>
                    <p class="mb-2 text-white"><?=$patient_details[0]['user_role']== 3 ? 'Nurse' : ($patient_details[0]['user_role'] == 4 ? 'Patient' : '') ?></p>
                   <!--  <div class="text-center mb-4">
                        <span><i class="fa fa-star text-warning"></i></span>
                        <span><i class="fa fa-star text-warning"></i></span>
                        <span><i class="fa fa-star text-warning"></i></span>
                        <span><i class="fa fa-star-half-o text-warning"></i></span>
                        <span><i class="fa fa-star-o text-warning"></i></span>
                    </div> -->

                    <a href="patient/patient_profile_update/<?=$patient_details[0]['loginid']?>" class="btn btn-pink btn-sm"><i class="fas fa-pencil-alt" aria-hidden="true"></i>Edit profile</a>
                </div>
    <div class="card-body">
            <div class="nav-wrapper p-0">
                <ul class="nav nav-pills dragscroll horizontal nav-fill flex-row" id="tabs-icons-text" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0 <?=$this->session->flashdata('active')=='' ? 'active show' : '' ?>" id="profile" data-toggle="tab" href="#tabs-profile" role="tab" aria-controls="profile" aria-selected="true"><i class="fa fa-home mr-2"></i>Account</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0 <?=$this->session->flashdata('active')=='biodata' ? 'active show' : ''?>" id="bio" data-toggle="tab" href="#tabs-bio" role="tab" aria-controls="bio" aria-selected="false"><i class="fa fa-user mr-2"></i>My Condition</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0 <?=$this->session->flashdata('active')=='qualification' ? 'active show' : ''?>" id="qualification" data-toggle="tab" href="#tabs-qualification" role="tab" aria-controls="tabs-qualification" aria-selected="false"><i class="fa fa-picture-o mr-2"></i>My Care Needs</a>
                    </li>
<!--                     <li class="nav-item">
                        <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0 <?=$this->session->flashdata('active') == 'experience' ? 'active show' : ''?>" id="experience" data-toggle="tab" href="#tabs-experience" role="tab" aria-controls="experience" aria-selected="false"><i class="fa fa-newspaper-o mr-2 mt-1"></i>Booking </a>
                    </li> -->
                <!--     <li class="nav-item">
                        <a class="nav-link mb-sm-0 mb-md-0 mt-md-2 mt-0 mt-lg-0 <?=$this->session->flashdata('active')=='competency' ? 'active show' : ''?>" id="competency" data-toggle="tab" href="#tabs-competency" role="tab" aria-controls="competency" aria-selected="false"><i class="fa fa-cog mr-2"></i>Review</a>
                    </li> -->
                </ul>
            </div>
    </div>
    </div>
    <div class="card">
        <div class="card-body pb-0">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade <?=$this->session->flashdata('active')=='' ? 'active show' : ''?>" id="tabs-profile" role="tabpanel" aria-labelledby="profile">
                <div class="row">
                    <div class="col-md-12">
<form action="nurse/register_user_as_nurse" method="post">
    <div class="row">
    <?php if($this->session->flashdata('msg')){ ?>
        <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert-message">
                <span><?=$this->session->flashdata('msg');?></span>
            </div>
        </div>
    <?php } ?>
    <div class="col-md-12">
        <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">


            <div class="table-responsive mb-3">
                                <pre >
                                <table class="table row table-borderless w-100 m-0 border">
                                  <h4>Contact Details </h4>

                                    <tbody class="col-lg-6 p-0">


                                        <tr>
                                            <td><strong>First Name :</strong> <?=ucwords($patient_details[0]['first_name'])?></td>

                                        </tr>
                                        <tr>
                                            <td><strong>Email      :</strong> <?=$patient_details[0]['email']?></td>
                                        </tr>

                                        <tr>
                                            <td><strong>Gender     :</strong> <?=$patient_details[0]['gender']==1 ? 'Male' : 'Female' ?></td>
                                        </tr>

                                    </tbody>

                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Last Name :</strong> <?=ucwords($patient_details[0]['last_name'])?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Contact   :</strong> <?=$patient_details[0]['country_code']." ".$patient_details[0]['contact_no']?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                </pre>

                                <br>
                                <pre>
                                <table class="table row table-borderless w-100 m-0 border">
                                    <h4>Bio Data</h4>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>ID     :</strong><?=!empty($patient_details_info[0]['nrc_passport_id'])? $patient_details_info[0]['nrc_passport_id'] : ''?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Age    :</strong><?=!empty($patient_details_info[0]['age']) ? $patient_details_info[0]['age'] : ''?> </td>
                                        </tr>

                                        <tr>
                                            <td><strong>Height :</strong> <?=!empty($patient_details_info[0]['height']) ? $patient_details_info[0]['height'] : ''?> </td>
                                        </tr>
                                    </tbody>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Weight :</strong><?=!empty($patient_details_info[0]['weight']) ? $patient_details_info[0]['weight'] : ''?></td>
                                        </tr>
                                    </tbody>
                                </table>
                              </pre>
                                <br>
                                <pre>
                                <table class="table row table-borderless w-100 m-0 border">
                                    <h4>Location Details </h4>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Address          :</strong><?=!empty($patient_details_info[0]['address']) ? $patient_details_info[0]['address'] : ''?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Type of Resident :</strong><?php if (!empty($patient_details_info[0]['resident_type'])) {
                                                if ($patient_details_info[0]['resident_type'] == 1) {
                                                    echo "Landed House";
                                                }
                                                else if ($patient_details_info[0]['resident_type'] == 2) {
                                                    echo "Multi Storey Residence with lift";
                                                }
                                                else if ($patient_details_info[0]['resident_type'] == 3) {
                                                    echo "Multi Storey Residence without lift";
                                                }
                                                else if ($patient_details_info[0]['resident_type'] == 4) {
                                                    echo  "Nursing Home/Hostpital";
                                                }
                                            }
                                            else{
                                                echo "--";
                                            }
                                            ?>

                                            </td>
                                        </tr>

                                        <tr>
                                            <td><strong>Type of Security :</strong><?php
                                                if (!empty($patient_details_info[0]['security_type'])) {
                                                    if ($patient_details_info[0]['security_type'] == 1) {
                                                        echo "Gated & Guarded";
                                                    }
                                                    elseif ($patient_details_info[0]['security_type'] == 2) {
                                                        echo "Gated";
                                                    }
                                                    elseif ($patient_details_info[0]['security_type'] == 3) {
                                                        echo "Restricted Access";
                                                    }
                                                    elseif ($patient_details_info[0]['security_type'] == 4) {
                                                        echo "None";
                                                    }

                                                }
                                                else {
                                                echo "--";
                                                }

                                            ?>


                                            </td>
                                        </tr>
                                    </tbody>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Living Status       :</strong><?php
                                                    if (!empty($patient_details_info[0]['living_status'])) {
                                                        if ($patient_details_info[0]['living_status'] == 1) {
                                                            echo "Alone";
                                                        }
                                                        elseif ($patient_details_info[0]['living_status'] == 2) {
                                                            echo "Family";
                                                        }
                                                    }
                                                    else {
                                                        echo "--";
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Current Care Status :</strong><?php
                                                if (!empty($patient_details_info[0]['current_care_status'])) {
                                                    if ($patient_details_info[0]['current_care_status'] == 1) {
                                                        echo "Self";
                                                    }
                                                    elseif ($patient_details_info[0]['current_care_status'] == 2) {
                                                        echo "Family";
                                                    }
                                                    elseif ($patient_details_info[0]['current_care_status'] == 3) {
                                                        echo "Caregiver/Maid";
                                                    }
                                                    elseif ($patient_details_info[0]['current_care_status'] == 4) {
                                                        echo "Nurse";
                                                    }

                                                    }
                                                    else {
                                                        echo "--";
                                                    }

                                            ?>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Pets in House       :</strong><?php
                                                if (!empty($patient_details_info[0]['pets_in_house'])) {
                                                    if ($patient_details_info[0]['pets_in_house'] == 1) {
                                                        echo "Dog";
                                                    }
                                                    elseif ($patient_details_info[0]['pets_in_house'] == 2) {
                                                        echo "Cat";
                                                    }


                                                }
                                                else {
                                                        echo "--";
                                                     }

                                            ?>  </td>
                                        </tr>
                                    </tbody>
                                </table>
                              </pre>

                            </div>

        </div>
    </div>



            <!-- <div class="col-md-12">
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a large text here ..."></textarea>
            </div> -->

    </form>



                    </div>
                </div>
                </div>
                <div aria-labelledby="" class="tab-pane fade <?=$this->session->flashdata('active')=='biodata' ? 'active show' : ''?>" id="tabs-bio" role="tabpanel">
    <form action="nurse/register_user_as_nurse" method="post">
    <div class="row">
    <?php if($this->session->flashdata('msg')){ ?>
        <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert-message">
                <span><?=$this->session->flashdata('msg');?></span>
            </div>
        </div>
    <?php } ?>
    <div class="col-md-12">
        <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">
             <div class="table-responsive mb-3">
                                  <pre>
                                <table class="table row table-borderless w-100
                                m-0 border">
                                    <h4>My Health Status </h4>

                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Independent  :</strong><?php
                                                if (!empty($health_status[0]['dependency'])) {
                                                    if ($health_status[0]['dependency']==1) {
                                                        echo " Independent";
                                                    }
                                                    elseif ($health_status[0]['dependency']==2) {
                                                        echo " Semi Dependent";
                                                    }
                                                    elseif ($health_status[0]['dependency']==3) {
                                                        echo " Dependent - FULLY DEPENDENT IN ONE OR MORE OF MOVE/FEED/CLEAN OWN SELF";
                                                    }
                                                    elseif ($health_status[0]['dependency']==4) {
                                                        echo " Dependent - FULLY DEPENDENT TO MOVE/FEED/CLEAN OWN SELF";
                                                    }
                                                    else{
                                                        echo "";
                                                    }

                                                }
                                             ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Mental State :</strong><?php
                                                if (!empty($health_status[0]['mental_state'])) {
                                                    if ($health_status[0]['mental_state']==1) {
                                                        echo " AWAKE AND RESPONSIVE";
                                                    }
                                                    elseif ($health_status[0]['mental_state']==2) {
                                                        echo " AWAKE BUT NON-RESPONSIVE";
                                                    }
                                                    elseif ($health_status[0]['mental_state']==3) {
                                                        echo " DROWSY/CONFUSED";
                                                    }
                                                    elseif ($health_status[0]['mental_state']==4) {
                                                        echo " COMATOSE";
                                                    }
                                                    else{
                                                        echo "";
                                                    }

                                                }
                                            ?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><strong>Feeding      :</strong><?php
                                                if (!empty($health_status[0]['feeding'])) {
                                                    if ($health_status[0]['feeding']==1) {
                                                        echo " FEEDS OWN SELF ";
                                                    }
                                                    elseif ($health_status[0]['feeding']==2) {
                                                        echo " NEEDS HELP TO FEED ";
                                                    }
                                                    elseif ($health_status[0]['feeding']==3) {
                                                        echo " TUBE FEEDING ";
                                                    }
                                                    elseif ($health_status[0]['feeding']==4) {
                                                        echo " OTHERS ";
                                                    }
                                                    else{
                                                        echo "";
                                                    }

                                                }
                                            ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Allergies    :</strong><?php
                                                if (!empty($health_status[0]['allergies'])) {
                                                    if ($health_status[0]['allergies']==1) {
                                                        echo " None";
                                                    }
                                                    elseif ($health_status[0]['allergies']==2) {
                                                        echo " DRUGS -FHN (+5)";
                                                    }
                                                    elseif ($health_status[0]['allergies']==3) {
                                                        echo " FOOD -FHN (+5)";
                                                    }
                                                    elseif ($health_status[0]['allergies']==4) {
                                                        echo " OTHERS – FHN (+5)";
                                                    }
                                                    else{
                                                        echo "";
                                                    }
                                                }

                                                ?>

                                            </td>
                                        </tr>
                                    </tbody>

                                </table>
                                </pre>
                                <br>
                                <pre>
                                <table class="table row table-borderless w-100 m-0 border">
                                    <h4>Items available at my residence </h4>
                                    <tbody class="col-lg-6 p-0">
                                        <?php
                                            if(!empty($patient_residence_items_list)) {
                                            $i = 1;

                                            $countRecords = count($patient_residence_items_list);

                                            $col1 = array_slice($patient_residence_items_list, 0, $countRecords/2 + 0.5);

                                            $col2 = array_slice($patient_residence_items_list, $countRecords/2 + 0.5, $countRecords);

                                            $row = array("col1" => $col1, "col2" => $col2);


                                            foreach ($row['col1'] as $key => $value) {

                                                echo "<tr>
                                                        <td><strong>".$i.".".$value['item_name']."</strong>
                                                        <span>".(in_array($value['id'], $patient_residence_items) ? ": Yes" : ": No")."</span>
                                                        </td>
                                                     </tr>";
                                                $i++;
                                                }

                                        ?>
                                    </tbody>
                                    <tbody class="col-lg-6 p-0">
                                        <?php

                                            foreach ($row['col2'] as $key => $value) {

                                                echo "<tr>
                                                        <td><strong>".$i.".".$value['item_name']."</strong>
                                                        <span>".(in_array($value['id'], $patient_residence_items) ? ": Yes" : ": No")."</span>
                                                        </td>
                                                     </tr>";
                                                $i++;
                                                }
                                            }

                                        ?>
                                    </tbody>
                                </table>
                              </pre>
                            </div>
        </div>
    </div>



            <!-- <div class="col-md-12">
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a large text here ..."></textarea>
            </div> -->

    </form>

                </div>
                <div class="tab-pane fade <?=$this->session->flashdata('active')=='experience' ? 'active show' : ''?>" id="tabs-experience" role="tabpanel" aria-labelledby="tabs-experience">
                    <form action="nurse/update_experience/<?=$patient_details[0]['loginid']?>" method="post" enctype='multipart/form-data'>
                       <div class="table-responsive mb-3">
                                <table class="table table-bordered new_table w-100">
                                    <thead>
                                        <tr>
                                            <th>Booking ID</th>
                                            <th>Nurse Name</th>

                                            <th>Booking Date</th>
                                            <th>Booking Hour</th>
                                            <th>Charge</th>
                                            <th>Review</th>
                                            <th>Notes</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody class="col-lg-12 p-0">
                                        <?php
                                        $ol=0;
foreach ($booking_details_new as $key => $value) {
    $book_status=$value['book_status'];
    if($book_status==1)
    {
        $bt="New";
    }
    elseif($book_status==2)
    {
        $bt="Approved (Nurse)";
    }
    elseif($book_status==3)
    {
        $bt="Completed (Nurse)";
    }
    elseif($book_status==4)
    {
        $bt="Patient Review";
    }
    elseif($book_status==5)
    {
        $bt="Finish";
    }
    elseif($book_status==6)
    {
        $bt="Complete";
    }
    $ol+=1;

    ?>
                                           <tr>
                                              <td>#<?php echo $ol?></td>
                                            <td><?=$value['nurse_name']?></td>
                                           <td><?=$value['fromtime']?> - <?=$value['totime']?></td>
                                            <td><?=$value['time_in_hours']?></td>
                                            <td>RM <?=$value['charge'];?></td>
                                            <td><a href="add-review/1" class="btn btn-primary">Review</a></td>
                                            <td><?=$value['notes'];?></td>

                                            <td>
                                               <div class="text-success"><?php echo $bt?></div>

                                            </td>
                                      </tr>
    <?php
}

                                        ?>

                                    </tbody>
                                </table>
                            </div>
                    </form>
                </div>
                <div class="tab-pane fade <?=$this->session->flashdata('active')=='qualification' ? 'active show' : ''?>" id="tabs-qualification" role="tabpanel" aria-labelledby="tabs-qualification">
    <form action="patient/care_for_patient_update/<?=!empty($patient_details[0]['loginid']) ? $patient_details[0]['loginid'] : ''?>" method="post">
    <div class="row">
    <?php if($this->session->flashdata('msg')){ ?>
        <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert-message">
                <span><?=$this->session->flashdata('msg');?></span>
            </div>
        </div>
    <?php } ?>
    <div class="col-md-12">
        <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">

<!-- <form action="" method="post"> -->
   <fieldset class="biodata">
    <!-- <h4>Health Status</h4> -->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group form-elements">
                            <div class="form-label">Accompaniment</div>
                            <div class="custom-controls-stacked">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" disabled name="accompaniment[]" value="1" <?=in_array(1, $care_for_patient_accompany) ? 'checked' : ''; ?>>
                                    <span class="custom-control-label">At Home</span>
                                </label>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" disabled name="accompaniment[]" value="2" <?=in_array(2, $care_for_patient_accompany)? 'checked' : ''; ?>>
                                    <span class="custom-control-label">To Doctors Appoinment</span>
                                </label>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" disabled name="accompaniment[]" value="3" <?=in_array(3, $care_for_patient_accompany)? 'checked' : ''; ?>>
                                    <span class="custom-control-label">Outing / Others</span>
                                </label>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" disabled name="accompaniment[]" value="4" <?=in_array(4, $care_for_patient_accompany) ? 'checked' : ''; ?>>
                                    <span class="custom-control-label">With Transport(Grab)</span>
                                </label>
                            </div>
                        </div>
                </div>
                <div class="col-md-6">
                     <div class="form-label">
                                        Other Care
                                    </div>
                                    <div class="form-group form-elements">
                                    <div class="custom-controls-stacked">
                                        <?php
                                            foreach ($care_for_patient_other_details as $key => $value) :  ?>

                                            <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" disabled name="care_for_patient[]" value="<?=$value['id']?>" <?=(in_array($value['id'], $care_for_patient_others) ? "checked" : null); ?>/>
                                            <span class="custom-control-label">
                                                <?=$value['details'];?>
                                            </span>
                                            </label>

                                            <?php endforeach; ?>

                                        </div>
                                    </div>
                </div>
   <!--              <div class="col-md-12">
                    <button class="btn btn-primary" type="submit" name="submit">Submit</button>
                </div> -->
            </div>
        </fieldset>

                </div>




            </div>
        </div>
    </div>
</div>
</div>

    <!-- Right-sidebar-->
    <?php $this->load->view('backend/right_sidebar');?>
    <!-- End Rightsidebar-->

    <?php $this->load->view('backend/footer');?>
</div>
<!-- End app-content-->
</div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>



<?php $this->load->view('backend/footer_link');?>

<script>
// function capitalize(str) {
//   strVal = '';
//   str = str.split(' ');
//   for (var chr = 0; chr < str.length; chr++) {
//     strVal += str[chr].substring(0, 1).toUpperCase() + str[chr].substring(1, str[chr].length) + ' '
//   }
//   return strVal
// }
// $('#datereg').datepicker({uiLibrary: 'bootstrap4'});
// $('#start').datepicker({uiLibrary: 'bootstrap4'});
// $('#end').datepicker({uiLibrary: 'bootstrap4'});

$('#datereg').Zebra_DatePicker();
$('#start').Zebra_DatePicker();
$('#end').Zebra_DatePicker();
</script>

<script>
let pass = document.querySelector('#password');
let pass2 = document.querySelector('#passconf');

pass2.onkeyup = () => {
if (pass.value != pass2.value ) {
document.getElementById("alert_pass").innerHTML =
`<div class="alert alert-danger alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert">&times;</button><div class="alert-message">
    <span>Password Don't Match!</span>
</div>
</div>`;

document.getElementById("submitBtn").disabled = true;
}
else{
document.getElementById("alert_pass").innerHTML = '';
document.getElementById("submitBtn").disabled = false;
}
}
// $('.checkbox').change(function(){
//     alert('hi');
// });

</script>


<script>
let template = document.querySelectorAll('template')[0];
let additional_exp = document.getElementById('additional_exp');
let count = 1;

removeEvent(document, 'click', '.remove_exp',removeItem);
addEvent(document, 'click', '.add_exp', addItem);



function addEvent(element, event, selector,callback){
element.addEventListener(event, e =>{
if (e.target.matches(selector)) {
    if (count < 10) {
        count++;
        addItem(count);
        console.log(count);
        $('#start_'+count).datepicker({uiLibrary: 'bootstrap4'});
        $('#end_'+count).datepicker({uiLibrary: 'bootstrap4'});
    }
    else{
        alert('Sorry can\'t add more!');
    }
    }
})
}

function removeEvent(element, event, selector, callback){
element.addEventListener(event, e => {
if (e.target.matches(selector)) {
    removeItem(e)
}
})
}

function addItem(count){
additional_exp.innerHTML += `<div class="row-target">
                    <h4 class="" style="line-height:2.7em">
                        Nursing Experience
                        <span class="add_exp float-right btn btn-primary">Add+</span>
                        <span  data-row-target='.row-target' class="remove_exp float-right btn btn-danger">Remove-</span>
                    </h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 text-left">
                                        <label for="organisation">Organisation</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control"  name="organisation[]" placeholder="Organisation" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 text-left">
                                        <label for="start_period">Start Date</label>
                                    </div>
                                    <div class="col-md-6">
                                    <input class="form-control add_start" style="width:100%" name="start_date[]" id="start_${count}" placeholder="MM/DD/YYYY" type="text" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 text-left">
                                        <label for="start_period">End Date</label>
                                    </div>
                                    <div class="col-md-6">
                                    <input class="form-control add_end" name="end_date[]" id="end_${count}" placeholder="MM/DD/YYYY" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 text-left">
                                        <label for="title">Title</label>
                                    </div>
                                    <div class="col-md-9">
                                    <input type="text" class="form-control" id="title" name="title[]" placeholder="Title" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 text-left">
                                        <label for="desc_of_duties">Description of duties</label>
                                    </div>
                                    <div class="col-md-9">
                                    <textarea class="form-control" id="desc_of_duties" name="duty_description[]" cols="30" rows="1">Description of duties
                                    </textarea>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 text-left">
                                        <label for="references">References</label>
                                    </div>
                                    <div class="col-md-9">
                                    <input type="text" class="form-control" id="references" name="ref[]" placeholder="References" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 text-left">
                                        <label for="desc_of_duties">Trainings Attended</label>
                                    </div>
                                    <div class="col-md-9">
                                    <input type="text" class="form-control" id="training_attended" name="training_attended[]" cols="30" rows="1" placeholder="Trainings Attended">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`;
}

function removeItem(params) {
let buttonClicked = params.target
buttonClicked.closest(buttonClicked.dataset.rowTarget).remove()
count--;
}


</script>


<script>
// let otherComp = document.querySelector('#others_comp');
// let otherCompBox = document.getElementById('other_comp_box');
// let checkbox = document.querySelectorAll('.checkbox');
// let selectBox = document.querySelectorAll('.select-box');

// otherComp.addEventListener('change', () => {
//   if(otherComp.checked){
//     otherCompBox.style.display = 'block';
//   }else{
//     otherCompBox.style.display = 'none';
//   }
// });



// for (let i = 0; i < checkbox.length; i++) {
//   checkbox[i].addEventListener('change', () => {
//   if(checkbox[i].checked){
//     selectBox[i].style.display = 'block';
//   }else{
//     selectBox[i].style.display = 'none';
//   }
// });
// }

</script>
</body>
</html>
