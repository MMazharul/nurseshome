<?php $this->load->view('backend/head_link');?>


<style>
    @media (max-width: 480px) {
    .nav-wrapper {
    display: block;
    /*overflow: hidden;*/
    height: calc(1.5rem + 1rem + 2px); /** 1.5 is font-size, 1 is padding top and bottom, 2 is border width top and bottom */
    position: relative;
    z-index: 1;
    margin-bottom: -1px;
    }
    .nav-pills {
        overflow-x: auto;
        flex-wrap: nowrap;
        border-bottom: 0;
    }
     .nav-item {
        margin-bottom: 0;
        min-width: 12em !important;
    }
   /* .nav-item :first-child {
        padding-left: 15px;
    }
    .nav-item :last-child {
        padding-right: 15px;
    }*/
    .nav-link {
        white-space: nowrap;
        /*min-width: 5em !important;*/
    }
    .dragscroll:active,
    .dragscroll:active a {
        cursor: -webkit-grabbing;
    }

    }

</style>

<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
<img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
<div class="page-main">
<!--app-header-->
<?php $this->load->view('backend/header');?>
<!-- app-content-->
<div class="container content-patient">
<div class="side-app">
<!-- page-header -->
<!-- <div class="page-header">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
</ol>
<div class="ml-auto">
<div class="input-group">
    <a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
        <span>
            <i class="fa fa-calendar"></i> Events Settings
        </span>
        <i class="fa fa-caret-down"></i>
    </a>
    <a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
        <span>
            <i class="fa fa-star"></i>
        </span>
    </a>
</div>
</div>
</div> -->
<!-- End page-header -->

<div class="row">
<div class="col-md-12 col-lg-12">
<div class="card">
<div class="card-header">
    <div class="col-md-10">
    <div class="card-title">Assesement Details by <?php 
                        if(isset($patient_details[0]) && !empty($patient_details[0])){
                            echo $patient_details[0]['first_name']." ".$patient_details[0]['last_name'];
                        }?> 
                   <span style="font-size: 14px">(<?=date_format(date_create($assessment_details[0]['created_at']),'d-m-Y')?>)</span>
               </div>
    </div>
    <!--<div class="col-md-2">
    <a href="nurse/nurse_list" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
        <span> Nurse List
        </span>
    </a>
    </div> -->
</div>
<div class="card-body">
    <form action="nurse/register_user_as_nurse" method="post">
    <div class="row">
    <?php if($this->session->flashdata('msg')) { ?>
        <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert-message">
                <span><?=$this->session->flashdata('msg');?></span>
            </div>
        </div>
    <?php } ?> 
    <div class="col-md-12">
        <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          
            <div class="card">
                <div class="card-body pb-0">
                   <?php if ($row_count > 0) { ?>
                        <fieldset class="biodata">
                        <!-- <h4>Nutrition</h4> -->
                            <table class="table table-bordered">
                              <tr>
                                <th><strong>Eating Disorder</strong></th>
                                <th><?php 
                                  if (!empty($assessment_details[0]['eating_disorder'])) {
                                    if ($assessment_details[0]['eating_disorder'] == 1) {
                                       echo "Nil";
                                    }elseif ($assessment_details[0]['eating_disorder'] == 2) {
                                      echo "Malnourished";
                                    }
                                    elseif ($assessment_details[0]['eating_disorder'] == 3) {
                                      echo "Obese";
                                    }
                                  };
                                  ?></th>
                              </tr>

                              <tr>
                                <th><strong>Diet Type</strong></th>
                                <th><?php 
                                  if (!empty($assessment_details[0]['diet_type'])) {
                                    if ($assessment_details[0]['diet_type'] == 1) {
                                       echo "Regular";
                                    }elseif ($assessment_details[0]['diet_type'] == 2) {
                                      echo "Theraputic";
                                    }
                                    elseif ($assessment_details[0]['diet_type'] == 3) {
                                      echo "Supplement (Endsure etc)";
                                    }
                                    elseif ($assessment_details[0]['diet_type'] == 4) {
                                      echo "Parental (TPN)";
                                    }
                                  };
                                  ?></th>
                              </tr>

                              <tr>
                                <th><strong>Musculosketel</strong></th>
                                <th><?php 
                                  if (!empty($assessment_details[0]['musculosketel'])) {
                                    if ($assessment_details[0]['musculosketel'] == 1) {
                                       echo "Normal";
                                    }elseif ($assessment_details[0]['musculosketel'] == 2) {
                                      echo "Paralysis-Free hand notes";
                                    }
                                    elseif ($assessment_details[0]['musculosketel'] == 3) {
                                      echo "Joint Swelling-Free hand notes";
                                    }
                                    elseif ($assessment_details[0]['musculosketel'] == 4) {
                                      echo "Pain-Free hand notes";
                                    }
                                    elseif ($assessment_details[0]['musculosketel'] == 5) {
                                      echo "Others-Free hand notes";
                                    }
                                  };
                                  ?></th>
                              </tr>

                               <tr>
                                <th><strong>Genitallia</strong></th>
                                <th><?php 
                                  if (!empty($assessment_details[0]['genitallia'])) {
                                    if ($assessment_details[0]['genitallia'] == 1) {
                                       echo "Normal";
                                    }elseif ($assessment_details[0]['genitallia'] == 2) {
                                      echo "Others-FHN";
                                    }
                                  };
                                  ?></th>
                              </tr>

                               <tr>
                                <th><strong>Vascular Access</strong></th>
                                <th><?php 
                                  if (!empty($assessment_details[0]['vascular_access'])) {
                                    if ($assessment_details[0]['vascular_access'] == 1) {
                                       echo "Normal";
                                    }elseif ($assessment_details[0]['vascular_access'] == 2) {
                                      echo "Others-FHN";
                                    }
                                  };
                                  ?></th>
                              </tr>
                               <tr>
                                <th><strong>Fall Risk</strong></th>
                                <th><?php 
                                  if (!empty($assessment_details[0]['fall_risk'])) {
                                    if ($assessment_details[0]['fall_risk'] == 1) {
                                       echo "No Risk";
                                    }elseif ($assessment_details[0]['fall_risk'] == 2) {
                                      echo "At Risk";
                                    }elseif ($assessment_details[0]['fall_risk'] == 3) {
                                      echo "High Risk";
                                    }
                                  };
                                  ?></th>
                              </tr>

                               <tr>
                                <th><strong>Fall Risk Details</strong></th>
                                <th><?=!empty($assessment_details[0]['fall_history']) ? $assessment_details[0]['fall_history']  : ''?></th>
                              </tr>


                               <tr>
                                <th><strong>Walking Device</strong></th>
                                <th><?=!empty($assessment_details[0]['walking_device']) ? $assessment_details[0]['walking_device'] : ''?></th>
                              </tr>

                               <tr>
                                <th><strong>Urine Colour</strong></th>
                                <th><?php 
                                  if (!empty($assessment_details[0]['urine_colour'])) {
                                    if ($assessment_details[0]['urine_colour'] == 1) {
                                       echo "Clear";
                                    }elseif ($assessment_details[0]['urine_colour'] == '2_1') {
                                      echo "Cloudy";
                                      if (!empty($assessment_details[0]['cloudy_sub'])) {
                                          if ($assessment_details[0]['cloudy_sub'] ==1) {
                                            echo "<small>\n\r(Dark)</small>";
                                          }elseif ($assessment_details[0]['cloudy_sub'] ==2) {
                                            echo "<small>\n\r(Hemanturia)</small>";
                                            
                                          }
                                      }
                                    }elseif ($assessment_details[0]['urine_colour'] == 3) {
                                      echo "Others (Endsure etc)";
                                    }elseif ($assessment_details[0]['urine_colour'] == 4) {
                                      echo "Normal Voiding";
                                    }
                                  };
                                  ?></th>
                              </tr>

                              <tr>
                                <th><strong>Incontinent/Dysuria/Catheter</strong></th>
                                <th><?php 
                                  if (!empty($assessment_details[0]['incontinent_dysuria_catheter'])) {
                                    if ($assessment_details[0]['incontinent_dysuria_catheter'] == 1) {
                                       echo "FHN";
                                    }elseif ($assessment_details[0]['incontinent_dysuria_catheter'] == 2) {
                                      echo "Dialysis";
                                    }elseif ($assessment_details[0]['incontinent_dysuria_catheter'] == 3) {
                                      echo "Others-FHN";
                                    }
                                  };
                                  ?></th>
                              </tr>

                              <tr>
                                <th><strong>Home Assessment</strong></th>
                                <th><?=!empty($assessment_details[0]['home_assessment'])? trim($assessment_details[0]['home_assessment']): ''?></th>
                              </tr>
                            </table>   
                            <div class="row">
                            <div class="col-md-12">

                                
                                
                                </div>

                            </div>
                        <!--<h4>Genitourinary</h4> -->
                        </fieldset>
                        <?php }else{ ?>


                            <table class="table table-bordered">
                              <tbody><tr>
                                <th><strong>Eating Disorder</strong></th>
                                <th></th>
                              </tr>

                              <tr>
                                <th><strong>Diet Type</strong></th>
                                <th></th>
                              </tr>

                              <tr>
                                <th><strong>Musculosketel</strong></th>
                                <th></th>
                              </tr>

                               <tr>
                                <th><strong>Genitallia</strong></th>
                                <th></th>
                              </tr>

                               <tr>
                                <th><strong>Vascular Access</strong></th>
                                <th></th>
                              </tr>
                               <tr>
                                <th><strong>Fall Risk</strong></th>
                                <th></th>
                              </tr>

                               <tr>
                                <th><strong>Fall Risk Details</strong></th>
                                <th></th>
                              </tr>


                               <tr>
                                <th><strong>Walking Device</strong></th>
                                <th></th>
                              </tr>

                               <tr>
                                <th><strong>Urine Colour</strong></th>
                                <th></th>
                              </tr>

                              <tr>
                                <th><strong>Incontinent/Dysuria/Catheter</strong></th>
                                <th></th>
                              </tr>

                              <tr>
                                <th><strong>Home Assessment</strong></th>
                                <th></th>
                              </tr>
                            </tbody></table>

                        <?php } ?>
                </div>
            </div>
        </div>
    </div>
    

            
            <!-- <div class="col-md-12">
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a large text here ..."></textarea>
            </div> -->
    
    </form>
</fieldset>
<!-- table-wrapper -->
</div>
<!-- section-wrapper -->
</div>
</div>

</div><!--End side app-->

<!-- Right-sidebar-->
<?php $this->load->view('backend/right_sidebar');?>
<!-- End Rightsidebar-->

<?php $this->load->view('backend/footer');?>
</div>
<!-- End app-content-->
</div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>
  <script type="text/javascript">
    let dataTable = $(".new_table").dataTable(
      {  "bLengthChange" : false,
           "responsive": true,
          "oLanguage": 
        {
            "sSearch": ""
        }
      });
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust()
           .responsive.recalc();
        });    
      
</script>

</body>
</html>