<?php $this->load->view('backend/head_link');?>
<body class="app sidebar-mini rtl">
   <style>
      .select-box{
      /*display: none;*/
      }
      .competency td:first-child{
      width: 50%;
      }
   </style>
   <!--Global-Loader-->
   <!-- <div id="global-loader">
      <img src="back_assets/images/icons/loader.svg" alt="loader">
      </div> -->
   <div class="page">
   <div class="page-main">
   <!--app-header-->
   <?php $this->load->view('backend/header');?>
   <!-- app-content-->
   <div class="container content-patient">
   <div class="side-app">
   <!-- page-header -->
   <!-- End page-header -->
   <?php if($this->session->flashdata('msg')){ ?>
   <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <div class="alert-message">
         <span><?=$this->session->flashdata('msg');?></span>
      </div>
   </div>
   <?php } ?>
   <div class="row">
   <div class="col-md-12">
      <?php
         $fname=$patient_details[0]['first_name'];
         $lname=$patient_details[0]['last_name'];
         $name=$fname." ".$lname;
             ?>
      <div class="card card-profile  overflow-hidden">
         <div class="card-body text-center profile-bg">
            <div class=" card-profile">
               <div class="row justify-content-center">
                  <div class="">
                     <div class="">
                        <a href="#">
                        <img src="uploads/<?=$patient_details[0]['profile_pic'];?>" class="avatar-xxl rounded-circle" alt="profile">
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            <h3 class="mt-3 text-white"><?=$patient_details[0]['first_name']." ".$patient_details[0]['last_name'];?></h3>
            <p class="mb-2 text-white"><?=$patient_details[0]['user_role']== 3 ? 'Nurse' : ($patient_details[0]['user_role'] == 4 ? 'Patient' : '') ?></p>
            <!--  <div class="text-center mb-4">
               <span><i class="fa fa-star text-warning"></i></span>
               <span><i class="fa fa-star text-warning"></i></span>
               <span><i class="fa fa-star text-warning"></i></span>
               <span><i class="fa fa-star-half-o text-warning"></i></span>
               <span><i class="fa fa-star-o text-warning"></i></span>
               </div> -->
            <!-- <a href="patient/patient_details/<?=$patient_details[0]['loginid']?>" class="btn btn-info btn-sm"><i class="fas fa-pencil-alt" aria-hidden="true"></i>Edit profile</a> -->
         </div>
         <div class="card-body">
            <div class="nav-wrapper p-0">
               <ul class="nav nav-pills dragscroll horizontal nav-fill flex-row" id="tabs-icons-text" role="tablist">
                  <li class="nav-item">
                     <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0 <?=$this->session->flashdata('active')=='' ? 'active show' : '' ?>" id="profile" data-toggle="tab" href="#tabs-profile" role="tab" aria-controls="profile" aria-selected="true"><i class="fa fa-home mr-2"></i>Account</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0 <?=$this->session->flashdata('active')=='biodata' ? 'active show' : ''?>" id="bio" data-toggle="tab" href="#tabs-bio" role="tab" aria-controls="bio" aria-selected="false"><i class="fa fa-user mr-2"></i>My Condition</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0 <?=$this->session->flashdata('active')=='qualification' ? 'active show' : ''?>" id="qualification" data-toggle="tab" href="#tabs-qualification" role="tab" aria-controls="tabs-qualification" aria-selected="false"><i class="fa fa-picture-o mr-2"></i>My Care Needs</a>
                  </li>
                  <!--                     <li class="nav-item">
                     <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0 <?=$this->session->flashdata('active') == 'experience' ? 'active show' : ''?>" id="experience" data-toggle="tab" href="#tabs-experience" role="tab" aria-controls="experience" aria-selected="false"><i class="fa fa-newspaper-o mr-2 mt-1"></i>Booking </a>
                     </li> -->
                  <!--     <li class="nav-item">
                     <a class="nav-link mb-sm-0 mb-md-0 mt-md-2 mt-0 mt-lg-0 <?=$this->session->flashdata('active')=='competency' ? 'active show' : ''?>" id="competency" data-toggle="tab" href="#tabs-competency" role="tab" aria-controls="competency" aria-selected="false"><i class="fa fa-cog mr-2"></i>Review</a>
                     </li> -->
               </ul>
            </div>
         </div>
      </div>
      <div class="card">
         <div class="card-body pb-0">
            <div class="tab-content" id="myTabContent">
               <div class="tab-pane fade <?=$this->session->flashdata('active')=='' ? 'active show' : ''?>" id="tabs-profile" role="tabpanel" aria-labelledby="profile">
                  <div class="row">
                     <div class="col-md-12">
                        <form  name="accountForm"  action="patient/insert_patient_details" method="post" enctype="multipart/form-data" onsubmit="return checkForm()">
                           <!-- <?php  echo form_open('patient/insert_patient_details', 'class="email" method="post"  enctype="multipart/form-data" id="myform"');?> -->
                           <input type="hidden" name="id" value="<?=$this->session->userdata('loginid')?>">
                           <div class="row">
                              <div class="col-md-6">
                                 <h4 class="" style="line-height:2.7em">Account Information</h4>
                              </div>
                              <div class="col-md-6">
                                 <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                    <div class="">
                                       <!--    <button class="btn btn-primary" type="submit" name="save" id="save_contact">Save</button> -->
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <fieldset>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-md-3 text-right">
                                             <label for="firstname">First Name</label>
                                          </div>
                                          <div class="col-md-9">
                                             <input name="firstname" type="text" value="<?=!empty($patient_details_users[0]['first_name']) ? $patient_details_users[0]['first_name'] : ''?>" class="form-control" required placeholder="First Name" required="">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="row">
                                       <div class="col-md-3 text-right">
                                          <label for="lastname">Last Name</label>
                                       </div>
                                       <div class="col-md-9">
                                          <input name="lastname" value="<?=!empty($patient_details_users[0]['last_name']) ? $patient_details_users[0]['last_name'] : ''?>" type="text" class="form-control" required placeholder="Last Name" required="">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-md-3 text-right">
                                             <label for="email">E-mail</label>
                                          </div>
                                          <div class="col-md-9">
                                             <input type="email" value="<?=!empty($patient_details_users[0]['email']) ? $patient_details_users[0]['email'] : ''?>" class="form-control" id="email" name="email" placeholder="Email"required="">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="row">
                                       <div class="col-md-3 text-right">
                                          <label for="contact">Contact</label>
                                       </div>
                                       <div class="col-md-9">
                                          <div class="input-group mb-3">
                                             <input style='width:2rem;text-align:center;background:#ccc;color:#111; outline:0;border:0' value="<?=!empty($patient_details_users[0]['country_code']) ? $patient_details_users[0]['country_code'] : ''?>" type="text" name="country_code" readonly value="+60">
                                             <input name="contact" value="<?=!empty($patient_details_users[0]['contact_no']) ? $patient_details_users[0]['contact_no'] : ''?>" type="text" class="form-control"  placeholder="Contact no">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-md-3 text-right">
                                             <label for="id">Gender</label>
                                          </div>
                                          <div class="col-md-9">
                                             <select name="gender" class="form-control" >
                                                <option value="1" <?=$patient_details_users[0]['gender'] == 1 ? 'selected' : '' ;?>  >Male</option>
                                                <option value="2" <?=$patient_details_users[0]['gender'] == 2 ? 'selected' : '' ;?> >Female</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-md-3 text-left">
                                             <label for="id">Update Profile Photo</label>
                                          </div>
                                          <div class="col-md-6">
                                             <input type="file" class="dropify" data-default-file="uploads/<?=!empty($patient_details_users[0]['profile_pic']) ? $patient_details_users[0]['profile_pic'] : '';?>"
                                                name="bio_file">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </fieldset>
                           <div class="row">
                              <div class="col-md-6">
                                 <h4 class="" style="line-height:2.7em">Bio Data</h4>
                              </div>
                              <div class="col-md-6">
                                 <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                    <div class="">
                                       <!--    <button class="btn btn-primary" type="submit" name="save" id="save_contact">Save</button> -->
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <fieldset>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-md-3 text-right">
                                             <label for="nrc_passport_id">IC/Passport NUmber</label>
                                          </div>
                                          <div class="col-md-9">
                                             <input type="text" class="form-control" id="nrc_passport_id" name="nrc_passport_id"  placeholder="NRC/PASSPORT" value="<?=!empty($patient_details_info[0]['nrc_passport_id']) ? $patient_details_info[0]['nrc_passport_id'] : ''?>">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-md-3 text-right">
                                             <label for="gender">Nationality</label>
                                          </div>
                                          <div class="col-md-9">
                                             <select name="gender" class="form-control" id="gender">
                                                <option value="1">Malaysian </option>
                                                <option value="2">Non-Malaysian</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-md-3 text-right">
                                             <label for="weight">Weight (kg)</label>
                                          </div>
                                          <div class="col-md-9">
                                             <input type="text" class="form-control" id="weight" name="weight"  placeholder="Weight" value="<?=!empty($patient_details_info[0]['weight']) ? $patient_details_info[0]['weight'] : ''?>">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-md-3 text-right">
                                             <label for="current_care_status ">Age</label>
                                          </div>
                                          <div class="col-md-9">
                                             <input class="form-control" type="number" name="age" value="<?=!empty($patient_details_info[0]['age']) ? $patient_details_info[0]['age'] : ''?>">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-md-3 text-right">
                                             <label for="height">Height (cm)</label>
                                          </div>
                                          <div class="col-md-9">
                                             <input type="text" class="form-control" id="height" name="height"  placeholder="Height" value="<?=!empty($patient_details_info[0]['height']) ? $patient_details_info[0]['height'] : ''?>">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </fieldset>
                           <div class="row">
                              <div class="col-md-6">
                                 <h4 class="" style="line-height:2.7em">Patient Location Details</h4>
                              </div>
                              <div class="col-md-6">
                                 <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                    <div class="">
                                       <!-- <button class="btn btn-primary" type="submit" name="save" id="save_bio">Save</button> -->
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <fieldset>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-md-3 text-right">
                                             <label for="address">Address</label>
                                          </div>
                                          <div class="col-md-9">
                                             <textarea class="form-control" id="address" name="address" cols="30" rows="1" ><?=!empty($patient_details_info[0]['address']) ? $patient_details_info[0]['address'] : ''?><?php echo set_value('address'); ?></textarea>
                                             <span class="alert-danger"><?php echo form_error('address'); ?> </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-md-3 text-right">
                                             <label for="gender">Type of Resident</label>
                                          </div>
                                          <div class="col-md-9">
                                             <select name="resident_type" class="form-control" id="" required>
                                                <option disabled selected="true">Select</option>
                                                <option value="1" <?=!empty($patient_details_info[0]['resident_type']) && $patient_details_info[0]['resident_type'] == 1 ? 'selected' : '';?>>Landed House</option>
                                                <option value="2" <?=!empty($patient_details_info[0]['resident_type']) && $patient_details_info[0]['resident_type'] == 2 ? 'selected' : '';?>>Multi Storey Residence with lift</option>
                                                <option value="3" <?=!empty($patient_details_info[0]['resident_type']) &&$patient_details_info[0]['resident_type'] == 3 ? 'selected' : '';?>>Multi Storey Residence without lift</option>
                                                <option value="4" <?=!empty($patient_details_info[0]['resident_type']) &&$patient_details_info[0]['resident_type'] == 4 ? 'selected' : '';?>>Nursing Home/Hostpital</option>
                                             </select>
                                             <span class="alert-danger"><?php echo form_error('resident_type'); ?> </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-md-3 text-right">
                                             <label for="security_type">Type of Scurity</label>
                                          </div>
                                          <div class="col-md-9">
                                             <select name="security_type" class="form-control" id="security_type">
                                                <option disabled selected="true">Select</option>
                                                <option value="1" <?php echo set_select('security_type', '1', TRUE);?><?=!empty($patient_details_info[0]['security_type']) && $patient_details_info[0]['security_type'] == 1 ? 'selected' : '';?>>Gated & Guarded</option>
                                                <option value="2" <?php echo set_select('security_type', '2', TRUE);?><?=!empty($patient_details_info[0]['security_type']) && $patient_details_info[0]['security_type'] == 2 ? 'selected' : '';?>>Gated</option>
                                                <option value="3" <?php echo set_select('security_type', '3', TRUE);?><?=!empty($patient_details_info[0]['security_type']) && $patient_details_info[0]['security_type'] == 3 ? 'selected' : '';?>>Restricted Access</option>
                                                <option value="4" <?php echo set_select('security_type', '4', TRUE);?><?=!empty($patient_details_info[0]['security_type']) && $patient_details_info[0]['security_type'] == 4 ? 'selected' : '';?>>None</option>
                                             </select>
                                             <span class="alert-danger"><?php echo form_error('security_type'); ?> </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-md-3 text-right">
                                             <label for="type_of_security">Living Status</label>
                                          </div>
                                          <div class="col-md-9">
                                             <select name="living_status" class="form-control" id="living_status">
                                                <option disabled selected="true">Select</option>
                                                <option value="1" <?php echo set_select('living_status', '1', TRUE);?> <?=!empty($patient_details_info[0]['living_status'] ) && $patient_details_info[0]['living_status'] == 1 ? 'selected' : '';?>>Alone</option>
                                                <option value="2" <?php echo set_select('living_status', '2', TRUE);?> <?=!empty($patient_details_info[0]['living_status'] ) && $patient_details_info[0]['living_status'] == 2 ? 'selected' : '';?>>Family</option>
                                             </select>
                                             <span class="alert-danger"><?php echo form_error('living_status'); ?> </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-md-3 text-right">
                                             <label for="pets_in_house">Pets In House</label>
                                          </div>
                                          <div class="col-md-9">
                                             <select name="pets_in_house" class="form-control" id="gender">
                                                <option value="1" <?=!empty($patient_details_info[0]['pets_in_house']) && $patient_details_info[0]['pets_in_house'] == 1 ? 'selected' : '';?>>Dog</option>
                                                <option value="2" <?=!empty($patient_details_info[0]['pets_in_house']) && $patient_details_info[0]['pets_in_house'] == 2 ? 'selected' : '';?>>Cat</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-md-3 text-right">
                                             <label for="current_living_condition">Current Living Condition</label>
                                          </div>
                                          <div class="col-md-9">
                                             <select name="current_living_condition" class="form-control" id="current_living_condition">
                                                <option disabled selected>Select</option>
                                                <option value="1" <?php echo  set_select('current_living_condition', '1', TRUE); ?> <?=!empty($patient_details_info[0]['current_living_condition']) && $patient_details_info[0]['current_living_condition'] == 1 ? 'selected' : '';?>>Private room with ac with attached bathroom</option>
                                                <option value="2" <?php echo  set_select('current_living_condition', '2', TRUE); ?> <?=!empty($patient_details_info[0]['current_living_condition']) && $patient_details_info[0]['current_living_condition'] == 2 ? 'selected' : '';?>>Private room without ac with attached bathroom</option>
                                                <option value="3" <?php echo  set_select('current_living_condition', '3', TRUE); ?> <?=!empty($patient_details_info[0]['current_living_condition']) && $patient_details_info[0]['current_living_condition'] == 3 ? 'selected' : '';?>>Private room with ac with common bathroom</option>
                                                <option value="4" <?php echo  set_select('current_living_condition', '4', TRUE); ?> <?=!empty($patient_details_info[0]['current_living_condition']) && $patient_details_info[0]['current_living_condition'] == 4 ? 'selected' : '';?>>Private room without ac with common bathroom</option>
                                                <option value="5" <?php echo  set_select('current_living_condition', '5', TRUE); ?> <?=!empty($patient_details_info[0]['current_living_condition']) && $patient_details_info[0]['current_living_condition'] == 5 ? 'selected' : '';?>>Shared Room</option>
                                                <option value="6" <?php echo  set_select('current_living_condition', '6', TRUE); ?> <?=!empty($patient_details_info[0]['current_living_condition']) && $patient_details_info[0]['current_living_condition'] == 6 ? 'selected' : '';?>>Communal Space</option>
                                             </select>
                                             <span class="alert-danger"><?php echo form_error('current_living_condition'); ?> </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-md-3 text-right">
                                             <label for="current_care_status ">Current Care Status</label>
                                          </div>
                                          <div class="col-md-9">
                                             <select name="current_care_status" class="form-control" id="">
                                                <option disabled selected>Select</option>
                                                <option value="1" <?php echo set_select('current_care_status', '1', TRUE);?><?=!empty($patient_details_info[0]['current_care_status']) && $patient_details_info[0]['current_care_status'] == 1 ? 'selected' : '';?>>Self</option>
                                                <option value="2" <?php echo set_select('current_care_status', '2', TRUE);?><?=!empty($patient_details_info[0]['current_care_status']) && $patient_details_info[0]['current_care_status'] == 2 ? 'selected' : '';?>>Family</option>
                                                <option value="3" <?php echo set_select('current_care_status', '3', TRUE);?><?=!empty($patient_details_info[0]['current_care_status']) && $patient_details_info[0]['current_care_status'] == 3 ? 'selected' : '';?>>Caregiver/maid</option>
                                                <option value="4" <?php echo set_select('current_care_status', '4', TRUE);?><?=!empty($patient_details_info[0]['current_care_status']) && $patient_details_info[0]['current_care_status'] == 4 ? 'selected' : '';?>>Nurse</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </fieldset>
                           <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                              <div class="">
                                 <button class="btn btn-primary" type="submit" name="submit">Update</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
               <div aria-labelledby="" class="tab-pane fade <?=$this->session->flashdata('active')=='biodata' ? 'active show' : ''?>" id="tabs-bio" role="tabpanel">
                  <form name="updateCondition" action="patient/update_condition/<?php echo $patient_details[0]['loginid'];?>" onsubmit="return validateForm()" method="post">

                     <!-- <form action="patient/update_condition/<?php //echo $patient_details[0]['loginid'];?>" method="post"> -->
                     <fieldset class="biodata">
                        <h4>Health Status</h4>
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group form-elements">
                                 <div class="form-label">Dependency</div>
                                 <div class="custom-controls-stacked">
                                    <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="dependency" value="1" <?php if (!empty($health_status[0]['dependency']) && $health_status[0]['dependency'] == 1)
                                       echo "checked";
                                       else '';
                                       ?>>
                                    <span class="custom-control-label">Independent - ABLE TO MOVE/FEED/CLEAN OWNSELF
                                    </span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="dependency" value="2" <?php if (!empty($health_status[0]['dependency']) && $health_status[0]['dependency'] == 2)
                                       echo "checked";
                                       else '';
                                       ?>>
                                    <span class="custom-control-label">Semi Dependent -
                                    NEEDS ASSISTANCE IN ONE OR MORE OF MOVE/FEED/CLEAN OWNSELF</span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="dependency" value="3" <?php if (!empty($health_status[0]['dependency']) && $health_status[0]['dependency'] == 3)
                                       echo "checked";
                                       else '';
                                       ?>>
                                    <span class="custom-control-label">Dependent - FULLY DEPENDENT IN ONE OR MORE OF MOVE/FEED/CLEAN OWN SELF</span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="dependency" value="4" <?php if (!empty($health_status[0]['dependency']) && $health_status[0]['dependency'] == 4)
                                       echo "checked";
                                       else '';
                                       ?>>
                                    <span class="custom-control-label">Dependent - FULLY DEPENDENT TO MOVE/FEED/CLEAN OWN SELF</span>
                                    </label>
                                 </div>
                                 <span id="dependency"></span>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group form-elements">
                                 <div class="form-label">Mental State</div>
                                 <div class="custom-controls-stacked">
                                    <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="mental_state" value="1" <?php if (!empty($health_status[0]['dependency']) && $health_status[0]['mental_state'] == 1)
                                       echo "checked";
                                       else ''; ?>
                                       >
                                    <span class="custom-control-label"> AWAKE AND RESPONSIVE </span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="mental_state" value="2" <?php if (!empty($health_status[0]['dependency']) && $health_status[0]['mental_state'] == 2)
                                       echo "checked";
                                       else ''; ?> >
                                    <span class="custom-control-label">AWAKE BUT NON-RESPONSIVE </span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="mental_state" value="3" <?php if (!empty($health_status[0]['dependency']) && $health_status[0]['mental_state'] == 3)
                                       echo "checked";
                                       else ''; ?> >
                                    <span class="custom-control-label"> DROWSY/CONFUSED </span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="mental_state" value="4" <?php if (!empty($health_status[0]['dependency']) && $health_status[0]['mental_state'] == 4)
                                       echo "checked";
                                       else ''; ?>>
                                    <span class="custom-control-label">COMATOSE</span>
                                    </label>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group form-elements">
                                 <div class="form-label">Feeding</div>
                                 <div class="custom-controls-stacked">
                                    <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="feeding" value="1" <?php if (!empty($health_status[0]['feeding']) && $health_status[0]['mental_state'] == 1)
                                       echo "checked";
                                       else ''; ?>>
                                    <span class="custom-control-label"> FEEDS OWN SELF </span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="feeding" value="2" <?php if (!empty($health_status[0]['feeding']) && $health_status[0]['mental_state'] == 2)
                                       echo "checked";
                                       else ''; ?> >
                                    <span class="custom-control-label"> NEEDS HELP TO FEED </span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="feeding" value="3" <?php if (!empty($health_status[0]['feeding']) && $health_status[0]['mental_state'] == 3)
                                       echo "checked";
                                       else ''; ?>>
                                    <span class="custom-control-label" <?php if (!empty($health_status[0]['feeding']) && $health_status[0]['mental_state'] == 4)
                                       echo "checked";
                                       else ''; ?>> TUBE FEEDING </span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="feeding" value="4">
                                    <span class="custom-control-label" <?php if (!empty($health_status[0]['feeding']) && $health_status[0]['mental_state'] == 5)
                                       echo "checked";
                                       else ''; ?>> OTHERS </span>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group form-elements">
                                 <div class="form-label">Allergies</div>
                                 <div class="custom-controls-stacked">
                                    <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="allergies" value="1" <?php if (!empty($health_status[0]['feeding']) && $health_status[0]['allergies'] == 1)
                                       echo "checked";
                                       else ''; ?>>
                                    <span class="custom-control-label"> None </span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="allergies" value="2" <?php if (!empty($health_status[0]['feeding']) && $health_status[0]['allergies'] == 2)
                                       echo "checked";
                                       else ''; ?>>
                                    <span class="custom-control-label">DRUGS -FHN (+5)</span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="allergies" value="3" <?php if (!empty($health_status[0]['feeding']) && $health_status[0]['allergies'] == 3)
                                       echo "checked";
                                       else ''; ?>>
                                    <span class="custom-control-label">FOOD -FHN (+5)</span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="allergies" value="4" <?php if (!empty($health_status[0]['feeding']) && $health_status[0]['allergies'] == 4)
                                       echo "checked";
                                       else ''; ?>>
                                    <span class="custom-control-label">OTHERS – FHN (+5)</span>
                                    </label>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group form-elements">
                                 <div class="form-label">PLEASE INDICATE  ITEMS AVAILABLE AT PATIENTS RESIDENCE (PLS TICK AS AVAILABLE)</div>
                                 <div class="custom-controls-stacked">
                                    <?php
                                       if(!empty($patient_residence_items_list)) {

                                       foreach ($patient_residence_items_list as $key => $value) {
                                           echo "<label class='custom-control custom-checkbox'>
                                               <input type='checkbox' class='custom-control-input' name='patient_res_items[]' value='".$value['id']."'"
                                                   .(in_array($value['id'], $patient_residence_items) ? "checked" : null)."/>
                                               <span class='custom-control-label'>".$value['item_name']."</span>
                                           </label>";

                                       }
                                       }else{
                                           foreach ($patient_residence_items_list as $key => $value) {
                                               echo "<label class='custom-control custom-checkbox'>
                                                   <input type='checkbox' class='custom-control-input' name='patient_res_items[]' value=".$value['id'].">
                                                   <span class='custom-control-label'>".$value['item_name']."</span>
                                               </label>";
                                           }
                                       }




                                       ?>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                              <button class="btn btn-primary" id="my_condition" name="submit">Update</button>
                           </div>
                        </div>
                     </fieldset>
                  </form>
               </div>
               <div class="tab-pane fade <?=$this->session->flashdata('active')=='qualification' ? 'active show' : ''?>" id="tabs-qualification" role="tabpanel" aria-labelledby="tabs-qualification">
                  <form  action="patient/care_for_patient_update/<?=!empty($patient_details[0]['loginid']) ? $patient_details[0]['loginid'] : ''?>"  method="post">

                     <div class="row">
                        <div class="col-md-12">
                           <!-- <form action="" method="post"> -->
                           <fieldset class="biodata">
                  <form  action="patient/care_for_patient_update/<?=!empty($patient_details[0]['loginid']) ? $patient_details[0]['loginid'] : ''?>"   method="post">

                  <div class="row">
                  <div class="col-md-12">
                  <!-- <form action="" method="post"> -->
                  <fieldset class="biodata">
                  <!-- <h4>Health Status</h4> -->
                  <div class="row">
                  <div class="col-md-6">
                  <div class="form-group form-elements">
                  <div class="form-label">Accompaniment</div>
                  <div class="custom-controls-stacked">
                  <label class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" name="accompaniment[]" value="1" <?=in_array(1, $care_for_patient_accompany) ? 'checked' : ''; ?>>
                  <span class="custom-control-label">AT HOME</span>
                  </label>
                  <label class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" name="accompaniment[]" value="2" <?=in_array(2, $care_for_patient_accompany)? 'checked' : ''; ?>>
                  <span class="custom-control-label">TO DOCTORS APPOINTMENT </span>
                  </label>
                  <label class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" name="accompaniment[]" value="3" <?=in_array(3, $care_for_patient_accompany)? 'checked' : ''; ?>>
                  <span class="custom-control-label">OUTING / OTHERS</span>
                  </label>
                  <label class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" name="accompaniment[]" value="4" <?=in_array(4, $care_for_patient_accompany) ? 'checked' : ''; ?>>
                  <span class="custom-control-label">WITH TRANSPORT(GRAB)</span>
                  </label>
                  </div>
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-label">
                  Other Care
                  </div>
                  <div class="form-group form-elements">
                  <div class="custom-controls-stacked">
                  <?php
                     // echo "<pre>";

                     // print_r($care_for_patient_accompany);


                         foreach ($care_for_patient_other_details as $key => $value) :  ?>
                  <label class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" name="care_for_patient[]" value="<?=$value['id']?>" <?=(in_array($value['id'], $care_for_patient_others) ? "checked" : null); ?>/>
                  <span class="custom-control-label">
                  <?=$value['details']?>
                  </span>
                  </label>
                  <?php endforeach; ?>
                  </div>
                  </div>
                  </div>
                  <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                  <button class="btn btn-primary" type="submit"    name="submit">Update</button>
                  </div>
                  </div>
                  </fieldset>
                  </form>
                  </fieldset>
                  </div>
                  </div>
                  </div>
                  </div>
               </div>
            </div>
            <!-- Right-sidebar-->
            <?php $this->load->view('backend/right_sidebar');?>
            <!-- End Rightsidebar-->
            <?php $this->load->view('backend/footer');?>
         </div>
         <!-- End app-content-->
      </div>
   </div>
   <!-- End Page -->
   <!-- Back to top -->
   <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>
   <?php $this->load->view('backend/footer_link');?>
   <script>
      // $('#datereg').datepicker({uiLibrary: 'bootstrap4'});
      // $('#start').datepicker({uiLibrary: 'bootstrap4'});
      // $('#end').datepicker({uiLibrary: 'bootstrap4'});

      $('#datereg').Zebra_DatePicker();
      $('#start').Zebra_DatePicker();
      $('#end').Zebra_DatePicker();
   </script>
   <script>
      let pass = document.querySelector('#password');
      let pass2 = document.querySelector('#passconf');

      pass2.onkeyup = () => {
      if (pass.value != pass2.value ) {
      document.getElementById("alert_pass").innerHTML =
      `<div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert">&times;</button><div class="alert-message">
          <span>Password Don't Match!</span>
      </div>
      </div>`;

      document.getElementById("submitBtn").disabled = true;
      }
      else{
      document.getElementById("alert_pass").innerHTML = '';
      document.getElementById("submitBtn").disabled = false;
      }
      }
      // $('.checkbox').change(function(){
      //     alert('hi');
      // });

   </script>
   <script>
      let template = document.querySelectorAll('template')[0];
      let additional_exp = document.getElementById('additional_exp');
      let count = 1;

      removeEvent(document, 'click', '.remove_exp',removeItem);
      addEvent(document, 'click', '.add_exp', addItem);



      function addEvent(element, event, selector,callback){
      element.addEventListener(event, e =>{
      if (e.target.matches(selector)) {
          if (count < 10) {
              count++;
              addItem(count);
              console.log(count);
              $('#start_'+count).datepicker({uiLibrary: 'bootstrap4'});
              $('#end_'+count).datepicker({uiLibrary: 'bootstrap4'});
          }
          else{
              alert('Sorry can\'t add more!');
          }
          }
      })
      }

      function removeEvent(element, event, selector, callback){
      element.addEventListener(event, e => {
      if (e.target.matches(selector)) {
          removeItem(e)
      }
      })
      }

      function addItem(count){
      additional_exp.innerHTML += `<div class="row-target">
                          <h4 class="" style="line-height:2.7em">
                              Nursing Experience
                              <span class="add_exp float-right btn btn-primary">Add+</span>
                              <span  data-row-target='.row-target' class="remove_exp float-right btn btn-danger">Remove-</span>
                          </h4>
                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <div class="row">
                                          <div class="col-md-3 text-left">
                                              <label for="organisation">Organisation</label>
                                          </div>
                                          <div class="col-md-9">
                                              <input type="text" class="form-control"  name="organisation[]" placeholder="Organisation" value="">
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <div class="row">
                                          <div class="col-md-3 text-left">
                                              <label for="start_period">Start Date</label>
                                          </div>
                                          <div class="col-md-6">
                                          <input class="form-control add_start" style="width:100%" name="start_date[]" id="start_${count}" placeholder="MM/DD/YYYY" type="text" >
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <div class="row">
                                          <div class="col-md-3 text-left">
                                              <label for="start_period">End Date</label>
                                          </div>
                                          <div class="col-md-6">
                                          <input class="form-control add_end" name="end_date[]" id="end_${count}" placeholder="MM/DD/YYYY" type="text">
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <div class="row">
                                          <div class="col-md-3 text-left">
                                              <label for="title">Title</label>
                                          </div>
                                          <div class="col-md-9">
                                          <input type="text" class="form-control" id="title" name="title[]" placeholder="Title" value="">
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <div class="row">
                                          <div class="col-md-3 text-left">
                                              <label for="desc_of_duties">Description of duties</label>
                                          </div>
                                          <div class="col-md-9">
                                          <textarea class="form-control" id="desc_of_duties" name="duty_description[]" cols="30" rows="1">Description of duties
                                          </textarea>

                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <div class="row">
                                          <div class="col-md-3 text-left">
                                              <label for="references">References</label>
                                          </div>
                                          <div class="col-md-9">
                                          <input type="text" class="form-control" id="references" name="ref[]" placeholder="References" value="">
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <div class="row">
                                          <div class="col-md-3 text-left">
                                              <label for="desc_of_duties">Trainings Attended</label>
                                          </div>
                                          <div class="col-md-9">
                                          <input type="text" class="form-control" id="training_attended" name="training_attended[]" cols="30" rows="1" placeholder="Trainings Attended">

                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>`;
      }

      function removeItem(params) {
      let buttonClicked = params.target
      buttonClicked.closest(buttonClicked.dataset.rowTarget).remove()
      count--;
      }


   </script>
   <script type="text/javascript">
      function checkForm() {


        var address = document.forms["accountForm"]["address"].value;
        var security_type= document.forms["accountForm"]["security_type"].value;
        var living_status= document.forms["accountForm"]["living_status"].value;
        var current_care_status= document.forms["accountForm"]["current_care_status"].value;
        var current_living_condition= document.forms["accountForm"]["current_living_condition"].value;

          if (address == "") {
            alert("The address field is required.");
            return false;
          }
          else if(security_type == "")
          {
            alert("The security type field is required.");
            return false;
          }
          else if(living_status == "")
          {
            alert("The living status field is required.");
            return false;
          }
          else if(current_care_status == "")
          {
            alert("The current care status field is required.");
            return false;
          }
          else if(current_living_condition == "")
          {
            alert("The current living condition field is required.");
            return false;
          }
      }

      function validateForm() {

      var dependency = document.forms["updateCondition"]["dependency"].value;
      var mental_state= document.forms["updateCondition"]["mental_state"].value;
      var feeding= document.forms["updateCondition"]["feeding"].value;
      var allergies= document.forms["updateCondition"]["allergies"].value;
        if (dependency == "") {
          alert("The Dependency field is required.");
          return false;
        }
        else if(mental_state == "")
        {
          alert("The Mental State field is required.");
          return false;
        }
        else if(feeding == "")
        {
          alert("The feeding field is required.");
          return false;
        }
        else if(allergies == "")
        {
          alert("The allergies field is required.");
          return false;
        }
      }

      // function careForm() {
      //
      //
      //
      // var accompaniment = document.forms["carePatientUpdate"]["accompaniment[]"].value;
      //
      //
      // var carePatient= document.forms["carePatientUpdate"]["care_for_patient[]"].value;
      //
      //   if (accompaniment == "") {
      //     alert("The Accompaniment field is required.");
      //     return false;
      //   }
      //   else if(carePatient == "") {
      //     alert("The Other Care field is required.");
      //     return false;
      //   }
      //
      // }
   </script>
</body>
</html>
