<?php $this->load->view('backend/head_link');?>

<style>
    @media (max-width: 480px) {
    .nav-wrapper {
    display: block;
    /*overflow: hidden;*/
    height: calc(1.5rem + 1rem + 2px); /** 1.5 is font-size, 1 is padding top and bottom, 2 is border width top and bottom */
    position: relative;
    z-index: 1;
    margin-bottom: -1px;
    }
    .nav-pills {
        overflow-x: auto;
        flex-wrap: nowrap;
        border-bottom: 0;
    }
     .nav-item {
        margin-bottom: 0;
        min-width: 12em !important;
    }
   /* .nav-item :first-child {
        padding-left: 15px;
    }
    .nav-item :last-child {
        padding-right: 15px;
    }*/
    .nav-link {
        white-space: nowrap;
        /*min-width: 5em !important;*/
    }
    .dragscroll:active,
    .dragscroll:active a {
        cursor: -webkit-grabbing;
    }

    }

</style>

<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
<img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
<div class="page-main">
<!--app-header-->
<?php $this->load->view('backend/header');?>
<!-- app-content-->
<div class="container content-patient">
<div class="side-app">
<!-- page-header -->
<!-- <div class="page-header">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
</ol>
<div class="ml-auto">
<div class="input-group">
    <a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
        <span>
            <i class="fa fa-calendar"></i> Events Settings
        </span>
        <i class="fa fa-caret-down"></i>
    </a>
    <a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
        <span>
            <i class="fa fa-star"></i>
        </span>
    </a>
</div>
</div>
</div> -->
<!-- End page-header -->

<div class="row">
<div class="col-md-12 col-lg-12">
<div class="card">
<div class="card-header">
    <div class="col-md-12">
    <div class="card-title"><?=$form_title?> 
    <!-- <a class="btn bg-pink p-2" href="edit_condition">Edit</a>  -->
    </div>
    </div>
    <!-- <div class="col-md-2">
    <a href="nurse/nurse_list" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
        <span> Nurse List
        </span>
    </a>
    </div> -->
</div>
<div class="card-body">
    <form action="patient/care_for_patient_update/<?=!empty($patient_details[0]['loginid']) ? $patient_details[0]['loginid'] : ''?>" method="post">
    <div class="row">
    <?php if($this->session->flashdata('msg')){ ?>
        <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert-message">
                <span><?=$this->session->flashdata('msg');?></span>
            </div>
        </div>
    <?php } ?> 
    <div class="col-md-12">
        <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">

<!-- <form action="" method="post"> -->
   <fieldset class="biodata">
    <!-- <h4>Health Status</h4> -->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group form-elements">
                            <div class="form-label">ACCOMPANIMENT (PLEASE TICK AS NECESSARY BELOW AND INCLUDE “WITH TRANSPORT” IF YOU NEED US TO PROVIDE TRANSPORT)</div>
                            <div class="custom-controls-stacked">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="accompaniment[]" value="1" <?=in_array(1, $care_for_patient_accompany) ? 'checked' : ''; ?>>
                                    <span class="custom-control-label">AT HOME</span>
                                </label>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="accompaniment[]" value="2" <?=in_array(2, $care_for_patient_accompany)? 'checked' : ''; ?>>
                                    <span class="custom-control-label">TO DOCTORS APPOINTMENT </span>
                                </label>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="accompaniment[]" value="3" <?=in_array(3, $care_for_patient_accompany)? 'checked' : ''; ?>>
                                    <span class="custom-control-label">OUTING / OTHERS</span>
                                </label>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="accompaniment[]" value="4" <?=in_array(4, $care_for_patient_accompany) ? 'checked' : ''; ?>>
                                    <span class="custom-control-label">WITH TRANSPORT(GRAB)</span>
                                </label>
                            </div>
                        </div>            
                </div>
                <div class="col-md-6">
                     <div class="form-label">
                                        Other Care
                                    </div>
                                    <div class="form-group form-elements">
                                    <div class="custom-controls-stacked">


                                        <?php

                                        // echo "<pre>";

                                        // print_r($care_for_patient_accompany);


                                            foreach ($care_for_patient_other_details as $key => $value) :  ?>
                                               
                                            <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_for_patient[]" value="<?=$value['id']?>" <?=(in_array($value['id'], $care_for_patient_others) ? "checked" : null); ?>/>
                                            <span class="custom-control-label"> 
                                                <?=$value['details']?>
                                            </span>
                                            </label>

                                            <?php endforeach; ?>
                                       <!--  <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_for_patient[]" value="1">
                                            <span class="custom-control-label">BASIC PERSONAL CARE – BASIC CARE FOR CLEANING, FEEDING, MEDICATION ETC</span>
                                        </label>
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_for_patient[]" value="2">
                                            <span class="custom-control-label">INFANT BABY CARE – 0 TO 12 MONTHS </span>
                                        </label>
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_for_patient[]" value="3">
                                            <span class="custom-control-label">TODDLER/CHILD CARE – BELOW 12 YEARS OLD</span>
                                        </label>
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_for_patient[]" value="4">
                                            <span class="custom-control-label">WOUND DRESSING – BED SORES / DIABETIC WOUNDS /SURGICAL WOUNDS</span>
                                        </label>
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_for_patient[]" value="5">
                                            <span class="custom-control-label">FEEDING TUBE INSERTION</span>
                                        </label>

                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_for_patient[]" value="6">
                                            <span class="custom-control-label">SUCTION</span>
                                        </label>

                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_for_patient[]" value="7">
                                            <span class="custom-control-label">URINARY CATHETER INSERTION/REMOVAL</span>
                                        </label>

                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_for_patient[]" value="8">
                                            <span class="custom-control-label">COLOSTOMY CARE</span>
                                        </label>
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_for_patient[]" value="9">
                                            <span class="custom-control-label">INJECTIONS</span>
                                        </label>

                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_for_patient[]" value="10">
                                            <span class="custom-control-label">DRIP </span>
                                        </label>
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_for_patient[]" value="11">
                                            <span class="custom-control-label">OXYGEN SUPPORT</span>
                                        </label>

                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_for_patient[]" value="12">
                                            <span class="custom-control-label">TRACHEOSTOMY CARE</span>
                                        </label>

                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_for_patient[]" value="13">
                                            <span class="custom-control-label">TERMINALLY ILL/END OF LIFE SUPPORT</span>
                                        </label>

                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_for_patient[]" value="14">
                                            <span class="custom-control-label">OTHERS – PLEASE DESCRIBE -FHN</span>
                                        </label -->
                                        </div>
                                    </div>
                </div>
                <div class="col-md-12">
                    <button class="btn btn-primary" type="submit" name="submit">Submit</button>
                </div>
            </div>
        </fieldset>
    </form>

</div>
</div>  
            
            <!-- <div class="col-md-12">
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a large text here ..."></textarea>
            </div> -->
    
    </form>
</fieldset>
<!-- table-wrapper -->
</div>
<!-- section-wrapper -->
</div>
</div>

</div><!--End side app-->

<!-- Right-sidebar-->
<?php $this->load->view('backend/right_sidebar');?>
<!-- End Rightsidebar-->

<?php $this->load->view('backend/footer');?>
</div>
<!-- End app-content-->
</div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>
  <script type="text/javascript">
    let dataTable = $(".new_table").dataTable(
      {  "bLengthChange" : false,
           "responsive": true,
          "oLanguage": 
        {
            "sSearch": ""
        }
      });
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust()
           .responsive.recalc();
        });    
      
</script>

</body>
</html>