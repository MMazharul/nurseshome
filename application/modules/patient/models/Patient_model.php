<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Patient_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function insert($table_name,$data)
    {
          $this->db->insert($table_name, $data);
    }
    public function insert_ret($tablename, $tabledata)
    {
        $this->db->insert($tablename, $tabledata);
        return $this->db->insert_id();
    }
    public function check_login($email, $pass) {
        $this->db->select('*');
        $this->db->from('login');
        $this->db->where('email', $email);
        $this->db->where('password', $pass);
        $result = $this->db->get();
        return $result->result_array();
    }
     public function password_change($email,$data)
    {
        $this->db->where('email',$email);
        $this->db->update('login',$data);
    }
    public function select_with_where($selector, $condition, $tablename)
    {
    	$this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result = $this->db->get();
        return $result->result_array();
    }
    public function select_with_where_desc($selector, $condition, $tablename,$col,$order_type)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by($col,$order_type);
        $result = $this->db->get();
        return $result->result_array();
    }
    public function select_with_where_without_array($selector, $condition, $tablename)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result = $this->db->get();
        return $result->row_array();

    }

    public function select_condition_random_with_limit($table_name,$condition,$limit)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by('created_at','RANDOM');
        $this->db->limit($limit);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
      public function select_condition_decending_with_limit($table_name,$condition,$limit)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by('created_at','DESC');
        $this->db->limit($limit);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }


    public function select_all_acending($table_name,$col_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by($col_name,'ASC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function select_all($table_name)
    {
    	$this->db->select('*');
		$this->db->from($table_name);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
    }

       public function update_function($columnName, $columnVal, $tableName, $data)
    {
        $this->db->where($columnName, $columnVal);
        $this->db->update($tableName, $data);
    }

    // ==== Search Model Query Began =====//

    public function category_id_like($search_content)
    {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->like('name', $search_content, 'both');
        $result=$this->db->get();
        return $result->result_array();
    }

    public function get_search_item($condition,$table_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by($table_name.'.created_at','DESC');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->limit(5);
        $result=$this->db->get();
        return $result->result_array();
    }
      public function get_search_input_item($table_name,$condition,$search_content)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->like('p_name', $search_content,'after');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->limit(5);
        $result=$this->db->get();
        return $result->result_array();
    }

      public function fetch_item($query)
    {
        $this->db->select('*');
        $this->db->from('product');
        if($query!=''){
            $this->db->like('p_name',$query);
        }
        $this->db->order_by('created_at','DESC');
        // $where = '(' . $condition . ')';
        // $this->db->where($where);
        // $this->db->limit(10);
        return $this->db->get();

    }

    public function delete($table, $condition){
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->delete($table);
    }
    public function delete_function($tableName, $columnName, $columnVal)
    {
        $this->db->where($columnName, $columnVal);
        $this->db->delete($tableName);
    }

    public function select_where_left_join($selector,$table_name,$join_table,$join_condition,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function select_where_left_two_join($selector,$table_name,$join_table,
        $join_condition,$join_table2,$join_condition2,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }

 public function special_join_query_booking($patient_id,$book_status)
    {
  $query="select time_in_hours,start_time,end_time,fromtime,totime,adddate,book_charge,patient_id,addons_one,addons_two,addons_three,notes,book_status,booking_random_code,
patient_bookid,a.loginid as nurse_id,concat(a.first_name,' ',a.last_name)as nurse_name,a.charge,a1.price as a1price,a2.price as a2price,a3.price as a3price,
b.loginid as patient_id,concat(b.first_name,' ',b.last_name)as patient_name
from nurse_booking inner join users a on a.loginid=nurse_booking.nurse_id
left join addons a1 on a1.id=nurse_booking.addons_one left join addons as a2 on a2.id=nurse_booking.addons_two left join addons as a3 on a3.id=nurse_booking.addons_three
inner join users b on b.loginid=nurse_booking.patient_id where b.loginid=$patient_id and book_status=$book_status";
        $result = $this->db->query($query) or die ("Schema Query Failed");
        $result=$result->result_array();
        return $result;
    }


 public function special_join_query_all($patient_id)
    {
      $query="select time_in_hours,fromtime,totime,start_time,end_time,adddate,patient_id,addons_one,addons_two,addons_three,notes,book_status,a1.price as a1price,a2.price as a2price,a3.price as a3price,booking_random_code,
patient_bookid,a.loginid as nurse_id,concat(a.first_name,' ',a.last_name)as nurse_name,a.charge,
b.loginid as patient_id,concat(b.first_name,' ',b.last_name)as patient_name
from nurse_booking inner join users a on a.loginid=nurse_booking.nurse_id
inner join users b on b.loginid=nurse_booking.patient_id left join addons a1 on a1.id=nurse_booking.addons_one left join addons as a2 on a2.id=nurse_booking.addons_two left join addons as a3 on a3.id=nurse_booking.addons_three where b.loginid=$patient_id and book_status != 4";
      $result = $this->db->query($query) or die ("Schema Query Failed");
      $result=$result->result_array();
      return $result;
    }

 public function special_join_query_booking_multi_condition($patient_id,$first_condition, $second_condition)
    {
  $query="select time_in_hours,fromtime,totime,adddate,patient_id,addons_one,addons_two,addons_three,notes,book_status,
patient_bookid,a.loginid as nurse_id,concat(a.first_name,' ',a.last_name)as nurse_name,a.charge,booking_random_code,
b.loginid as patient_id,concat(b.first_name,' ',b.last_name)as patient_name
from nurse_booking inner join users a on a.loginid=nurse_booking.nurse_id
inner join users b on b.loginid=nurse_booking.patient_id where b.loginid=$patient_id and book_status=$first_condition or book_status=$second_condition";
        $result = $this->db->query($query) or die ("Schema Query Failed");
        $result=$result->result_array();
        return $result;
    }
 public function special_join_query_booking_single($book_id)
    {
  $query="select time_in_hours,fromtime,totime,adddate,patient_id,addons_one,addons_two,addons_three,notes,book_status,a1.price as a1price,a2.price as a2price,a3.price as a3price,
patient_bookid,a.loginid as nurse_id,concat(a.first_name,' ',a.last_name)as nurse_name,a.charge,booking_random_code,
b.loginid as patient_id,concat(b.first_name,' ',b.last_name)as patient_name
from nurse_booking inner join users a on a.loginid=nurse_booking.nurse_id
inner join users b on b.loginid=nurse_booking.patient_id
left join addons a1 on a1.id=nurse_booking.addons_one left join addons as a2 on a2.id=nurse_booking.addons_two left join addons as a3 on a3.id=nurse_booking.addons_three
 where nurse_booking.patient_bookid=$book_id";
       $result = $this->db->query($query) or die ("Schema Query Failed");
        $result=$result->result_array();
        return $result;
    }

     public function between_query($date,$time,$id)
  {
    $query="select `fromtime`,`totime`,`start_time`,`end_time` from nurse_booking  WHERE '$date' BETWEEN `fromtime` AND `totime` AND '$time' BETWEEN `start_time` AND `end_time` AND nurse_id=$id";
     $result = $this->db->query($query) or die ("Schema Query Failed");
     $result=$result->result_array();
     return $result;
  }

    // ====  Search  Model Query End=== ///
}
