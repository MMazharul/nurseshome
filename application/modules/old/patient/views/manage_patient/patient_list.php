<?php $this->load->view('backend/head_link');?>
<style type="text/css">
	td,th{text-transform: none !important;}
</style>
<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
	<img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
	<div class="page-main">
		<!--app-header-->
		
		
		<?php $this->load->view('backend/header');?>



		<!-- app-content-->
		<div class="container content-patient">
			<div class="side-app">

				<!-- page-header -->
				<!-- <div class="page-header">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
					</ol>
					<div class="ml-auto">
						<div class="input-group">
							<a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
								<span>
									<i class="fa fa-calendar"></i> Events Settings
								</span>
								<i class="fa fa-caret-down"></i>
							</a>
							<a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
								<span>
									<i class="fa fa-star"></i>
								</span>
							</a>
						</div>
					</div>
				</div> -->
				<!-- End page-header -->
				

				<div class="row">
		
				


					<div class="col-md-12 col-lg-12">
					<div class="card">
				
						<div class="card-header">
							<div class="col-md-10">
							<div class="card-title">Patient List</div>
							</div>
							<div class="col-md-2">
							<a href="patient" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
								<span> Add Patient
								</span>
								
							</a>
							</div>
						</div>
						
						<div class="card-body">
							<div class="table-responsive">
							<?php if($this->session->flashdata('msg')){ ?>
								<div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<div class="alert-message">
										<span><?=$this->session->flashdata('msg');?></span>
									</div>
								</div>
							<?php } ?> 
								<table id="example" class="table table-striped table-bordered text-nowrap w-100">
									<thead>
										<tr>
										<!-- 	No | Name | | Type | Location | Status | Action (View Profile/View Bookings)  -->
											<th class="wd-15p">No</th>
											<th class="wd-15p">Name</th>
											<th class="wd-15p">Date Registered</th>
											<th class="wd-15p">Type</th>
											<th class="wd-15p">Location</th>
											<th class="wd-15p">Status</th>
											<!-- <th class="wd-15p">Gender</th> -->
										<!-- 	<th class="wd-15p">Contact Number</th>
											
											<th class="wd-15p">Email</th> -->
											<th class="wd-15p">Edit Profile</th>
											<th class="wd-20p">Action</th>
										</tr>
									</thead>
									<tbody>
									<?php 
										$i = 1;
										foreach ($patient_details as $key => $value) { ?>
										<tr>
											<td><?=$i;?></td>
											<td><?=$value['first_name']." ".$value['last_name']?></td>
											<td><?=$value['registered_date']?></td>
											<!-- <td><?=$value['gender'] == '1' ? 'Male' : 'Female';
											?></td> -->
											<td>Patient</td>
											<td>CC</td>
											<td><?=$value['verify_status'] == '1' ? 'Active' : 'Inactive';
											?></td>
											<!-- <td><?=$value['country_code']." ".$value['contact_no']?></td>
											
											<td><?=$value['email']?></td> -->
											<td>
											
												<a href="patient-edit-profile/<?=$value['loginid']?>"   
												class="btn btn-primary text-white mr-2"  id="">
													<span>
														View Profile
													</span>
												</a>
										<a href="home"  class="btn btn-primary text-white mr-2"  id="">
												<span>
														View Booking
												</span>
											</a>
												<a href="patient/add_patient_details/<?=$value['loginid']?>"  class="btn btn-primary text-white mr-2"  id="">
												<span>
														Edit Profile
												</span>
											</a>
											</td>
											<td>
											
											<a data-toggle="modal" data-target="#smallModal_<?=$i?>"   class="btn btn-danger text-white mr-2"  id="">
												<span>
														Delete 
												</span>
											</a>
											</td>
										</tr>
										
										
									<div id="smallModal_<?=$i?>" class="modal fade">
											<div class="modal-dialog modal-sm" role="document">
												<div class="modal-content">
													
													<div class="modal-body bg-danger">
														<h4>Do you want to delete?</h4>
													</div><!-- modal-body -->
													<div class="modal-footer">
														<a href="patient/delete_patient/<?=$value['loginid']?>" type="" class="btn btn-danger">YES</a>
														<button type="button" class="btn btn-primary" data-dismiss="modal">NO</button>
													</div>
												</div>
											</div>
										</div>
										<?php $i++; } ?>
										
									
									</tbody>
								</table>
								
							</div>
						</div>
						<!-- table-wrapper -->
					</div>
					<!-- section-wrapper -->
					</div>
				</div>

			</div><!--End side app-->

			<!-- Right-sidebar-->
			<?php $this->load->view('backend/right_sidebar');?>
			<!-- End Rightsidebar-->

			<?php $this->load->view('backend/footer');?>

		</div>
		<!-- End app-content-->
	</div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>
</body>
</html>