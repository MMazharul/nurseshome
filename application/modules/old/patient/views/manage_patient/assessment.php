<?php $this->load->view('backend/head_link');?>


<style>
    @media (max-width: 480px) {
    .nav-wrapper {
    display: block;
    /*overflow: hidden;*/
    height: calc(1.5rem + 1rem + 2px); /** 1.5 is font-size, 1 is padding top and bottom, 2 is border width top and bottom */
    position: relative;
    z-index: 1;
    margin-bottom: -1px;
    }
    .nav-pills {
        overflow-x: auto;
        flex-wrap: nowrap;
        border-bottom: 0;
    }
     .nav-item {
        margin-bottom: 0;
        min-width: 12em !important;
    }
   /* .nav-item :first-child {
        padding-left: 15px;
    }
    .nav-item :last-child {
        padding-right: 15px;
    }*/
    .nav-link {
        white-space: nowrap;
        /*min-width: 5em !important;*/
    }
    .dragscroll:active,
    .dragscroll:active a {
        cursor: -webkit-grabbing;
    }

    }

</style>

<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
<img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
<div class="page-main">
<!--app-header-->
<?php $this->load->view('backend/header');?>
<!-- app-content-->
<div class="container content-patient">
<div class="side-app">
<!-- page-header -->
<!-- <div class="page-header">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
</ol>
<div class="ml-auto">
<div class="input-group">
    <a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
        <span>
            <i class="fa fa-calendar"></i> Events Settings
        </span>
        <i class="fa fa-caret-down"></i>
    </a>
    <a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
        <span>
            <i class="fa fa-star"></i>
        </span>
    </a>
</div>
</div>
</div> -->
<!-- End page-header -->

<div class="row">
<div class="col-md-12 col-lg-12">
<div class="card">
<div class="card-header">
    <div class="col-md-10">
    <div class="card-title"><?=$form_title?></div>
    </div>
    <!-- <div class="col-md-2">
    <a href="nurse/nurse_list" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
        <span> Nurse List
        </span>
    </a>
    </div> -->
</div>
<div class="card-body">
    <form action="nurse/register_user_as_nurse" method="post">
    <div class="row">
    <?php if($this->session->flashdata('msg')){ ?>
        <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert-message">
                <span><?=$this->session->flashdata('msg');?></span>
            </div>
        </div>
    <?php } ?> 
    <div class="col-md-12">
        <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                                        <table id="example" class="table table-striped table-bordered text-nowrap w-100">
                                            <thead>
                                                <tr>
                                                    <th class="wd-15p">Booking ID</th>
                                                    <th class="wd-15p">Assessment Date</th>
                                                    <th class="wd-20p">Nurse Name</th>
                                                    <th class="wd-15p">View Assessment</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>#6555222</td>
                                                    <td>2018/03/12</td>
                                                    <td>Chloe</td>
                                                    <td><a href="" class="btn btn-primary">View</a></td>
                                                </tr>
                                                <tr>
                                                    <td>#6555222</td>
                                                    <td>2018/03/12</td>
                                                    <td>Chloe</td>
                                                    <td><a href="" class="btn btn-primary">View</a></td>
                                                </tr>
                                                <tr>
                                                    <td>#6555222</td>
                                                    <td>2018/03/12</td>
                                                    <td>Chloe</td>
                                                    <td><a href="" class="btn btn-primary">View</a></td>
                                                </tr>
                                            
                                            
                                            </tbody>
                                        </table>
                                    </div>
        </div>
    </div>
    

            
            <!-- <div class="col-md-12">
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a large text here ..."></textarea>
            </div> -->
    
    </form>
</fieldset>
<!-- table-wrapper -->
</div>
<!-- section-wrapper -->
</div>
</div>

</div><!--End side app-->

<!-- Right-sidebar-->
<?php $this->load->view('backend/right_sidebar');?>
<!-- End Rightsidebar-->

<?php $this->load->view('backend/footer');?>
</div>
<!-- End app-content-->
</div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>
  <script type="text/javascript">
    let dataTable = $(".new_table").dataTable(
      {  "bLengthChange" : false,
           "responsive": true,
          "oLanguage": 
        {
            "sSearch": ""
        }
      });
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust()
           .responsive.recalc();
        });    
      
</script>

</body>
</html>