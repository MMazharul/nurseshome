<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Nurse extends MX_Controller 
{

    //public $counter=0;
    
    protected $login_id;
    
    function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->model('nurse_model');
        // $this->load->model('product/product_model');
        $this->load->helper('text');
        // $this->load->helper('common_helper');
        $this->load->helper(array('form','url'));
        $this->load->helper('inflector');
        //$this->load->library('encrypt');
        // $this->load->model('home_model');
        // $this->load->model('admin/admin_model');
        // $this->load->model('product/product_model');
        $this->login_id = $this->session->userdata('loginid');
     }

    // add this line for responsive table 
    // $data['responsive_table'] = 'true';

    //==========================PATIENT PROFILE =========================== //
    public function profile()
    {
        $id = $this->uri->segment(2);
        $patient = $data['patient_details'] = $this->nurse_model->select_with_where('*',"loginid=".$id,'users');
        $data['form_title'] = 'Profile of '.ucwords($patient[0]['first_name']." ".$patient[0]['last_name']);
        $this->load->view('patient_profile',$data);
        
    }

    //==========================BOOKING DETAILS=========================== //
    public function booking_details()
    {
        $data['form_title'] = 'Booking Details';
        $this->load->view('booking_details',$data);
        
    }

    //==========================NURSE LIST=========================== //
    public function nurse_list()
    {
        
        $data['nurse_details'] = $this->nurse_model->select_with_where_desc('*',"user_role=3",'users','loginid','Desc');
        $data['responsive_table'] = 'true';
        $this->load->view('manage_nurse/nurse_list', $data);
        
    }
    //===================ADD NURSE AS USER VIEW=================== //
    public function index()
    {

        $this->load->view('manage_nurse/register_user_nurse');
        
    }
    //=================ADD NURSE AS USER POST===================== //
    public function register_user_as_nurse(){

        $data['email'] = $email = html_escape(trim($this->input->post('email')));

        $data['first_name'] = html_escape(trim($this->input->post('firstname')));

        $data['last_name'] = html_escape(trim($this->input->post('lastname')));

        $data['country_code'] = html_escape(trim($this->input->post('country_code')));
        
        $data['contact_no'] = html_escape(trim($this->input->post('contact')));

        $data['user_role'] = 3;  // role 3 = NURSE

        $data['verify_status'] = 1; 

        $data['registered_date'] = date('Y-m-d H:i:s');

        $data['last_login_ip'] = $this->input->ip_address();

        $data['profile_pic'] = "avatar.png";
        $data['charge']=25; 
         $data['gender'] =html_escape(trim($this->input->post('gender')));

        $password = html_escape(trim($this->input->post('password')));

        $passconf = html_escape(trim($this->input->post('passconf')));

        $data['password'] = $this->encryptIt($password);
        
        if($password == $passconf) {
            $res = $this->nurse_model->select_with_where('*',"email='{$email}'",'users');
            if (count($res) > 0) 
            {  
                $this->session->set_flashdata('type', 'danger');
                $this->session->set_flashdata('msg', 'Email already exists!!');
                redirect('nurse');
            }
            else{
                $this->nurse_model->insert('users',$data);
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', 'Registraion Done Successfully!');
                redirect('nurse/nurse_list');
            } 
        }else{
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', 'Password not matched!');
                redirect('nurse/nurse_list');
        } 
    }
    //===================EDIT AUTH NURSE VIEW=================== //
    public function edit_nurse_auth_details($id)
    {
        $data['nurse_auth_details'] = $this->nurse_model->select_with_where('*',"user_role=3",'users');
        $this->load->view('manage_nurse/edit_nurse_auth_details',$data);
        
    }
    //=================Bookings VIEW=================== //
    public function bookings()
    {
        $data['form_title'] = 'Bookings';
        $data['nurse_details'] = $this->nurse_model->select_with_where('*',"loginid=".$this->login_id,'users');
        $data['book_details']=$this->nurse_model->select_where_join('patient_bookid,first_name,last_name,contact_no,
profile_pic,email,time_in_hours,fromtime,totime,adddate,notes,book_status,book_charge,patient_id','nurse_booking','users','users.loginid=nurse_booking.patient_id',"nurse_id=".$this->login_id);

        $this->load->view('nurse_profile/booking/bookings', $data);
        
    }
    //=================ADD NURSE DETAILS VIEW=================== //
    public function add_nurse_details()
    {
  
        $this->load->view('manage_nurse/add_nurse_details');
        
    }

    // ================= NURSE PROFILE =================== //

    public function nurse_profile_view($id)
    {
        // $data['responsive_table'] = 'true';
        $data['form_title'] = 'My Profile ';
        $data['nurse_details'] = $this->nurse_model->select_with_where('*',"loginid=".$this->login_id,'users');
        
 
        $data['nurse_bio'] = $this->nurse_model->select_with_where('*',"nurse_id=".$this->login_id,'nurse_bio');

        $data['nurse_education_certificate'] = $this->nurse_model->select_with_where('*',"nurse_id=".$this->login_id,'nurse_education_certificate');

        $data['nurse_additional_certificate'] = $this->nurse_model->select_with_where('*',"nurse_id=".$this->login_id,'nurse_additional_certificate');

        $data['nurse_experience'] = $this->nurse_model->select_where_left_join("*","nurse_experience","nurse_experience_details","nurse_experience.expid=nurse_experience_details.exp_id","nurse_experience.nurse_id=".$this->login_id);

        $data['competencies'] = $this->nurse_model->select_where_left_join("competencies.compe_id as id, competencies.compet_ttile as title,competencies_details.* ","competencies","competencies_details","competencies.compe_id=competencies_details.compe_id","competencies_details.nurse_id=".$this->login_id);



        $this->load->view('nurse_profile/profile',$data);  
    }
    // ================= NURSE PROFILE EDIT =================== //

    public function edit_profile($id)
    {
        $data['form_title'] = 'Edit Profile';

        $data['state']= $this->nurse_model->select_all('states');
        $data['nurse_details'] = $this->nurse_model->select_with_where('*',"loginid=".$this->login_id,'users');
// print_r($data['nurse_details']);
        $data['nurse_bio'] = $this->nurse_model->select_with_where('*',"nurse_id=".$this->login_id,'nurse_bio');

        $data['nurse_education_certificate'] = $this->nurse_model->select_with_where('*',"nurse_id=".$this->login_id,'nurse_education_certificate');

        $data['nurse_additional_certificate'] = $this->nurse_model->select_with_where('*',"nurse_id=".$this->login_id,'nurse_additional_certificate');

        $data['nurse_experience'] = $this->nurse_model->select_where_left_join("*","nurse_experience","nurse_experience_details","nurse_experience.expid=nurse_experience_details.exp_id","nurse_experience.nurse_id=".$this->login_id);

        $data['competencies'] = $this->nurse_model->select_all_acending('competencies','compe_id');

        $data['competencies_details'] = $this->nurse_model->select_with_where_asc_spe('*',"nurse_id=".$this->login_id,'competencies_details','compe_id');
                $cnt=count($data['competencies_details']);

        $exist_competency = $this->nurse_model->select_with_where('compe_id,com_value',"nurse_id=".$this->login_id,'competencies_details');

        $data['competencies_selected'] = $a1 = array_column($exist_competency, 'compe_id');
        $data['competency_selected_value'] = $a2 =  array_column($exist_competency, 'com_value');

        $data['new_arr'] = array_combine($a1, $a2);

        // print_r($data['new_arr']); exit;



        $data['cn']=$cnt;

        // $data['special_comepetancy']=$this->nurse_model->special_comepetancy($this->login_id);

          
        $data['nid']=$this->login_id;
         //echo  $data['nid'];

        $this->load->view('nurse_profile/index',$data); 

    }

    public function nurse_edit_profile($id)
    {
        // $data['form_title'] = 'Profile';

        $data['nurse_details'] = $this->nurse_model->select_with_where('*',"loginid=".$id,'users');

        $data['nurse_bio'] = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'nurse_bio');

        $data['nurse_education_certificate'] = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'nurse_education_certificate');

        $data['nurse_additional_certificate'] = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'nurse_additional_certificate');

        $data['nurse_experience'] = $this->nurse_model->select_where_left_join("*","nurse_experience","nurse_experience_details","nurse_experience.expid=nurse_experience_details.exp_id","nurse_experience.nurse_id=".$id);

        // $data['competencies']=$this->nurse_model->select_with_where('*',"nurse_id=".$id,'competencies_details');
  // $data['competencies'] = $this->nurse_model->select_where_left_join("competencies.compe_id as id, competencies.compet_ttile as title,competencies_details.* ","competencies","competencies_details","competencies.compe_id=competencies_details.compe_id","competencies_details.nurse_id=".$id);

        $data['competencies'] = $this->nurse_model->select_all('competencies');

        $data['competencies_details'] = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'competencies_details');
       
        $cnt=count($data['competencies_details']);
        $data['cn']=$cnt;
        //echo $cnt;

        $data['special_comepetancy']=$this->nurse_model->special_comepetancy($id);    
       
        
        // echo json_encode($data['competencies_details']);
        
        $exist_competency = $this->nurse_model->select_with_where('compe_id,com_value',"nurse_id=".$id,'competencies_details');

        $data['competencies_selected'] = $a1 = array_column($exist_competency, 'compe_id');
        $data['competency_selected_value'] = $a2 =  array_column($exist_competency, 'com_value');

        $data['new_arr'] = array_combine($a1, $a2);

        // print_r($data['new_arr']); exit;



        $data['cn']=$cnt;

        // $data['special_comepetancy']=$this->nurse_model->special_comepetancy($this->login_id);

          
        $data['nid']=$this->login_id;
         //echo  $data['nid'];

        $this->load->view('nurse_profile/index',$data);  
    }
    // ================= NURSE PROFILE EDIT ACTION =================== //

    public function update_profile($id)
    {
        $data['email'] = $email = html_escape(trim($this->input->post('email')));

        $data['first_name'] = html_escape(trim($this->input->post('firstname')));

        $data['last_name'] = html_escape(trim($this->input->post('lastname')));

        $data['country_code'] = html_escape(trim($this->input->post('country_code')));

        $data['contact_no'] = html_escape(trim($this->input->post('contact')));

        $data['gender'] = html_escape(trim($this->input->post('gender')));
        
        $insData=$this->input->post('langauge');

        $data['langauge']=implode(',',array_values($insData));
        
        $data['location']=$this->input->post('location');

        $data['nurse_work_type']=$this->input->post('nurse_type');
        //print_r($data['langauge']);

        // $password = html_escape(trim($this->input->post('password')));

        // $passconf = html_escape(trim($this->input->post('passconf')));

        // if (!empty($password) && !empty($passconf) && ($password == $passconf)) {

        //     $data['password'] = $this->encryptIt($password);     
        // }
        
        $data['nurse_details'] = $this->nurse_model->update_function('loginid', $id, 'users',$data);
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', 'Profile Update Successful.');

        if ($this->session->userdata('user_role') == 1) {

            redirect('nurse-edit-profile/'.$id); 
        }
        else{
             redirect('nurse-profile'); 
        }
        

    }


   // ================= NURSE BIO =================== //
    public function add_bio($id)
    {
        $data['form_title'] = 'Add Bio';
        $this->load->view('nurse_profile/bio/add_bio',$data);  
    }

    public function list_bio($id)
    {
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'Bio List';
        $data['nurse_details'] = $this->nurse_model->select_with_where('*',"loginid=".$this->login_id,'users');
        $this->load->view('nurse_profile/bio/list_bio',$data);  
    }
    public function edit_bio($id)
    {
        $data['form_title'] = 'Update Bio';
        $this->load->view('nurse_profile/bio/edit_bio',$data);  
    }

    public function update_bio($id)
    {
      

        $data['nric_passport_id'] = html_escape(trim($this->input->post('nric_passport_id')));
        $data['age'] = html_escape(trim($this->input->post('age')));

        $data['height'] = html_escape(trim($this->input->post('height')));

        $data['weight'] = html_escape(trim($this->input->post('weight')));

        $data['nurse_id'] = $id;

        $data['add_date'] = date("Y-m-d H:i:s");

        if($_FILES['bio_file']['name'] != '')
            { 
             $data['bio_file'] ='';
             $i_ext = explode('.', $_FILES['bio_file']['name']);
             $target_path = $id."_".date("YmdHis").'_nurse_photo.'.end($i_ext);
             $_FILES['bio_file']['name'] = $target_path;
 
             $this->upload->initialize($this->set_upload_options($_FILES['bio_file']['name'],'uploads/'));
             $size = getimagesize($_FILES['bio_file']['tmp_name']);
             $this->upload->do_upload();  
             
             if (move_uploaded_file($_FILES['bio_file']['tmp_name'], 'uploads/' . $target_path))
             {
                 if ($size[0] == 150 || $size[1] == 150) 
                 {


                 } 
                else {
                 $imageWidth = 150; //Contains the Width of the Image
                 $imageHeight = 150;
                 $this->resize($imageWidth, $imageHeight, "uploads/" . $target_path, 
                    "uploads/" . $target_path);
                } 
                $data['bio_file'] = $target_path;
                $data_img['profile_pic'] = $target_path;
$this->nurse_model->update_function('loginid', $id, 'users', $data_img); 
             } 
         }



        // exit;

        $res = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'nurse_bio');
        if (count($res) > 0) {
            #update
            $this->nurse_model->update_function('nurse_id', $id, 'nurse_bio', $data);
           
            
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'biodata');
            $this->session->set_flashdata('msg', 'Proeile Update Successful');

        }
        else{
            #insert
            $this->nurse_model->insert('nurse_bio',$data);
            $data_img['profile_pic']=$target_path;
            //$this->nurse_model->update_function('loginid', $id, 'users', $data_img);
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'biodata');
            $this->session->set_flashdata('msg', 'Profile Update Successful.');
              
        }

        if ($this->session->userdata('user_role') == 1) {

            redirect('nurse-edit-profile/'.$id); 
        }
        else{
            redirect('nurse-profile'); 
        }


    }
    // ================= NURSE EDUCATION =================== //

    public function add_education($id)
    {
        $data['form_title'] = 'Add Education Qualification';
        $this->load->view('nurse_profile/education/add_education',$data);  
    }
    public function list_education($id)
    {
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'Education Qualification List';
        $this->load->view('nurse_profile/education/list_education',$data);  
    }
    public function edit_education($id)
    {
        $data['form_title'] = 'Education Qualification Update';
        $this->load->view('nurse_profile/education/edit_education',$data);  
    }



      // ================= NURSE ADDITIONAL CERTIFICATE =================== //

      public function add_additional_certificate($id)
      {
          $data['form_title'] = 'Add Additional Certifcate';
          $this->load->view('nurse_profile/education/add_additional_certificate',$data);  
      }
      public function list_additional_certificate($id)
      {
          $data['responsive_table'] = 'true';
          $data['form_title'] = 'Additional Certifcate List';
          $this->load->view('nurse_profile/education/list_additional_certificate',$data);  
      }
      public function edit_additional_certificate($id)
      {
          $data['form_title'] = 'Additional Certifcate Update';
          $this->load->view('nurse_profile/education/edit_education',$data);  
      }




    public function update_qualification($id)
    {
  
        // echo json_encode($_FILES); 
        // exit;

        //for nurse_education_certificate
        $data['post_basic'] = html_escape(trim($this->input->post('postbasic')));

        $data['degree'] = html_escape(trim($this->input->post('degree')));

        $data['diploma'] = html_escape(trim($this->input->post('diploma')));

        $data['certificate'] = html_escape(trim($this->input->post('certificate')));

        $data['reg_number'] = html_escape(trim($this->input->post('lim_reg_number')));

        // $date=date_create("2013-03-15"); echo date_format($date,"Y/m/d H:i:s");

        $date = html_escape(trim($this->input->post('reg_date')));

        $data['reg_date'] = date_format(date_create($date), "Y/m/d");

        //for nurse_additional_certificate
        $data2['bls'] = html_escape(trim($this->input->post('bls')));

        $data2['cannulation'] = html_escape(trim($this->input->post('cannulation')));

        $data2['palliative'] = html_escape(trim($this->input->post('palliative')));

        $data2['others'] = html_escape(trim($this->input->post('others')));

        $data2['retention'] = html_escape(trim($this->input->post('retention')));

        $data2['driver_license'] = html_escape(trim($this->input->post('driver_license')));

        $data2['have_own_transport'] = html_escape(trim($this->input->post('have_own_transport')));

        $data['nurse_id'] = $data2['nurse_id'] = $id;

        $data['add_date'] = $data2['add_date'] = date("Y-m-d H:i:s");

        if($_FILES['lm_reg_certificate']['name'] != '')
            { 
             $data2['lm_reg_certificate'] ='';
             
             $i_ext = explode('.', $_FILES['lm_reg_certificate']['name']);
             $target_path = $id."_".date("YmdHis").'_lm_reg_certificate.'.end($i_ext);
             $_FILES['lm_reg_certificate']['name'] = $target_path;
 
             $this->upload->initialize($this->set_upload_options($_FILES['lm_reg_certificate']['name'],'uploads/'));
             $size = getimagesize($_FILES['lm_reg_certificate']['tmp_name']);
             $this->upload->do_upload();  
             
             if (move_uploaded_file($_FILES['lm_reg_certificate']['tmp_name'], 'uploads/' . $target_path))
             {
                 if ($size[0] == 150 || $size[1] == 150) 
                 {


                 } 
                else {
                 $imageWidth = 150; //Contains the Width of the Image
                 $imageHeight = 150;
                 $this->resize($imageWidth, $imageHeight, "uploads/" . $target_path, 
                    "uploads/" . $target_path);
                } 
                $data2['lm_reg_certificate'] = $target_path;
             } 
         }


        // exit;

        $res = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'nurse_education_certificate');

        $res2 = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'nurse_additional_certificate');

        if (count($res) > 0 && count($res2) > 0) {
            #update
            $this->nurse_model->update_function('nurse_id', $id, 'nurse_education_certificate', $data);
            $this->nurse_model->update_function('nurse_id', $id, 'nurse_additional_certificate', $data2);

            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'qualification');
            $this->session->set_flashdata('msg', 'Profile Update Successful');

        }
        else{
            #insert
            $this->nurse_model->insert('nurse_education_certificate',$data);
            $this->nurse_model->insert('nurse_additional_certificate',$data2);
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'qualification');
            $this->session->set_flashdata('msg', 'Profile Update Successful.');
              
        }

        if ($this->session->userdata('user_role') == 1) {

            redirect('nurse-edit-profile/'.$id); 
        }
        else{
            redirect('nurse-profile'); 
        }

    }
    // ================= NURSE EXPERIENCE =================== //
    public function add_experience($id)
    {
        $data['form_title'] = 'Add Experience';
        $this->load->view('nurse_profile/experience/add_experience',$data);  
    }
    public function list_experience($id)
    {
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'List Experience';
        $this->load->view('nurse_profile/experience/list_experience',$data);  
    }
    public function edit_experience()
    {
        $data['custom_datepicker'] = '<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>';

        $id = $this->uri->segment(2);


        $data['form_title'] = 'Update Experience';
         $expdet = $this->nurse_model->select_with_where('*',"exper_det_id=".$id,'nurse_experience_details');
         $data['organisation']= $expdet[0]['organisation'];
         $data['title']= $expdet[0]['title'];
        $data['duty_description']= $expdet[0]['duty_description']; 
        $data['training_attended']=$expdet[0]['training_attended']; 
        $data['ref']=$expdet[0]['ref']; 
        $data['start_date']=date( 'm/d/Y',strtotime($expdet[0]['start_date'])); 
        $data['end_date']=date( 'm/d/Y',strtotime($expdet[0]['end_date']));
        $data['exper_det_id']=$expdet[0]['exper_det_id'];  
        $data['nurse_id']=$expdet[0]['nurse_id'];  
        $this->load->view('nurse_profile/experience/edit_experience',$data);  
    }

public function update_experience_post()
{


        $exper_det_id=$this->input->post('exper_det_id');
        $id=$this->input->post('nurse_id');
        $organisation = $this->input->post('organisation');

        $start_date = $this->input->post('start_date');

        $end_date = $this->input->post('end_date');

        $title = $this->input->post('title');



        $duty_description = $this->input->post('desc_of_duties');
        $ref = $this->input->post('references');
        // $start_date = date_format(date_create($startdate), "Y/m/d");
        // $end_date = date_format(date_create($enddate), "Y/m/d");

        $training_attended = $this->input->post('training_attended');
      
        $data['nurse_id'] = $id;
        $data['add_date'] = date("Y-m-d H:i:s");
        $data2['organisation']=$organisation;
        $data2['title']=$title;
        $data2['start_date']=date_format(date_create($start_date), "Y/m/d");
        $data2['end_date']=date_format(date_create($end_date), "Y/m/d");
        $data2['duty_description']=$duty_description;
        $data2['ref']=$ref;
       $this->nurse_model->update_function('exper_det_id', $exper_det_id, 'nurse_experience_details', $data2);

            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active','experience');
            $this->session->set_flashdata('msg', 'Profile Update Successful');
        if ($this->session->userdata('user_role') == 1) {

            redirect('nurse-edit-profile/'.$id); 
        }
        else{
            redirect('nurse-profile'); 
        }
}



    public function update_experience($id)
    {

        // echo json_encode($_POST); 
        // exit;


        $organisation = $this->input->post('organisation');

        $start_date = $this->input->post('start_date');

        $end_date = $this->input->post('end_date');

        $title = $this->input->post('title');



        $duty_description = $this->input->post('duty_description');
        $ref = $this->input->post('ref');

        $startdate = $this->input->post('start_date');
        $enddate = $this->input->post('end_date');

        // $start_date = date_format(date_create($startdate), "Y/m/d");
        // $end_date = date_format(date_create($enddate), "Y/m/d");

        $training_attended = $this->input->post('training_attended');
      
        $data['nurse_id'] = $id;
        $data['add_date'] = date("Y-m-d H:i:s");
  

            $data['nurse_id'] = $id;
            $data['add_date'] = date("Y-m-d H:i:s");
      $res = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'nurse_experience');
      $cnt=count($res);
      if($cnt==0)
      {
        $ret = $this->nurse_model->insert_ret('nurse_experience',$data);
      }
      else
      {
        $ret=$res[0]['expid'];
      }

 $expdet = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'nurse_experience_details');
 $count=count($expdet);
 if($count==10)
 {
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'experience');
            $this->session->set_flashdata('msg', 'Cant Add More than 10 Experience');
 }
 elseif($count<10)
 {

            foreach ($title as $key => $value) {
                $data2['exp_id'] = $ret;
                $data2['nurse_id'] = $id;
                $data2['start_date'] = date_format(date_create($startdate[$key]), "Y/m/d") ;
                $data2['end_date'] = date_format(date_create($enddate[$key]), "Y/m/d") ;
                $data2['organisation'] = html_escape(trim($organisation[$key]));
                $data2['title'] = html_escape(trim($title[$key]));
                $data2['duty_description'] = html_escape(trim($duty_description[$key]));
                $data2['training_attended'] = html_escape(trim($training_attended[$key]));
                $data2['ref'] = html_escape(trim($ref[$key]));
                $data2['add_date'] = date("Y-m-d H:i:s");
                $this->nurse_model->insert('nurse_experience_details',$data2);
            }
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'experience');
            $this->session->set_flashdata('msg', 'Profile Update Successful.');
        }
            

        if ($this->session->userdata('user_role') == 1) {

            redirect('nurse-edit-profile/'.$id); 
        }
        else{
            redirect('nurse-profile'); 
        }

    }


public function delete_experience($exp_id,$id)
{
     $this->nurse_model->delete_function('nurse_experience_details','exper_det_id',$exp_id);
     $this->session->set_flashdata('type', 'success');
     $this->session->set_flashdata('active', 'experience');
     $this->session->set_flashdata('msg', 'Delete Successful.');
        if ($this->session->userdata('user_role') == 1) {

            redirect('nurse-edit-profile/'.$id); 
        }
        else{
            redirect('nurse-profile'); 
        }
}
    // ================= NURSE NOTES =================== //
    public function add_notes($id)
    {
        $data['form_title'] = 'Add Notes';

        $this->load->view('nurse_profile/notes/add_notes',$data);  
    }
    public function list_notes($id)
    {
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'List of Notes for Arju';
        $this->load->view('nurse_profile/notes/list_notes',$data);  
    }
    public function edit_notes($id)
    {
        $data['form_title'] = 'Update Notes';
        $this->load->view('nurse_profile/notes/edit_notes',$data);  
    }

    // ================= NURSE REVIEWS =================== //
    public function add_reviews($id)
    {
        $data['form_title'] = 'Add Review';
        $this->load->view('nurse_profile/reviews/add_reviews',$data);  
    }
    public function list_reviews($id)
    {   
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'List Review';
        $this->load->view('nurse_profile/reviews/list_reviews',$data);  
    }
    
    public function edit_reviews($id)
    {
        $data['form_title'] = 'Update Review';
        $this->load->view('nurse_profile/reviews/edit_reviews',$data);  
    }

    // ================= NURSE COMPTENCIES =================== //
    public function add_competency($id)
    {
        $data['form_title'] = 'Add Competencies';
        $this->load->view('nurse_profile/competency/add_competency',$data);  
    }
    public function list_competency($id)
    {
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'List Competencies';
        $this->load->view('nurse_profile/competency/list_competency',$data);  
    }
    public function edit_competency($id)
    {
        $data['form_title'] = 'Update Competencies';
        $this->load->view('nurse_profile/competency/edit_competency',$data);  
    }
    public function update_Competencies($id)
    {

        // print_r($)
        $nursing_competency=$this->input->post('nursing_competency');
        $nursing_competency_desc=$this->input->post('nursing_competency_desc');
        // print_r($nursing_competency);
        // print_r($nursing_competency_desc);
        $new_arr = array();

        foreach ($nursing_competency_desc as $value) {
            if ($value != '') {
                array_push($new_arr, $value);
            }
        }

        // print_r($new_arr);
        // // 
        // exit;
        $cnt=count($nursing_competency);
        $res = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'competencies_details');
        if($res==0)
        {
        foreach ($nursing_competency as $key => $value) 
        {  
            $data2['nurse_id'] = $id;                
            $data2['compe_id'] = $nursing_competency[$key];
            // if ($nursing_competency_desc[$key] != 0) {
            //     $data2['com_value'] =$nursing_competency_desc[$key];
            // }
            $data2['com_value'] = $new_arr[$key];
            $data2['add_date'] = date("Y-m-d H:i:s");
            //check this competancy id exist or not
            
            $this->nurse_model->insert_ret('competencies_details',$data2);
       }
   
       }
       else        
       {
        foreach ($nursing_competency as $key => $value) 
        {
              
        $data2['nurse_id'] = $id;                
        $data2['compe_id'] =$nursing_competency[$key];
        //echo $data2['compe_id'];
        //echo "</br>";
        // if ($nursing_competency_desc[$key] != '') {
        //     $data2['com_value'] = $nursing_competency_desc[$key];
        // }
        $data2['com_value'] = $new_arr[$key];
        $data2['add_date'] = date("Y-m-d H:i:s");
        //check this competancy id exist or not
        //echo "scac";
        //echo "</br>";

        $cid=$data2['compe_id'];
        
        if(isset($data2['compe_id']))
        {
            //echo $cid;

        $ch=$this->nurse_model->double_cond_where('competencies_details','nurse_id',$data2['nurse_id'],'compe_id',$cid);
        $cn=count($ch);
        if($cn==0)
        {
         $this->nurse_model->insert_ret('competencies_details',$data2);   
        }
        else
        {
           $this->nurse_model->update_function('compe_id',$data2['compe_id'],'competencies_details',$data2);  
        }
        }
        else
        {
          $this->nurse_model->insert_ret('competencies_details',$data2);     
        }


       }

       }
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'competency');
            $this->session->set_flashdata('msg', 'Insert Successful.');

        if ($this->session->userdata('user_role') == 1) {

            redirect('nurse-edit-profile/'.$id); 
        }
        else{
            redirect('nurse-profile'); 
        } 
    }
    // ================= NURSE SHEDULE =================== //

    public function add_schedule($id)
    {
        $data['form_title'] = 'Add Schedule';     
       $data['nurse_details'] = $this->nurse_model->select_with_where('*',"loginid=".$this->login_id,'users');  
        $data['custom_datepicker'] = '<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>';
        $data['schedule_day']= $this->nurse_model->daily_schedule($this->login_id);
        $data['day_title']=$this->nurse_model->select_all('daily_schedule');
        $data['special_schedule']= $this->nurse_model->select_with_where('*',"nurse_id=".$this->login_id,'special_schedule');   
        $data['options'] = '';

        //for time options 

        for($hours=0; $hours<24; $hours++) // the interval for hours is '1'
        for($mins=0; $mins<60; $mins+=15)  // the interval for mins is '30'
        $data['options'] .= '<option value="'.str_pad($hours,2,'0',STR_PAD_LEFT).':'
               .str_pad($mins,2,'0',STR_PAD_LEFT).'">'.str_pad($hours,2,'0',STR_PAD_LEFT).':'
               .str_pad($mins,2,'0',STR_PAD_LEFT).'</option>';  



        $this->load->view('nurse_profile/schedule/add_schedule',$data);  
    }
    public function add_schedule_admin()
    {
        $id = $this->uri->segment(2);
        $data['form_title'] = 'Add Schedule';     
       $data['nurse_details'] = $this->nurse_model->select_with_where('*',"loginid=".$id,'users');  
        $data['custom_datepicker'] = '<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>';
        $data['schedule_day']= $this->nurse_model->daily_schedule($id);
        $data['day_title']=$this->nurse_model->select_all('daily_schedule');
        $data['special_schedule']= $this->nurse_model->select_with_where('*',"nurse_id=".$id,'special_schedule');   
        $data['options'] = '';

        //for time options 

        for($hours=0; $hours<24; $hours++) // the interval for hours is '1'
        for($mins=0; $mins<60; $mins+=15)  // the interval for mins is '30'
        $data['options'] .= '<option value="'.str_pad($hours,2,'0',STR_PAD_LEFT).':'
               .str_pad($mins,2,'0',STR_PAD_LEFT).'">'.str_pad($hours,2,'0',STR_PAD_LEFT).':'
               .str_pad($mins,2,'0',STR_PAD_LEFT).'</option>';  



        $this->load->view('nurse_profile/schedule/add_schedule',$data);  
    }
    public function schedule_add()
    {
        $id = $this->uri->segment(2);
        $data['form_title'] = 'Add Schedule';     
       $data['nurse_details'] = $this->nurse_model->select_with_where('*',"loginid=".$id,'users');  
        $data['custom_datepicker'] = '<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>';
        $data['schedule_day']= $this->nurse_model->daily_schedule($id);
        $data['day_title']=$this->nurse_model->select_all('daily_schedule');
        $data['special_schedule']= $this->nurse_model->select_with_where('*',"nurse_id=".$id,'special_schedule');   
        $data['options'] = '';

        //for time options 

        for($hours=0; $hours<24; $hours++) // the interval for hours is '1'
        for($mins=0; $mins<60; $mins+=15) // the interval for mins is '30'
        $data['options'] .= '<option value="'.str_pad($hours,2,'0',STR_PAD_LEFT).':'
               .str_pad($mins,2,'0',STR_PAD_LEFT).'">'.str_pad($hours,2,'0',STR_PAD_LEFT).':'
               .str_pad($mins,2,'0',STR_PAD_LEFT).'</option>';  



        $this->load->view('nurse_profile/schedule/add_schedule',$data);  
    }
    public function nurse_weekly_schedule_add($id)
    {
        // print_r($_POST);
        // exit;
        $day=$this->input->post('day');
        $sun_start_time=$this->input->post('sun_start_time');
        $sun_end_time=$this->input->post('sun_end_time');

        $mon_start_time=$this->input->post('mon_start_time');
        $mon_end_time=$this->input->post('mon_end_time'); 
        $tue_start_time=$this->input->post('tue_start_time');
        $tue_end_time=$this->input->post('tue_end_time'); 
        $wed_start_time=$this->input->post('wed_start_time');
        $wed_end_time=$this->input->post('wed_end_time'); 
        $thu_start_time=$this->input->post('thu_start_time');
        $thu_end_time=$this->input->post('thu_end_time');  
        $fri_start_time=$this->input->post('fri_start_time');
        $fri_end_time=$this->input->post('fri_end_time'); 
        $sat_start_time=$this->input->post('sat_start_time');
        $sat_end_time=$this->input->post('sat_end_time');
        $cnt=count($day);
    
        foreach ($day as $key => $value)
           {
               $daytile=$day[$key];
               $data['schedule_day']=$daytile;
               $data['nurse_id']=$id;
               $data['add_date']=date("Y-m-d H:i:s");
                  if($data['schedule_day']=="sunday")
                  {
                    $datasc['schedule_day']="Sun";
                  } 
                  elseif($data['schedule_day']=="monday")
                  {
                    $datasc['schedule_day']="Mon";
                  }
                  elseif($data['schedule_day']=="tuesday")
                  {
                    $datasc['schedule_day']="Tue";
                  } 
                  elseif($data['schedule_day']=="tuesday")
                  {
                    $datasc['schedule_day']="Tue";
                  } 
                  elseif($data['schedule_day']=="wednesday")
                  {
                    $datasc['schedule_day']="Wed";
                  } 
                  elseif($data['schedule_day']=="thursday")
                  {
                    $datasc['schedule_day']="Thu";
                  } 
                  elseif($data['schedule_day']=="friday")
                  {
                    $datasc['schedule_day']="Fri";
                  }   
                  elseif($data['schedule_day']=="saturday")
                  {
                    $datasc['schedule_day']="Sat";
                  }  
                   $date_title=$datasc['schedule_day']; 
               $dt=$this->nurse_model->double_cond_where('schedule_day','schedule_day',$date_title,'nurse_id',$id);
                $count=count($dt);
                if($count==0)
                {
                  $chk['dayt']= $this->nurse_model->select_with_where('*','day_title="'.$daytile.'"','day_title');
                  $data['schedule_day']=$chk['dayt'][0]['day_title_sum']; 

                  if($data['schedule_day']=="sunday")
                  {
                    $datasc['schedule_day']="Sun";
                  } 
                  elseif($data['schedule_day']=="monday")
                  {
                    $datasc['schedule_day']="Mon";
                  }
                  elseif($data['schedule_day']=="tuesday")
                  {
                    $datasc['schedule_day']="Tue";
                  } 
                  elseif($data['schedule_day']=="tuesday")
                  {
                    $datasc['schedule_day']="Tue";
                  } 
                  elseif($data['schedule_day']=="wednesday")
                  {
                    $datasc['schedule_day']="Wed";
                  } 
                  elseif($data['schedule_day']=="thursday")
                  {
                    $datasc['schedule_day']="Thu";
                  } 
                  elseif($data['schedule_day']=="friday")
                  {
                    $datasc['schedule_day']="Fri";
                  }   
                  elseif($data['schedule_day']=="saturday")
                  {
                    $datasc['schedule_day']="Sat";
                  } 

                  $rat=$this->nurse_model->insert_ret('schedule_day',$data);   
                }
                else
                {
                    $rat=$dt[0]['scheudle_id'];
                }

              
               if($daytile=="sunday")
               {
                 $i=1;
                $dt="Sun";

                foreach ($sun_start_time as $timekey => $value)
                 {
                    $i+=1;
                   $start_time=$sun_start_time[$timekey];
                   $end_time=$sun_end_time[$timekey];
                   //print_r($start_time);
                    $datetime1 = new DateTime($start_time);
                    $datetime2 = new DateTime($end_time);
                    $interval = $datetime1->diff($datetime2);

                        //echo "okk";
               $dtd=$this->nurse_model->double_cond_where('schedule_day_time','day_title',$dt,'nurse_id',$id);
                $count=count($dtd);
                if($count==0)
                {
                    //echo "okoko";
                    $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);

                    if(!empty($start_time))
                    {
                       if(!empty($end_time))
                       {
                $m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime); 
                       }  
                    }
                   
                } 
                else
                {
                  if($count==1)
                      {
                        //echo "okoko11";
                   $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);
                    if(!empty($start_time))
                    {
                       if(!empty($end_time))
                       {
                $m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime); 
                       }  
                    }      
                      } 
                    elseif($count==2)
                    {
                        //echo "okoko12";
                        $scid=$dtd[0]['scid'];
                   $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);
                    $m=$this->nurse_model->update_function('scid',$scid,'schedule_day_time',$schedule_dtime);       
                    }   
                }

                 }
               }
               elseif ($daytile=="monday")
                {
                     $i=0;
                     $dt="Mon";
                    foreach ($mon_start_time as $timekey => $value)
                     {
                        $i+=1;
                       $start_time=$mon_start_time[$timekey];
                       $end_time=$mon_end_time[$timekey];
               $dtd=$this->nurse_model->double_cond_where('schedule_day_time','day_title',$dt,'nurse_id',$id);
                $count=count($dtd);
                if($count==0)
                {
                    $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);
            //$m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime);

                if(!empty($start_time))
                    {
                       if(!empty($end_time))
                       {
                $m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime); 
                       }  
                    }            
                } 
                else
                {
                  if($count==1)
                      {
                   $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);
                //$m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime);  
                if(!empty($start_time))
                    {
                       if(!empty($end_time))
                       {
                $m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime); 
                       }  
                    }        
                      } 
                     elseif($count==2)
                    {
                        $scid=$dtd[0]['scid'];
                   $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);
                    $m=$this->nurse_model->update_function('scid',$scid,'schedule_day_time',$schedule_dtime);       
                    }   
                }
                     }
                }
               elseif ($daytile=="tuesday")
                {
                     $i=0;
                     $dt="Tue";
                    foreach ($tue_start_time as $timekey => $value)
                     {
                        $i+=1;
                       $start_time=$tue_start_time[$timekey];
                       $end_time=$tue_end_time[$timekey];
               $dtd=$this->nurse_model->double_cond_where('schedule_day_time','day_title',$dt,'nurse_id',$id);
                $count=count($dtd);
                if($count==0)
                {
                    $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);
        //$m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime);
                if(!empty($start_time))
                    {
                       if(!empty($end_time))
                       {
                $m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime); 
                       }  
                    }            
                } 
                else
                {
                   if($count==1)
                      {
                   $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);
        //$m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime);
                if(!empty($start_time))
                    {
                       if(!empty($end_time))
                       {
                $m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime); 
                       }  
                    }       
                      } 
                     elseif($count==2)
                    {
                        $scid=$dtd[0]['scid'];
                   $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$daytile);
                    $m=$this->nurse_model->update_function('scid',$scid,'schedule_day_time',$schedule_dtime);       
                    }   
                }
                     }
                }
               elseif($daytile=="wednesday")
                {
                     $i=0;
                     $dt="Wed";
                    foreach ($wed_start_time as $timekey => $value)
                     {
                         $i+=1;
                       $start_time=$wed_start_time[$timekey];
                       $end_time=$wed_end_time[$timekey];
               $dtd=$this->nurse_model->double_cond_where('schedule_day_time','day_title',$dt,'nurse_id',$id);
                $count=count($dtd);
                if($count==0)
                {
                    $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);
    //$m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime);
                if(!empty($start_time))
                    {
                       if(!empty($end_time))
                       {
                $m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime); 
                       }  
                    }                 
                } 
                else
                {
                    //echo "okoko";
                  if($count==1)
                      {
                   $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);
    //$m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime); 
                if(!empty($start_time))
                    {
                       if(!empty($end_time))
                       {
                $m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime); 
                       }  
                    }       
                      } 
                    elseif($count==2)
                    {
                        $scid=$dtd[0]['scid'];
                   $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$daytile);
                    $m=$this->nurse_model->update_function('scid',$scid,'schedule_day_time',$schedule_dtime);       
                    }   
                }
                     }
                }
               elseif ($daytile=="thursday")
                {
                     $i=0;
                     $dt="Thu";
                    foreach ($thu_start_time as $timekey => $value)
                     {
                        $i+=1;
                       $start_time=$thu_start_time[$timekey];
                       $end_time=$thu_end_time[$timekey];
               $dtd=$this->nurse_model->double_cond_where('schedule_day_time','day_title',$dt,'nurse_id',$id);
                $count=count($dtd);
                if($count==0)
                {
                    $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);
    //$m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime);
                if(!empty($start_time))
                    {
                       if(!empty($end_time))
                       {
                $m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime); 
                       }  
                    } 

                } 
                else
                {
                  if($count==1)
                      {
                   $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);
    //$m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime); 
                if(!empty($start_time))
                    {
                       if(!empty($end_time))
                       {
                $m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime); 
                       }  
                    }       
                      } 
                    elseif($count==2)
                    {
                        $scid=$dtd[0]['scid'];
                   $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);
                    $m=$this->nurse_model->update_function('scid',$scid,'schedule_day_time',$schedule_dtime);       
                    }   
                }
                     }
                }
               elseif ($daytile=="friday")
                {
                     $i=0;
                     $dt="Fri";
                    foreach ($fri_start_time as $timekey => $value)
                     {
                        $i+=1;
                       $start_time=$fri_start_time[$timekey];
                       $end_time=$fri_end_time[$timekey];
               $dtd=$this->nurse_model->double_cond_where('schedule_day_time','day_title',$dt,'nurse_id',$id);
                $count=count($dtd);
                if($count==0)
                {
                    $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);
    //$m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime);

                if(!empty($start_time))
                    {
                       if(!empty($end_time))
                       {
                $m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime); 
                       }  
                    }                 
                } 
                else
                {
                  if($count==1)
                      {
                   $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);
    //$m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime);  
                    if(!empty($start_time))
                    {
                       if(!empty($end_time))
                       {
                $m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime); 
                       }  
                    }     
                      } 
                   elseif($count==2)
                    {
                        $scid=$dtd[0]['scid'];
                   $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$daytile);
                    $m=$this->nurse_model->update_function('scid',$scid,'schedule_day_time',$schedule_dtime);       
                    }   
                }
                     }
                }
               elseif ($daytile=="saturday")
                {
                     $i=0;
                     $dt="Sat";
                    foreach ($fri_start_time as $timekey => $value)
                     {
                        $i+=1;
                       $start_time=$sat_start_time[$timekey];
                       $end_time=$sat_end_time[$timekey];
               $dtd=$this->nurse_model->double_cond_where('schedule_day_time','day_title',$dt,'nurse_id',$id);
                $count=count($dtd);
                if($count==0)
                {
                    $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);
    //$m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime);
                if(!empty($start_time))
                    {
                       if(!empty($end_time))
                       {
                $m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime); 
                       }  
                    } 

                } 
                else
                {
                  if($count==1)
                      {
                   $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);
    //$m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime);
                if(!empty($start_time))
                    {
                       if(!empty($end_time))
                       {
                $m=$this->nurse_model->insert_ret('schedule_day_time',$schedule_dtime); 
                       }  
                    } 

                      } 
                    elseif($count==2)
                    {
                        $scid=$dtd[0]['scid'];
                   $schedule_dtime = array('nurse_id' =>$id ,'start_time' =>$start_time,'end_time'=> $end_time,'sch_id'=>$rat,'add_date'=>date("Y-m-d H:i:s"),'day_title'=>$dt);
                    $m=$this->nurse_model->update_function('scid',$scid,'schedule_day_time',$schedule_dtime);       
                    }   
                } 
                     }
                }
           } 

        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('active', 'weekly');
        $this->session->set_flashdata('msg', 'Insert Successful.');
        if ($this->session->userdata('user_role') == 1) {

            redirect('nurse-schedule-add_admin/'.$id, 'refresh'); 
        }
        else{
           redirect('nurse-schedule-add', 'refresh'); 
        } 
   
    }

    public function nurse_special_schedule_add($id)
    {
       $sdate=$this->input->post('sdate');
       $start_time=$this->input->post('start_time');
       $end_time=$this->input->post('end_time');
$special = array('nurse_id'=>$id,'sdate'=>$sdate,'start_time'=>$start_time,'end_time'=>$end_time,'add_date'=>date("Y-m-d H:i:s"));
        $m=$this->nurse_model->insert_ret('special_schedule',$special); 
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'tabs-special');
            $this->session->set_flashdata('msg', 'Insert Successful.');
        if ($this->session->userdata('user_role') == 1) {

            redirect('nurse-schedule-add/'.$id); 
        }
        else{
            redirect('nurse-schedule-add'); 
        } 
    }
    public function schedule_day_delete($scid,$id)
    {
         $m=$this->nurse_model->delete_function('schedule_day_time','scid',$scid); 
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'weekly');
            $this->session->set_flashdata('msg', 'Insert Successful.');

        if ($this->session->userdata('user_role') == 1) {

            redirect('nurse-schedule-add/'.$id); 
        }
        else{
            redirect('nurse-schedule-add'); 
        }   
    }
    public function schedule_day_title_delete($scid,$id)
    {
         $m=$this->nurse_model->delete_function('schedule_day','scheudle_id',$scid); 
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'weekly');
            $this->session->set_flashdata('msg', 'Insert Successful.');

        if ($this->session->userdata('user_role') == 1) {

            redirect('nurse-schedule-add/'.$id); 
        }
        else{
            redirect('nurse-schedule-add'); 
        }   
    }

    public function special_schedule_delete($scid,$id)
     {
            $m=$this->nurse_model->delete_function('special_schedule','special_sc_id',$scid); 
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'tabs-special');
            $this->session->set_flashdata('msg', 'Insert Successful.');

        if ($this->session->userdata('user_role') == 1) {

            redirect('nurse-schedule-add/'.$id); 
        }
        else{
            redirect('nurse-schedule-add'); 
        } 
     }
public function add_schedule_monthly($id)
    {
        $data['form_title'] = 'My Monthly Schedule';
        $id = $this->login_id;
        $schedule_list = $this->nurse_model->select_where_left_join('*, schedule_day.nurse_id AS nurse_id_sche', 'schedule_day', 'schedule_day_time', 'schedule_day.scheudle_id = schedule_day_time.sch_id', "schedule_day.nurse_id=".$id);
        $inputs = [];

        date_default_timezone_set('Asia/Dhaka');

        for ($j=0; $j<count($schedule_list); $j++)
        {
            $schedule_day = $schedule_list[$j]['schedule_day'];
            $getThisMonth = date('m');
            $getThisMonthDays = date('t');
            $getDayName = date('D');

                for($i=1; $i<=$getThisMonthDays; $i++)
                {

                    $date = date('Y')."-".$getThisMonth."-".($i<9?"0".$i:$i);

                    $day = date("D", strtotime($date));
                    if( lcfirst($day) == lcfirst($schedule_day)){

                        $d = array(
                            'title' => " ",
                            'start' => $date.(isset($schedule_list[$j]['start_time'])?"T".$schedule_list[$j]['start_time']:"")
                             
                        );
                        (isset($schedule_list[$j]['end_time'])? $d['end'] = $date."T".$schedule_list[$j]['end_time']:"");

                        $inputs[] = $d;
                    }
                }

        }

        $special_schedule_list = $this->nurse_model->select_with_where('*', "nurse_id=".$id, "special_schedule");

        for ($z=0; $z<count($special_schedule_list); $z++)
        {
            $date = $special_schedule_list[$z]['sdate'];
            $d = array(
                'title' => " ",
                'start' => $date.(isset($special_schedule_list[$z]['start_time'])?"T".$special_schedule_list[$z]['start_time']:"")
            );

            (isset($special_schedule_list[$z]['end_time'])? $d['end'] = $date."T".$special_schedule_list[$z]['end_time']:"");

            $inputs[] = $d;
        }

        $data['schedule_list'] = $inputs;
        //print_r($data['schedule_list']);

        $this->load->view('nurse_profile/schedule/add_schedule_monthly',$data);
    }  

    
    public function list_schedule($id)
    {
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'List Schedule';
        $this->load->view('nurse_profile/schedule/list_schedule',$data);  
    }
    public function edit_schedule($id)
    {
        $data['form_title'] = 'Update Schedule';
        $this->load->view('nurse_profile/schedule/edit_schedule',$data);  
    }
    public function add_charge($value='')
    {
        $id = $this->uri->segment(3);
        $data['nurse_id']=$id;
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'Add Charge';
        $nurse_details=$this->nurse_model->select_with_where('*',"loginid=".$id,'users'); 
        $data['name']=$nurse_details[0]['first_name'].' '.$nurse_details[0]['last_name'];
        $data['contact_no']=$nurse_details[0]['contact_no'];
        $this->load->view('nurse_profile/nurse_charge/add_charge',$data);  
    }
    public function add_booking_charge($value='')
    {
        $nurse_id=$this->input->post('nurse_id');
        $nurse_charge=$this->input->post('nurse_charge');
        $data['charge']=$nurse_charge;
        $nurse_charge_set = array('nurse_id' =>$nurse_id,'nurse_charge'=>$nurse_charge,'add_date'=>date("Y-m-d H:i:s"));
        $ins=$this->nurse_model->insert_ret('nurse_charge',$nurse_charge_set);
        $update=$this->nurse_model->update_function('loginid',$nurse_id,'users',$data);
        $this->session->set_flashdata('msg','Booking Charge Successfully Set ');
        redirect("nurse/add_charge/$nurse_id",'refresh');
    }
    public function set_experience($value='')
    {
        $id = $this->uri->segment(3);
        $data['nurse_id']=$id;
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'Add Experience';
        $nurse_details=$this->nurse_model->select_with_where('*',"loginid=".$id,'users'); 
        $data['name']=$nurse_details[0]['first_name'].' '.$nurse_details[0]['last_name'];
        $data['contact_no']=$nurse_details[0]['contact_no'];
        $this->load->view('nurse_profile/nurse_charge/add_exp',$data);  
    } 
    public function set_experience_post($value='')
    {
        $nurse_id=$this->input->post('nurse_id');
        $nurse_exp=$this->input->post('nurse_exp');
        $data['total_experience']=$nurse_exp;

        $update=$this->nurse_model->update_function('loginid',$nurse_id,'users',$data);
        $this->session->set_flashdata('msg','Experience  Successfully Added');
        redirect("nurse/set_experience/$nurse_id",'refresh');
    }
    public function load()
    {
         $event_data = $this->nurse_model->fetch_all_event();
          foreach($event_data->result_array() as $row)
          {
           $data[] = array(
            'id' => $row['id'],
            'title' => $row['title'],
            'start' => $row['start_event'],
            'end' => $row['end_event']
           );
          }
          echo json_encode($data);
    }
    public function insert()
    {
        if($this->input->post('title'))
          {
           $data = array(
            'title'  => $this->input->post('title'),
            'start_event'=> $this->input->post('start'),
            'end_event' => $this->input->post('end')
           );
           $this->nurse_model->insert_event($data);
          }
    }

     public function update()
     {
      if($this->input->post('id'))
      {
       $data = array(
        'title'   => $this->input->post('title'),
        'start_event' => $this->input->post('start'),
        'end_event'  => $this->input->post('end')
       );

       $this->nurse_model->update_event($data, $this->input->post('id'));
      }
     }
    public function delete()
     {
      if($this->input->post('id'))
      {
       $this->nurse_model->delete_event($this->input->post('id'));
      }
     }
    // public function add_schedule_monthly()
    // {
    //     $data['result'] = $this->db->get("events")->result();
   
    //     foreach ($data['result'] as $key => $value) {
    //         $data['data'][$key]['title'] = $value->title;
    //         $data['data'][$key]['start'] = $value->start_date;
    //         $data['data'][$key]['end'] = $value->end_date;
    //         $data['data'][$key]['backgroundColor'] = "#00a65a";
    //     }
           
    //      $this->load->view('nurse_profile/schedule/add_schedule_monthly',$data);  
    // }

    // ================= NURSE PATIENT LIST =================== //

    public function nurse_patient_list($id)
    {
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'My Patient List';
//  $data['book_details']=$this->nurse_model->select_where_join('first_name,last_name,contact_no,
// profile_pic,email,time_in_hours,fromtime,totime,adddate,notes,book_status,book_charge,patient_id','nurse_booking','users','users.loginid=nurse_booking.patient_id',"nurse_id=".$this->login_id);
        $data['book_details']=$this->nurse_model->mypatient($this->login_id);
        $this->load->view('nurse_profile/booking/list_booking',$data);  
    }

    // ================= NURSE ASSESSMENT =================== //

    public function add_assessment($id)
    {
        $data['form_title'] = 'Add Assessment';
        $this->load->view('nurse_profile/assessment/add_assessment',$data);  
    }
    public function list_assessment($id)
    {
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'List Assessment';
        $this->load->view('nurse_profile/assessment/list_assessment',$data);  
    }
    public function edit_assessment($id)
    {
        $data['form_title'] = 'Update Assessment';
        $this->load->view('nurse_profile/assessment/edit_assessment',$data);  
    }
    //=========================OTHER FUNCTIONS=========================== //

    function encryptIt($string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
        $secret_iv = 'This is my secret iv';
        // hash
        $key = hash('sha256', $secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        $output=str_replace("=", "", $output);
        return $output;
    }

    private function set_upload_options($file_name,$folder_name)
    {   
        //upload an image options
        $url=base_url();

        $config = array();
        $config['file_name'] = $file_name;
        $config['upload_path'] = 'uploads/'.$folder_name;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']      = '0';
        $config['overwrite']     = TRUE;

        return $config;
    }
    //general function start

    public function resize($height, $width, $source, $destination)
    {
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = $source;
        $config['overwrite'] = TRUE;
        $image_config['quality'] = "100%";
        $image_config['maintain_ratio'] = FALSE;
        $config['height'] = $height;
        $config['width'] = $width;
        $config['new_image'] = $destination; //you should have write permission here..
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }
    public function delete_nurse($patient_id)
    {
        $this->nurse_model->delete_function('users','loginid',$patient_id);         
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', 'Delete Successful.');
        redirect('nurse/nurse_list'); 
    }

}

?>
