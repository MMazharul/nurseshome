<?php $this->load->view('backend/head_link');?>

<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
    <img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
    <div class="page-main">
        <!--app-header-->
        <?php $this->load->view('backend/header');?>
        <!-- app-content-->
        <div class="container content-patient">
            <div class="side-app">
                <!-- page-header -->
                <!-- <div class="page-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
                    </ol>
                    <div class="ml-auto">
                        <div class="input-group">
                            <a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
                                <span>
                                    <i class="fa fa-calendar"></i> Events Settings
                                </span>
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                                <span>
                                    <i class="fa fa-star"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div> -->
                <!-- End page-header -->
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-10">
                            <div class="card-title">Add Nurse</div>
                            </div>
                            <div class="col-md-2">
                            <a href="nurse/nurse_list" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
                                <span> Nurse List
                                </span>
                            </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="" method="post">
                            <fieldset class="typeofwork">
                            <h4 class="">Please indicate what type of work you are looking for :</h4>
                            <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-2 text-right">
                                                    <label for="">Select one</label>
                                                </div>
                                                <div class="col-md-10">
                                                <label class="custom-control custom-radio">
                                                    <input type="radio" class="custom-control-input" name="example-radios" value="option1" checked>
                                                    <span class="custom-control-label">FUll time - Employee with Salary/EPF/SOCSO - minimum 20 days work/month</span>
                                                    </label>
                                                    <label class="custom-control custom-radio">
                                                    <input type="radio" class="custom-control-input" name="example-radios" value="option2">
                                                    <span class="custom-control-label">
                                                        Freelance or part time - work on available basis. Paid hourly
                                                    </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <!-- <fieldset class="contact">
                                <h4 class="">Contact Details</h4>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="firstname">First Name</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="firstname" name="firstname" name="input" placeholder="First Name" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="row">
                                            <div class="col-md-3 text-right">
                                                <label for="lastname">Last Name</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="lastname" name="lastname" name="input" placeholder="Last Name" value="">
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                    <div class="col-md-3 text-right">
                                                        <label for="contact_no">Mobile Number</label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control"  id="contact_no" name="contact_no" name="input" placeholder="Last Name" value="">
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                    <div class="col-md-3 text-right">
                                                        <label for="email">Email</label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="email" class="form-control" id="email" name="email" name="input" placeholder="Email" value="">
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset> -->
                            <div class="row">
                                <div class="col-md-6"><h4 class="" style="line-height:2.7em">Bio Data</h4></div>
                                <div class="col-md-6">
                                    <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                    <!-- <div class="">
                                        <button class="btn btn-primary" type="submit" name="save" id="save_contact">Save</button>
                                    </div> -->
                                    </div>
                                </div>
                            </div>
                            <hr class="mt-0 ">
                            <fieldset class="biodata">
                            
                           
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                    <div class="col-md-3 text-right">
                                                        <label for="id">ID</label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control"  id="id" name="id" placeholder="NRIC/Passport" value="">
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                    <div class="col-md-3 text-right">
                                                        <label for="age">Age</label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control" id="age" name="age"  placeholder="Age" value="">
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                    <div class="col-md-3 text-right">
                                                        <label for="id">Gender</label>
                                                    </div>
                                                    <div class="col-md-9">
                                                    <select name="gender" class="form-control" id="gender">
                                                        <option value="male">Male</option>
                                                        <option value="femlae">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                    <div class="col-md-3 text-right">
                                                        <label for="age">Height</label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control" id="age" name="age"  placeholder="Age" value="">
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                    <div class="col-md-3 text-right">
                                                        <label for="age">Weight</label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control" id="age" name="age"  placeholder="Age" value="">
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                    <div class="col-md-3 text-right">
                                                        <label for="id">Photo</label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="file" name="photo" id="">
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-md-12"><h4 class="" style="line-height:2.7em">Qualification in Nursing</h4></div>
                                <!-- <div class="col-md-6"><div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                    <div class="">
                                        <button class="btn btn-primary" type="submit" name="save" id="save_contact">Save</button>
                                    </div>
                                </div></div> -->
                                </div>
                                <!-- <h4 class="">Qualifitacion in Nursing</!-->
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="age">Post basic</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="postbasic" name="postbasic"  placeholder="Post Basic" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="degree">Degree</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="degree" name="degree"  placeholder="Degree" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="age">Diploma</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="diploma" name="diploma"  placeholder="Diploma" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="certificate">Certificate</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="certificate" name="certificate"  placeholder="Certificate" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="lim_reg_number">LIM Reg. Number</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="lim_reg_number" name="lim_reg_number"  placeholder="LIM Registration Number" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="certificate">Date Registered</label>
                                                </div>
                                                <div class="col-md-9">
                                                <input class="form-control fc-datepicker" placeholder="MM/DD/YYYY" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h5>Additional Certificate</h5>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="bls">BLS</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="bls" name="bls"  placeholder="BLS" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="cannulation">Cannulation</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="cannulation" name="cannulation"  placeholder="Cannulation" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="palliative">Palliative</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="palliative" name="palliative"  placeholder="Palliative" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="Others">Others</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="Others" name="Others"  placeholder="Others" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="valid_apc_retention">Current Valid APC/Retention</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="valid_apc_retention" name="valid_apc_retention"  placeholder="Current Valid APC/Retention" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="upload_lim_reg_cert">Upload LIM REG. Certificate</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="file" class="form-control" id="upload_lim_reg_cert" name="upload_lim_reg_cert"  placeholder="Upload LIM REG. Certificate" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="driver_license">Driver License</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="driver_license" name="driver_license"  placeholder="CAR/MOTORCYCLE" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="have_own_transport">Have Own Transport</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="have_own_transport" name="have_own_transport"  placeholder="CAR/MOTORCYCLE" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                    <div class="">
                                        <button class="btn btn-primary" type="submit" name="save" id="save_contact">Save</button>
                                    </div>
                                </div> -->
                                </div>
                                
                            </fieldset>

                            <br>

                            <fieldset class="nursing_experiece">
                            <div class="row">
                            <div class="col-md-6">
                                <h4 class="" style="line-height:2.7em">Nursing Experience</h4>
                            </div>
                            <div class="col-md-6 text-right">
                                <button class="btn btn-primary" name="add_exp" id="save_contact">Add+</button>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-right">
                                                <label for="organisation">Organisation</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="organisation" name="organisation"  placeholder="Organisation" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-right">
                                                <label for="start_period">Start Date</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input class="form-control fc-datepicker" placeholder="MM/DD/YYYY" type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-right">
                                                <label for="start_period">End Date</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input class="form-control fc-datepicker" placeholder="MM/DD/YYYY" type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-right">
                                                <label for="title">Title</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input type="text" class="form-control" id="title" name="title"  placeholder="Title" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-right">
                                                <label for="desc_of_duties">Description of duties</label>
                                            </div>
                                            <div class="col-md-9">
                                            <textarea class="form-control" id="desc_of_duties" name="desc_of_duties" cols="30" rows="1">Description of duties
                                            </textarea>
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-right">
                                                <label for="references">References</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input type="text" class="form-control" id="references" name="references"  placeholder="References" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-right">
                                                <label for="desc_of_duties">Trainings Attended</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input type="text" class="form-control" id="training_attended" name="training_attended" cols="30" rows="1" placeholder="Trainings Attended" >
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </fieldset>
                            <div class="row">
                            <div class="col-md-12">
                                <h4 class="" style="line-height:2.7em">List of competencies</h4>
                            </div>
                            <!-- <div class="col-md-6 text-right">
                                <button class="btn btn-primary" name="add_exp" id="save_contact">Add+</button>
                            </div> -->
                            </div>
                            <fieldset class="competencies">
                            <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                            <div class="col-md-12 text-left">
                                                    <label for="nursing_competency_desc">Select one of following description for each competency below : </label>
                                        </div>
                                        <div class="col-md-12">
                                            <select name="nursing_competency_desc" class="form-control" id="gender">
                                                <option value="1">Trained and very confident (More than 3 years experience)</option>
                                                <option value="2">Trained but low experience and not very confident</option>
                                                <option value="3">Trained but no experience</option>
                                                <option value="4">Not trained</option>
                                            </select>
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                            <div class="col-md-12 text-left">
                                                <label for="nursing_competency">Nursing Competency</label>
                                            </div>
                                        <div class="col-md-12">
                                            <select name="nursing_competency" class="form-control" id="nursing_competency">
                                                <option value="1">
                                                    Basic personal care - basic care for cleaning, feeding, medication etc.
                                                </option>
                                                <option value="2">
                                                    Wound Dressing - Bed sores/diabetic wounds/surgical wounds
                                                </option>
                                                <option value="3">
                                                    NG Tube - Insertion and feeding
                                                </option>
                                                <option value="4">
                                                    Suction - Adults
                                                </option>
                                                <option value="5">
                                                   Urinary catheter insertion - male/female
                                                </option>
                                                <option value="5">
                                                   Colostomy Care
                                                </option>
                                                <option value="6">
                                                   Injections - IM
                                                </option>
                                                <option value="7">
                                                   Cannulation - Certified
                                                </option>
                                                <option value="8">
                                                   Central use and care
                                                </option>
                                                <option value="9">
                                                   Chemoport use and care
                                                </option>
                                                <option value="10">
                                                   Tracheostomy Care
                                                </option>
                                                <option value="11">
                                                   Palliative Care
                                                </option>
                                                <option value="12">
                                                   Others - Please Describe
                                                </option>
                                            </select>
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            </fieldset>
                            <!-- <fieldset class>
                            </fieldset> -->
                            <!-- <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="dateofbirth">Date of birth</label>
                                                </div>
                                                <div class="col-md-9">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                                        </div>
                                                    </div><input class="form-control fc-datepicker" placeholder="MM/DD/YYYY" type="text">
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="gender">Gender</label>
                                                </div>
                                                <div class="col-md-9">
                                                <select name="gender" class="form-control" id="gender">
                                                    <option value="male">Male</option>
                                                    <option value="femlae">Female</option>
                                                </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->

                                    

                                <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                    
                                    <div class="">
                                        <button class="btn btn-primary" type="submit" name="save" id="save_contact">Submit</button>
                                    </div>
                            
                                </div>
                                    
                                    <!-- <div class="col-md-12">
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a large text here ..."></textarea>
                                    </div> -->
                            
                            </form>
                        </fieldset>
                        <!-- table-wrapper -->
                    </div>
                    <!-- section-wrapper -->
                    </div>
                </div>

            </div><!--End side app-->

            <!-- Right-sidebar-->
            <?php $this->load->view('backend/right_sidebar');?>
            <!-- End Rightsidebar-->

            <?php $this->load->view('backend/footer');?>
        </div>
        <!-- End app-content-->
    </div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>


<script>

    // $('.checkbox').change(function(){
    //     alert('hi');
    // });

</script>
</body>
</html>