 <form action="nurse/update_Competencies/<?=$nurse_details[0]['loginid']?>" method="post">
            <fieldset class="competencies">
                <div class="row">
                <div class="col-md-6">
                    <h4 class="" style="line-height:2.7em">List of Competencies</h4>
                </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                    <div class="row">
                        <div class="col-md-12 text-left">
                        <label for="nursing_competency_desc[]">Select one of following description for each
                        competency below : </label>
                        </div>
                        <div class="col-md-12">

                        <table class="table-bordered competency" cellpadding="8">
                            <thead>

                            </thead>
                            <tbody>

                               
                                
                               
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "nursing_app";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql="SELECT * FROM competencies";
echo $sql;
$result = $conn->query($sql);
while($row = $result->fetch_assoc()) {
         $compet_ttile=$row["compet_ttile"];

?>
                               
                                <tr>
                                <td><label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input checkbox" name="nursing_competency[]" value="<?php echo $compet_ttile?>">
                                    <span class="custom-control-label"><?php echo $compet_ttile?></span>
                                    </label>
                                </td>
                                <td>
                                    <select name="nursing_competency_desc[]" class="select-box form-control" >
                                    <option>Select Your Experience</option>
                                   
                                    <option value="Trained and very confident (More than 3 years experience)">Trained and very confident (More than 3 years experience)</option>
                                    <option value="Trained but low experience and not very confident">Trained but low experience and not very confident</option>
                                    <option value="Trained but no experience">Trained but no experience</option>
                                    <option value="Not trained">Not trained</option>
                                    </select>
                                </td>
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </fieldset>
            <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                <div class="">
                <button class="btn btn-primary" type="submit" name="save" >Submit</button>
                </div>
            </div>

            </form>