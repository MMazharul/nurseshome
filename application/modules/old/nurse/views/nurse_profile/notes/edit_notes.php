<?php $this->load->view('backend/head_link');?>

<style>
    @media (max-width: 480px) {
    .nav-wrapper {
    display: block;
    /*overflow: hidden;*/
    height: calc(1.5rem + 1rem + 2px); /** 1.5 is font-size, 1 is padding top and bottom, 2 is border width top and bottom */
    position: relative;
    z-index: 1;
    margin-bottom: -1px;
    }
    .nav-pills {
        overflow-x: auto;
        flex-wrap: nowrap;
        border-bottom: 0;
    }
     .nav-item {
        margin-bottom: 0;
        min-width: 12em !important;
    }
   /* .nav-item :first-child {
        padding-left: 15px;
    }
    .nav-item :last-child {
        padding-right: 15px;
    }*/
    .nav-link {
        white-space: nowrap;
        /*min-width: 5em !important;*/
    }
    .dragscroll:active,
    .dragscroll:active a {
        cursor: -webkit-grabbing;
    }

    }

</style>

<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
<img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
<div class="page-main">
<!--app-header-->
<?php $this->load->view('backend/header');?>
<!-- app-content-->
<div class="container content-patient">
<div class="side-app">
<!-- page-header -->
<!-- <div class="page-header">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
</ol>
<div class="ml-auto">
<div class="input-group">
    <a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
        <span>
            <i class="fa fa-calendar"></i> Events Settings
        </span>
        <i class="fa fa-caret-down"></i>
    </a>
    <a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
        <span>
            <i class="fa fa-star"></i>
        </span>
    </a>
</div>
</div>
</div> -->
<!-- End page-header -->

<div class="row">
<div class="col-md-12 col-lg-12">
<div class="card">
<div class="card-header">
    <div class="col-md-12">
    <div class="card-title"><?=$form_title?> 
    <!-- <a class="btn bg-pink p-2" href="edit_condition">Edit</a>  -->
    </div>
    </div>
    <!-- <div class="col-md-2">
    <a href="nurse/nurse_list" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
        <span> Nurse List
        </span>
    </a>
    </div> -->
</div>
<div class="card-body">
    <form action="nurse/register_user_as_nurse" method="post">
    <div class="row">
    <?php if($this->session->flashdata('msg')){ ?>
        <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert-message">
                <span><?=$this->session->flashdata('msg');?></span>
            </div>
        </div>
    <?php } ?> 
    <div class="col-md-12">
        <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">

<form action="" method="post">
                                    <div class="form-label mb-2 text-center">Summary For Client</div>
                                    <div class="form-label">
                                        I HAVE COMPLETED THE FOLLOWING CARE:
                                    </div>
                                    <div class="form-group form-elements">
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_done[]" value="1">
                                            <span class="custom-control-label">
                                                BASIC PERSONAL CARE – BASIC CARE FOR CLEANING, FEEDING, MEDICATION ETC
                                            </span>          
                                        </label>
                                    </div>
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_done[]" value="1">
                                            <span class="custom-control-label">
                                                BASWOUND DRESSING – BED SORES / DIABETIC WOUNDS /SURGICAL WOUNDS
                                            </span>          
                                        </label>
                                    </div>
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_done[]" value="1">
                                            <span class="custom-control-label">
                                                INFANT BABY CARE – 0 TO 12 MONTHS
                                            </span>          
                                        </label>
                                    </div>
                                    </div>
                                    <div class="form-label mb-2 text-center">Basic Vital Signs</div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                    <label for="note">Temp </label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="temp">
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                            <div class="col-md-3">
                                                    <label for="note">BP</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="bp">
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                        <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                    <label for="note">Pulse Rate </label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="pulse">
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                    <label for="note">Respiration </label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="respiration">
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-6"><div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                        <label for="note">Pain Sore </label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="pain_sore">
                                                </div>
                                            </div>
                                        </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                        <label for="note">SPO2 </label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="spo2">
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6"><div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                        <label for="note">ETC </label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="etc">
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-label">
                                        Notes for Care
                                    </div>
                                    <div class="form-group form-elements">
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_done[]" value="1">
                                            <span class="custom-control-label">
                                                BASIC PERSONAL CARE – BASIC CARE FOR CLEANING, FEEDING, MEDICATION ETC
                                            </span>
                                            <textarea class="form-control" name="" id="" cols="30" rows="1"></textarea>          
                                        </label>
                                    </div>
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_done[]" value="1">
                                            <span class="custom-control-label">
                                                BASWOUND DRESSING – BED SORES / DIABETIC WOUNDS /SURGICAL WOUNDS
                                            </span>   
                                            <textarea class="form-control" name="" id="" cols="30" rows="1"></textarea>         
                                        </label>
                                    </div>
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_done[]" value="1">
                                            <span class="custom-control-label">
                                                INFANT BABY CARE – 0 TO 12 MONTHS
                                            </span>      
                                            <textarea class="form-control" name="" id="" cols="30" rows="1"></textarea>      
                                        </label>
                                    </div>
                                    </div>
                                        </div>
                                        <div class="col-md-6">
                                             <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                            <label for="photo_wound">Photo for wounds</label>
                                            </div>
                                            <div class="col-md-12">
                                                <input type="file" class="dropify" 
                                                    name="bio_file">
                                            </div>
                                        </div>
                                    </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">Summary</div>
                                                <div class="col-md-9">
                                                    <textarea class="form-control" name="" id="" cols="30" rows="2"></textarea> 
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                            <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
                                            <button type="button" class="btn bg-primary">Add</button>
                                    </div>
                                    </form>

                                        
                            </div>
                            </div>  
            
            <!-- <div class="col-md-12">
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a large text here ..."></textarea>
            </div> -->
    
    </form>
</fieldset>
<!-- table-wrapper -->
</div>
<!-- section-wrapper -->
</div>
</div>

</div><!--End side app-->

<!-- Right-sidebar-->
<?php $this->load->view('backend/right_sidebar');?>
<!-- End Rightsidebar-->

<?php $this->load->view('backend/footer');?>
</div>
<!-- End app-content-->
</div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>
  <script type="text/javascript">
    let dataTable = $(".new_table").dataTable(
      {  "bLengthChange" : false,
           "responsive": true,
          "oLanguage": 
        {
            "sSearch": ""
        }
      });
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust()
           .responsive.recalc();
        });    
      
</script>

</body>
</html>