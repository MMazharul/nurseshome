<?php $this->load->view('backend/head_link');?>

<body class="app sidebar-mini rtl">

  <!--Global-Loader-->
  <!-- <div id="global-loader">
			<img src="back_assets/images/icons/loader.svg" alt="loader">
		</div> -->

  <div class="page">
    <div class="page-main">
      <!--app-header-->


      <?php $this->load->view('backend/header');?>



      <!-- app-content-->
      <div class="container content-area">
        <div class="side-app">

          <!-- page-header -->
          <!-- <div class="page-header">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
                            </ol>
							<div class="ml-auto">
								<div class="input-group">
									<a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
										<span>
											<i class="fa fa-calendar"></i> Events Settings
										</span>
										<i class="fa fa-caret-down"></i>
									</a>
									<a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
										<span>
											<i class="fa fa-star"></i>
										</span>
									</a>
								</div>
							</div>
						</div> -->
          <!-- End page-header -->

          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <div class="col-md-10">
                    <div class="card-title"><?=$form_title?></div>
                  </div>
                  <div class="col-md-2">
                    <a class="btn btn-primary text-white mr-2" style="width:100%" id="">
                      <span> Update 
                      </span>
                    </a>
                  </div>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered text-nowrap w-100">
                      <thead>
                        <tr>
                          <th class="wd-15p">#</th>
                          <th class="wd-15p">Name</th>
                          <th class="wd-20p">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>##</td>
                          <td>
                            <a class="btn btn-primary text-white mr-2" id="">
                              <span>
                                Edit
                              </span>
                            </a>
                            <a class="btn btn-danger text-white mr-2" id="">
                              <span>
                                Delete
                              </span>
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>##</td>
                          <td>
                            <a class="btn btn-primary text-white mr-2" id="">
                              <span>
                                Edit
                              </span>
                            </a>
                            <a class="btn btn-danger text-white mr-2" id="">
                              <span>
                                Delete
                              </span>
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>##</td>
                          <td>
                            <a class="btn btn-primary text-white mr-2" id="">
                              <span>
                                Edit
                              </span>
                            </a>
                            <a class="btn btn-danger text-white mr-2" id="">
                              <span>
                                Delete
                              </span>
                            </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->
            </div>
          </div>

        </div>
        <!--End side app-->

        <!-- Right-sidebar-->
        <?php $this->load->view('backend/right_sidebar');?>
        <!-- End Rightsidebar-->

        <?php $this->load->view('backend/footer');?>

      </div>
      <!-- End app-content-->
    </div>
  </div>
  <!-- End Page -->

  <!-- Back to top -->
  <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

  <?php $this->load->view('backend/footer_link');?>


</body>

</html>