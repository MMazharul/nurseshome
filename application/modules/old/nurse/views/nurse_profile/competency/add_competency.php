<?php $this->load->view('backend/head_link');?>

<body class="app sidebar-mini rtl">

<style>
  .select-box{
    display: none;
  }
  td:first-child{
    width: 50%;
  }
</style>

<div class="page">
  <div class="page-main">
    <!--app-header-->
    <?php $this->load->view('backend/header');?>
    <!-- app-content-->
    <div class="container content-patient">
      <div class="side-app">
        <div class="row">
          <div class="col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <div class="col-md-10">
                  <div class="card-title"><?=$form_title?></div>
                </div>
                <div class="col-md-2">
                  <a href="nurse-competency-list" class="btn btn-primary text-white mr-2" style="width:100%" id="">
                    <span> Compency List
                    </span>
                  </a>
                </div>
              </div>
              <div class="card-body">
                <form action="" method="post">
                  <div class="row">
                    <?php if($this->session->flashdata('msg')){ ?>
                    <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <div class="alert-message">
                        <span><?=$this->session->flashdata('msg');?></span>
                      </div>
                    </div>
                    <?php } ?>
                    <div id="alert_pass"></div>
                    <div class="col-md-12">
                      <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
                    </div>
                  </div>
                  <form action="" method="post">
                    <fieldset class="competencies">
                      <div class="row">
                        <div class="col-md-6">
                          <h4 class="" style="line-height:2.7em">List of Competencies</h4>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-12 text-left">
                                <label for="nursing_competency_desc[]">Select one of following description for each
                                competency below : </label>
                              </div>
                              <div class="col-md-12">

                              <table class="table-bordered" cellpadding="8">
                                <thead>
                                 
                                </thead>
                                <tbody>
                                  <tr>
                                    <td><label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkbox" name="nursing_competency[]" value="1">
                                        <span class="custom-control-label">Basic personal care - basic care for cleaning, feeding, medication etc.</span>
                                      </label></td>
                                    <td>
                                      <select name="nursing_competency_desc[]" class="select-box form-control" id="">
                                        <option value="1">Trained and very confident (More than 3 years experience)</option>
                                        <option value="2">Trained but low experience and not very confident</option>
                                        <option value="3">Trained but no experience</option>
                                        <option value="4">Not trained</option>
                                      </select>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkbox" name="nursing_competency[]" value="2">
                                        <span class="custom-control-label">Wound Dressing - Bed sores/diabetic wounds/surgical wounds</span>
                                      </label>
                                    </td>
                                    <td>
                                      <select name="nursing_competency_desc[]" class="form-control select-box" id="">
                                        <option value="1">Trained and very confident (More than 3 years experience)</option>
                                        <option value="2">Trained but low experience and not very confident</option>
                                        <option value="3">Trained but no experience</option>
                                        <option value="4">Not trained</option>
                                      </select>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                       <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkbox" name="nursing_competency[]" value="3">
                                        <span class="custom-control-label">NG Tube - Insertion and feeding</span>
                                      </label>
                                    </td>
                                    <td>
                                      <select name="nursing_competency_desc[]" class="form-control select-box" id="gender">
                                        <option value="1">Trained and very confident (More than 3 years experience)</option>
                                        <option value="2">Trained but low experience and not very confident</option>
                                        <option value="3">Trained but no experience</option>
                                        <option value="4">Not trained</option>
                                      </select>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                       <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkbox" name="nursing_competency[]" value="4">
                                        <span class="custom-control-label">Suction - Adults</span>
                                      </label>
                                    </td>
                                    <td>
                                      <select name="nursing_competency_desc[]" class="form-control select-box" id="gender">
                                        <option value="1">Trained and very confident (More than 3 years experience)</option>
                                        <option value="2">Trained but low experience and not very confident</option>
                                        <option value="3">Trained but no experience</option>
                                        <option value="4">Not trained</option>
                                      </select>
                                    </td>
                                  </tr>
                                  <tr>
                                     <td>
                                       <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkbox" name="nursing_competency[]" value="5">
                                        <span class="custom-control-label">Urinary catheter insertion - male/female</span>
                                      </label>
                                     </td>
                                     <td>
                                      <select name="nursing_competency_desc[]" class="form-control select-box" id="gender">
                                        <option value="1">Trained and very confident (More than 3 years experience)</option>
                                        <option value="2">Trained but low experience and not very confident</option>
                                        <option value="3">Trained but no experience</option>
                                        <option value="4">Not trained</option>
                                      </select>
                                     </td>
                                  </tr>
                                  <tr>
                                    <td>
                                        <label class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input checkbox" name="nursing_competency[]" value="6">
                                          <span class="custom-control-label">Colostomy Care</span>
                                        </label>
                                    </td>
                                    <td>
                                      <select name="nursing_competency_desc[]" class="form-control select-box" id="gender">
                                        <option value="1">Trained and very confident (More than 3 years experience)</option>
                                        <option value="2">Trained but low experience and not very confident</option>
                                        <option value="3">Trained but no experience</option>
                                        <option value="4">Not trained</option>
                                      </select>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkbox" name="nursing_competency[]" value="7">
                                        <span class="custom-control-label">Cannulation - Certified</span>
                                      </label>
                                    </td>
                                    <td>
                                      <select name="nursing_competency_desc[]" class="form-control select-box" id="gender">
                                        <option value="1">Trained and very confident (More than 3 years experience)</option>
                                        <option value="2">Trained but low experience and not very confident</option>
                                        <option value="3">Trained but no experience</option>
                                        <option value="4">Not trained</option>
                                      </select>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td><label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkbox" name="nursing_competency[]" value="8">
                                        <span class="custom-control-label">Central use and care</span>
                                      </label></td>
                                    <td>
                                      <select name="nursing_competency_desc[]" class="form-control select-box" id="gender">
                                        <option value="1">Trained and very confident (More than 3 years experience)</option>
                                        <option value="2">Trained but low experience and not very confident</option>
                                        <option value="3">Trained but no experience</option>
                                        <option value="4">Not trained</option>
                                      </select>
                                    </td>
                                  </tr>

                                  <tr>
                                    <td>
                                       <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkbox" name="nursing_competency[]" value="9">
                                        <span class="custom-control-label">Chemoport use and care</span>
                                      </label>
                                    </td>
                                    <td>
                                      <select name="nursing_competency_desc[]" class="form-control select-box" id="gender">
                                        <option value="1">Trained and very confident (More than 3 years experience)</option>
                                        <option value="2">Trained but low experience and not very confident</option>
                                        <option value="3">Trained but no experience</option>
                                        <option value="4">Not trained</option>
                                      </select>
                                    </td>
                                  </tr>

                                  <tr>
                                    <td><label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkbox" name="nursing_competency[]" value="10">
                                        <span class="custom-control-label"> Tracheostomy Care</span>
                                      </label>
                                    </td>
                                    <td>
                                      <select name="nursing_competency_desc[]" class="form-control select-box" id="gender">
                                        <option value="1">Trained and very confident (More than 3 years experience)</option>
                                        <option value="2">Trained but low experience and not very confident</option>
                                        <option value="3">Trained but no experience</option>
                                        <option value="4">Not trained</option>
                                      </select>
                                    </td>
                                  </tr>

                                  <tr>
                                    <td> 
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkbox" name="nursing_competency[]" value="11">
                                        <span class="custom-control-label">Palliative Care</span>
                                      </label>
                                    </td>
                                    <td>
                                      <select name="nursing_competency_desc[]" class="form-control select-box" id="gender">
                                        <option value="1">Trained and very confident (More than 3 years experience)</option>
                                        <option value="2">Trained but low experience and not very confident</option>
                                        <option value="3">Trained but no experience</option>
                                        <option value="4">Not trained</option>
                                      </select>
                                    </td>
                                  </tr>

                                  <tr>
                                    <td> 
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkbox" id="others_comp" name="nursing_competency[]" value="12">
                                        <span class="custom-control-label">Others - Please Describe</span>
                                      </label>
                                    </td>
                                    <td>
                                      <select name="nursing_competency_desc[]" class="form-control select-box" id="gender">
                                        <option value="1">Trained and very confident (More than 3 years experience)</option>
                                        <option value="2">Trained but low experience and not very confident</option>
                                        <option value="3">Trained but no experience</option>
                                        <option value="4">Not trained</option>
                                      </select>
                                    </td>
                                  </tr>

                             
                                </tbody>

                              </table>

                                
                              </div>
                            </div>
                          </div>
                        </div>
                        
                      </div>
                    
                    </fieldset>


                    <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                      <div class="">
                        <button class="btn btn-primary" type="submit" name="save" id="">Submit</button>
                      </div>
                    </div>
      
                  </form>

                  <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->
            </div>
          </div>

        </div>
        <!--End side app-->

        <!-- Right-sidebar-->
        <?php $this->load->view('backend/right_sidebar');?>
        <!-- End Rightsidebar-->

        <?php $this->load->view('backend/footer');?>
      </div>
      <!-- End app-content-->
    </div>
  </div>
  <!-- End Page -->

  <!-- Back to top -->
  <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

  <?php $this->load->view('backend/footer_link');?>

  <script>
    let otherComp = document.querySelector('#others_comp');
    let otherCompBox = document.getElementById('other_comp_box');

    otherComp.addEventListener('change', () => {
      if(otherComp.checked){
        otherCompBox.style.display = 'block';
      }else{
        otherCompBox.style.display = 'none';
      }
    });
    const checkbox = document.querySelectorAll('.checkbox');
    const selectBox = document.querySelectorAll('.select-box');

    for (let i = 0; i < checkbox.length; i++) {
      checkbox[i].addEventListener('change', () => {
      if(checkbox[i].checked){
        selectBox[i].style.display = 'block';
      }else{
        selectBox[i].style.display = 'none';
      }
    });
    } 
  </script>
</body>

</html>