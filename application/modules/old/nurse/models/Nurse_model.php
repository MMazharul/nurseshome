<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Nurse_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function insert($table_name,$data)
    {
          $this->db->insert($table_name, $data);
    }
        public function insert_ret($tablename, $tabledata)
    {
        $this->db->insert($tablename, $tabledata);
        return $this->db->insert_id();
    }
    public function check_login($email, $pass) {
        $this->db->select('*');
        $this->db->from('login');
        $this->db->where('email', $email);
        $this->db->where('password', $pass);
        $result = $this->db->get();
        return $result->result_array();
    }
     public function password_change($email,$data)
    {
        $this->db->where('email',$email);
        $this->db->update('login',$data);
    }
    public function select_with_where($selector, $condition, $tablename)
    {
    	$this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result = $this->db->get();
        return $result->result_array();
    }
    public function select_with_where_desc($selector, $condition, $tablename,$col,$order_type)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by($col,$order_type);
        $result = $this->db->get();
        return $result->result_array();
    }
    public function select_where_left_join($selector,$table_name,$join_table,$join_condition,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }
    public function select_condition_random_with_limit($table_name,$condition,$limit)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by('created_at','RANDOM');
        $this->db->limit($limit);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
      public function select_condition_decending_with_limit($table_name,$condition,$limit)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by('created_at','DESC');
        $this->db->limit($limit);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }


    public function select_all_acending($table_name,$col_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by($col_name,'ASC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function select_all_ascending_spe($table_name,$col_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by($col_name,'ASC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function select_with_where_asc_spe($selector, $condition, $tablename,$col_name)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by($col_name,'ASC');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function select_all($table_name)
    {
    	$this->db->select('*');
		$this->db->from($table_name);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
    }   

       public function update_function($columnName, $columnVal, $tableName, $data)
    {
        $this->db->where($columnName, $columnVal);
        $this->db->update($tableName, $data);
    }

    // ==== Search Model Query Began =====//

    public function category_id_like($search_content)
    {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->like('name', $search_content, 'both');
        $result=$this->db->get();
        return $result->result_array();
    }
    
    public function get_search_item($condition,$table_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by($table_name.'.created_at','DESC');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->limit(5);
        $result=$this->db->get();
        return $result->result_array();
    }
      public function get_search_input_item($table_name,$condition,$search_content)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->like('p_name', $search_content,'after');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->limit(5);
        $result=$this->db->get();
        return $result->result_array();
    }

      public function fetch_item($query)
    {
        $this->db->select('*');
        $this->db->from('product');
        if($query!=''){
            $this->db->like('p_name',$query);
        }
        $this->db->order_by('created_at','DESC');
        // $where = '(' . $condition . ')';
        // $this->db->where($where);
        // $this->db->limit(10);
        return $this->db->get();
       
    }
    
     public function delete_function_cond($tableName, $cond)
    {
        $where = '( ' . $cond . ' )';
        $this->db->where($where);
        $this->db->delete($tableName);
    }
    public function delete_function($tableName, $columnName, $columnVal)
    {
        $this->db->where($columnName, $columnVal);
        $this->db->delete($tableName);
    }


    // ====  Search  Model Query End=== ///

    //=== Below for Nurse Model==//
    public function fetch_all_event(){
      $this->db->order_by('id');
      return $this->db->get('events');
     }

    public function insert_event($data)
     {
      $this->db->insert('events', $data);
     }

    public function update_event($data, $id)
     {
      $this->db->where('id', $id);
      $this->db->update('events', $data);
     }

    public function delete_event($id)
     {
      $this->db->where('id', $id);
      $this->db->delete('events');
     }
    //==upper for event model==//

    public function double_cond_where($table,$firstcolumn,$firstvalue,$secondcolumn,$secondvalue)
    {
        $query="select * from $table where `$firstcolumn`='$firstvalue' and `$secondcolumn`='$secondvalue'";
        // echo $query;
        // echo "</br>";
        $result = $this->db->query($query) or die ("Schema Query Failed");
        $result=$result->result_array();
        return $result;
    }

    public function special_comepetancy($nurse_id)
    {
      $query="select competencies.compet_ttile,(case when competencies_details.compe_id!='' then competencies_details.compe_id else '0' end)
as compe_id,
(case when competencies_details.com_value!='' then competencies_details.com_value else '0' end) 
as com_value
from competencies left join competencies_details on competencies.compe_id=competencies_details.compe_id 
and competencies_details.nurse_id=$nurse_id";
       $result = $this->db->query($query) or die ("Schema Query Failed");
        $result=$result->result_array();
        return $result;
    }
    public function special_comepetancy_new($nurse_id)
    {
      $query="select competencies.compet_ttile,(case when competencies_details.compe_id!='' then 
competencies_details.compe_id else competencies.compe_id end)
as compe_id,
(case when competencies_details.com_value!='' then competencies_details.com_value else '0' end) 
as com_value
from competencies left join competencies_details on competencies.compe_id=competencies_details.compe_id 
and competencies_details.nurse_id=$nurse_id";
       $result = $this->db->query($query) or die ("Schema Query Failed");
        $result=$result->result_array();
        return $result;
    }
    public function daily_schedule($nurse_id)
    {
    $query="select day_title.day_title_sum,(case when schedule_day.schedule_day!='' then 
    schedule_day.schedule_day else '0' end) as sday,schedule_day.scheudle_id,schedule_day.schedule_day,
    schedule_day.add_date,schedule_day.nurse_id from day_title
      left join schedule_day on day_title.day_title_sum=schedule_day.schedule_day
    and schedule_day.nurse_id=$nurse_id";
    $result = $this->db->query($query) or die ("Schema Query Failed");
    $result=$result->result_array();
    return $result;
    }
        public function select_where_join($selector,$table_name,$join_table,$join_condition,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }
    public function FunctionName($value='')
    {
 
    }
    public function mypatient($nurse_id)
    {
    $query="select count(time_in_hours) as total_hr,sum(book_charge) as totalamnt,count(*) as total_book,users.first_name,users.last_name,
users.contact_no,users.email,users.profile_pic,nurse_booking.patient_id 
from nurse_booking inner join users on users.loginid=nurse_booking.patient_id
where nurse_booking.nurse_id=$nurse_id
group by patient_id ";
    $result = $this->db->query($query) or die ("Schema Query Failed");
    $result=$result->result_array();
    return $result;
    }

    public function select_all_ascending_spe_desc($table_name,$col_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by($col_name,'DESC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
}

