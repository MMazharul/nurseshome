<?php $this->load->view('backend/head_link');?>


<style>
    @media (max-width: 480px) {
    .nav-wrapper {
    display: block;
    /*overflow: hidden;*/
    height: calc(1.5rem + 1rem + 2px); /** 1.5 is font-size, 1 is padding top and bottom, 2 is border width top and bottom */
    position: relative;
    z-index: 1;
    margin-bottom: -1px;
    }
    .nav-pills {
        overflow-x: auto;
        flex-wrap: nowrap;
        border-bottom: 0;
    }
     .nav-item {
        margin-bottom: 0;
        min-width: 12em !important;
    }
   /* .nav-item :first-child {
        padding-left: 15px;
    }
    .nav-item :last-child {
        padding-right: 15px;
    }*/
    .nav-link {
        white-space: nowrap;
        /*min-width: 5em !important;*/
    }
    .dragscroll:active,
    .dragscroll:active a {
        cursor: -webkit-grabbing;
    }

    }

</style>

<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
<img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
<div class="page-main">
<!--app-header-->
<?php $this->load->view('backend/header');?>
<!-- app-content-->
<div class="container content-patient">
<div class="side-app">
<!-- page-header -->
<!-- <div class="page-header">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
</ol>
<div class="ml-auto">
<div class="input-group">
    <a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
        <span>
            <i class="fa fa-calendar"></i> Events Settings
        </span>
        <i class="fa fa-caret-down"></i>
    </a>
    <a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
        <span>
            <i class="fa fa-star"></i>
        </span>
    </a>
</div>
</div>
</div> -->
<!-- End page-header -->

<div class="row">
<div class="col-md-12 col-lg-12">
<div class="card">
<div class="card-header">
    <div class="col-md-10">
    <div class="card-title"><?=$form_title?></div>
    </div>
    <!-- <div class="col-md-2">
    <a href="nurse/nurse_list" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
        <span> Nurse List
        </span>
    </a>
    </div> -->
</div>
<div class="card-body">
    <form action="nurse/register_user_as_nurse" method="post">
    <div class="row">
    <?php if($this->session->flashdata('msg')){ ?>
        <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert-message">
                <span><?=$this->session->flashdata('msg');?></span>
            </div>
        </div>
    <?php } ?> 
    <div class="col-md-12">
        <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <div class="card card-profile  overflow-hidden">
                <div class="card-body text-center profile-bg">
                    <div class=" card-profile">
                        <div class="row justify-content-center">
                            <div class="">
                                <div class="">
                                    <a href="#">
                                        <img src="back_assets/images/users/female/33.png" class="avatar-xxl rounded-circle" alt="profile">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3 class="mt-3 text-white"><?=$patient_details[0]['first_name']." ".$patient_details[0]['last_name'];?></h3>
                    <p class="mb-2 text-white"><?=$patient_details[0]['user_role']== 3 ? 'Nurse' : ($patient_details[0]['user_role'] == 4 ? 'Patient' : '') ?></p>
                    <div class="text-center mb-4">
                        <span><i class="fa fa-star text-warning"></i></span>
                        <span><i class="fa fa-star text-warning"></i></span>
                        <span><i class="fa fa-star text-warning"></i></span>
                        <span><i class="fa fa-star-half-o text-warning"></i></span>
                        <span><i class="fa fa-star-o text-warning"></i></span>
                    </div>
                 
                    <a href="patient/patient_details/<?=$patient_details[0]['loginid']?>" class="btn btn-info btn-sm"><i class="fas fa-pencil-alt" aria-hidden="true"></i>Edit profile</a>
                </div>
               
            </div>

            <div class="table-responsive mb-3">
                                <table class="table row table-borderless w-100 m-0 border">
                              <h4>Contact Details </h4>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>First Name :</strong> <?=ucwords($patient_details[0]['first_name'])?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Email :</strong> <?=$patient_details[0]['email']?></td>
                                        </tr>
                                        
                                        <tr>
                                            <td><strong>Gender :</strong> <?=$patient_details[0]['gender']==1 ? 'Male' : 'Female' ?></td>
                                        </tr>
                                    </tbody>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Last Name :</strong> <?=ucwords($patient_details[0]['last_name'])?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Contact :</strong> <?=$patient_details[0]['country_code']." ".$patient_details[0]['contact_no']?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                                <table class="table row table-borderless w-100 m-0 border">
                                    <h4>Bio Data</h4>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>ID :</strong>
                                                <?=!empty($patient_details_info[0]['nrc_passport_id'])? $patient_details_info[0]['nrc_passport_id'] : ''?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Age :</strong><?=!empty($patient_details_info[0]['age']) ? $patient_details_info[0]['age'] : ''?> </td>
                                        </tr>
                                        
                                        <tr>
                                            <td><strong>Height :</strong> <?=!empty($patient_details_info[0]['height']) ? $patient_details_info[0]['height'] : ''?> </td>
                                        </tr>
                                    </tbody>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Weight :</strong> <?=!empty($patient_details_info[0]['weight']) ? $patient_details_info[0]['weight'] : ''?> </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                                <table class="table row table-borderless w-100 m-0 border">
                                    <h4>Location Details </h4>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Address :</strong> <?=!empty($patient_details_info[0]['address']) ? $patient_details_info[0]['address'] : ''?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Type Resident :</strong> 
                                            
                                            <?php
                                            if (!empty($patient_details_info[0]['resident_type'])) {
                                                if ($patient_details_info[0]['resident_type'] == 1) {
                                                    echo "Landed House";
                                                }
                                                else if ($patient_details_info[0]['resident_type'] == 2) {
                                                    echo "Multi Storey Residence with lift";
                                                }
                                                else if ($patient_details_info[0]['resident_type'] == 3) {
                                                    echo "Multi Storey Residence without lift";
                                                }
                                                else if ($patient_details_info[0]['resident_type'] == 4) {
                                                    echo "Nursing Home/Hostpital";
                                                }
                                            }
                                            else{
                                                echo "--";
                                            }
                                            ?>
                                        
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td><strong>Type Security :</strong>

                                             <?php
                                                if (!empty($patient_details_info[0]['security_type'])) {
                                                    if ($patient_details_info[0]['security_type'] == 1) {
                                                        echo "Gated & Guarded";
                                                    }
                                                    elseif ($patient_details_info[0]['security_type'] == 2) {
                                                        echo "Gated";
                                                    }
                                                    elseif ($patient_details_info[0]['security_type'] == 3) {
                                                        echo "Restricted Access";
                                                    }
                                                    elseif ($patient_details_info[0]['security_type'] == 4) {
                                                        echo "None";
                                                    }

                                                }
                                                else {
                                                echo "--";
                                                }

                                            ?> 


                                            </td>
                                        </tr>
                                    </tbody>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Living Status :</strong> 
                                                <?php

                                                    if (!empty($patient_details_info[0]['living_status'])) {
                                                        if ($patient_details_info[0]['living_status'] == 1) {
                                                            echo "Alone";
                                                        }
                                                        elseif ($patient_details_info[0]['living_status'] == 2) {
                                                            echo "Family";
                                                        }
                                                    }
                                                    else {
                                                        echo "--";
                                                    }


                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Current Care Status :</strong> 
                                            <?php
                                                if (!empty($patient_details_info[0]['current_care_status'])) {
                                                    if ($patient_details_info[0]['current_care_status'] == 1) {
                                                        echo "Self";
                                                    }
                                                    elseif ($patient_details_info[0]['current_care_status'] == 2) {
                                                        echo "Family";
                                                    }
                                                    elseif ($patient_details_info[0]['current_care_status'] == 3) {
                                                        echo "Caregiver/Maid";
                                                    }
                                                    elseif ($patient_details_info[0]['current_care_status'] == 4) {
                                                        echo "Nurse";
                                                    }

                                                    }
                                                    else {
                                                        echo "--";
                                                    }

                                            ?>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Pets in House :</strong> 
                                                <?php
                                                if (!empty($patient_details_info[0]['pets_in_house'])) {
                                                    if ($patient_details_info[0]['pets_in_house'] == 1) {
                                                        echo "Dog";
                                                    }
                                                    elseif ($patient_details_info[0]['pets_in_house'] == 2) {
                                                        echo "Cat";
                                                    }
                                                    

                                                }
                                                else {
                                                        echo "--";
                                                     }

                                            ?>  </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
           
        </div>
    </div>
    

            
            <!-- <div class="col-md-12">
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a large text here ..."></textarea>
            </div> -->
    
    </form>
</fieldset>
<!-- table-wrapper -->
</div>
<!-- section-wrapper -->
</div>
</div>

</div><!--End side app-->

<!-- Right-sidebar-->
<?php $this->load->view('backend/right_sidebar');?>
<!-- End Rightsidebar-->

<?php $this->load->view('backend/footer');?>
</div>
<!-- End app-content-->
</div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>
  <script type="text/javascript">
    let dataTable = $(".new_table").dataTable(
      {  "bLengthChange" : false,
           "responsive": true,
          "oLanguage": 
        {
            "sSearch": ""
        }
      });
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust()
           .responsive.recalc();
        });    
      
</script>

</body>
</html>