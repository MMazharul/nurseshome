<?php $this->load->view('backend/head_link');?>


<style>
    @media (max-width: 480px) {
    .nav-wrapper {
    display: block;
    /*overflow: hidden;*/
    height: calc(1.5rem + 1rem + 2px); /** 1.5 is font-size, 1 is padding top and bottom, 2 is border width top and bottom */
    position: relative;
    z-index: 1;
    margin-bottom: -1px;
    }
    .nav-pills {
        overflow-x: auto;
        flex-wrap: nowrap;
        border-bottom: 0;
    }
     .nav-item {
        margin-bottom: 0;
        min-width: 12em !important;
    }
   /* .nav-item :first-child {
        padding-left: 15px;
    }
    .nav-item :last-child {
        padding-right: 15px;
    }*/
    .nav-link {
        white-space: nowrap;
        /*min-width: 5em !important;*/
    }
    .dragscroll:active,
    .dragscroll:active a {
        cursor: -webkit-grabbing;
    }

    }

</style>

<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
<img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
<div class="page-main">
<!--app-header-->
<?php $this->load->view('backend/header');?>
<!-- app-content-->
<div class="container content-patient">
<div class="side-app">
<!-- page-header -->
<!-- <div class="page-header">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
</ol>
<div class="ml-auto">
<div class="input-group">
    <a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
        <span>
            <i class="fa fa-calendar"></i> Events Settings
        </span>
        <i class="fa fa-caret-down"></i>
    </a>
    <a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
        <span>
            <i class="fa fa-star"></i>
        </span>
    </a>
</div>
</div>
</div> -->
<!-- End page-header -->

<div class="row">
<div class="col-md-12 col-lg-12">
<div class="card">
<div class="card-header">
    <div class="col-md-12">
    <div class="card-title"><?=$form_title?> 
    <a class="btn bg-pink p-2" href="edit-condition">Edit</a> 
    </div>
    </div>
    <!-- <div class="col-md-2">
    <a href="nurse/nurse_list" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
        <span> Nurse List
        </span>
    </a>
    </div> -->
</div>
<div class="card-body">
    <form action="nurse/register_user_as_nurse" method="post">
    <div class="row">
    <?php if($this->session->flashdata('msg')){ ?>
        <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert-message">
                <span><?=$this->session->flashdata('msg');?></span>
            </div>
        </div>
    <?php } ?> 
    <div class="col-md-12">
        <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">
             <div class="table-responsive mb-3">
                                <table class="table row table-borderless w-100 
                                m-0 border">
                                    <h4>My Health Status </h4>
                                    <tbody class="col-lg-6 p-0">
                                        <tr>
                                            <td><strong>Independent :</strong> 
                                             <?php
                                                if (!empty($health_status[0]['dependency'])) {
                                                    if ($health_status[0]['dependency']==1) {
                                                        echo "Independent";
                                                    }
                                                    elseif ($health_status[0]['dependency']==2) {
                                                        echo "Semi Dependent";
                                                    }
                                                    elseif ($health_status[0]['dependency']==3) {
                                                        echo "Dependent - FULLY DEPENDENT IN ONE OR MORE OF MOVE/FEED/CLEAN OWN SELF";
                                                    }
                                                    elseif ($health_status[0]['dependency']==4) {
                                                        echo "Dependent - FULLY DEPENDENT TO MOVE/FEED/CLEAN OWN SELF";
                                                    }
                                                    else{
                                                        echo "";
                                                    }
                                                }
                                             ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Mental State :</strong> 
                                            <?php
                                                if (!empty($health_status[0]['mental_state'])) {
                                                    if ($health_status[0]['mental_state']==1) {
                                                        echo "AWAKE AND RESPONSIVE";
                                                    }
                                                    elseif ($health_status[0]['mental_state']==2) {
                                                        echo "AWAKE BUT NON-RESPONSIVE";
                                                    }
                                                    elseif ($health_status[0]['mental_state']==3) {
                                                        echo "DROWSY/CONFUSED";
                                                    }
                                                    elseif ($health_status[0]['mental_state']==4) {
                                                        echo "COMATOSE";
                                                    }
                                                    else{
                                                        echo "";
                                                    }
                                                }
                                            ?>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td><strong>Feeding :</strong> 
                                            <?php
                                                if (!empty($health_status[0]['feeding'])) {
                                                    if ($health_status[0]['feeding']==1) {
                                                        echo " FEEDS OWN SELF ";
                                                    }
                                                    elseif ($health_status[0]['feeding']==2) {
                                                        echo " NEEDS HELP TO FEED ";
                                                    }
                                                    elseif ($health_status[0]['feeding']==3) {
                                                        echo " TUBE FEEDING ";
                                                    }
                                                    elseif ($health_status[0]['feeding']==4) {
                                                        echo " OTHERS ";
                                                    }
                                                    else{
                                                        echo "";
                                                    }
                                                }
                                            ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Allergies :</strong> 
                                            
                                                <?php

                                                if (!empty($health_status[0]['allergies'])) {
                                                    if ($health_status[0]['allergies']==1) {
                                                        echo "None";
                                                    }
                                                    elseif ($health_status[0]['allergies']==2) {
                                                        echo "DRUGS -FHN (+5)";
                                                    }
                                                    elseif ($health_status[0]['allergies']==3) {
                                                        echo "FOOD -FHN (+5)";
                                                    }
                                                    elseif ($health_status[0]['allergies']==4) {
                                                        echo "OTHERS – FHN (+5)";
                                                    }
                                                    else{
                                                        echo "";
                                                    }
                                                }

                                                ?>

                                            </td>
                                        </tr>
                                    </tbody>
                                   
                                </table>
                                <br>
                                <table class="table row table-borderless w-100 m-0 border">
                                    <h4>Items available at my residence </h4>
                                    <tbody class="col-lg-6 p-0">

                                        <?php

                                            if(!empty($patient_residence_items_list)) {
                                            $i = 1;

                                            $countRecords = count($patient_residence_items_list);

                                            $col1 = array_slice($patient_residence_items_list, 0, $countRecords/2 + 0.5);

                                            $col2 = array_slice($patient_residence_items_list, $countRecords/2 + 0.5, $countRecords);

                                            $row = array("col1" => $col1, "col2" => $col2);


                                            foreach ($row['col1'] as $key => $value) {

                                                echo "<tr>
                                                        <td><strong>".$i.".".$value['item_name'].":</strong> 
                                                        <span>".(in_array($value['id'], $patient_residence_items) ? "Yes" : "No")."</span>
                                                        </td>
                                                     </tr>";
                                                $i++;  
                                                }
                                            
                                        ?>
                                    </tbody>
                                    <tbody class="col-lg-6 p-0">
                                        <?php

                                            foreach ($row['col2'] as $key => $value) {

                                                echo "<tr>
                                                        <td><strong>".$i.".".$value['item_name'].":</strong> 
                                                        <span>".(in_array($value['id'], $patient_residence_items) ? "Yes" : "No")."</span>
                                                        </td>
                                                     </tr>";
                                                $i++;  
                                                }
                                            }
                                            
                                        ?>
                                    </tbody> 
                                </table>
                               
                            </div>
        </div>
    </div>
    

            
            <!-- <div class="col-md-12">
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a large text here ..."></textarea>
            </div> -->
    
    </form>
</fieldset>
<!-- table-wrapper -->
</div>
<!-- section-wrapper -->
</div>
</div>

</div><!--End side app-->

<!-- Right-sidebar-->
<?php $this->load->view('backend/right_sidebar');?>
<!-- End Rightsidebar-->

<?php $this->load->view('backend/footer');?>
</div>
<!-- End app-content-->
</div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>
  <script type="text/javascript">
    let dataTable = $(".new_table").dataTable(
      {  "bLengthChange" : false,
           "responsive": true,
          "oLanguage": 
        {
            "sSearch": ""
        }
      });
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust()
           .responsive.recalc();
        });    
      
</script>

</body>
</html>