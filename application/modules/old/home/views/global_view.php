<!-- 		<div class="row">
			<div class="col-md-12">
				<div class="owl-carousel owl-carousel2 owl-theme mb-5">
					<div class="item">
						<div class="card mb-0">
							<div class="row">
								<div class="col-4">
									<div class="feature">
										<div class="fa-stack fa-lg fa-2x icon bg-primary-transparent">
											<i class="si si-briefcase  fa-stack-1x text-primary"></i>
										</div>
									</div>
								</div>
								<div class="col-8">
									<div class="card-body p-3  d-flex">
										<div>
											<p class="text-muted mb-1">Total Patient</p>
											<h2 class="mb-0 text-dark">18,367K</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="card mb-0">
							<div class="row">
								<div class="col-4">
									<div class="feature">
										<div class="fa-stack fa-lg fa-2x icon bg-success-transparent">
											<i class="si si-drawer fa-stack-1x text-success"></i>
										</div>
									</div>
								</div>
								<div class="col-8">
									<div class="card-body p-3  d-flex">
										<div>
											<p class="text-muted mb-1">Total Hours</p>
											<h2 class="mb-0 text-dark">700 Hrs</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="card mb-0">
							<div class="row">
								<div class="col-4">
									<div class="feature">
										<div class="fa-stack fa-lg fa-2x icon bg-pink-transparent">
											<i class="si si-layers fa-stack-1x text-pink"></i>
										</div>
									</div>
								</div>
								<div class="col-8">
									<div class="card-body p-3  d-flex">
										<div>
											<p class="text-muted mb-1">Total Revenue</p>
											<h2 class="mb-0 text-dark">RM 3,548K </h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="card mb-0">
							<div class="row">
								<div class="col-4">
									<div class="feature">
										<div class="fa-stack fa-lg fa-2x icon bg-warning-transparent">
											<i class="si si-chart fa-stack-1x text-warning"></i>
										</div>
									</div>
								</div>
								<div class="col-8">
									<div class="card-body p-3  d-flex">
										<div>
											<p class="text-muted mb-1">Review</p>
											<h2 class="mb-0 text-dark">4.5 Star</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div> -->

						<div class="row">
							<div class="col-md-12">
								<div class="card card-bgimg br-7">
									<div class="row">
										<div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
											<div class="card-body text-center">
												<h5 class="text-white">Active Bookings</h5>
									            <h2 class="mb-2 mt-3 fs-2 text-white mainvalue">4</h2>
									             <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
									        </div>
										</div>
										<div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
											<div class="card-body text-center">
												<h5 class="text-white">Completed Bookings</h5>
									            <h2 class="mb-2 mt-3 fs-2 text-white mainvalue">12</h2>
									             <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
									        </div>
										</div>
										<div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
											<div class="card-body text-center">
												<h5 class="text-white">Total Bookings</h5>
									            <h2 class="mb-2 mt-3 fs-2 text-white mainvalue">15</h2>
									             <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
									        </div>
										</div>
										<div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
											<div class="card-body text-center">
												<h5 class="text-white">Total Patients</h5>
									            <h2 class="mb-2 mt-3 fs-2 text-white mainvalue">13</h2>
									             <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
									        </div>
										</div>
										<div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
											<div class="card-body text-center">
												<h5 class="text-white">Total Hours</h5>
									            <h2 class="mb-2 mt-3 fs-2 text-white mainvalue">29</h2>
									             <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
									        </div>
										</div>
										<div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
											<div class="card-body text-center">
												<h5 class="text-white">Total Earnings</h5>
									            <h2 class="mb-2 mt-3 fs-2 text-white mainvalue">RM 284K </h2>
									             <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
									        </div>
										</div>
									</div>
								</div>
							</div>
						</div>	

						<div class="row">
							<div class="col-xl-12 col-lg-12 col-md-12">
								<div class="card">
														<div class="card-header">
									<div class="card-title">Latest Booking</div>
								</div>
									<div class="card-body">
										<div class="table-responsive">
						 <table id="example" class="table table-striped table-bordered text-nowrap w-100 text-nowrap mb-0">
						                      <thead>
						                      <tr>
						                       
						                        <th>Nurse Name</th>
						 <th>Contact No</th>
					<!-- 	 <th>Nurse Pic</th>        -->                
             <th>Charge</th>
             <th>Patient Name</th>
            <!--  <th>Patient Pic</th> -->
              <th>Booking Date</th>
              <th>Booking Hour</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php

          foreach ($nurselist as $key => $value)
           {
            $nursename=$value['nfmae'].' '.$value['nlname'];
            $nurse_conatct=$value['ncontactno'];
            $nursepic=$value['nursepic'];
            $patientname=$value['bfname'].' '.$value['blname'];
            $patientpic=$value['patient_pic'];
            //$bookdate=date('Y-m-d H:i:s',$value['adddate']);
            ?>
                                  <tr>
                                   
                                    <td><img src="uploads/<?php echo $nursepic?>" width="30px" height="30px"/> <?php echo $nursename?></td>
             <td><?php echo $nurse_conatct?></td>
             <!-- <td></td>      -->                  
             <td>RM <?=$value['charge']?></td>
             <td><img src="uploads/<?php echo $patientpic?>" width="30px" height="30px"/> <?php echo $patientname?></td>
            <!--  <td></td> -->
              <td><?=$value['adddate']?></td>
              <td><?=$value['time_in_hours']?></td>
                      </tr>  
            <?php
           }

                    ?>

  
                    </tbody>
                    </table>	
											
					</div>
				</div>
			</div>
		</div>
	</div>
				