<?php
    $id=$this->session->userdata('loginid');
    $sql ="select * from users  where loginid='".$id."'";
    $query = $this->db->query($sql);
    foreach ($query->result() as $row) 
        {
       
         $prfpic=$row->profile_pic;
         $st=$row->profile_update;
         $id=$row->loginid;
         $name=$row->first_name.' '.$row->last_name;
        //echo $upgrade_st;
        }
?>

<style type="text/css">
  th{
        text-transform: none !important;
  }
</style>
<div class="row">
<div class="col-md-12">
    <div class="card-body">
     <h5>Welcome : <span style="color: blue;font-weight: bold"><?php echo $name?></span></h5>
    </div>

 </div>
</div>
<div class="row">
                            <div class="col-md-12">
                                    <div class="card-header" style="background: #FFF;">
                <div class="card-title">My Monthly Summary</div>
                    </div>
                                <div class="card card-bgimg br-7">
                                    <div class="row">
                                        <div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
                                            <div class="card-body text-center">
                                                <h5 class="text-white">Active Bookings</h5>
                                                <h2 class="mb-2 mt-3 fs-2 text-white mainvalue">0</h2>
                                                 <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
                                            <div class="card-body text-center">
                                                <h5 class="text-white">Completed Bookings</h5>
                                                <h2 class="mb-2 mt-3 fs-2 text-white mainvalue">0</h2>
                                                 <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
                                            <div class="card-body text-center">
                                                <h5 class="text-white">Total Bookings</h5>
                                                <h2 class="mb-2 mt-3 fs-2 text-white mainvalue">0</h2>
                                                 <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
                                            <div class="card-body text-center">
                                                <h5 class="text-white">Total Patients</h5>
                                                <h2 class="mb-2 mt-3 fs-2 text-white mainvalue">0</h2>
                                                 <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
                                            <div class="card-body text-center">
                                                <h5 class="text-white">Total Hours</h5>
                                                <h2 class="mb-2 mt-3 fs-2 text-white mainvalue">0</h2>
                                                 <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-sm-6 pr-0 pl-0">
                                            <div class="card-body text-center">
                                                <h5 class="text-white">Total Earnings</h5>
                                                <h2 class="mb-2 mt-3 fs-2 text-white mainvalue">RM 0.00 </h2>
                                                 <div><i class="si si-graph mr-1 text-danger"></i><span class="text-white"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  

  <div class="row">
        <div class="col-md-12">
            <div class="card card-profile  overflow-hidden">
                <div class="card-header">
                <div class="card-title">My Latest Booking</div>
                    </div>
                <div class="card-body">
                    <div class="nav-wrapper p-0">
                        <ul class="nav nav-pills dragscroll horizontal nav-fill flex-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0 active show" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="fa fa-home mr-2"></i>All (0)</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="fa fa-user mr-2"></i>Active (0)</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i class="fa fa-picture-o mr-2"></i>Completed (0)</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-4-tab" data-toggle="tab" href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-4" aria-selected="false"><i class="fa fa-newspaper-o mr-2 mt-1"></i>Cancelled (0)</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body pb-0">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                            <div class="table-responsive mb-3">
                                 <div class="card-title">My Recent Booking Summary </div>
                              
                                <table id="example" class="table table-striped table-bordered text-nowrap w-100 text-nowrap mb-0">
                                              <thead>

                                              <tr>
                                               
                 <!-- Booking Date | Hours Booked (19.00 - 20.00) | Total Hours | Patient Name (Click for Profile) | Contact No | Action (View Details)   -->
                 
                  <th>Booking Date</th>    
                  <th> Hours Booked</th> 
                  <th> Total Hours</th> 
                  <th>Patient Name</th>                     
                         <th>Contact No</th>
                     <th>Action</th>                   
            <!--  <th>Charge</th> -->
             
           <!--   <th>Patient Pic</th> -->
             
              
                      </tr>
                    </thead>
                    <tbody>
                    <?php

          foreach ($nurselist as $key => $value)
           {
            $nursename=$value['nfmae'].''.$value['nlname'];
            $pcontactno=$value['pcontactno'];
            $nursepic=$value['nursepic'];
            $patientname=$value['bfname'].''.$value['blname'];
            $patientpic=$value['patient_pic'];
            $nurseid=$value['nurseid'];
            if($id==$nurseid)
            {
            //$bookdate=date('Y-m-d H:i:s',$value['adddate']);
            ?>
                                  <tr>
                                   
                    <td><?=$value['adddate']?></td>  
                     <td><?=$value['time_in_hours']?></td>   
                      <td><?=$value['time_in_hours']?></td>        
           
                                   
           <!--   <td>RM <?=$value['charge']?></td> -->
             <td><?php echo $patientname?></td>
              <td><?php echo $pcontactno?></td> 
            <!--  <td><img src="uploads/<?php echo $patientpic?>" width="100px" height="70px"/></td> -->
              <td><button class="btn btn-info">View Details</button></td> 
             
                      </tr>  
            <?php
           }
         }

                    ?>

  
                    </tbody>
                    </table>    
                        </div>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
    