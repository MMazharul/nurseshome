<header class="header_sticky"><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<a href="#menu" class="btn_mobile">
			<div class="hamburger hamburger--spin" id="hamburger">
				<div class="hamburger-box">
					<div class="hamburger-inner"></div>
				</div>
			</div>
		</a>
		<!-- /btn_mobile-->
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-6">
					<div id="logo_home">
						<h1><a href="<?=base_url()?>" title="CarePlus Technologies">CarePlus Technologies</a></h1>
					</div>
				</div>
				<div class="col-lg-9 col-6">
					<!--<ul id="top_access">-->
					<!--	<li><a href="<?=base_url()?>auth/index"><i class="pe-7s-user"></i></a></li>-->
					<!--	<li><a href="<?=base_url()?>auth/registration"><i class="pe-7s-add-user"></i></a></li>-->
					<!--</ul>-->
					<nav id="menu" class="main-menu">
						<ul>
							<li>
								<span><a href="<?=base_url()?>front_home">Home</a></span>
								
							</li>
							<li>
								<span><a href="<?=base_url()?>auth/registration">Signup</a></span>
	
							</li>
							<li>
								<span><a href="<?=base_url()?>auth/index">Login</a></span>
								
							</li>
							
						</ul>
					</nav>
					<!-- /main-menu -->
				</div>
			</div>
		</div>
		<!-- /container -->
	</header>
	<!-- /header -->
