<!-- COMMON SCRIPTS -->
	<script src="<?=base_url()?>front_assets/js/jquery-2.2.4.min.js"></script>
	<script src="<?=base_url()?>front_assets/js/common_scripts.min.js"></script>
	<script src="<?=base_url()?>front_assets/js/functions.js"></script>

	<!-- SPECIFIC SCRIPTS -->
		<script src="<?=base_url()?>back_assets/plugins/select2/select2.full.min.js"></script>
<script src="<?=base_url()?>back_assets/js/select2.js"></script>

<script type="text/javascript">
   $(document).ready(function() {
    $('.select2').select2();
  });
</script>

<script>
  $(function() {
    $('#language').multipleSelect({
      placeholder: 'Here is the placeholder via javascript'
    })
  })
</script>
