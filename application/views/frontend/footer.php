
<footer>
  <div class="container margin_60_35">
<div class="row">
				<div class="col-lg-3 col-md-12">
					<p>
						<a href="front_home" title="Nurses At Home">
							<img src="back_assets/images/brand/logo.png" data-retina="true" alt="" width="163" height="36" class="img-fluid">
						</a>
					</p>
				</div>
				<div class="col-lg-3 col-md-4">
					<h5>About</h5>
					<ul class="links">
						<li><a href="#">About us</a></li>
						<li><a href="#">FAQ</a></li>
						<li><a href="./auth/index">Login</a></li>
						<li><a href="./auth/registration">Register</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-md-4">
					<h5>Useful links</h5>
					<ul class="links">
						<li><a href="#page">About Our Nurses</a></li>
						<li><a href="#page">Join As A Nurse</a></li>
				
					</ul>
				</div>
				<div class="col-lg-3 col-md-4">
					<h5>Contact with Us</h5>
					<ul class="contacts">
						<li><a href="tel://1-300-22-6877"><i class="icon_mobile"></i> Call / WhatsApp Us at +6012-6217580</a></li>
						<li><a href="mailto:bc@nursesathome.com.my"><i class="icon_mail_alt"></i> bc@nursesathome.com.my</a></li>
					</ul>
					
				</div>
			</div>
			<!--/row-->
    <!--/row-->
    <hr>
    <div class="row">
      <div class="col-md-7">
        <ul id="additional_links">
          <li><a href="#">Terms & Conditions</a></li>
          <li><a href="#">Privacy Policy</a></li>
        </ul>
      </div>
      <div class="col-md-5">
        <div id="copy">On Demand Nursing & Caregiving Services.</div>
      </div>
    </div>
  </div>
</footer>
<!--/footer-->
</div>
<!-- page -->

<div id="toTop"></div>
