<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Find easily a doctor and book online an appointment">
	<meta name="author" content="Ansonika">
		<title>CarePlus | On Demand Nursing Services Malaysia</title>

	<!-- Favicons-->
	<link rel="shortcut icon" href="back_assets/images/brand/logo.png" type="image/x-icon">
	<link rel="apple-touch-icon" type="image/x-icon" href="back_assets/images/brand/logo.jpg">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="back_assets/images/brand/logo.jpg">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="back_assets/images/brand/logo.jpg">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="back_assets/images/brand/logo.jpg">

	<!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800" rel="stylesheet">

	<!-- BASE CSS -->
	<link href="<?=base_url()?>front_assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?=base_url()?>front_assets/css/style.css" rel="stylesheet">
	<link href="<?=base_url()?>front_assets/css/menu.css" rel="stylesheet">
	<link href="<?=base_url()?>front_assets/css/vendors.css" rel="stylesheet">
	<link href="<?=base_url()?>front_assets/css/icon_fonts/css/all_icons_min.css" rel="stylesheet">

	<!-- SPECIFIC CSS -->
 <link href="<?=base_url()?>front_assets/css/date_picker.css" rel="stylesheet">
	 <link  href="<?=base_url()?>front_assets/fonts/fonts/font-awesome.min.css" rel="stylesheet">
<link href="<?=base_url()?>back_assets/plugins/select2/select2.min.css" rel="stylesheet" />
	<!-- YOUR CUSTOM CSS -->
	<link href="<?=base_url()?>css/custom.css" rel="stylesheet">
	<style type="<?=base_url()?>text/css">
	/*search box css start here*/
.search-sec{
    padding: 2rem;
}
.search-slt{
    display: block;
    width: 100%;
    font-size: 0.875rem;
    line-height: 1.5;
    color: #55595c;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    height: calc(3rem + 2px) !important;
    border-radius:0;
}
.wrn-btn{
    width: 100%;
    font-size: 16px;
    font-weight: 400;
    text-transform: capitalize;
    height: calc(3rem + 2px) !important;
    border-radius:0;
}
@media (min-width: 992px){
    .search-sec{
        position: relative;

        background: rgba(26, 70, 104, 0.51);
    }
}

@media (max-width: 992px){
    .search-sec{
        background: #1A4668;
    }
}
</style>
	</head>
