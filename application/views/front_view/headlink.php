<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<base href="<?=base_url();?>"/>
    
    <meta http-equiv="x-ua-compatible" content="ie=edge">

        <!-- Favicon -->
		<link rel="icon" href="back_assets/images/brand/logo.jpg" type="image/x-icon"/>
		<link rel="shortcut icon" type="image/x-icon" href="back_assets/images/brand/logo.jpg" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicons -->
    <link rel="shortcut icon" href="front_assets/img/logo/logo13.png">
    <!-- Fontawesome css -->
    <link rel="stylesheet" href="front_assets/css/font-awesome.min.css">
    <!-- Ionicons css -->
    <link rel="stylesheet" href="front_assets/css/ionicons.min.css">
    <!-- linearicons css -->
    <link rel="stylesheet" href="front_assets/css/linearicons.css">
    <!-- Nice select css -->
    <link rel="stylesheet" href="front_assets/css/nice-select.css">
    <!-- Jquery fancybox css -->
    <link rel="stylesheet" href="front_assets/css/jquery.fancybox.css">
    <!-- Jquery ui price slider css -->
    <link rel="stylesheet" href="front_assets/css/jquery-ui.min.css">
    <!-- Meanmenu css -->
    <link rel="stylesheet" href="front_assets/css/meanmenu.min.css">
    <!-- Nivo slider css -->
    <link rel="stylesheet" href="front_assets/css/nivo-slider.css">
    <!-- Owl carousel css -->
    <link rel="stylesheet" href="front_assets/css/owl.carousel.min.css">
    <!-- Bootstrap css -->
    <link rel="stylesheet" href="front_assets/css/bootstrap.min.css">
    <!-- Custom css -->
    <link rel="stylesheet" href="front_assets/css/default.css">
    <!-- Main css -->
    <link rel="stylesheet" href="front_assets/css/style.css">
    <!-- Responsive css -->
    <link rel="stylesheet" href="front_assets/css/responsive.css">
    <!-- Select 2 css  -->
    <link rel="stylesheet" href="front_assets/css/select2.min.css">
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" /> -->


    <!-- Modernizer js -->
    <script src="front_assets/js/vendor/modernizr-3.5.0.min.js"></script>
</head>