 <!-- jquery 3.2.1 -->
    <script src="front_assets/js/vendor/jquery-3.2.1.min.js"></script>
    <!-- Countdown js -->
    <script src="front_assets/js/jquery.countdown.min.js"></script>
    <!-- Mobile menu js -->
    <script src="front_assets/js/jquery.meanmenu.min.js"></script>
    <!-- ScrollUp js -->
    <script src="front_assets/js/jquery.scrollUp.js"></script>
    <!-- Nivo slider js -->
    <script src="front_assets/js/jquery.nivo.slider.js"></script>
    <!-- Fancybox js -->
    <script src="front_assets/js/jquery.fancybox.min.js"></script>
    <!-- Jquery nice select js -->
    
    <!-- Jquery ui price slider js -->
    <script src="front_assets/js/jquery-ui.min.js"></script>
    <!-- Owl carousel -->
    <script src="front_assets/js/owl.carousel.min.js"></script>
    <!-- Bootstrap popper js -->
    <script src="front_assets/js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="front_assets/js/bootstrap.min.js"></script>
    <!-- Plugin js -->
    <script src="front_assets/js/plugins.js"></script>
    <!-- Main activaion js -->
    <script src="front_assets/js/main.js"></script>
    <script src="front_assets/js/select2.min.js" type="text/javascript"></script>
    <!-- <script src="front_assets/js/jquery.nice-select.min.js"></script> -->
    <!-- Notify Js -->
    <script src="front_assets/js/notify.min.js"></script>
    <!-- Select 2 JS -->
    
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> -->


    <!-- === Search js Start === -->
    <script>

    // $(document).on('input','#search_input',function(event)
    // {
    //     var search_content=$(this).val();
    //       alert(search_content);
    // });
    
      $(document).on('input','#search_input',function(event)
    {
        var search_content=$(this).val();
          //alert(search_content);
        $("#loader_gif").show();
        $("#search_item").fadeIn();
         $.ajax({
              url: "<?php echo site_url('home/get_search_input_item');?>",
              type: "post",
              data: {search_content:search_content},
              success: function(msg)
              {
               
                $("#search_item").html(msg);
                $("#loader_gif").hide();
              }      
         });  
      });
      
      function hide_search_item_div() 
      {
        $("#search_item").fadeOut();
      }
  
   </script>
    <!-- ===== Search Js End === -->


