<header>
            <!-- Header Top Start Here -->
            <div class="header-top-area">
                <div class="container">
                    <!-- Header Top Start -->
                    <div class="header-top">
                        <ul>
                            <li><a href="#">Free Shipping on order over AED 200</a></li>
                            <li><a href="checkout">Checkout</a></li>
                            <li><a href="logout">Log Out</a></li>
                        </ul>
                        <ul>
                            <li><span>Language</span> <a href="#">English<i class="lnr lnr-chevron-down"></i></a>
                                <!-- Dropdown Start -->
                                <ul class="ht-dropdown">
                                    <li>
                                        <a href="#"><img src="front_assets/img/header/1.jpg" alt="language-selector">English</a>
                                    </li>
                                   
                                </ul>
                                <!-- Dropdown End -->
                            </li>
                            <!-- <li><span>Currency</span><a href="#"> USD $ <i class="lnr lnr-chevron-down"></i></a>
                                <!-- Dropdown Start -->
                                <ul class="ht-dropdown">
                                    <li><a href="#">&#36; USD</a></li>
                                    <li><a href="#"> € Euro</a></li>
                                    <li><a href="#">&#163; Pound Sterling</a></li>
                                </ul>
                                <!-- Dropdown End -->
                            </li> -->
                            <li><a href="#">My Account<i class="lnr lnr-chevron-down"></i></a>
                                <!-- Dropdown Start -->
                                <ul class="ht-dropdown">
                                    <li><a href="login">Login</a></li>
                                    <li><a href="regs">Register</a></li>
                                </ul>
                                <!-- Dropdown End -->
                            </li>
                        </ul>
                    </div>
                    <!-- Header Top End -->
                </div>
                <!-- Container End -->
            </div>
            <!-- Header Top End Here -->
            <!-- Header Middle Start Here -->
            <div class="header-middle ptb-10 ">
                <div class="container">
                    <div class="row align-items-center no-gutters">
                        <div class="col-lg-2 col-md-12">
                            <div class="logo mb-all-30">
                                <a href="home"><img src="front_assets/img/logo/logo14.png" alt="logo-image"></a>
                            </div>
                        </div>
                        <!-- Categorie Search Box Start Here -->
                    <div class="col-lg-5 col-md-8 ml-auto mr-auto col-10">
                            <div class="categorie-search-box">
                             <form action="#" autocomplete="off">
                                <input onblur="hide_search_item_div()" id="search_input" type="text"  placeholder="Vape products...">
                                <button><i class="lnr lnr-magnifier" style="font-size: 14px;">&nbsp;<span style="font-size:16px;letter-spacing: 0.50px;">Search</span></i></button>

                                <div class="col-md-12 margin_padding_0">
                                     <div class="auto_search_box_css " id="search_item">
                                    <img style="display: none" id="loader_gif" src="front_assets/images/loader.gif" alt="">
                                    <ul class="margin_padding_0">
                                     <li><i class="fa fa-search"></i> Enter what are you looking for</li>
                                 </ul>
                             </div>
                         </div>
                    </form>
                </div>      
            </div>
                   
                  
                        <!-- Categorie Search Box End Here -->
                        <!-- Cart Box Start Here -->
                        <div class="col-lg-4 col-md-12">
                            <div class="cart-box mt-all-30">
                                <ul class="d-flex justify-content-lg-end justify-content-center align-items-center">
                                <?php $cart=$this->cart->contents();?>
                                    <li><a href="#"><i class="lnr lnr-cart"></i><span class="my-cart"><span class="total-pro" id="cart_counter"><?=count($cart);?></span><span>cart</span></span></a>
                                        <ul class="ht-dropdown cart-box-width" id="cart_list">
                                            <li>
                                                <!-- Cart Box Start -->
                                                <?php foreach ($cart as $key => $value) {?>
                                                <div class="single-cart-box">
                                                    <div class="cart-img">
                                                        <a href="#"><img src="uploads/product/<?=$value['image'];?>" alt="cart-image"></a>
                                                        <span class="pro-quantity"><?=$value['qty'];?></span>
                                                    </div>
                                                    <div class="cart-content">
                                                        <h6><a href="product"><?=$value['name'];?> </a></h6>
                                                        <span class="cart-price"><?=number_format($value['price'],2);?> &#x9f3; </span>
                                                        <span><?=number_format($value['subtotal'],2);?> &#x9f3; </span>
                                                        <!-- <span>Color: Yellow</span> -->
                                                    </div>
                                                    <a onclick="remove_cart('<?=$value['rowid'];?>')" class="del-icone" href="javascript:void(0)" >
                                                    <i class="ion-close"></i>
                                                    </a>

                                                    <!-- <a href="javascript:void(0)" onclick="remove_cart('<?=$value['rowid'];?>')"><i class="fa fa-times" aria-hidden="true"></i> -->
                                            </a>
                                                </div>
                                                <!-- Cart Box End -->
                                                <?php } ?>
                                                <!-- Cart Footer Inner Start -->
                                                <div class="cart-footer">
                                                    <ul class="price-content">
                                                        <li>Subtotal <span><?=number_format($this->cart->total(),2);?> &#x9f3; </span></li>
                                                        <!-- <li>Shipping <span>$7.00</span></li>
                                                        <li>Taxes <span>$0.00</span></li>
                                                        <li>Total <span>$64.95</span></li> -->
                                                    </ul>
                                                    
                                                    <div class="cart-actions text-center">
                                                        <a class="cart-checkout" href="checkout">Checkout</a>
                                                        <a class="cart-checkout" href="cart">View Cart</a>
                                                    </div>
                                                  
                                                </div>
                                                <!-- Cart Footer Inner End -->
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#"><i class="lnr lnr-heart"></i><span class="my-cart"><span>Shopping List</span><span>list (0)</span></span></a>
                                    </li>
                                    <li><a href="login"><i class="lnr lnr-user"></i><span class="my-cart"><span> <strong>Sign in</strong> Or</span><span> Join My Site</span></span></a>

                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- Cart Box End Here -->
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Container End -->
            </div>
            <!-- Header Middle End Here -->
            <!-- Header Bottom Start Here -->
            <div class="header-bottom  header-sticky">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-xl-3 col-lg-4 col-md-6 vertical-menu d-none d-lg-block">
                            <span class="categorie-title">Shop by Categories</span>
                        </div>
                        <div class="col-xl-9 col-lg-8 col-md-12">
                            <nav class="d-none d-lg-block">
                                <ul class="header-bottom-list d-flex">
                                    <li><a href="home">Home</a></li>

                                    <?php $top_menu=$this->config->item('top_category');?>
                                    <?php foreach ($top_menu as $key => $value):?>
                                    <li>

                                    <a href="product/category/<?=$value['name'];?>"><?php print_r($value['name']);?><i class="fa fa-angle-down"></i>
                                    </a>

                                        <!-- Home Version Dropdown Start -->
                                        <ul class="ht-dropdown dropdown-style-two">

                                        <?php $top_sub_catgory=$this->config->item('sub_category');?>

                                        <?php foreach ($top_sub_catgory as $key => $value2):?>
                                        <?php
                                        if($value2['cat_id']==$value['id']){
                                            ?>
                                        
                                            <li><a href="product/sub_category/<?=$value['name'];?>/<?=$value2['sub_name'];?>">
                                            <?php print_r($value2['sub_name']);?>
                                                  
                                                  </a></li>
                                                  <?php
                                                    }
                                                  ?>
                                            
                                            
                                                  <?php endforeach ?>
                                        </ul>
                                        <!-- Home Version Dropdown End -->

                                    </li>
                                    <?php endforeach ?>
                                   
                                    
                                
                                    
                                 
                                    <li><a href="contact">contact us</a></li>
                                </ul>
                            </nav>
                            <div class="mobile-menu d-block d-lg-none">
                                <nav>
                                    <ul>
                                        <li><a href="index">home</a>
                                            <!-- Home Version Dropdown Start -->
                                            <ul>
                                                <li><a href="index">Home Version 1</a></li>
                                                <li><a href="index-2">Home Version 2</a></li>
                                                <li><a href="index-3">Home Version 3</a></li>
                                                <li><a href="index-4">Home Version 4</a></li>
                                            </ul>
                                            <!-- Home Version Dropdown End -->
                                        </li>
                                        <li><a href="">shop</a>
                                            <!-- Mobile Menu Dropdown Start -->
                                            <ul>
                                                <li><a href="product">product details</a></li>
                                                <li><a href="compare">compare</a></li>
                                                <li><a href="cart">cart</a></li>
                                                <li><a href="checkout">checkout</a></li>
                                                <li><a href="wishlist">wishlist</a></li>
                                            </ul>
                                            <!-- Mobile Menu Dropdown End -->
                                        </li>
                                        <li><a href="blog">Blog</a>
                                            <!-- Mobile Menu Dropdown Start -->
                                            <ul>
                                                <li><a href="single-blog">blog details</a></li>
                                            </ul>
                                            <!-- Mobile Menu Dropdown End -->
                                        </li>
                                        <li><a href="#">pages</a>
                                            <!-- Mobile Menu Dropdown Start -->
                                            <ul>
                                                <li><a href="register">register</a></li>
                                                <li><a href="login">sign in</a></li>
                                                <li><a href="forgot-password">forgot password</a></li>
                                                <li><a href="404">404</a></li>
                                            </ul>
                                            <!-- Mobile Menu Dropdown End -->
                                        </li>
                                        <li><a href="about">about us</a></li>
                                        <li><a href="contact">contact us</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Container End -->
            </div>
            <!-- Header Bottom End Here -->
            <!-- Mobile Vertical Menu Start Here -->
            <div class="container d-block d-lg-none">
                <div class="vertical-menu mt-30">
                    <span class="categorie-title mobile-categorei-menu">Shop by Categories </span>
                    <nav>
                        <div id="cate-mobile-toggle" class="category-menu sidebar-menu sidbar-style mobile-categorei-menu-list menu-hidden ">
                            <ul>
                                <li class="has-sub"><a href="#">vape flavours</a>
                                    <ul class="category-sub">
                                        <li class="has-sub"><a href="">lorem</a>
                                            <ul class="category-sub">
                                                <li><a href="">lorem</a></li>
                                                <li><a href="">lorem Turpis</a></li>
                                                <li><a href="">lorem</a></li>
                                                <li><a href="">lorem</a></li>
                                            </ul>
                                        </li>
                                        <li class="has-sub"><a href="">Purus Lacus</a>
                                            <ul class="category-sub">
                                                <li><a href="">Magna Pellentesq</a></li>
                                                <li><a href="">Molestie Tortor</a></li>
                                                <li><a href="">Vehicula Element</a></li>
                                                <li><a href="">Sagittis Blandit</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">gps lorem</a></li>
                                        <li><a href="">lorem</a></li>
                                        <li><a href="">lorem</a></li>
                                    </ul>
                                    <!-- category submenu end-->
                                </li>
                                <li class="has-sub"><a href="#">lorem jueruie</a>
                                    <ul class="category-sub">
                                        <li class="menu-tile">jueruie</li>
                                        <li><a href="">lorem</a></li>
                                        <li><a href="">lorem</a></li>
                                        <li><a href="">lorem</a></li>
                                        <li><a href="">lorem frrwew</a></li>
                                    </ul>
                                    <!-- category submenu end-->
                                </li>
                                <li class="has-sub"><a href="#">lorermwe </a>
                                    <ul class="category-sub">
                                        <li><a href="">lorem</a></li>
                                        <li><a href="">lorem</a></li>
                                        <li><a href="">lorem</a></li>
                                        <li><a href="">lorem</a></li>
                                        
                                    </ul>
                                    <!-- category submenu end-->
                                </li>
                                <li class="has-sub"><a href="#">loeres</a>
                                    <ul class="category-sub">
                                            <li><a href="">lorem</a></li>
                                            <li><a href="">lorem</a></li>
                                            <li><a href="">lorem</a></li>
                                            <li><a href="">lorem</a></li>
                                           
                                    </ul>
                                    <!-- category submenu end-->
                                </li>
                                <li class="has-sub"><a href="#">Tloem </a>
                                    <ul class="category-sub">
                                            <li><a href="">lorem</a></li>
                                            <li><a href="">lorem</a></li>
                                            <li><a href="">lorem</a></li>
                                            <li><a href="">lorem</a></li>
                                       
                                    </ul>
                                    <!-- category submenu end-->
                                </li>
                                <li><a href="#">loeremvd</a> </li>
                                <li><a href="#">loeremi</a></li>
                                <li><a href="#">ojfiofo</a></li>
                            </ul>
                        </div>
                        <!-- category-menu-end -->
                    </nav>
                </div>
            </div>
            <!-- Mobile Vertical Menu Start End -->
        </header>

        <script>
function remove_cart(row_id)
    { 
       $.ajax({
            url: "<?php echo site_url('cart/remove');?>",
            type: "post",
            data: {row_id:row_id},
            success: function(msg)
            {
                location.reload();
                
            }      
        });  
    }
</script>

