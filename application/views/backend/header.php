<?php
   $id=$this->session->userdata('loginid');
   $sql ="select * from users  where loginid='".$id."'";
   $query = $this->db->query($sql);
   foreach ($query->result() as $row)
       {

        $prfpic=$row->profile_pic;
        $st=$row->profile_update;
       //echo $upgrade_st;
       }
   ?>
<div class="app-header header hor-topheader d-flex">
   <div class="container">
      <div class="d-flex">
         <a class="header-brand" href="home">
            <!-- <h4 style="line-height: 3">Nurse Booking App</h4> -->
            <img src="back_assets/images/logo.png" class="header-brand-img main-logo" alt="logo">
            <!-- <img src="back_assets/images/brand/icon.png" class="header-brand-img icon-logo" alt="logo"> -->
         </a>
         <!-- logo-->
         <a id="horizontal-navtoggle" class="animated-arrow hor-toggle"><span></span></a>
         <!-- <a href="javascript:void(0)" data-toggle="search" class="nav-link nav-link  navsearch"><i class="fa fa-search"></i></a> -->
         <div class="header-form">

         </div>
         <div class="d-flex order-lg-2 ml-auto header-rightmenu">
            <div class="dropdown header-user">
               <a class="nav-link leading-none siderbar-link"  data-toggle="sidebar-right" data-target=".sidebar-right">
               <span class="mr-3 d-none d-lg-block ">
               <span class="text-gray-white"><span class="ml-2">
               <?=$this->session->userdata('email');?>
               </span></span>
               </span>
               <span class="avatar avatar-md brround"><img src="uploads/<?php echo $prfpic?>" alt="Profile-img" class="avatar avatar-md brround"></span>
               </a>
               <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                  <div class="header-user text-center mt-4 pb-4">
                     <span class="avatar avatar-xxl brround"><img src="back_assets/images/users/female/33.png" alt="Profile-img" class="avatar avatar-xxl brround"></span>
                     <a href="javascript:void(0)" class="dropdown-item text-center font-weight-semibold user h3 mb-0">Alison</a>
                     <small>Web Designer</small>
                  </div>
                  <a class="dropdown-item" href="javascript:void(0)">
                  <i class="dropdown-icon mdi mdi-account-outline "></i> Spruko technologies
                  </a>
                  <a class="dropdown-item" href="javascript:void(0)">
                  <i class="dropdown-icon  mdi mdi-account-plus"></i> Add another Account
                  </a>
                  <div class="card-body border-top">
                     <div class="row">
                        <div class="col-6 text-center">
                           <a class="" href=""><i class="dropdown-icon mdi  mdi-message-outline fs-30 m-0 leading-tight"></i></a>
                           <div>Inbox</div>
                        </div>
                        <div class="col-6 text-center">
                           <a class="##" href=""><i class="dropdown-icon mdi mdi-logout-variant fs-30 m-0 leading-tight"></i></a>
                           <div>Sign out</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- profile -->
            <div class="dropdown">
               <a  class="nav-link icon siderbar-link" data-toggle="sidebar-right" data-target=".sidebar-right">
               <i class="fe fe-more-horizontal"></i>
               </a>
            </div>
            <!-- Right-siebar-->
         </div>
      </div>
   </div>
</div>
<!--app-header end-->
<!-- Horizontal-menu -->
<div class="horizontal-main hor-menu clearfix">
   <div class="horizontal-mainwrapper container clearfix">
      <nav class="horizontalMenu clearfix">
         <ul class="horizontalMenu-list">
            <!-- for superadmin / admin  -->
            <?php
               $role=$this->session->userdata('user_role');
               if ($role == 1){ ?>
            <li aria-haspopup="true">
               <a href="home" class="active"><i class="typcn typcn-device-desktop hor-icon"></i>Dashboard</a>
            </li>
            <li aria-haspopup="true">
               <a href="javascript:void(0)" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i> Location <i class="fa fa-angle-down horizontal-icon"></i></a>
               <ul class="sub-menu">
                  <li aria-haspopup="true">
                     <a href="location/state_list">Manage States</a>
                  </li>
               </ul>
            </li>
            <li aria-haspopup="true">
               <a href="javascript:void(0)" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i> Patient <i class="fa fa-angle-down horizontal-icon"></i></a>
               <ul class="sub-menu">
                  <li aria-haspopup="true">
                     <a href="patient">Add Patient</a>
                  </li>
                  <li aria-haspopup="true">
                     <a href="patient/patient_list">Patient List</a>
                  </li>
               </ul>
            </li>
            <li aria-haspopup="true">
               <a href="javascript:void(0)" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i>Nurse <i class="fa fa-angle-down horizontal-icon"></i></a>
               <ul class="sub-menu">
                  <li aria-haspopup="true">
                     <a href="nurse">Add Nurse</a>
                  </li>
                  <li aria-haspopup="true">
                     <a href="nurse/nurse_list">Nurse List</a>
                  </li>
                  <!-- <li aria-haspopup="true">
                     <a href="nurse/nurse_list">Schedule</a>
                     </li>	 -->
               </ul>
            </li>
            <li aria-haspopup="true">
               <a href="javascript:void(0)" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i>Bookings <i class="fa fa-angle-down horizontal-icon"></i></a>
               <ul class="sub-menu">
                  <li aria-haspopup="true">
                     <a href="booking">Booking List</a>
                  </li>
               </ul>
            </li>
            <li aria-haspopup="true">
               <a href="javascript:void(0)" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i> Payment <i class="fa fa-angle-down horizontal-icon"></i></a>
               <ul class="sub-menu">
                  <li aria-haspopup="true">
                     <a href="booking/all_transaction">Transactions History</a>
                  </li>
               </ul>
            </li>
            <li aria-haspopup="true">
               <a href="javascript:void(0)" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i>Review <i class="fa fa-angle-down horizontal-icon"></i></a>
               <ul class="sub-menu">
                  <li aria-haspopup="true">
                     <a href="booking/review/2">Patient Review</a>
                  </li>
                  <li aria-haspopup="true">
                     <a href="booking/review/1">Nurse Review</a>
                  </li>

               </ul>
            </li>
            <li aria-haspopup="true">
               <a href="javascript:void(0)" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i>Report <i class="fa fa-angle-down horizontal-icon"></i></a>
               <ul class="sub-menu">
                  <li aria-haspopup="true">
                     <a href="<?=base_url('report/summary_report')?>">Daily Summary</a>
                  </li>
                  <li aria-haspopup="true">
                     <a href="<?=base_url('report/booking_report')?>">Booking</a>
                  </li>
                  <li aria-haspopup="true">
                     <a href="<?=base_url('report/nurse_wise_report')?>">Nurse wise Report</a>
                  </li>
                  <li aria-haspopup="true">
                     <a href="<?=base_url('report/transaction_report')?>">Transaction Report</a>
                  </li>
               </ul>
            </li>
            <li aria-haspopup="true">
               <a href="javascript:void(0)" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i>Settings <i class="fa fa-angle-down horizontal-icon"></i></a>
               <ul class="sub-menu">
                  <li aria-haspopup="true">
                     <a href="add-addons">Add Addons</a>
                  </li>
                  <li aria-haspopup="true">
                     <a href="list-addons">List Addons</a>
                  </li>
               </ul>
            </li>
            <!-- for nurse  -->
            <?php
               }
                elseif($role==3)
               {
                		?>
            <li aria-haspopup="true">
               <a href="home" class="sub-icon 	active"><i class="typcn typcn-device-desktop hor-icon"></i> Dashboard </a>
            </li>
            <li aria-haspopup="true">
               <a href="nurse-profile-view" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i>Nurse Profile </a>
            </li>

            <?php
               if($st==0)
               {

               ?>
            <li aria-haspopup="true">
               <a href="nurse-profile-view" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i>Nurse Schedule</a>
               <ul class="sub-menu">
                  <li aria-haspopup="true">
                     <a href="nurse-profile-view">Add Schedule</a>
                  </li>
                  <li aria-haspopup="true">
                     <a href="nurse-profile-view">Schedule Monthly View</a>
                  </li>
               </ul>
            </li>
            <li aria-haspopup="true">
               <a href="nurse-profile-view" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i>Nurse Bookings</a>
            </li>
            <li aria-haspopup="true">
               <a href="nurse-profile-view" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i>Nurse Patients </a>
            </li>
            <?php
               }
               else
               {
               	?>
            <li aria-haspopup="true">
               <a href="nurse-schedule-monthly" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i>Nurse Schedule</a>
            </li>
            <li aria-haspopup="true">
               <a href="patient-bookings-for-nurse" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i>Nurse Bookings</a>
            </li>
            <li aria-haspopup="true">
               <a href="nurse_patient_list" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i>Nurse  Patients </a>
            </li>
            <?php
               }
               }

               elseif($role>4)
              {
                  ?>
           <li aria-haspopup="true">
              <a href="home" class="sub-icon 	active"><i class="typcn typcn-device-desktop hor-icon"></i> Dashboard </a>
           </li>
           <li aria-haspopup="true">
              <a href="nurse-profile-view" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i> Profile </a>
           </li>
           <li aria-haspopup="true">
              <a href="nurse-profile-view" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i> Book Nurse </a>
           </li>
           <li aria-haspopup="true">
              <a href="nurse-profile-view" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i>Request Nurse</a>
           </li>

           <?php

              }

               elseif (($role>3)) {
                             if($st==0)
                             {


               ?>
            ?>
            <li aria-haspopup="true">
               <a href="home" class="active"><i class="typcn typcn-device-desktop hor-icon"></i>Dashboard</a>
            </li>
            <li aria-haspopup="true">
               <a href="profile"  class="sub-icon active">
               <i class="typcn typcn-device-desktop hor-icon"></i>
               Patient Condition</a>
            </li>
            <li aria-haspopup="true">
               <a href="profile"  class="sub-icon active">
               <i class="typcn typcn-device-desktop hor-icon"></i>
               Care for patient</a>
            </li>
            <li aria-haspopup="true">
               <a href="profile"  class="sub-icon active">
               <i class="typcn typcn-device-desktop hor-icon"></i>
               Patient Assessment</a>
            </li>
            <li aria-haspopup="true">
               <a href="profile" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i>Bookings</a>
            </li>
            <li aria-haspopup="true">
               <a href="profile" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i>My Transaction <i class="fa fa-angle-down horizontal-icon"></i></a>
               <ul class="sub-menu">
                  <li aria-haspopup="true">
                     <a href="profile">Transaction History</a>
                  </li>
               </ul>
            </li>
            <?php
               }
               else
               {
               	?>
            <li aria-haspopup="true">
               <a href="home" class="active"><i class="typcn typcn-device-desktop hor-icon"></i>Dashboard</a>
            </li>
            <li aria-haspopup="true">
               <a href="<?=base_url('patient/nurse_book')?>" class="active"><i class="typcn typcn-device-desktop hor-icon"></i>Book Nurse</a>
            </li>
            <li aria-haspopup="true">
               <a href="profile"  class="sub-icon active">
               <i class="typcn typcn-device-desktop hor-icon"></i>
               Patient Profile</a>
            </li>
            <li aria-haspopup="true">
               <a href="assessment"  class="sub-icon active">
               <i class="typcn typcn-device-desktop hor-icon"></i>
               Patient Assessment</a>
            </li>
            <li aria-haspopup="true">
               <a href="nurse-bookings-for-patient" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i>Patient Bookings</a>
            </li>
            <li aria-haspopup="true">
               <a href="booking_transaction-for-patient" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i>Patient Transactions</a>
            </li>
            <?php
               }
               }

               ?>
         </ul>
      </nav>
      <!--Nav end -->
   </div>
</div>
<br>
