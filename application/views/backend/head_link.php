<!doctype html>
<html lang="en" dir="ltr">
	<head>
    <base href="<?=base_url();?>"/>
		<meta charset="UTF-8">
		<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="Nurse App" name="description">
		<meta content="" name="author">
		<meta name="keywords" content="nurse app"/>

		<!-- Favicon -->
		<link rel="icon" href="back_assets/images/brand/logo.png" type="image/x-icon"/>
		<link rel="shortcut icon" type="image/x-icon" href="back_assets/images/brand/logo.jpg" />
		<!-- Title -->
		<title>CarePlus Technologies | On Demand Nursing Services Malaysia</title>

		<!--Bootstrap.min css-->
		<link rel="stylesheet" href="back_assets/plugins/bootstrap/css/bootstrap.min.css">

		<!-- Dashboard css -->
		<link href="back_assets/css/style.css" rel="stylesheet" />

		<!-- Custom scroll bar css-->
		<link href="back_assets/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

		<!-- Horizontal-menu css -->
		<link href="back_assets/plugins/horizontal-menu/dropdown-effects/fade-down.css" rel="stylesheet">
		<link href="back_assets/plugins/horizontal-menu/horizontalmenu.css" rel="stylesheet">

		<!--Select2 css -->
		<link href="back_assets/plugins/select2/select2.min.css" rel="stylesheet" />
		<!--Daterangepicker css-->
		<link href="back_assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

		<!-- Rightsidebar css -->
		<link href="back_assets/plugins/sidebar/sidebar.css" rel="stylesheet">

		<!-- Sidebar Accordions css -->
		<link href="back_assets/plugins/accordion1/css/easy-responsive-tabs.css" rel="stylesheet">

		<!-- Owl Theme css-->
		<link href="back_assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">

		<!-- Morris  Charts css-->
		<link href="back_assets/plugins/morris/morris.css" rel="stylesheet" />

		<!---Font icons css-->
		<link href="back_assets/plugins/iconfonts/plugin.css" rel="stylesheet" />
		<link href="back_assets/plugins/iconfonts/icons.css" rel="stylesheet" />
		<link  href="back_assets/fonts/fonts/font-awesome.min.css" rel="stylesheet">

		<!-- Data table css -->
		<link href="back_assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />
		<link href="back_assets/plugins/datatable/responsivebootstrap4.min.css" rel="stylesheet" />

		<!-- Time picker css-->
		<!-- <link href="back_assets/plugins/time-picker/jquery.timepicker.css" rel="stylesheet" /> -->
		<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css"> -->

		<!-- <link rel="stylesheet" href="cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css"> -->


		<!-- Date Picker css-->
		<!-- <link href="back_assets/plugins/date-picker/spectrum.css" rel="stylesheet" /> -->


	<!---Sweetalert Css-->
		<link href="back_assets/plugins/sweet-alert/jquery.sweet-modal.min.css" rel="stylesheet" />
		<link href="back_assets/plugins/sweet-alert/sweetalert.css" rel="stylesheet" />



		<!-- Fullcalendar css -->
		<link href="back_assets/plugins/fullcalendar/fullcalendar.css" rel='stylesheet' />
		<link href="back_assets/plugins/fullcalendar/fullcalendar.print.min.css" rel='stylesheet' media='print' />


			<!-- Rating css-->
		<link rel="stylesheet" href="back_assets/plugins/rating/css/examples.css">


		<link rel="stylesheet" href="back_assets/plugins/rating/dist/themes/fontawesome-stars.css">

		<!-- File Uploads css-->
        <link href="back_assets/plugins/fileuploads/css/dropify.css" rel="stylesheet" type="text/css" />

		<!-- custom date picker -->
        <!-- <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" /> -->


		<link rel="stylesheet" href="back_assets/zebra_datepicker.css">


	<link rel="stylesheet" href="back_assets/zebra_datepicker_default.css">
	<link href="back_assets/plugins/multipleselect/multiple-select.css" rel="stylesheet" />
	
		<?php
			if (isset($css_to_load)) {
				foreach ($css_to_load as $value) {
					echo "<link href='back_assets/css/$value' rel='stylesheet'/>";
				}
			}
		?>


	</head>
