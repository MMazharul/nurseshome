<!-- Jquery js-->
<script src="<?=base_url()?>back_assets/js/vendors/jquery-3.2.1.min.js"></script>


<!--Bootstrap.min js-->
<script src="<?=base_url()?>back_assets/plugins/bootstrap/popper.min.js"></script>
<script src="<?=base_url()?>back_assets/plugins/bootstrap/js/bootstrap.min.js"></script>

<!--Jquery Sparkline js-->
<script src="<?=base_url()?>back_assets/js/vendors/jquery.sparkline.min.js"></script>

<!-- Chart Circle js-->
<script src="<?=base_url()?>back_assets/js/vendors/circle-progress.min.js"></script>

<!-- Star Rating js-->
<script src="<?=base_url()?>back_assets/plugins/rating/jquery.rating-stars.js"></script>

<!--Moment js-->
<script src="<?=base_url()?>back_assets/plugins/moment/moment.min.js"></script>

<!-- Daterangepicker js-->
<script src="<?=base_url()?>back_assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Horizontal-menu js -->
<script src="<?=base_url()?>back_assets/plugins/horizontal-menu/horizontalmenu.js"></script>

<!-- Sidebar Accordions js -->
<script src="<?=base_url()?>back_assets/plugins/accordion1/js/easyResponsiveTabs.js"></script>

<!-- Custom scroll bar js-->
<script src="<?=base_url()?>back_assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

<!--Owl Carousel js -->
<script src="<?=base_url()?>back_assets/plugins/owl-carousel/owl.carousel.js"></script>
<script src="<?=base_url()?>back_assets/plugins/owl-carousel/owl-main.js"></script>

<!-- Rightsidebar js -->
<script src="<?=base_url()?>back_assets/plugins/sidebar/sidebar.js"></script>

<!-- Charts js-->
<script src="<?=base_url()?>back_assets/plugins/chart/chart.bundle.js"></script>
<script src="<?=base_url()?>back_assets/plugins/chart/utils.js"></script>

<!--Select2 js -->
<script src="<?=base_url()?>back_assets/plugins/select2/select2.full.min.js"></script>
<script src="<?=base_url()?>back_assets/js/select2.js"></script>

<!--Time Counter js-->
<script src="<?=base_url()?>back_assets/plugins/counters/jquery.missofis-countdown.js"></script>
<script src="<?=base_url()?>back_assets/plugins/counters/counter.js"></script>

<!--Morris  Charts js-->
<script src="<?=base_url()?>back_assets/plugins/morris/raphael-min.js"></script>
<script src="<?=base_url()?>back_assets/plugins/morris/morris.js"></script>

<!-- Custom-charts js-->
<script src="<?=base_url()?>back_assets/js/index1.js"></script>


<!-- Data tables js-->
<script src="<?=base_url()?>back_assets/plugins/datatable/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>back_assets/plugins/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>back_assets/plugins/datatable/datatable.js"></script>
<!-- <script src="back_assets/plugins/datatable/datatable-2.js"></script> -->
<script src="<?=base_url()?>back_assets/plugins/datatable/dataTables.responsive.min.js"></script>

<!-- File uploads js -->
<script src="<?=base_url()?>back_assets/plugins/fileuploads/js/dropify.js"></script>
<script src="<?=base_url()?>back_assets/plugins/fileuploads/js/dropify-demo.js"></script>

<!--MutipleSelect js-->
<script src="<?=base_url()?>back_assets/plugins/multipleselect/multiple-select.js"></script>
<script src="<?=base_url()?>back_assets/plugins/multipleselect/multi-select.js"></script>

<!-- Timepicker js -->
<!-- <script src="back_assets/plugins/time-picker/jquery.timepicker.js"></script> -->
<!-- <script src="back_assets/plugins/time-picker/toggles.min.js"></script> -->

<!-- Datepicker js -->
<!-- <script src="back_assets/plugins/date-picker/spectrum.js"></script>
<script src="back_assets/plugins/date-picker/jquery-ui.js"></script>
<script src="back_assets/plugins/input-mask/jquery.maskedinput.js"></script> -->
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>  -->


<!--Fullcalendar js-->
<script src="<?=base_url()?>back_assets/plugins/fullcalendar/moment.min.js"></script>
<script src="<?=base_url()?>back_assets/plugins/fullcalendar/jquery-ui.min.js"></script>
<script src="<?=base_url()?>back_assets/plugins/fullcalendar/fullcalendar.min.js"></script>
<script src="<?=base_url()?>back_assets/js/fullcalendar.js"></script>


<!--Rating js-->
<script src="<?=base_url()?>back_assets/plugins/rating/jquery.rating-stars.js"></script>
<script src="<?=base_url()?>back_assets/plugins/rating/jquery.barrating.js"></script>
<script src="<?=base_url()?>back_assets/plugins/rating/js/examples.js"></script>

<!-- custom date picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Zebra_datepicker/1.9.13/zebra_datepicker.min.js"></script>


<!-- Sweet alert js-->
<script src="<?=base_url()?>back_assets/plugins/sweet-alert/jquery.sweet-modal.min.js"></script>
<script src="<?=base_url()?>back_assets/plugins/sweet-alert/sweetalert.min.js"></script>
<script src="<?=base_url()?>back_assets/js/sweet-alert.js"></script>





<?php //echo isset($custom_datepicker) ? $custom_datepicker : ''?>

<!-- Custom js-->
<script src="<?=base_url()?>back_assets/js/custom.js"></script>


<script>
// $(".alert").delay(5000).slideUp(200, function() {
//     $(this).alert('close');
// });

setTimeout(()=>{
document.querySelectorAll('.alert').forEach((each)=>{
  each.classList.toggle('slide_up');
});
}, 10000);
</script>

<?php

if (isset($js_to_load)) {
	foreach ($js_to_load as $value) {
		echo "<script src='back_assets/js/$value'></script>";
	}
}
if (isset($responsive_table) && $responsive_table == 'true') { ?>
    <script type="text/javascript">
      let dataTable = $("table").dataTable(
      {  "bLengthChange" : false,
           "responsive": true,

          "oLanguage":
        {
            "sSearch": ""
        }
      });
    </script>


<?php } ?>


<script type="text/javascript">
   $(document).ready(function() {
    $('.select2').select2();
  });
</script>

<script>
  $(function() {
    $('#language').multipleSelect({
      placeholder: 'Here is the placeholder via javascript'
    })
  })
</script>
