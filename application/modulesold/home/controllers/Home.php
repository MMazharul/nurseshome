<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends MX_Controller {

    //public $counter=0;
    function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->model('auth/auth_model');
        $this->load->model('home_model');
        // $this->load->model('product/product_model');
        // $this->load->helper('text');
        // $this->load->helper(array('form', 'url'));
        // $this->load->helper('inflector');
        //$this->load->library('encrypt');
        // $this->load->model('home_model');
        // $this->load->model('admin/admin_model');
        // $this->load->model('product/product_model');
    }
    public function index()
    {
    
        $role=$this->session->userdata('user_role');
        $nurselist['loginid']=$this->session->userdata('loginid');
        $id=$this->session->userdata('loginid');


        if($role==1)
        {
        $nurselist['nurselist']=$this->home_model->select_with_join_serialize('a.first_name as nfmae,a.last_name as nlname,a.contact_no as ncontactno,a.profile_pic as nursepic,a.email as nursemail,a.charge as charge,
b.first_name as bfname,b.last_name as blname,b.contact_no as pcontactno,b.profile_pic as patient_pic,b.email as pmail,a1.price as a1price,a2.price as a2price,a3.price as a3price,
nurse_booking.fromtime,nurse_booking.totime,nurse_booking.adddate,nurse_booking.book_status,a.loginid as nurseid,
nurse_booking.notes,nurse_booking.time_in_hours','nurse_booking','users a','a.loginid=nurse_booking.nurse_id','users b','b.loginid=nurse_booking.patient_id','addons a1','a1.id=nurse_booking.addons_one','addons a2','a2.id=nurse_booking.addons_two','addons a3', 'a3.id=nurse_booking.addons_two','adddate','desc');
        }
        elseif($role==3)
        {
        $nurselist['nurselist']=$this->home_model->select_with_join_serialize_where('a.first_name as nfmae,a.last_name as nlname,a.contact_no as ncontactno,a.profile_pic as nursepic,a.email as nursemail,a.charge as charge,
b.first_name as bfname,b.last_name as blname,b.contact_no as pcontactno,b.profile_pic as patient_pic,b.email as pmail,a1.price as a1price,a2.price as a2price,a3.price as a3price,
nurse_booking.fromtime,nurse_booking.totime,nurse_booking.adddate,nurse_booking.book_status,a.loginid as nurseid,
nurse_booking.notes,nurse_booking.time_in_hours','nurse_booking',' users a ','a.loginid=nurse_booking.nurse_id','users b','b.loginid=nurse_booking.patient_id','addons a1','a1.id=nurse_booking.addons_one','addons a2','a2.id=nurse_booking.addons_two','addons a3', 'a3.id=nurse_booking.addons_two','adddate','desc','a.loginid='.$id);
        }
        else
        {
        $nurselist['nurselist']=$this->home_model->select_with_where('*','user_role=3','users');
        $nurselist['state']=$this->home_model->select_all('states');
        }
$this->load->view('index',$nurselist);

    }

    public function search()
    {
      $this->load->view('list.php');
    }

public function search_nurse($value='')
{
    $nurselist['loginid']=$this->session->userdata('loginid');
        $charge=$this->input->post('charge');
        $location=$this->input->post('location');
        $nurse_type=$this->input->post('nurse_type');
        $Experience=$this->input->post('Experience');
        $Availability=$this->input->post('Availability');
        $langauge=$this->input->post('langauge');
        //echo $langauge;

        //print_r($langauge);
$nurselist['state']=$this->home_model->select_all('states');
    if(($location==0)and($nurse_type==0)and($Experience==0)and($Availability==0)and($langauge==0))
        {
     $nurselist['nurselist']=$this->home_model->custom_search_charge($charge);
        }

elseif(($nurse_type==0)and($Experience==0)and($Availability==0)and($langauge==0))
        {
     $nurselist['nurselist']=$this->home_model->custom_search_charge_location($charge,$location);
        }
 elseif(($Experience==0)and($Availability==0)and($langauge==0))
        {
$nurselist['nurselist']=$this->home_model->custom_search_charge_location_nurse_type($charge,$location,$nurse_type);
        }
 elseif(($Availability==0)and($langauge==0))
        {
$nurselist['nurselist']=$this->home_model->custom_search_charge_location_nurse_type_exp($charge,$location,$nurse_type,$Experience);
        }
 elseif(($langauge==0))
        {
$nurselist['nurselist']=$this->home_model->custom_search_charge_location_nurse_type_exp_avail($charge,$location,$nurse_type,$Experience,$Availability);
        }

elseif(($charge==0)and($nurse_type==0))
        {
$nurselist['nurselist']=$this->home_model->custom_search_charge_nurse($charge,$nurse_type);
        }

 else
        {
$nurselist['nurselist']=$this->home_model->custom_search_charge_location_nurse_type_exp_avail_lan($charge,$location,$nurse_type,$Experience,$Availability,$langauge);
        }
        print_r($nurselist);
$this->load->view('index',$nurselist);

}


}

?>
