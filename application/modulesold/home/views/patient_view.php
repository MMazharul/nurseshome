<?php
    $id=$this->session->userdata('loginid');
    $sql ="select * from users  where loginid='".$id."'";
    $query = $this->db->query($sql);
    foreach ($query->result() as $row) 
        {
       
         $prfpic=$row->profile_pic;
         $st=$row->profile_update;
         $id=$row->loginid;
         $name=$row->first_name.' '.$row->last_name;
        //echo $upgrade_st;
        }
?>

<style type="text/css">
	h5{
		font-size: 13.28px;
	}
			.price{
				color: #f35e90;
		    font-size: 17px;
		    font-weight: 700;
		    margin-bottom: 0px;
			}
        	.c-concierge-card__pulse {
			    background: #01a400;
			    margin-right: 10px;
			}

			.c-concierge-card__pulse, .u-pulse {
			    margin: 4px 20px 0 10px;
			        margin-right: 8px;
			    width: 10px;
			    height: 10px;
			    border-radius: 50%;
			    background: #14bef0;
			    cursor: pointer;
			    -webkit-box-shadow: 0 0 0 rgba(40,190,240,.4);
			    box-shadow: 0 0 0 rgba(40,190,240,.4);
			    -webkit-animation: pulse 1.2s infinite;
			    animation: pulse 1.2s infinite;
			    display: inline-block;
			}

				.m-right{
					margin-right: 7px;
				}
				.u-green-text {
				    color: #01a400;
				}

				.modal_mou{
					text-decoration: underline!important;
					font-size: 11px!important;
				}


				/*search box css start here*/
				.search-sec{
				    padding: 2rem;
				}
				.search-slt{
				    display: block;
				    width: 100%;
				    font-size: 13px!important;
				    line-height: 1.5;
				    color: #55595c;
				    background-color: #fff;
				    background-image: none;
				    border: 1px solid #f3ecec;
				    height: calc(3rem + 2px) !important;
				    border-radius:0;
				}
				.wrn-btn{
				    width: 100%;
				    font-size: 16px;
				    font-weight: 400;
				    text-transform: capitalize;
				    height: calc(3rem + 2px) !important;
				    border-radius:0;
				}
				@media (min-width: 992px){
				    .search-sec{
				        position: relative;
				        top: -114px;
				        background: rgba(26, 70, 104, 0.51);
				    }
				}

				@media (max-width: 992px){
				    .search-sec{
				        background: #1A4668;
				    }
				}
				.select2-container--default .select2-search--inline .select2-search__field{height: 37px}
    </style>
    <div class="row">
<div class="col-md-12">
    <div class="card-body">
      <h5>Welcome : <span style="color: blue;font-weight: bold"><?php echo $name?></span></h5>
    </div>

 </div>
</div>
    <div class="row">
		<div class="col-md-12">
			<div class="card-header" style="background: #FFF;">
                <div class="card-title">My Monthly Summary</div>
           	</div>
           	<div class="" >
           		 <div class="row" style="margin-top: 15px">
							<div class="col-sm-12 col-lg-6 col-xl-3">
								<div class="card">
									<div class="row">
										<div class="col-4">
											<div class="feature">
												<div class="fa-stack fa-lg fa-2x icon bg-purple">
													<i class="fa fa-bed fa-stack-1x text-white"></i>
												</div>
											</div>
										</div>
										<div class="col-8">
											<div class="card-body p-3  d-flex">
												<div>
													<p class="text-muted mb-1">Total Bookings</p>
													<h2 class="mb-0 text-dark">0</h2>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div><!-- col end -->
							<div class="col-sm-12 col-lg-6 col-xl-3">
								<div class="card">
									<div class="row">
										<div class="col-4">
											<div class="feature">
												<div class="fa-stack fa-lg fa-2x icon bg-green">
													<i class="fa fa-user-md fa-stack-1x text-white"></i>
												</div>
											</div>
										</div>
										<div class="col-8">
											<div class="card-body p-3  d-flex">
												<div>
													<p class="text-muted mb-1">Last Booking</p>
													<h2 class="mb-0 text-dark">--/--/----</h2>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div><!-- col end -->
							<div class="col-sm-12 col-lg-6 col-xl-3">
								<div class="card">
									<div class="row">
										<div class="col-4">
											<div class="feature">
												<div class="fa-stack fa-lg fa-2x icon bg-orange">
													<i class="fa fa-hospital-o fa-stack-1x text-white"></i>
												</div>
											</div>
										</div>
										<div class="col-8">
											<div class="card-body p-3  d-flex">
												<div>
													<p class="text-muted mb-1">Amount Spend</p>
													<h2 class="mb-0 text-dark">0</h2>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div><!-- col end -->
							<div class="col-sm-12 col-lg-6 col-xl-3">
								<div class="card">
									<div class="row">
										<div class="col-4">
											<div class="feature">
												<div class="fa-stack fa-lg fa-2x icon bg-yellow">
													<i class="fa fa-flask fa-stack-1x text-white"></i>
												</div>
											</div>
										</div>
										<div class="col-8">
											<div class="card-body p-3  d-flex">
												<div>
													<p class="text-muted mb-1">Nurse Served</p>
													<h2 class="mb-0 text-dark">0</h2>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div><!-- col end -->
						</div>
           	</div>
					
		</div>
	</div>
	<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
					<form method="post" action="home/search_nurse">
				            <div class="row  m-1 mb-3">
				                <div class="col-lg-12">
				                    <div class="row">
				                    	
				                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 p-0">
				                     		<select class="form-control search-slt" name="charge" id="exampleFormControlSelect1">
				                                <option value="0">Charge</option>
				                                <option value="25">RM 25</option>
				                                <option value="50">RM 50</option>
				                                <option value="100">RM 100</option>
				                                <option value="150">RM 150</option>
				                                <option value="200">RM 200</option>
				                                <option value="250">RM 250</option>
				                                 <option value="300">RM 300</option>
				                            </select>
				                        </div>
				                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 p-0">
				                            <select class="form-control search-slt" name="location" id="exampleFormControlSelect1">
				                                <option value="0">Location--</option>
                               <?php

foreach ($state as $key => $value) 
{
    // if($value==$nurse_details[0]['location'])
    $r=$value['state_name'];
    ?>
    <option value='<?php echo $r?>'><?php echo $r?></option>
   
    <?php
}


                               ?> 
				                            </select>
				                        </div>
				                          <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 p-0">
				                           <select class="form-control search-slt" name="nurse_type"  id="exampleFormControlSelect1">
				                                <option value="0">Type of Nurse</option>
				                                <option value="1">Full Time</option>
				                                <option value="2">Part Time</option>
				                               
				                            </select>
				                        </div>
				                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 p-0">
				                           <select class="form-control search-slt" name="Experience" id="exampleFormControlSelect1">
				                                <option value="0">Select Experience</option>
				                                <option value="1">1 Year</option>
				                                <option value="2">2 Year</option>
				                                <option value="3">3 Year</option>
				                                <option value="4">4 Year</option>
				                                <option value="5">5 Year</option>
				                                <option value="6">6 Year</option>
				                            </select>
				                        </div>
				                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 p-0">
				                            <select class="form-control search-slt" name="Availability" id="exampleFormControlSelect1">
				                                <option value="0">Availability</option>
				                                <option value="sun">Sunday </option>
				                                <option value="mon">Monday</option>
				                                <option value="tue">Tuesday</option>
				                                <option value="wed">Wednesday</option>
				                                <option value="thu">Thursday</option>
				                                <option value="fri">Friday</option>
				                            </select>
				                        </div>
				                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 p-0">

				                        	<div class="form-group">
											
		<select style="background: #FFF !important;height: 37px;" class="form-control select2" name="langauge" data-placeholder="Language" multiple>
	            <option value="1">English</option>
				<option value="2">Bahasa Malaysia</option>
				<option value="3">Mandarin</option>
				<option value="4">
													Hindi
												</option>
												<option value="5">
													Cantonese
												</option>
												<option value="6">
													Tamil
												</option>
											</select>
										</div>


<!-- 
                                        <select multiple="multiple" id="language" class="multi-select ms-offscreen" name="langauge">

                                       <option value="1">English</option>
                                       <option value="6">Bahasa Malaysia</option>
                                       <option value="2">Mandarin</option>
                                       <option value="3">Hindi</option>
                                       <option value="4">Cantonese</option>
                                       <option value="5">Tamil</option>
        

                                                    </select> -->
				                        </div>
				                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 p-0">
				                            <button type="submit" class="btn btn-pink wrn-btn"><i class="fa fa-search" aria-hidden="true"></i></button>
				                        </div>
				                    </div>
				                </div>
				            </div>
						</form>
					
				
					<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
					<div class="card Relatedpost nested-media">
						<div class="card-header">
							<h4 class="card-title">Search for A nurse</h4>
						</div>
						<div class="card-body">
							
								<?php
foreach ($nurselist as $key => $value) 
{
	$country_code=$value['country_code'];
	$contact_no=$value['contact_no'];
	$first_name=$value['first_name'];
	$last_name=$value['last_name'];
	$email=$value['email'];
	$registered_date=$value['registered_date'];
	$charge=$value['charge'];
	$profile_pic=$value['profile_pic'];
	$nurse_id=$value['loginid'];
	$point=$value['point'];
	$total_served_patient=$value['total_served_patient']

	




								?>
								<div class="row border-bottom mb-4">
								<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 mb-4">
									<div class="media media-lg mt-0">
										<img class="mr-3" src="uploads/<?php echo $profile_pic?>" alt="Generic placeholder image">
										<div class="media-body">
											<h4 class="mt-0"><strong><?php echo $first_name?></strong></h4>
										<!-- 	<p>Practo Surgery Guide</p> -->

									
											<h5>
												<strong>Competencies</strong>
											</h5>
      <?php
    $sql ="select competencies.compet_ttile from competencies inner join 
competencies_details on competencies.compe_id=competencies_details.compe_id where  nurse_id='$nurse_id' order by rand() limit 3";
//echo $sql;
    $query = $this->db->query($sql);
    
              foreach ($query->result() as $row) 
                {

               $compet_ttile=$row->compet_ttile;
             
         				echo "<span class='badge badge-default mr-1 mb-1 mt-1'>$compet_ttile</span> ";
               }
               ?>
								
										</div>
									</div>
							    </div>
							    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mb-4">
									<div class="media media-lg mt-0">
										
									<div class="media-body">
											<p class="price"><span class="m-right">RM</span><?php echo $charge?></p>
											<p class="u-green-text"><i class="fa fa-check-square-o m-right"></i> <?php echo $total_served_patient?> patients served </p>
											<a href="patient-book-nurse/<?php echo $nurse_id?>/<?php echo $loginid?>" class="btn btn-primary"></i>Book Now</a>
									</div>

									</div>
							    </div>
</div>
							    <?php
							}
							    ?>
							
						</div>
					</div>
				    </div>
					</div>
				</div>
			
	</div>






