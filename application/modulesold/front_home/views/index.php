<?php $this->load->view('frontend/head_link');?>

<body>

	<div id="preloader" class="Fixed">
		<div data-loader="circle-side"></div>
	</div>
	<!-- /Preload-->

	<div id="page">
		       <?php $this->load->view('frontend/header');?>
					 <main>
		<div class="hero_home version_1">
			<div class="content">
				<h3>Find a Nurse!</h3>
				<p>
					NURSES AT HOME is the ONLY nursing provider in Malaysia
				</p>

			          <form action="<?=base_url()?>front_home/search_nurse" method="post" autocomplete="off">
            <div class="container" style="background: #FFF;padding: 7px 0px;border-radius: 10px;box-shadow: 2px 3px #cccaca94;">

                    <div class="row ml-2 mr-2">
                        <div class="col-lg-2 col-md-2 col-sm-12 p-0">
                            <select class="form-control search-slt" name="charge" id="exampleFormControlSelect1">
                                                <option value="0">Charge</option>
				                                <option value="25">RM 25</option>
				                                <option value="50">RM 50</option>
				                                <option value="100">RM 100</option>
				                                <option value="150">RM 150</option>
				                                <option value="200">RM 200</option>
				                                <option value="250">RM 250</option>
				                                 <option value="300">RM 300</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 p-0">
                            <select class="form-control search-slt" name="location" id="exampleFormControlSelect1">
				                                <option value="0">Location--</option>
                               <?php

foreach ($state as $key => $value) 
{
    // if($value==$nurse_details[0]['location'])
    $r=$value['state_name'];
    ?>
    <option value='<?php echo $r?>'><?php echo $r?></option>
   
    <?php
}


                               ?> 
                            </select>
                        </div>
                          <div class="col-lg-2 col-md-2 col-sm-12 p-0">
                             <select class="form-control search-slt" name="nurse_type" id="exampleFormControlSelect1">
                                <option value="0">Type of Nurse</option>
				                                <option value="1">Full Time</option>
				                                <option value="2">Part Time</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 p-0">
                             <select class="form-control search-slt" name="Experience" id="exampleFormControlSelect1">
   <option value="0">Select Experience</option>
				                                <option value="1">1 Year</option>
				                                <option value="2">2 Year</option>
				                                <option value="3">3 Year</option>
				                                <option value="4">4 Year</option>
				                                <option value="5">5 Year</option>
				                                <option value="6">6 Year</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 p-0">
                            <select class="form-control search-slt"  name="Availability" id="exampleFormControlSelect1">
                                <option value="0">Availability</option>
                                <option value="sun">Sunday </option>
                                <option value="mon">Monday</option>
                                <option value="tue">Tuesday</option>
                                <option value="wed">Wednesday</option>
                                <option value="thu">Thursday</option>
                                <option value="fri">Friday</option>
                            </select>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-12 p-0">
                            <select class="form-control select2"   name="langauge"  id="language" data-placeholder="Language">
                                <option value="1">English</option>
                                <option value="2">Bahasa Malaysia</option>
                                <option value="3">Mandarin</option>
                                <option value="4">Hindi</option>
                                <option value="5">Cantonese</option>
                                <option value="6">Tamil</option>
							</select>
                            </select>
                        </div> 
                        <div class="col-lg-1 col-md-1 col-sm-12 p-0">
                            <button type="submit" class="btn btn-danger wrn-btn">Search</button>
                        </div>
                    </div>

            </div>

				</form>
			</div>
		</div>
		<!-- /Hero -->

		<div class="container margin_120_95">
			<div class="main_title">
				<h2>Discover the <strong>online</strong> Booking !</h2>
				<p>Nurses At Home is a professionally operated and hospital-linked service provider . Nurses At Home has been providing safe and reliable nursing services since 2005. We are Malaysia’s Home Health Care Company of the year 2015 according to Frost and Sullivans Malaysia Excellence Awards 2015.</p>
			</div>
			<div class="row add_bottom_30">
				<div class="col-lg-4">
					<div class="box_feat" id="icon_1">
						<span></span>
						<h3>Find a Nurse</h3>
						<p>Find the best nurse using our best filter option.</p>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="box_feat" id="icon_2">
						<span></span>
						<h3>View profile</h3>
						<p>View the nurse profile, experiences and best rate offers</p>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="box_feat" id="icon_3">
						<h3>Book a visit</h3>
						<p>Book a visit and see the nurse on your door.</p>
					</div>
				</div>
			</div>
			<!-- /row -->
			<p class="text-center"><a href="front_home" class="btn_1 medium">Find Nurse</a></p>

		</div>
		<!-- /container -->




		<div id="app_section">
			<div class="container">
				<div class="row justify-content-around">
					<div class="col-md-5">
						<p><img src="front_assets/img/app_img.svg" alt="" class="img-fluid" width="500" height="433"></p>
					</div>
					<div class="col-md-6">
						<small>Application</small>
						<h3>Download <strong>Nurse@Home App</strong> Now!</h3>
						<p class="lead">Booking Nurse appointments is the easiest on Nurses at Home Apps. Just select your location, choose a Nurse and done! You can also read genuine feedback from patients before booking your appointment.</p>
						<div class="app_buttons wow" data-wow-offset="100">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 43.1 85.9" style="enable-background:new 0 0 43.1 85.9;" xml:space="preserve">
							<path stroke-linecap="round" stroke-linejoin="round" class="st0 draw-arrow" d="M11.3,2.5c-5.8,5-8.7,12.7-9,20.3s2,15.1,5.3,22c6.7,14,18,25.8,31.7,33.1" />
							<path stroke-linecap="round" stroke-linejoin="round" class="draw-arrow tail-1" d="M40.6,78.1C39,71.3,37.2,64.6,35.2,58" />
							<path stroke-linecap="round" stroke-linejoin="round" class="draw-arrow tail-2" d="M39.8,78.5c-7.2,1.7-14.3,3.3-21.5,4.9" />
						</svg>
							<a href="#0" class="fadeIn"><img src="front_assets/img/apple_app.png" alt="" width="150" height="50" data-retina="true"></a>
							<a href="#0" class="fadeIn"><img src="front_assets/img/google_play_app.png" alt="" width="150" height="50" data-retina="true"></a>
						</div>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /app_section -->
	</main>
	<!-- /main content -->

    <?php $this->load->view('frontend/footer');?>
    <?php $this->load->view('frontend/footer_link');?>


	</body>
</html>
