<?php $this->load->view('frontend/head_link');?>

<body>

	<div id="preloader" class="Fixed">
		<div data-loader="circle-side"></div>
	</div>
	<!-- /Preload-->

	<div id="page">
		       <?php $this->load->view('frontend/header');?>


           <main class="theia-exception">

 

   <div class="container margin_60_35">

     <div class="row">
       <div class="col-lg-12">
                         <form action="<?=base_url()?>front_home/search_nurse" method="post" autocomplete="off">
        

                    <div class="row" style="margin-left: 0px !important;margin-bottom: 10px;background: #FFF;padding: 5px;border-radius: 5px;margin-right: 0px !important;">
                        <div class="col-lg-1 col-md-1 col-sm-12 p-0">
                            <select class="form-control search-slt" name="charge" id="exampleFormControlSelect1">
                                                   <option value="0">Charge</option>
                                        <option value="25">RM 25</option>
                                        <option value="50">RM 50</option>
                                        <option value="100">RM 100</option>
                                        <option value="150">RM 150</option>
                                        <option value="200">RM 200</option>
                                        <option value="250">RM 250</option>
                                         <option value="300">RM 300</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 p-0">
                            <select class="form-control search-slt" name="location" id="exampleFormControlSelect1">
                                        <option value="0">Location--</option>
                               <?php

foreach ($state as $key => $value) 
{
    // if($value==$nurse_details[0]['location'])
    $r=$value['state_name'];
    ?>
    <option value='<?php echo $r?>'><?php echo $r?></option>
   
    <?php
}


                               ?> 
                            </select>
                        </div>
                          <div class="col-lg-2 col-md-2 col-sm-12 p-0">
                             <select class="form-control search-slt" name="nurse_type" id="exampleFormControlSelect1">
                                <option value="0">Type of Nurse</option>
                                        <option value="1">Full Time</option>
                                        <option value="2">Part Time</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 p-0">
                             <select class="form-control search-slt" name="Experience" id="exampleFormControlSelect1">
   <option value="0">Select Experience</option>
                                        <option value="1">1 Year</option>
                                        <option value="2">2 Year</option>
                                        <option value="3">3 Year</option>
                                        <option value="4">4 Year</option>
                                        <option value="5">5 Year</option>
                                        <option value="6">6 Year</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 p-0">
                            <select class="form-control search-slt"  name="Availability" id="exampleFormControlSelect1">
                              <option value="0">Availability</option>
                                        <option value="sun">Sunday </option>
                                        <option value="mon">Monday</option>
                                        <option value="tue">Tuesday</option>
                                        <option value="wed">Wednesday</option>
                                        <option value="thu">Thursday</option>
                                        <option value="fri">Friday</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 p-0">
              <select class="form-control select2"   name="langauge"  id="language" data-placeholder="Language">
              <option value="1">English</option>
              <option value="2">Bahasa Malaysia</option>
              <option value="3">Mandarin</option>
              <option value="4">Hindi</option>
              <option value="5">Cantonese</option>
              <option value="6">Tamil</option>
                      </select>
                           
                        </div> 
                        <div class="col-lg-1 col-md-1 col-sm-12 p-0">
                            <button type="submit" class="btn btn-danger wrn-btn">Search</button>
                        </div>
                    </div>

           

        </form>



             <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="card Relatedpost">
                 <div class="card-header">
                   <h4 class="card-title">Search Result</h4>
                 </div>
                 <div class="card-body">
                <?php
                //echo "this is ok....";
foreach ($nurselist as $key => $value) 
{
  $country_code=$value['country_code'];
  $contact_no=$value['contact_no'];
  $first_name=$value['first_name'];
  $last_name=$value['last_name'];
  $email=$value['email'];
  $registered_date=$value['registered_date'];
  $charge=$value['charge'];
  $profile_pic=$value['profile_pic'];
  $nurse_id=$value['loginid'];
  $point=$value['point'];
  $total_served_patient=$value['total_served_patient']

  




                ?>    
                   <div class="row">
<div class="strip_list wow fadeIn" style="width: 100%;">
            <span class="wish_bt" style="float: right;font-weight: 700;color: #e8598b;font-size: 20px;"><span class="m-right mr-2">RM</span><?php echo $charge?></span>
            <figure>
             <img src="<?=base_url()?>uploads/<?php echo $profile_pic?>" alt="<?php echo $first_name?><?php echo $last_name?>">
            </figure>
            <h3><?php echo $first_name?> <?php echo $last_name?></h3>
             <small>Competencies</small>
            <p><?php
$sql ="select competencies.compet_ttile from competencies inner join 
competencies_details on competencies.compe_id=competencies_details.compe_id where  nurse_id='$nurse_id' order by rand() limit 1 ";
//echo $sql;
$query = $this->db->query($sql);
    foreach ($query->result() as $row) 
      {
     $compet_ttile=$row->compet_ttile;
echo " <span class='badge badge-default mr-1 mb-1 mt-1'>$compet_ttile</span> ";
     }
?></p>
            <span class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><small>(145)</small></span>
            <ul>
             
             
              <li><a href="<?=base_url('index.php/auth')?>">Book now</a></li>
            </ul>
          </div>
                   </div>
                   <?php

}
                   ?>
                 </div>
               </div>
                 </div>
               </div>
       

<!--          <nav aria-label="" class="add_top_20">
           <ul class="pagination pagination-sm">
             <li class="page-item disabled">
               <a class="page-link" href="#" tabindex="-1">Previous</a>
             </li>
             <li class="page-item active"><a class="page-link" href="#">1</a></li>
             <li class="page-item"><a class="page-link" href="#">2</a></li>
             <li class="page-item"><a class="page-link" href="#">3</a></li>
             <li class="page-item">
               <a class="page-link" href="#">Next</a>
             </li>
           </ul>
         </nav> -->
         <!-- /pagination -->
       </div>
     

     </div>
     <!-- /row -->
   </div>
   <!-- /container -->
 </main>
 <!-- /main -->


               <?php $this->load->view('frontend/footer');?>
               <?php $this->load->view('frontend/footer_link');?>


           	</body>
           </html>
