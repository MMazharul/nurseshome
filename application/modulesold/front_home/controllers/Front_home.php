<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Front_home extends MX_Controller {

    //public $counter=0;
    function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->model('front_home_model');
    }

    public function index()
    {

      $nurselist['state']=$this->front_home_model->select_all('states');
      $this->load->view('index',$nurselist);

    }
public function search_nurse($value='')
{
   
        $charge=$this->input->post('charge');
        //echo $charge;
        $location=$this->input->post('location');
        $nurse_type=$this->input->post('nurse_type');
        $Experience=$this->input->post('Experience');
        $Availability=$this->input->post('Availability');
        $langauge=$this->input->post('langauge');
        //echo $langauge;

        //print_r($langauge);
$nurselist['state']=$this->front_home_model->select_all('states');

    if(($location==0)and($nurse_type==0)and($Experience==0)and($Availability==0)and($langauge==0))
        {
     $nurselist['nurselist']=$this->front_home_model->custom_search_charge($charge);
        }

elseif(($nurse_type==0)and($Experience==0)and($Availability==0)and($langauge==0))
        {
     $nurselist['nurselist']=$this->front_home_model->custom_search_charge_location($charge,$location);
        }
 elseif(($Experience==0)and($Availability==0)and($langauge==0))
        {
$nurselist['nurselist']=$this->front_home_model->custom_search_charge_location_nurse_type($charge,$location,$nurse_type);
        }
 elseif(($Availability==0)and($langauge==0))
        {
$nurselist['nurselist']=$this->front_home_model->custom_search_charge_location_nurse_type_exp($charge,$location,$nurse_type,$Experience);
        }
 elseif(($langauge==0))
        {
$nurselist['nurselist']=$this->front_home_model->custom_search_charge_location_nurse_type_exp_avail($charge,$location,$nurse_type,$Experience,$Availability);
        }

elseif(($charge==0)and($nurse_type==0))
        {
$nurselist['nurselist']=$this->front_home_model->custom_search_charge_nurse($charge,$nurse_type);
        }

 elseif(($location!=0)and($nurse_type!=0)and($Experience!=0)and($Availability!=0)and($langauge!=0)and($charge!=0))
        {
$nurselist['nurselist']=$this->front_home_model->custom_search_charge_location_nurse_type_exp_avail_lan($charge,$location,$nurse_type,$Experience,$Availability,$langauge);
        }
        else
        {
 $nurselist['nurselist']=$this->front_home_model->select_with_where('*','user_role=3','users');      
        }

$this->load->view('list',$nurselist);

}

}

?>
