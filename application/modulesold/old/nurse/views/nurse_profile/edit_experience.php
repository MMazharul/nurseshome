<?php $this->load->view('backend/head_link');?>

<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
    <img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
    <div class="page-main">
        <!--app-header-->
        <?php $this->load->view('backend/header');?>
        <!-- app-content-->
        <div class="container content-patient">
            <div class="side-app">
                <!-- page-header -->
                <!-- <div class="page-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
                    </ol>
                    <div class="ml-auto">
                        <div class="input-group">
                            <a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
                                <span>
                                    <i class="fa fa-calendar"></i> Events Settings
                                </span>
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                                <span>
                                    <i class="fa fa-star"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div> -->
                <!-- End page-header -->

                <div class="row">
                    <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-10">
                            <div class="card-title"><?=$form_title?></div>
                            </div>
                            <div class="col-md-2">
                         
                            </div>
                        </div>
                        <div class="card-body">
                                <form action="nurse/update_experience_post" method="post">
       <input type="hidden" name="exper_det_id" value="<?php echo $exper_det_id ;?>">
                            <div class="row">
                            <?php if($this->session->flashdata('msg')){ ?>
                                <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <div class="alert-message">
                                        <span><?=$this->session->flashdata('msg');?></span>
                                    </div>
                                </div>
                            <?php } ?> 
                            <div id="alert_pass"></div>
                            <div class="col-md-12">
                                <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
                            </div>
                            </div>
                       
                            
                          
                            <fieldset class="nursing_experiece">
                            <div class="row">
                            <div class="col-md-12">
                                <h4 class="" style="line-height:2.7em">
                              
                               <a href='nurse-profile'> <span class="float-right btn btn-primary">Back to Profile</span></a>
                            </h4>
                            </div>
                            </div>
                        
                            <div class="main_exp">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="organisation">Organisation</label>
                                            </div>
                                            <div class="col-md-9">
                     <input type="text" class="form-control" id="organisation" name="organisation" placeholder="Organisation" value="<?php echo $organisation?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="start_period">Start Date</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input class="form-control fc-datepicker hasDatepicker" placeholder="MM/DD/YYYY" value="<?php echo $start_date?>" type="text" id="dp1573642265589">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="start_period">End Date</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input class="form-control fc-datepicker hasDatepicker" placeholder="MM/DD/YYYY"  value="<?php echo $end_date?>" type="text" id="dp1573642265590">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="title">Title</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="<?php echo $title?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="desc_of_duties">Description of duties</label>
                                            </div>
                                            <div class="col-md-9">
                                            <textarea class="form-control" id="desc_of_duties" name="desc_of_duties" cols="30" rows="1"><?php echo $duty_description?>
                                            </textarea>
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="references">References</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input type="text" class="form-control" id="references" name="references"  value="<?php echo $ref?>"  placeholder="References" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="desc_of_duties">Trainings Attended</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input type="text" class="form-control" id="training_attended" name="training_attended" cols="30" rows="1" value="<?php echo $training_attended?>" placeholder="Trainings Attended">
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div>

                            <div id="additional_exp">
                                    
                            </div>

                        
                            </fieldset>
                                    

                            <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                <div class="">
                                    <button class="btn btn-primary" type="submit" name="save" id="">Submit</button>
                                </div>
                            </div>
                                    
                                    <!-- <div class="col-md-12">
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a large text here ..."></textarea>
                                    </div> -->
                            
                            </form>
                       
                        <!-- table-wrapper -->
                    </div>
                    <!-- section-wrapper -->
                    </div>
                </div>

            </div><!--End side app-->

            <!-- Right-sidebar-->
            <?php $this->load->view('backend/right_sidebar');?>
            <!-- End Rightsidebar-->

            <?php $this->load->view('backend/footer');?>
        </div>
        <!-- End app-content-->
    </div>
</div>

<template>
    <div class="row-target">
                            <h4 class="" style="line-height:2.7em">
                                Nursing Experience
                                <span class="add_exp float-right btn btn-primary">Add+</span>
                                <span  data-row-target='.row-target' class="remove_exp float-right btn btn-danger">Remove-</span>
                            </h4>  
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="organisation">Organisation</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="" name="organisation[]" placeholder="Organisation" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="start_period">Start Date</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input class="form-control fc-datepicker hasDatepicker" placeholder="MM/DD/YYYY" type="text" id="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="start_period">End Date</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input class="form-control fc-datepicker hasDatepicker" placeholder="MM/DD/YYYY" type="text" id="dp1573642265590">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="title">Title</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="desc_of_duties">Description of duties</label>
                                            </div>
                                            <div class="col-md-9">
                                            <textarea class="form-control" id="desc_of_duties" name="desc_of_duties" cols="30" rows="1">Description of duties
                                            </textarea>
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="references">References</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input type="text" class="form-control" id="references" name="references" placeholder="References" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 text-left">
                                                <label for="desc_of_duties">Trainings Attended</label>
                                            </div>
                                            <div class="col-md-9">
                                            <input type="text" class="form-control" id="training_attended" name="training_attended" cols="30" rows="1" placeholder="Trainings Attended">
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
    </div>
</template>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>


<script>
    let template = document.querySelectorAll('template')[0];
    let additional_exp = document.getElementById('additional_exp');

    removeEvent(document, 'click', '.remove_exp',removeItem);
    
    addEvent(document, 'click', '.add_exp', addItem);
    
    let count = 1;

    function addEvent(element, event, selector,callback){
       element.addEventListener(event, e =>{
        if (e.target.matches(selector)) {
            if (count < 10) {
               count++; 
               addItem(count);  
            }
            else{
                alert('Sorry can\'t add more!');
            } 
            }
       })
    }

    function removeEvent(element, event, selector, callback){
      element.addEventListener(event, e => {
        if (e.target.matches(selector)) {
          removeItem(e)
        }
      })
    }

    function removeItem(params) {
      let buttonClicked = params.target
      buttonClicked.closest(buttonClicked.dataset.rowTarget).remove()
      count--; 
    }
    
    function addItem(count){
        additional_exp.append(template.content.cloneNode(true));  
    } 
</script>
</body>
</html>