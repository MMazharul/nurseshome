<?php $this->load->view('backend/head_link');?>

<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
    <img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
    <div class="page-main">
        <!--app-header-->
        <?php $this->load->view('backend/header');?>
        <!-- app-content-->
        <div class="container content-patient">
            <div class="side-app">
                <!-- page-header -->
                <!-- <div class="page-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
                    </ol>
                    <div class="ml-auto">
                        <div class="input-group">
                            <a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
                                <span>
                                    <i class="fa fa-calendar"></i> Events Settings
                                </span>
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                                <span>
                                    <i class="fa fa-star"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div> -->
                <!-- End page-header -->

                <div class="row">
                    <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-10">
                            <div class="card-title"><?=$form_title?></div>
                            </div>
                            
                        </div>
                        <div class="card-body">
                            
                            <div class="row">
                            <?php if($this->session->flashdata('msg')){ ?>
                                <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <div class="alert-message success">
                                        <span><?=$this->session->flashdata('msg');?></span>
                                    </div>
                                </div>
                            <?php } ?> 
                            <div id="alert_pass"></div>
                            <div class="col-md-12">
                                <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
                            </div>
                            </div>
                            <form action="nurse/add_booking_charge" method="post">
                            
                            <input type="hidden" class="form-control" id="id" name="nurse_id" placeholder="" value="<?php echo $nurse_id?>">
                          
                            <fieldset class="biodata">
                            <div class="row">
                                <!-- <div class="col-md-12"><h4 class="" style="line-height:2.7em">Notes</h4></div> -->
                                </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="id">Nurse Name</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" " name="name" placeholder="" value="<?php echo $name?>">
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                                <div class="col-md-3 text-left">
                                                    <label for="age">Nurse Charge</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="number" class="form-control" id="age" name="nurse_charge" min="10" placeholder="">
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                         
                            </fieldset>
                                    

                            <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                <div class="">
                                    <button class="btn btn-primary" type="submit" name="save" id="">Submit</button>
                                </div>
                            </div>
                                    
                             
                            
                            </form>
                       
                        <!-- table-wrapper -->
                    </div>
                    <!-- section-wrapper -->
                    </div>
                </div>

            </div><!--End side app-->

            <!-- Right-sidebar-->
            <?php $this->load->view('backend/right_sidebar');?>
            <!-- End Rightsidebar-->

            <?php $this->load->view('backend/footer');?>
        </div>
        <!-- End app-content-->
    </div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>

</body>
</html>