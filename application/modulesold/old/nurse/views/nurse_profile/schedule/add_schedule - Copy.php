<?php $this->load->view('backend/head_link');?>

<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
    <img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->
<style>
td{
    border-bottom:1px solid #ccc;
    /* padding-left:10px; */
    padding: 8px 20px 0 15px;
}
td:last-child{
    padding: 0 ;
}
td:last-child button{
    line-height: 1;
}
.input-group{
    /* display:inline; */
}
/*.select2-selection .select2-selection--single{
    widows: 100%;
}*/
/*@media only screen and (max-width: 600px) {
  #day, #timerange{
      width: 0;
  }
}*/
</style>
<div class="page">
    <div class="page-main">
        <!--app-header-->
        <?php $this->load->view('backend/header');?>
        <!-- app-content-->
        <div class="container content-patient">
            <div class="side-app">
                <!-- page-header -->
                <!-- <div class="page-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
                    </ol>
                    <div class="ml-auto">
                        <div class="input-group">
                            <a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
                                <span>
                                    <i class="fa fa-calendar"></i> Events Settings
                                </span>
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                                <span>
                                    <i class="fa fa-star"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div> -->
                <!-- End page-header -->
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-10">
                            <div class="card-title"><?=$form_title?></div>
                            </div>
                            <div class="col-md-2">
                          
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                            <?php if($this->session->flashdata('msg')){ ?>
                                <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <div class="alert-message">
                                        <span><?=$this->session->flashdata('msg');?></span>
                                    </div>
                                </div>
                            <?php } ?> 
                            <div id="alert_pass"></div>
                            <div class="col-md-12">
                                <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
                            </div>
                            </div>
                            <div class="card-body">
                            <div class="card card-profile  overflow-hidden bg-light">
                            <div class="nav-wrapper p-0">
                                <ul class="nav nav-pills dragscroll horizontal nav-fill flex-row" id="tabs-icons-text" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0 <?=$this->session->flashdata('active')== '' ? 'active show' : '' ?>" id="weekly" data-toggle="tab" href="#tabs-weekly" role="tab" aria-controls="weekly" aria-selected="true"><i class="fa fa-home mr-2"></i>Weekly Shedule</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0 <?=$this->session->flashdata('active')=='special_days' ? 'active show' : ''?>" id="special" data-toggle="tab" href="#tabs-special" role="tab" aria-controls="special" aria-selected="false"><i class="fa fa-user mr-2"></i>Special Days</a>
                                    </li>
                                    </ul>
                            </div>
                        </div>
                        </div>

                        <div class="tab-content" id="myTabContent">

                            <div class="tab-pane fade <?=$this->session->flashdata('active')== '' ? 'active show' : ''?>" id="tabs-weekly" role="tabpanel" aria-labelledby="weekly">
                            <div class="row">
                            <div class="col-md-12">
                            <form autocomplete="off" action="nurse/nurse_weekly_schedule_add/<?=$nurse_details[0]['loginid']?>" method="post" id="schedule_form">                           
                            
                            <fieldset class="add_shedule">

                            <table style="table-layout:fixed"  cellpadding=8>
                                <tbody>
                                    <thead>
                                        <th id="day" width="50%" style="text-align: left;">Day</th>
                                        <th id="timerange"  width="40%">Time-Range</th>
                                        <th>Add</th>
                                    </thead>
                                
                                <tr>
                                    <td colspan=1>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input day" name="day[]" value="sunday">
                                                    <h4 class="custom-control-label">Sunday</h4>
                                                </label>
                                            </div>
                                        </div>    
                                        </div>
                                        </td>
                                        <td>
                                        <div class="form-group ">
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                               <!-- <select class="form-control select2"  name="sun_start_time[]" id="sunday_start"  required="">
                                                 <?php //echo $options?>
                                               </select> -->
                                                <input class="form-control timepicker start" id="sunday_start"  placeholder="Start-time" name="sun_start_time[]" type="text">
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                        <div class="input-group">
                                        <!-- <select class="form-control select2"  name="sun_end_time[]" id="sunday_end"  required="">
                                        <?php // echo $options?>
                                        </select> -->
                                       <input class="form-control timepicker end" id="sunday_end" placeholder="End-time" name="sun_end_time[]" type="text">
                                        </div>
                                        </div>
                                        </div>
                                        <div class="custom_time_range">

                                        </div>
                                        
                                        </div>
                                    </td>
                                    <td>
                                        <span class="addCustomRange btn btn-primary">+</span>
                                        <div class="customBtnDelete"></div>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan=1>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input day"  name="day[]" value="monday">
                                                    <h4 class="custom-control-label">Monday</h4>
                                                </label>
                                            </div>
                                        </div>    
                                        </div>
                                        </td>
                                        <td>
                                        <div class="form-group ">
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <!-- <select class="form-control timepicker" autocomplete="off"  name="mon_start_time[]" id="monday_end"  required="">
                                                 <?php //echo $options?>
                                               </select> -->
                                                <input class="form-control timepicker" id="monday_start"  placeholder="Start-time" name="mon_start_time[]"  type="text">
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                        <div class="input-group">
                                           <!--  <select class="form-control timepicker" autocomplete="off"  name="mon_end_time[]" id="monday_end"  required="">
                                                 <?php //echo $options?>
                                               </select> -->
                                            <input class="form-control timepicker" id="monday_end" placeholder="End-time" name="mon_end_time[]" type="text">
                                        </div>
                                        </div>
                                        </div>
                                        <div class="custom_time_range">
                                        </div>
                                        
                                        </div>
                                    </td>
                                    <td>
                                    <span class="addCustomRange btn btn-primary">+</span>
                                    <div class="customBtnDelete"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=1>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input day" name="day[]" value="tuesday">
                                                    <h4 class="custom-control-label">Tuesday</h4>
                                                </label>
                                            </div>
                                        </div>    
                                        </div>
                                        </td>
                                        <td>
                                        <div class="form-group ">
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <!-- <select class="form-control select2"  name="tue_start_time[]" id=""  required="">
                                                 <?php // echo $options?>
                                                </select> -->
                                                <input class="form-control timepicker" autocomplete="off" id="tuesday_start"  placeholder="Start-time" name="tue_start_time[]" type="text">
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                        <div class="input-group">
                                        <!--     <select class="form-control select2"  name="tue_end_time[]" id=""  required="">
                                                 <?php //echo $options?>
                                               </select> -->
                                            <input class="form-control timepicker" autocomplete="off" id="tuesday_end" placeholder="End-time" name="tue_end_time[]"  type="text">
                                        </div>
                                        </div>
                                        </div>
                                        <div class="custom_time_range">
                                        </div>
                                        
                                        </div>
                                    </td>
                                    <td>
                                    <span class="addCustomRange btn btn-primary">+</span>
                                    <div class="customBtnDelete"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=1>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input day" name="day[]" value="wednesday">
                                                    <h4 class="custom-control-label">Wednesday</h4>
                                                </label>
                                            </div>
                                        </div>    
                                        </div>
                                        </td>
                                        <td>
                                        <div class="form-group ">
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <!-- <select class="form-control select2"  name="wed_start_time[]" id=""  required="">
                                                 <?php //echo $options?>
                                               </select> -->
                                                <input class="form-control timepicker" autocomplete="off" id="wednesday_start"  placeholder="Start-time" name="wed_start_time[]" type="text">
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                        <div class="input-group">
                                          <!--   <select class="form-control select2"  name="wed_end_time[]" id=""  required="">
                                                 <?php // echo $options?>
                                               </select> -->
                                            <input class="form-control timepicker" autocomplete="off" id="wednesday_end" placeholder="End-time" name="wed_end_time[]" type="text">
                                        </div>
                                        </div>
                                        </div>
                                        <div class="custom_time_range">
                                        </div>
                                        
                                        </div>
                                    </td>
                                    <td>
                                    <span class="addCustomRange btn btn-primary">+</span>
                                    <div class="customBtnDelete"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=1>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input day" name="day[]" id="" value="thursday">
                                                    <h4 class="custom-control-label">Thursday</h4>
                                                </label>
                                            </div>
                                        </div>    
                                        </div>
                                        </td>
                                        <td>
                                        <div class="form-group ">
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                              <!--   <select class="form-control select2"  name="thu_start_time[]" id=""  required="">
                                                 <?php //echo $options?>
                                               </select> -->
                                                <input class="form-control timepicker"  id="thursday_start"  placeholder="Start-time" name="thu_start_time[]" type="text">
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                        <div class="input-group">
                                           <!--  <select class="form-control select2"  name="thu_end_time[]" id=""  required="">
                                                 <?php// echo $options?>
                                               </select> -->
                                            <input class="form-control timepicker"  id="thursday_end"  placeholder="End-time" name="thu_end_time[]" type="text">
                                        </div>
                                        </div>
                                        </div>
                                        <div class="custom_time_range">
                                        </div>
                                        
                                        </div>
                                    </td>
                                    <td>
                                    <span class="addCustomRange btn btn-primary">+</span>
                                    <div class="customBtnDelete"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=1>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input day" name="day[]" value="friday">
                                                    <h4 class="custom-control-label">Friday</h4>
                                                </label>
                                            </div>
                                        </div>    
                                        </div>
                                        </td>
                                        <td>
                                        <div class="form-group ">
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <!-- <select class="form-control select2"  name="fri_start_time[]" id=""  required="">
                                                 <?php //echo $options?>
                                               </select> -->
                                                <input class="form-control timepicker"  id="friday_start" placeholder="Start-time" name="fri_start_time[]"  type="text">
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                        <div class="input-group">
                                            <!-- select class="form-control select2"  name="fri_end_time[]" id=""  required="">
                                                 <?php // echo $options?>
                                               </select> -->
                                            <input class="form-control timepicker" id="friday_end"  placeholder="End-time" name="fri_end_time[]" type="text">
                                        </div>
                                        </div>
                                        </div>
                                        <div class="custom_time_range">
                                        </div>
                                        
                                        </div>
                                    </td>
                                    <td>
                                    <span class="addCustomRange btn btn-primary">+</span>
                                    <div class="customBtnDelete"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=1>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input day" name="day[]" value="saturday">
                                                    <h4 class="custom-control-label">Saturday</h4>
                                                </label>
                                            </div>
                                        </div>    
                                        </div>
                                        </td>
                                        <td>
                                        <div class="form-group ">
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <!-- <select class="form-control select2"  name="sat_start_time[]" id=""  required="">
                                                 <?php //echo $options?>
                                               </select> -->
                                                <input class="form-control timepicker"  id="saturday_start"  placeholder="Start-time" name="sat_start_time[]" type="text">
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                        <div class="input-group">
                                            <!-- select class="form-control select2"  name="sat_end_time[]" id=""  required="">
                                                 <?php //echo $options ?>
                                               </select> -->
                                            <input class="form-control timepicker" id="saturday_end"  placeholder="End-time" name="sat_end_time[]" type="text">
                                        </div>
                                        </div>
                                        </div>
                                        <div class="custom_time_range"></div>
                                        </div>
                                    </td>
                                    <td>
                                    <span class="addCustomRange btn btn-primary">+</span>
                                    <div class="customBtnDelete"></div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                           
                            </fieldset>

                                    

                            <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2 mr-5">
                                <div class="">
                                    <button class="btn btn-primary" type="submit" name="save" id="submit">Add Daily Schedule</button>
                                </div>
                            </div>
                            
                            </form>
<div class="card-body">
                                        <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered text-nowrap w-100">
                <thead>
                <tr>
                    <th class="wd-15p">#</th>
                    <th>Day</th>
                    <th class="wd-15p">Schedule</th>                    
                    <th>Action</th>
   
                   
                </tr>
                </thead>
                <tbody>
                    <?php
                    $sl=0;
foreach ($schedule_day as $key => $value) 
{
    $scheudle_id=$value['scheudle_id'];
    $scday=$value['schedule_day'];
    $add_date=$value['add_date'];
     $nurse_id=$value['nurse_id'];
     $day_title_sum=$value['day_title_sum'];
     $sday=$value['sday'];
     $sl+=1;
     if($sday==0)
     {
        $t="";
        $tm="";
     }
     else
     {
        $t="nurse/schedule_day_title_delete/$scheudle_id/$nurse_id"; 

       $tm="<td align='center' valign='middle'><a href='$t'>Delete</td>";
     }
    
     //echo $t;

     ?>
                     <tr>
                    <td class="wd-15p" align="center"><?php echo $sl?></td>
                    <td align="center"><?php echo $day_title_sum?></td>
                    <td class="wd-15p" align="center">
                        <table class="table table-striped table-bordered text-nowrap w-100">
      <?php
    $sql ="select * from schedule_day_time where day_title='$scday' and nurse_id='$nurse_id' and sch_id='$scheudle_id'";
//echo $sql;
    $query = $this->db->query($sql);
    
              foreach ($query->result() as $row) 
                {

               $start_time=$row->start_time;
               $end_time=$row->end_time;
               $scid=$row->scid;
               echo"<tr>
                         <td align='center'>$start_time</td>
                         <td align='center'>$end_time</td>
                         <td align='center'><a href='nurse/schedule_day_delete/$scid/$nurse_id'>Delete</a></td>
                    </tr>";

               }
      ?>                  
</table>
                    </td>
                   
              <?php

    echo $tm;
              ?>
   
                   
                </tr>
     <?php
}

                    ?>

                
                </tbody>
            </table>
            </div>
        </div>
                            </div>
                            </div>
                            </div>

                            <div class="tab-pane fade <?=$this->session->flashdata('active')== '' ? 'active show' : ''?>" id="tabs-special" role="tabpanel" aria-labelledby="special">
                            <div class="row">
                            <div class="col-md-12">
                            <form action="nurse/nurse_special_schedule_add/<?=$nurse_details[0]['loginid']?>" method="post" autocomplete="off">
                            
                            
                            <fieldset class="">

                            <table style="table-layout:fixed"  cellpadding=8>
                                <tbody>
                                    <thead>
                                        <th id="day" width="50%">Day</th>
                                        <th id="timerange" width="40%">Time-Range</th>
                                    </thead>
                                
                                <tr>
                                    <td colspan=1>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="custom-control custom-checkbox">
                               <input class="form-control" data-time="date" placeholder="End-time" name="sdate" type="text">
                                                </label>
                                            </div>
                                        </div>    
                                        </div>
                                        </td>
                                        <td>
                                        <div class="form-group ">
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <input class="form-control timepicker start"   placeholder="Start-time" name="start_time" type="text">
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                        <div class="input-group">
                                            <input class="form-control timepicker end"  placeholder="End-time" name="end_time" type="text">
                                        </div>
                                        </div>
                                        </div>
                                        <div class="custom_time_range">

                                        </div>
                                        
                                        </div>
                                    </td>
                                    <!-- <td>
                                        <span class="addCustomRange btn btn-primary">+</span>
                                        <div class="customBtnDelete"></div>
                                    </td> -->
                                </tr>

                                
                                </tbody>
                            </table>
                           
                            </fieldset>

                                    

                            <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                <div class="">
                                    <button class="btn btn-primary" type="submit" name="save" id="">Submit</button>
                                </div>
                            </div>
                            
                            </form>
                            </div>
                            </div>
                            </div>
                            <div class="card-body">
                                        <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered text-nowrap w-100">
                <thead>
                <tr>
                    <th class="wd-15p" align="center">#</th>
                    <th align="center">Date</th>
                    <th class="wd-15p" align="center">Schedule</th>                    
                    <th align="center">Action</th>
   
                   
                </tr>
                </thead>
                <tbody>
                    <?php

                    $sl=0;
foreach ($special_schedule as $key => $value) 
{
    $sdate=$value['sdate'];
    $start_time=$value['start_time'];
    $end_time=$value['end_time'];
    $nurse_id=$value['nurse_id'];
    $special_sc_id=$value['special_sc_id'];
    $sl+=1;
    $t="nurse/special_schedule_delete/$special_sc_id/$nurse_id";
     //echo $t;

     ?>
                     <tr>
                    <td class="wd-15p" align="center"><?php echo $sl?></td>
                    <td align="center"><?php echo $sdate?></td>
                    <td class="wd-15p" align="center">
<?php echo $start_time?> to <?php echo $end_time?>
                    </td>
                   
               <td align="center" valign="middle"><a href='<?php echo $t ?>'>Delete</td>
   
                   
                </tr>
     <?php
}

                    ?>

                
                </tbody>
            </table>
            </div>
        </div>
               
                        </div>
                       
                        <!-- table-wrapper -->
                    </div>
                    <!-- section-wrapper -->
                    </div>
                </div>

            </div><!--End side app-->

            <!-- Right-sidebar-->
            <?php $this->load->view('backend/right_sidebar');?>
            <!-- End Rightsidebar-->

            <?php $this->load->view('backend/footer');?>
        </div>
        <!-- End app-content-->
    </div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>






<script>
 $("[data-time='range']").Zebra_DatePicker({
    format: 'h:i a'
 });

 $("[data-time='date']").Zebra_DatePicker();
 
 const addCustomRange = document.querySelectorAll('.addCustomRange');
 const custom_time_range = document.querySelectorAll('.custom_time_range');
 const customBtnDelete = document.querySelectorAll('.customBtnDelete');
 const customTime = document.querySelectorAll("[data-time='range']");
 const dltBtn = `<span class="deletebtn btn btn-danger pt-2">x</span>`;
    

 for (let i = 0; i < addCustomRange.length; i++) {
    addCustomRange[i].onclick = () => {

        custom_time_range[i].innerHTML = `<div class="row pt-2">
                    <div class="col-sm-6">
                        <div class="input-group">
                            <input class="form-control timepicker start" id=""  placeholder="Start-time" name="start_time[]" type="text">
                        </div>
                    </div>  
                    <div class="col-md-6">
                        <div class="input-group">
                       <input class="form-control timepicker start" id=""  placeholder="Start-time" name="start_time[]" type="text">
                        </div>
                    </div>
                </div>
                `; 
               
        customBtnDelete[i].innerHTML = dltBtn;
        // $("[data-time='range']").Zebra_DatePicker({ format: 'h:i a' });
        // $('.select2').select2();
        $('.timepicker').timepicker({
            timeFormat: 'HH:mm',
            interval: 15,
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });

    }
     
 }
 for (let i = 0; i < customBtnDelete.length; i++) {
    customBtnDelete[i].onclick = () => {
        custom_time_range[i].innerHTML = ''; 
        customBtnDelete[i].innerHTML = '';
        $("[data-time='range']").Zebra_DatePicker({ format: 'h:i a' });
        
    }  
 }

</script>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>


<script>
const form = document.getElementById('schedule_form'); 
const submit = document.getElementById('submit'); 

form.addEventListener('submit', event => {
const is_checked = document.querySelectorAll('.day:checked');
    if (is_checked.length == 0) {
        event.preventDefault();
        swal('You have not selected any day');
    }
    else{
        is_checked.forEach((val) => {
            let flag = check(val, event);
            if (flag == false) {
                event.preventDefault();
            }
        });
    }   
    
});
</script>

<script type="text/javascript">

let getTime = (m) => {
    return m.minutes() + m.hours() * 60;
}

$('.timepicker').timepicker({
    timeFormat: 'HH:mm',
    interval: 15,
    dynamic: false,
    dropdown: true,
    scrollbar: true
});

function check(range, e){

    let timeFrom = $('#'+range.value+'_start').val(),
    timeTo = $('#'+range.value+'_end').val();

    if (!timeFrom || !timeTo) {
        swal('Select time for '+range.value);
        return false
    }
    timeFrom = moment(timeFrom, 'hh:mm a');
    timeTo = moment(timeTo, 'hh:mm a');

    if (getTime(timeFrom) >= getTime(timeTo)) {
        swal(range.value+' Start time must not be greater than or equal to '+range.value+' End Field');
        return false
    } 
    else
        return true;
    // else {
    //     alert(start +'Time is valid');
    // }
}



</script>

</body>
</html>