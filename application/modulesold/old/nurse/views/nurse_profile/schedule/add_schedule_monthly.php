<?php $this->load->view('backend/head_link');?>

    <script>
    // $(document).ready(function(){
        //var calendar = $('#calendar1').fullCalendar({
        //    editable:true,
        //    header:{
        //        left:'prev,next today',
        //        center:'title',
        //        right:'month,agendaWeek,agendaDay'
        //    },
        //    events:"<?php //echo base_url(); ?>//nurse/load",
        //    selectable:true,
        //    selectHelper:true,
        //    select:function(start, end, allDay)
        //    {
        //        var title = prompt("Enter Event Title");
        //        if(title)
        //        {
        //            var start = $.fullCalendar.formatDate(start,"Y-MM-DD HH:mm:ss");
        //            var end = $.fullCalendar.formatDate(end,"Y-MM-DD HH:mm:ss");
        //            $.ajax({
        //                url:"<?php //echo base_url(); ?>//nurse/insert",
        //                type:"POST",
        //                data:{title:title,start:start,end:end},
        //                success:function()
        //                {
        //                    calendar.fullCalendar('refetchEvents');
        //                    alert("Added Successfully");
        //                }
        //            })
        //        }
        //    },
        //    editable:true,
        //    eventResize:function(event)
        //    {
        //        var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
        //        var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
    //
        //        var title = event.title;
    //
        //        var id = event.id;
    //
        //        $.ajax({
        //            url:"<?php //echo base_url(); ?>//nurse/update",
        //            type:"POST",
        //            data:{title:title, start:start, end:end, id:id},
        //            success:function()
        //            {
        //                calendar.fullCalendar('refetchEvents');
        //                alert("Event Update");
        //            }
        //        })
        //    },
        //    eventDrop:function(event)
        //    {
        //        var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
        //        //alert(start);
        //        var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
        //        //alert(end);
        //        var title = event.title;
        //        var id = event.id;
        //        $.ajax({
        //            url:"<?php //echo base_url(); ?>//nurse/update",
        //            type:"POST",
        //            data:{title:title, start:start, end:end, id:id},
        //            success:function()
        //            {
        //                calendar.fullCalendar('refetchEvents');
        //                alert("Event Updated");
        //            }
        //        })
        //    },
        //    eventClick:function(event)
        //    {
        //        if(confirm("Are you sure you want to remove it?"))
        //        {
        //            var id = event.id;
        //            $.ajax({
        //                url:"<?php //echo base_url(); ?>//nurse/delete",
        //                type:"POST",
        //                data:{id:id},
        //                success:function()
        //                {
        //                    calendar.fullCalendar('refetchEvents');
        //                    alert('Event Removed');
        //                }
        //            })
        //        }
        //    }
        //});
    // });
             
    </script>
<body class="app sidebar-mini rtl">

  <!--Global-Loader-->
  <!-- <div id="global-loader">
      <img src="back_assets/images/icons/loader.svg" alt="loader">
    </div> -->

  <div class="page">
    <div class="page-main">
      <!--app-header-->


      <?php $this->load->view('backend/header');?>



      <!-- app-content-->
      <div class="container content-area">
        <div class="side-app">


          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <div class="col-md-10">
                    <div class="card-title"> <?=$form_title?></div>
                  </div>
                  <div class="col-md-2">
                    <a href="nurse-schedule-add" class="btn btn-primary text-white mr-2" style="width:100%" id="">
                      <span> Set Schedule 
                      </span>
                    </a>
                  </div>
                </div>
                <div class="card-body">
               <div id='scheduling'></div>
                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->
            </div>
          </div>

        </div>
   
        <!-- Right-sidebar-->
        <?php $this->load->view('backend/right_sidebar');?>
        <!-- End Rightsidebar-->

        <?php $this->load->view('backend/footer');?>

      </div>
      <!-- End app-content-->
    </div>
  </div>
  <!-- End Page -->

  <!-- Back to top -->
  <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

  <?php $this->load->view('backend/footer_link');?>

<script>
  $(function() {
    "use strict";
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    // var $schedule_list =

    $('#scheduling').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listWeek'
      },
      defaultDate: date,
      navLinks: true, // can click day/week names to navigate views
      selectable: false,
      selectHelper: false,

      
      slotLabelFormat:"h:mm a",
      lang: "de",
      editable: false,
      displayEventEnd: true,
      hour12:true,
      
      eventLimit: true,// allow "more" link when too many events
      events: <?php echo json_encode($schedule_list, JSON_PRETTY_PRINT) ?>
    });
  });

</script>

</body>

</html>
