<?php $this->load->view('backend/head_link');?>

<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
<img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
<div class="page-main">
<!--app-header-->
<?php $this->load->view('backend/header');?>
<!-- app-content-->
<div class="container content-patient">
<div class="side-app">
<!-- page-header -->
<!-- <div class="page-header">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
</ol>
<div class="ml-auto">
<div class="input-group">
<a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
<span>
<i class="fa fa-calendar"></i> Events Settings
</span>
<i class="fa fa-caret-down"></i>
</a>
<a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
<span>
<i class="fa fa-star"></i>
</span>
</a>
</div>
</div>
</div> -->
<!-- End page-header -->

<div class="row">
<div class="col-md-12 col-lg-12">
<div class="card">
<div class="card-header">
<div class="col-md-10">
<div class="card-title"><?=$form_title?></div>
</div>
<div class="col-md-2">
<a href="nurse-assessment-list" class="btn btn-primary text-white mr-2"
    style="width:100%" id="">
    <span>Assessment List
    </span>
</a>
</div>
</div>
<div class="card-body">
<form action="" method="post">
<div class="row">
    <?php if($this->session->flashdata('msg')){ ?>
    <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible"
        role="alert">
        <button type="button" class="close"
            data-dismiss="alert">&times;</button>
        <div class="alert-message">
            <span><?=$this->session->flashdata('msg');?></span>
        </div>
    </div>
    <?php } ?>
    <div id="alert_pass"></div>
    <div class="col-md-12">
        <!-- <h4 class="" style="line-height:2.7em">Add Nurse</h4> -->
    </div>
</div>
<form action="" method="post">


    <fieldset class="biodata">
    <h4>Nutrition</h4>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-elements">
                    <div class="form-label">Eating Disorder</div>
                    <div class="custom-controls-stacked">
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="eating_disorder" value="1"
                                checked="">
                            <span class="custom-control-label">Nil</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="eating_disorder" value="2">
                            <span class="custom-control-label">Malnourished</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="eating_disorder" value="3">
                            <span class="custom-control-label">Obese</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-elements">
                <div class="form-label">Diet Type</div>
                    <div class="custom-controls-stacked">
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="diet_type" value="1"
                                checked="">
                            <span class="custom-control-label">Regular</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="diet_type" value="2">
                            <span class="custom-control-label">Theraputic</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="diet_type" value="3">
                            <span class="custom-control-label">Supplement (Endsure etc)</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="diet_type" value="4">
                            <span class="custom-control-label">Parental (TPN)</span>
                        </label>
                        
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
        <div class="col-md-6">
        <h4>Musculosketel</h4>
                <div class="form-group form-elements">
                    <div class="form-label"></div>
                    <div class="custom-controls-stacked">
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="musculosketel" value="1"
                                checked="">
                            <span class="custom-control-label">Normal</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="musculosketel" value="2">
                            <span class="custom-control-label">Paralysis-Free hand notes</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="musculosketel" value="3">
                            <span class="custom-control-label">Joint Swelling-Free hand notes</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="musculosketel" value="4">
                            <span class="custom-control-label">Pain-Free hand notes</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="musculosketel" value="5">
                            <span class="custom-control-label">Others-Free hand notes</span>
                        </label>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
            <h4>Fall Risk</h4>
            <div class="custom-controls-stacked">
            <label class="custom-control custom-radio">
                <input type="radio" class="custom-control-input"
                    name="risk_status" value="1"
                    checked="">
                <span class="custom-control-label">No Risk</span>
            </label>
            <label class="custom-control custom-radio">
                <input type="radio" class="custom-control-input"
                    name="risk_status" value="2">
                <span class="custom-control-label">At Risk</span>
            </label>
            <label class="custom-control custom-radio">
                <input type="radio" class="custom-control-input"
                    name="risk_status" value="3">
                <span class="custom-control-label">High Risk</span>
            </label>
        </div>
            <div class="form-group">
            <div class="row">
                <div class="col-md-3 text-left">
                    <label for="fall_history">Fall History</label>
                </div>
                <div class="col-md-9">
                    <textarea type="text" class="form-control" id="fall_history" name="fall_history" value=""></textarea>
                </div>
            </div>
            </div>
            <div class="form-group">
            <div class="row">
                <div class="col-md-3 text-left">
                    <label for="walking_device">Walking Device</label>
                </div>
                <div class="col-md-9">
                 <textarea type="text" class="form-control" id="fall_history" name="walking_device"></textarea>
                   
                </div>
            </div>
            </div>
            <div class="form-group">
            <div class="row">
                <div class="col-md-3 text-left">
                    <label for="fall_risk">Fall Risk</label>
                </div>
                <div class="col-md-9">
                    <textarea type="text" class="form-control" id="fall_risk" name="fall_risk"></textarea>
                
                </div>
            </div>
            </div>
            </div>
        </div>
        <h4>Genitourinary</h4>
        <div class="row">
        <div class="col-md-6">
                <div class="form-group form-elements">
                <div class="form-label">Urine Colour</div>
                    <div class="custom-controls-stacked">
                        <label class="custom-control custom-radio">
                            <input type="radio" class="genitourinary custom-control-input"
                                name="genitourinary" value="1_1"
                                checked="">
                            <span class="custom-control-label">Clear</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="genitourinary custom-control-input"
                                name="genitourinary" value="1_2">
                            <span class="custom-control-label">Cloudy</span>
                            <select name="cloudy_sub" class="form-control" id="cloudy_sub_select" style="display:none">
                                <option value="1">Dark</option>
                                <option value="2">Hemanturia</option>
                            </select>
                        </label>
                        <!-- <div id="cloudy_box">
                            
                        </div> -->
                        <!-- <div class="">
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="sub_urinecolor" value="1_0">
                            <span class="custom-control-label">Dark</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="sub_urinecolor" value="1_1">
                            <span class="custom-control-label">Hematuria</span>
                        </label>
                        </div> -->
                        <label class="custom-control custom-radio">
                            <input type="radio" class="genitourinary custom-control-input"
                                name="genitourinary" value="1_3">
                            <span class="custom-control-label">Others (Endsure etc)</span>
                        </label>
                    </div>

                        <label class="custom-control custom-radio">
                            <input type="radio" class="genitourinary custom-control-input"
                                name="genitourinary" value="2">
                            <span class="custom-control-label">Normal Voiding</span>
                        </label>
                        Incontinent/Dysuria/Catheter
                        <label class="custom-control custom-radio">
                            <input type="radio" class="genitourinary custom-control-input"
                                name="genitourinary" value="3">
                            <span class="custom-control-label">FHN</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="genitourinary custom-control-input"
                                name="genitourinary" value="4">
                            <span class="custom-control-label">Dialysis</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="genitourinary custom-control-input"
                                name="genitourinary" value="5">
                            <span class="custom-control-label">Others-FHN</span>
                        </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-elements">
                    <div class="form-label">Genitallia</div>
                    <div class="custom-controls-stacked">
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="genitallia" value="1"
                                checked="">
                            <span class="custom-control-label">Normal</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="genitallia" value="2">
                            <span class="custom-control-label">Others-FHN</span>
                        </label>
                        
                    </div>
                </div>
            </div>
            
        </div>
        <div class="row">
        <div class="col-md-6">
                <div class="form-group form-elements">
                    <div class="form-label">Vascular Access</div>
                    <div class="custom-controls-stacked">
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="vascullar_access" value="1"
                                checked="">
                            <span class="custom-control-label">Normal</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="vascullar_access" value="2">
                            <span class="custom-control-label">Others-FHN</span>
                        </label>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-elements">
                    <div class="form-label">Home Assessment</div>
                    <div class="custom-controls-stacked">
                    <textarea type="text" class="form-control" id="fall_risk" name="fall_risk"></textarea>
                        
                    </div>
                </div>
            </div>
        </div>
    </fieldset>


    <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
        <div class="">
            <button class="btn btn-primary" type="submit" name="save"
                id="">Submit</button>
        </div>
    </div>

    <!-- <div class="col-md-12">
<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a large text here ..."></textarea>
</div> -->

</form>

<!-- table-wrapper -->
</div>
<!-- section-wrapper -->
</div>
</div>

</div>
<!--End side app-->

<!-- Right-sidebar-->
<?php $this->load->view('backend/right_sidebar');?>
<!-- End Rightsidebar-->

<?php $this->load->view('backend/footer');?>
</div>
<!-- End app-content-->
</div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>
<script>
const cloudy = document.querySelectorAll('.genitourinary');
const cloudySubSelect = document.getElementById('cloudy_sub_select');

for (let i = 0; i < cloudy.length; i++) {
    cloudy[i].onchange = () => {
    if(cloudy[i].value == '1_2'){
        cloudySubSelect.style.display = 'block';
    }
    else {
        cloudySubSelect.style.display = 'none';
    }
}
}

</script>
</body>

</html>