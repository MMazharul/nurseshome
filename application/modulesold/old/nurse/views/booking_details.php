<?php $this->load->view('backend/head_link');?>

    <body class="app sidebar-mini rtl">

        <!--Global-Loader-->
        <!-- <div id="global-loader">
            <img src="back_assets/images/icons/loader.svg" alt="loader">
        </div> -->

        <div class="page">
            <div class="page-main">
                <!--app-header-->
                
                
       <?php $this->load->view('backend/header');?>



        <!-- app-content-->
                <div class="container content-area">
                    <div class="side-app">

<style type="text/css">
            .c-concierge-card__pulse {
                background: #01a400;
                margin-right: 10px;
            }

            .c-concierge-card__pulse, .u-pulse {
                margin: 4px 20px 0 10px;
                    margin-right: 8px;
                width: 10px;
                height: 10px;
                border-radius: 50%;
                background: #14bef0;
                cursor: pointer;
                -webkit-box-shadow: 0 0 0 rgba(40,190,240,.4);
                box-shadow: 0 0 0 rgba(40,190,240,.4);
                -webkit-animation: pulse 1.2s infinite;
                animation: pulse 1.2s infinite;
                display: inline-block;
            }

                .m-right{
                    margin-right: 7px;
                }
                .u-green-text {
                    color: #01a400;
                }

                .modal_mou{
                    text-decoration: underline!important;
                    font-size: 11px!important;
                }


                /*search box css start here*/
                .search-sec{
                    padding: 2rem;
                }
                .search-slt{
                    display: block;
                    width: 100%;
                    font-size: 11px!important;
                    line-height: 1.5;
                    color: #55595c;
                    background-color: #fff;
                    background-image: none;
                    border: 1px solid #f3ecec;
                    height: calc(3rem + 2px) !important;
                    border-radius:0;
                }
                .wrn-btn{
                    width: 100%;
                    font-size: 16px;
                    font-weight: 400;
                    text-transform: capitalize;
                    height: calc(3rem + 2px) !important;
                    border-radius:0;
                }
                @media (min-width: 992px){
                    .search-sec{
                        position: relative;
                        top: -114px;
                        background: rgba(26, 70, 104, 0.51);
                    }
                }

                @media (max-width: 992px){
                    .search-sec{
                        background: #1A4668;
                    }
                }
    </style>
   
    <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                
                     
                
                    <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">   
                    <div class="card Relatedpost nested-media">
                        <!-- <div class="card-header">
                            <h4 class="card-title">Search for Care</h4>
                        </div> -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <div class="media media-lg mt-0">
                                        
                                        <div class="media-body">
                                            <h2 class="mt-0">Booking ID : #654422
                                            </h2>
                                            

                                            <div style="margin-top: -10px;margin-bottom: 13px;"><span class="u-green-text"></span><span class="u-green-text" data-reactid="211">

                                            Patient :</span><span>

                                            Arju | November 23, 2019</span></div>
                                            

                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <div class="media media-lg mt-0">
                                        
                                        <div class="media-body">
                                                <h3 class="text-right"><span class="u-green-text"> 
                                                 RM 150
                                                </span></h3>
                                               
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <hr class="m-0 mb-4">
                            
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi, excepturi.</p>

                            <br>

                            <table class="table-bordered" cellpadding="8">
                                <thead class="bg-light text-dark">
                                    <tr>
                                        <th>Time (Hours)</th>
                                        <th>Days</th>
                                        <th>Duration</th>
                                        <th class="text-right">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>4</td>
                                        <td>November 27, 2019 - November 29, 2019</td>
                                        <td>3</td>
                                        <td class="text-right">RM 150</td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <p class="text-right"><strong>Total : RM 150</strong></p>
                            
                            <center>
                                <button class="btn bg-pink p-4">Get Started</button> 
                            </center>
                        </div>   
                    </div>
                    </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">   
                    <div class="card">
                        <!-- <div class="card-header">
                            <h4 class="card-title">Search for Care</h4>
                        </div> -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                               
                                        
                                        <div class="text-center d-block">
                                            <i class="fa fa-rocket text-info" style="font-size: 40px"></i>
                                            <br>
                                            <span style="font-size: 20px; text-transform: uppercase;"><strong>Service Started</strong></span>
                                            <br>
                                            <span>
                                            Please do the patient assessment
                                            </span>
                                            <hr>
                                        </div>

                                        <form action="" method="post">


    <fieldset class="biodata">
    <h4>Nutrition</h4>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-elements">
                    <div class="form-label">Eating Disorder</div>
                    <div class="custom-controls-stacked">
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="eating_disorder" value="1"
                                checked="">
                            <span class="custom-control-label">Nil</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="eating_disorder" value="2">
                            <span class="custom-control-label">Malnourished</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="eating_disorder" value="3">
                            <span class="custom-control-label">Obese</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-elements">
                <div class="form-label">Diet Type</div>
                    <div class="custom-controls-stacked">
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="diet_type" value="1"
                                checked="">
                            <span class="custom-control-label">Regular</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="diet_type" value="2">
                            <span class="custom-control-label">Theraputic</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="diet_type" value="3">
                            <span class="custom-control-label">Supplement (Endsure etc)</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="diet_type" value="4">
                            <span class="custom-control-label">Parental (TPN)</span>
                        </label>
                        
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
        <div class="col-md-6">
        <h4>Musculosketel</h4>
                <div class="form-group form-elements">
                    <div class="form-label"></div>
                    <div class="custom-controls-stacked">
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="musculosketel" value="1"
                                checked="">
                            <span class="custom-control-label">Normal</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="musculosketel" value="2">
                            <span class="custom-control-label">Paralysis-Free hand notes</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="musculosketel" value="3">
                            <span class="custom-control-label">Joint Swelling-Free hand notes</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="musculosketel" value="4">
                            <span class="custom-control-label">Pain-Free hand notes</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="musculosketel" value="5">
                            <span class="custom-control-label">Others-Free hand notes</span>
                        </label>
                    </div>
                </div>


                <div class="form-group form-elements">
                    <div class="form-label">Genitallia</div>
                    <div class="custom-controls-stacked">
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="genitallia" value="1"
                                checked="">
                            <span class="custom-control-label">Normal</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="genitallia" value="2">
                            <span class="custom-control-label">Others-FHN</span>
                        </label>
                        
                    </div>
                </div>

                <div class="form-group form-elements">
                    <div class="form-label">Vascular Access</div>
                    <div class="custom-controls-stacked">
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="vascullar_access" value="1"
                                checked="">
                            <span class="custom-control-label">Normal</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="vascullar_access" value="2">
                            <span class="custom-control-label">Others-FHN</span>
                        </label>
                        
                    </div>
                </div>
            
            </div>

            <div class="col-md-6">
            <h4>Fall Risk</h4>
            <div class="custom-controls-stacked">
            <label class="custom-control custom-radio">
                <input type="radio" class="custom-control-input"
                    name="risk_status" value="1"
                    checked="">
                <span class="custom-control-label">No Risk</span>
            </label>
            <label class="custom-control custom-radio">
                <input type="radio" class="custom-control-input"
                    name="risk_status" value="2">
                <span class="custom-control-label">At Risk</span>
            </label>
            <label class="custom-control custom-radio">
                <input type="radio" class="custom-control-input"
                    name="risk_status" value="3">
                <span class="custom-control-label">High Risk</span>
            </label>
        </div>
            <div class="form-group">
            <div class="row">
                <div class="col-md-3 text-left">
                    <label for="fall_history">Fall History</label>
                </div>
                <div class="col-md-9">
                    <textarea type="text" class="form-control" id="fall_history" name="fall_history" value=""></textarea>
                </div>
            </div>
            </div>
            <div class="form-group">
            <div class="row">
                <div class="col-md-3 text-left">
                    <label for="walking_device">Walking Device</label>
                </div>
                <div class="col-md-9">
                 <textarea type="text" class="form-control" id="fall_history" name="walking_device"></textarea>
                   
                </div>
            </div>
            </div>
            <div class="form-group">
            <div class="row">
                <div class="col-md-3 text-left">
                    <label for="fall_risk">Fall Risk</label>
                </div>
                <div class="col-md-9">
                    <textarea type="text" class="form-control" id="fall_risk" name="fall_risk"></textarea>
                
                </div>
            </div>
            </div>

            </div>
        </div>
        <h4>Genitourinary</h4>
        <div class="row">
        <div class="col-md-6">
                <div class="form-group form-elements">
                <div class="form-label">Urine Colour</div>
                    <div class="custom-controls-stacked">
                        <label class="custom-control custom-radio">
                            <input type="radio" class="genitourinary custom-control-input"
                                name="genitourinary" value="1_1"
                                checked="">
                            <span class="custom-control-label">Clear</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="genitourinary custom-control-input"
                                name="genitourinary" value="1_2">
                            <span class="custom-control-label">Cloudy</span>
                            <select name="cloudy_sub" class="form-control" id="cloudy_sub_select" style="display:none">
                                <option value="1">Dark</option>
                                <option value="2">Hemanturia</option>
                            </select>
                        </label>
                        <!-- <div id="cloudy_box">
                            
                        </div> -->
                        <!-- <div class="">
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="sub_urinecolor" value="1_0">
                            <span class="custom-control-label">Dark</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input"
                                name="sub_urinecolor" value="1_1">
                            <span class="custom-control-label">Hematuria</span>
                        </label>
                        </div> -->
                        <label class="custom-control custom-radio">
                            <input type="radio" class="genitourinary custom-control-input"
                                name="genitourinary" value="1_3">
                            <span class="custom-control-label">Others (Endsure etc)</span>
                        </label>
                    </div>

                        <label class="custom-control custom-radio">
                            <input type="radio" class="genitourinary custom-control-input"
                                name="genitourinary" value="2">
                            <span class="custom-control-label">Normal Voiding</span>
                        </label>
                        <strong>Incontinent/Dysuria/Catheter</strong>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="genitourinary custom-control-input"
                                name="genitourinary" value="3">
                            <span class="custom-control-label">FHN</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="genitourinary custom-control-input"
                                name="genitourinary" value="4">
                            <span class="custom-control-label">Dialysis</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="genitourinary custom-control-input"
                                name="genitourinary" value="5">
                            <span class="custom-control-label">Others-FHN</span>
                        </label>
                </div>



            </div>
            <div class="col-md-6">
                <div class="form-group form-elements">
                    <div class="form-label">Home Assessment</div>
                    <div class="custom-controls-stacked">
                    <textarea type="text" class="form-control" id="fall_risk" name="fall_risk"></textarea>
                        
                    </div>
                </div>
            </div>
           
            
        </div>
        
    </fieldset>


    <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
        <div class="">
            <button class="btn btn-primary" type="submit" name="save"
                id="">Save</button>
        </div>
    </div>

</form>
                                </div>
                            </div>

                            <hr>

                            <center>
                                <button class="btn bg-lime p-4 text-white">Service Done</button> 
                            </center>
                                <hr>
                             <div class="text-center d-block mt-5">
                                            <i class="fa fa-check-square-o text-info" style="font-size: 40px"></i>
                                            <br>
                                            <span style="font-size: 20px; text-transform: uppercase;"><strong>Work Completed</strong></span>
                                            <br>
                                            <span>
                                                Your service has been marked as completed.
                                            </span>
                                       
                                        </div>
                           <hr>

                           <div class="text-center d-block mt-5">
                                            <i class="fa fa-comment-o text-info" style="font-size: 30px"></i>
                                            <br>
                                            <span style="font-size: 20px; text-transform: uppercase;"><strong>Patient Review</strong></span>
                                            <br>
                                        </div>
                            <hr>

                            <div class="row">
                                <div class="col-md-2">
                                    <img src="back_assets/images/users/avatars/1.png" alt="">
                                </div>
                                <div class="col-md-10 pt-4">
                                    <h4>Arju 
                                        <span>
                                            <i class="fa fa-star text-warning"></i> <strong class="text-warning">5</strong>
                                        </span> 
                                    </h4>
                                    <p style="font-size: 14px">Wonderful</p>
                                </div>
                            
                            </div>
                                <hr>
                            <div class="row">
                                 <div class="col-md-2">
                                    <img src="back_assets/images/users/avatars/1.png" alt="">
                                </div>
                                <div class="col-md-10 pt-4">
                                    <h4>Sophie</h4>
                                    <span>Give your review : 
                                    
                                      <div class="stars stars-example-fontawesome">
                                        <select id="example-fontawesome" name="rating" autocomplete="off">
                                          <option value="1">1</option>
                                          <option value="2">2</option>
                                          <option value="3">3</option>
                                          <option value="4">4</option>
                                          <option value="5">5</option>
                                        </select>
                                      </div>
                                    
                                    </span>
                                    <textarea name="comment" class="form-control" id="" cols="30" rows="4"></textarea>
                                </div>
                            </div>
                        </div>   
                    </div>
                    </div>

                    </div>
                </div>
                <div class="col-md-3">
                    
                    <div class="row">
                        
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Private Note</h4>
                                </div>
                                <div class="card-body">
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal" class="btn btn-primary">
                                        <i class="fa fa-plus"></i>
                                        Add Note
                                    </a>
                                    <a href="notes-list/1" class="btn bg-pink">
                                        Note List
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Note</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                   <form action="" method="post">
                                    <div class="form-label mb-2 text-center">Summary For Client</div>
                                    <div class="form-label">
                                        I HAVE COMPLETED THE FOLLOWING CARE:
                                    </div>
                                    <div class="form-group form-elements">
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_done[]" value="1">
                                            <span class="custom-control-label">
                                                BASIC PERSONAL CARE – BASIC CARE FOR CLEANING, FEEDING, MEDICATION ETC
                                            </span>          
                                        </label>
                                    </div>
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_done[]" value="1">
                                            <span class="custom-control-label">
                                                BASWOUND DRESSING – BED SORES / DIABETIC WOUNDS /SURGICAL WOUNDS
                                            </span>          
                                        </label>
                                    </div>
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_done[]" value="1">
                                            <span class="custom-control-label">
                                                INFANT BABY CARE – 0 TO 12 MONTHS
                                            </span>          
                                        </label>
                                    </div>
                                    </div>
                                    <div class="form-label mb-2 text-center">Basic Vital Signs</div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                    <label for="note">Temp </label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="temp">
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                            <div class="col-md-3">
                                                    <label for="note">BP</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="bp">
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                        <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                    <label for="note">Pulse Rate </label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="pulse">
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                    <label for="note">Respiration </label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="respiration">
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-6"><div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                        <label for="note">Pain Sore </label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="pain_sore">
                                                </div>
                                            </div>
                                        </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                        <label for="note">SPO2 </label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="spo2">
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6"><div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                        <label for="note">ETC </label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="etc">
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="form-label">
                                        Notes for Care
                                    </div>
                                    <div class="form-group form-elements">
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_done[]" value="1">
                                            <span class="custom-control-label">
                                                BASIC PERSONAL CARE – BASIC CARE FOR CLEANING, FEEDING, MEDICATION ETC
                                            </span>
                                            <textarea class="form-control" name="" id="" cols="30" rows="1"></textarea>          
                                        </label>
                                    </div>
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_done[]" value="1">
                                            <span class="custom-control-label">
                                                BASWOUND DRESSING – BED SORES / DIABETIC WOUNDS /SURGICAL WOUNDS
                                            </span>   
                                            <textarea class="form-control" name="" id="" cols="30" rows="1"></textarea>         
                                        </label>
                                    </div>
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_done[]" value="1">
                                            <span class="custom-control-label">
                                                INFANT BABY CARE – 0 TO 12 MONTHS
                                            </span>      
                                            <textarea class="form-control" name="" id="" cols="30" rows="1"></textarea>      
                                        </label>
                                    </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                            <label for="photo_wound">Photo for wounds</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="file" class="dropify" 
                                                    name="bio_file">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">Summary</div>
                                            <div class="col-md-9">
                                                <textarea class="form-control" name="" id="" cols="30" rows="2"></textarea> 
                                            </div>
                                        </div>
                                    </div>
                                    

                                        

                                        


                                        <div class="modal-footer">
                                            <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
                                            <button type="button" class="btn bg-primary">Add</button>
                                        </div>
                                        </form>
                                </div>
                                   
                                </div>
                                
                            </div>
                        </div>
                    </div>

                

                    </div>


                </div>
            
    </div>

                </div><!--End side app-->

                    <!-- Right-sidebar-->
                <?php $this->load->view('backend/right_sidebar');?>

                    <?php $this->load->view('backend/footer');?>

                </div>
                <!-- End app-content-->
            </div>
        </div>
        <!-- End Page -->

        <!-- Back to top -->
        <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

    <?php $this->load->view('backend/footer_link');?>

    <script>
const cloudy = document.querySelectorAll('.genitourinary');
const cloudySubSelect = document.getElementById('cloudy_sub_select');

for (let i = 0; i < cloudy.length; i++) {
    cloudy[i].onchange = () => {
    if(cloudy[i].value == '1_2'){
        cloudySubSelect.style.display = 'block';
    }
    else {
        cloudySubSelect.style.display = 'none';
    }
}
}

</script>
    </body>
</html>