<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Nurse extends MX_Controller {

    //public $counter=0;
    
    protected $login_id;
    
    function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->model('nurse_model');
        // $this->load->model('product/product_model');
        $this->load->helper('text');
        // $this->load->helper('common_helper');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('inflector');
        //$this->load->library('encrypt');
        // $this->load->model('home_model');
        // $this->load->model('admin/admin_model');
        // $this->load->model('product/product_model');
        $this->login_id = $this->session->userdata('loginid');
     }

    // add this line for responsive table 
    // $data['responsive_table'] = 'true';

    //==========================PATIENT PROFILE =========================== //
    public function profile()
    {
        
        $patient = $data['patient_details'] = $this->nurse_model->select_with_where('*',"user_role=4",'users');
        $data['form_title'] = 'Profile of '.ucwords($patient[0]['first_name']." ".$patient[0]['last_name']);
        $this->load->view('patient_profile',$data);
        
    }

     //==========================BOOKING DETAILS=========================== //
    public function booking_details()
    {
        $data['form_title'] = 'Booking Details';
        $this->load->view('booking_details',$data);
        
    }

    //==========================NURSE LIST=========================== //
    public function nurse_list()
    {
        
        $data['nurse_details'] = $this->nurse_model->select_with_where('*',"user_role=3",'users');
        $data['responsive_table'] = 'true';
        $this->load->view('manage_nurse/nurse_list', $data);
        
    }
    //===================ADD NURSE AS USER VIEW=================== //
    public function index()
    {

        $this->load->view('manage_nurse/register_user_nurse');
        
    }
    //=================ADD NURSE AS USER POST===================== //
    public function register_user_as_nurse(){

        $data['email'] = $email = html_escape(trim($this->input->post('email')));

        $data['first_name'] = html_escape(trim($this->input->post('firstname')));

        $data['last_name'] = html_escape(trim($this->input->post('lastname')));

        $data['country_code'] = html_escape(trim($this->input->post('country_code')));
        
        $data['contact_no'] = html_escape(trim($this->input->post('contact')));

        $data['user_role'] = 3;  // role 3 = NURSE

        $data['verify_status'] = 1; 

        $data['registered_date'] = date('Y-m-d H:i:s');

        $data['last_login_ip'] = $this->input->ip_address();

        $password = html_escape(trim($this->input->post('password')));

        $passconf = html_escape(trim($this->input->post('passconf')));

        $data['password'] = $this->encryptIt($password);
        
        if($password == $passconf) {
            $res = $this->nurse_model->select_with_where('*',"email='{$email}'",'users');
            if (count($res) > 0) 
            {  
                $this->session->set_flashdata('type', 'danger');
                $this->session->set_flashdata('msg', 'Email already exists!!');
                redirect('nurse');
            }
            else{
                $this->nurse_model->insert('users',$data);
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', 'Registraion Done Successfully!');
                redirect('nurse/nurse_list');
            } 
        }else{
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', 'Password not matched!');
                redirect('nurse/nurse_list');
        } 
    }
    //===================EDIT AUTH NURSE VIEW=================== //
    public function edit_nurse_auth_details($id)
    {
        $data['nurse_auth_details'] = $this->nurse_model->select_with_where('*',"user_role=3",'users');
        $this->load->view('manage_nurse/edit_nurse_auth_details',$data);
        
    }
    //=================Bookings VIEW=================== //
    public function bookings()
    {
        $data['form_title'] = 'Bookings';
        $data['nurse_details'] = $this->nurse_model->select_with_where('*',"loginid=".$this->login_id,'users');
        $this->load->view('nurse_profile/booking/bookings', $data);
        
    }
    //=================ADD NURSE DETAILS VIEW=================== //
    public function add_nurse_details()
    {
  
        $this->load->view('manage_nurse/add_nurse_details');
        
    }

    // ================= NURSE PROFILE =================== //

    public function nurse_profile_view($id)
    {
        // $data['responsive_table'] = 'true';
        $data['form_title'] = 'Profile';
        $data['nurse_details'] = $this->nurse_model->select_with_where('*',"loginid=".$this->login_id,'users');
        $this->load->view('nurse_profile/profile',$data);  
    }
    // ================= NURSE PROFILE EDIT =================== //

    public function edit_profile($id)
    {
        $data['form_title'] = 'Profile';

        $data['nurse_details'] = $this->nurse_model->select_with_where('*',"loginid=".$this->login_id,'users');

        $data['nurse_bio'] = $this->nurse_model->select_with_where('*',"nurse_id=".$this->login_id,'nurse_bio');

        $data['nurse_education_certificate'] = $this->nurse_model->select_with_where('*',"nurse_id=".$this->login_id,'nurse_education_certificate');

        $data['nurse_additional_certificate'] = $this->nurse_model->select_with_where('*',"nurse_id=".$this->login_id,'nurse_additional_certificate');

        $data['nurse_experience'] = $this->nurse_model->select_where_left_join("*","nurse_experience","nurse_experience_details","nurse_experience.expid=nurse_experience_details.exp_id","nurse_experience.nurse_id=".$this->login_id);

        $data['competencies'] = $this->nurse_model->select_with_where('*',"nurse_id=".$this->login_id,'competencies_details');

        $this->load->view('nurse_profile/index',$data);  
    }

    public function nurse_edit_profile($id)
    {
        // $data['form_title'] = 'Profile';

        $data['nurse_details'] = $this->nurse_model->select_with_where('*',"loginid=".$id,'users');

        $data['nurse_bio'] = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'nurse_bio');

        $data['nurse_education_certificate'] = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'nurse_education_certificate');

        $data['nurse_additional_certificate'] = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'nurse_additional_certificate');

        $data['nurse_experience'] = $this->nurse_model->select_where_left_join("*","nurse_experience","nurse_experience_details","nurse_experience.expid=nurse_experience_details.exp_id","nurse_experience.nurse_id=".$id);

        $data['competencies']=$this->nurse_model->select_with_where('*',"nurse_id=".$id,'competencies_details');

        $this->load->view('nurse_profile/index',$data);  
    }
    // ================= NURSE PROFILE EDIT ACTION =================== //

    public function update_profile($id)
    {
        $data['email'] = $email = html_escape(trim($this->input->post('email')));

        $data['first_name'] = html_escape(trim($this->input->post('firstname')));

        $data['last_name'] = html_escape(trim($this->input->post('lastname')));

        $data['country_code'] = html_escape(trim($this->input->post('country_code')));

        $data['contact_no'] = html_escape(trim($this->input->post('contact')));

        $data['gender'] = html_escape(trim($this->input->post('gender')));

        $password = html_escape(trim($this->input->post('password')));

        $passconf = html_escape(trim($this->input->post('passconf')));

        if (!empty($password) && !empty($passconf) && ($password == $passconf)) {

            $data['password'] = $this->encryptIt($password);     
        }
        
        $data['nurse_details'] = $this->nurse_model->update_function('loginid', $id, 'users', $data);
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', 'Update Successful.');

        if ($this->session->userdata('user_role') == 1) {

            redirect('nurse-edit-profile/'.$id); 
        }
        else{
            redirect('nurse-profile'); 
        }
        

    }


   // ================= NURSE BIO =================== //
    public function add_bio($id)
    {
        $data['form_title'] = 'Add Bio';
        $this->load->view('nurse_profile/bio/add_bio',$data);  
    }

    public function list_bio($id)
    {
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'Bio List';
        $data['nurse_details'] = $this->nurse_model->select_with_where('*',"loginid=".$this->login_id,'users');
        $this->load->view('nurse_profile/bio/list_bio',$data);  
    }
    public function edit_bio($id)
    {
        $data['form_title'] = 'Update Bio';
        $this->load->view('nurse_profile/bio/edit_bio',$data);  
    }

    public function update_bio($id)
    {


        $data['nric_passport_id'] = html_escape(trim($this->input->post('nric_passport_id')));
        $data['age'] = html_escape(trim($this->input->post('age')));

        $data['height'] = html_escape(trim($this->input->post('height')));

        $data['weight'] = html_escape(trim($this->input->post('weight')));

        $data['nurse_id'] = $id;

        $data['add_date'] = date("Y-m-d H:i:s");

        if($_FILES['bio_file']['name'] != '')
            { 
             $data['bio_file'] ='';
             $i_ext = explode('.', $_FILES['bio_file']['name']);
             $target_path = $id."_".date("YmdHis").'_nurse_photo.'.end($i_ext);
             $_FILES['bio_file']['name'] = $target_path;
 
             $this->upload->initialize($this->set_upload_options($_FILES['bio_file']['name'],'uploads/'));
             $size = getimagesize($_FILES['bio_file']['tmp_name']);
             $this->upload->do_upload();  
             
             if (move_uploaded_file($_FILES['bio_file']['tmp_name'], 'uploads/' . $target_path))
             {
                 if ($size[0] == 150 || $size[1] == 150) 
                 {


                 } 
                else {
                 $imageWidth = 150; //Contains the Width of the Image
                 $imageHeight = 150;
                 $this->resize($imageWidth, $imageHeight, "uploads/" . $target_path, 
                    "uploads/" . $target_path);
                } 
                $data['bio_file'] = $target_path;
             } 
         }


        // exit;

        $res = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'nurse_bio');
        if (count($res) > 0) {
            #update
            $this->nurse_model->update_function('nurse_id', $id, 'nurse_bio', $data);
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'biodata');
            $this->session->set_flashdata('msg', 'Update Successful');

        }
        else{
            #insert
            $this->nurse_model->insert('nurse_bio',$data);
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'biodata');
            $this->session->set_flashdata('msg', 'Insert Successful.');
              
        }

        if ($this->session->userdata('user_role') == 1) {

            redirect('nurse-edit-profile/'.$id); 
        }
        else{
            redirect('nurse-profile'); 
        }


    }
    // ================= NURSE EDUCATION =================== //

    public function add_education($id)
    {
        $data['form_title'] = 'Add Education Qualification';
        $this->load->view('nurse_profile/education/add_education',$data);  
    }
    public function list_education($id)
    {
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'Education Qualification List';
        $this->load->view('nurse_profile/education/list_education',$data);  
    }
    public function edit_education($id)
    {
        $data['form_title'] = 'Education Qualification Update';
        $this->load->view('nurse_profile/education/edit_education',$data);  
    }



      // ================= NURSE ADDITIONAL CERTIFICATE =================== //

      public function add_additional_certificate($id)
      {
          $data['form_title'] = 'Add Additional Certifcate';
          $this->load->view('nurse_profile/education/add_additional_certificate',$data);  
      }
      public function list_additional_certificate($id)
      {
          $data['responsive_table'] = 'true';
          $data['form_title'] = 'Additional Certifcate List';
          $this->load->view('nurse_profile/education/list_additional_certificate',$data);  
      }
      public function edit_additional_certificate($id)
      {
          $data['form_title'] = 'Additional Certifcate Update';
          $this->load->view('nurse_profile/education/edit_education',$data);  
      }




    public function update_qualification($id)
    {

        // echo json_encode($_FILES); 
        // exit;

        //for nurse_education_certificate
        $data['post_basic'] = html_escape(trim($this->input->post('postbasic')));

        $data['degree'] = html_escape(trim($this->input->post('degree')));

        $data['diploma'] = html_escape(trim($this->input->post('diploma')));

        $data['certificate'] = html_escape(trim($this->input->post('certificate')));

        $data['reg_number'] = html_escape(trim($this->input->post('lim_reg_number')));

        // $date=date_create("2013-03-15"); echo date_format($date,"Y/m/d H:i:s");

        $date = html_escape(trim($this->input->post('reg_date')));

        $data['reg_date'] = date_format(date_create($date), "Y/m/d");

        //for nurse_additional_certificate
        $data2['bls'] = html_escape(trim($this->input->post('bls')));

        $data2['cannulation'] = html_escape(trim($this->input->post('cannulation')));

        $data2['palliative'] = html_escape(trim($this->input->post('palliative')));

        $data2['others'] = html_escape(trim($this->input->post('others')));

        $data2['retention'] = html_escape(trim($this->input->post('retention')));

        $data2['driver_license'] = html_escape(trim($this->input->post('driver_license')));

        $data2['have_own_transport'] = html_escape(trim($this->input->post('have_own_transport')));

        $data['nurse_id'] = $data2['nurse_id'] = $id;

        $data['add_date'] = $data2['add_date'] = date("Y-m-d H:i:s");

        if($_FILES['lm_reg_certificate']['name'] != '')
            { 
             $data2['lm_reg_certificate'] ='';
             $i_ext = explode('.', $_FILES['lm_reg_certificate']['name']);
             $target_path = $id."_".date("YmdHis").'_lm_reg_certificate.'.end($i_ext);
             $_FILES['lm_reg_certificate']['name'] = $target_path;
 
             $this->upload->initialize($this->set_upload_options($_FILES['lm_reg_certificate']['name'],'uploads/'));
             $size = getimagesize($_FILES['lm_reg_certificate']['tmp_name']);
             $this->upload->do_upload();  
             
             if (move_uploaded_file($_FILES['lm_reg_certificate']['tmp_name'], 'uploads/' . $target_path))
             {
                 if ($size[0] == 150 || $size[1] == 150) 
                 {


                 } 
                else {
                 $imageWidth = 150; //Contains the Width of the Image
                 $imageHeight = 150;
                 $this->resize($imageWidth, $imageHeight, "uploads/" . $target_path, 
                    "uploads/" . $target_path);
                } 
                $data2['lm_reg_certificate'] = $target_path;
             } 
         }


        // exit;

        $res = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'nurse_education_certificate');

        $res2 = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'nurse_additional_certificate');

        if (count($res) > 0 && count($res2) > 0) {
            #update
            $this->nurse_model->update_function('nurse_id', $id, 'nurse_education_certificate', $data);
            $this->nurse_model->update_function('nurse_id', $id, 'nurse_additional_certificate', $data2);

            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'qualification');
            $this->session->set_flashdata('msg', 'Update Successful');

        }
        else{
            #insert
            $this->nurse_model->insert('nurse_education_certificate',$data);
            $this->nurse_model->insert('nurse_additional_certificate',$data2);
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'qualification');
            $this->session->set_flashdata('msg', 'Insert Successful.');
              
        }

        if ($this->session->userdata('user_role') == 1) {

            redirect('nurse-edit-profile/'.$id); 
        }
        else{
            redirect('nurse-profile'); 
        }

    }
    // ================= NURSE EXPERIENCE =================== //
    public function add_experience($id)
    {
        $data['form_title'] = 'Add Experience';
        $this->load->view('nurse_profile/experience/add_experience',$data);  
    }
    public function list_experience($id)
    {
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'List Experience';
        $this->load->view('nurse_profile/experience/list_experience',$data);  
    }
    public function edit_experience($id)
    {
        $data['form_title'] = 'Update Experience';
        $this->load->view('nurse_profile/experience/edit_experience',$data);  
    }





    public function update_experience($id)
    {

        // echo json_encode($_POST); 
        // exit;

        
        $organisation = $this->input->post('organisation');

        $start_date = $this->input->post('start_date');

        $end_date = $this->input->post('end_date');

        $title = $this->input->post('title');



        $duty_description = $this->input->post('duty_description');
        $ref = $this->input->post('ref');

        $startdate = $this->input->post('start_date');
        $enddate = $this->input->post('end_date');

        // $start_date = date_format(date_create($startdate), "Y/m/d");
        // $end_date = date_format(date_create($enddate), "Y/m/d");

        $training_attended = $this->input->post('training_attended');
      
        $data['nurse_id'] = $id;
        $data['add_date'] = date("Y-m-d H:i:s");
  

        $res = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'nurse_experience');
 $this->nurse_model->insert('nurse_experience_details',$data2);

        if (count($res) > 0) {
            #update

            $data['nurse_id'] = $id;
            $data['add_date'] = date("Y-m-d H:i:s");
            //$ret = $this->nurse_model->insert_ret('nurse_experience',$data);
            
            foreach ($title as $key => $value) {
                $data2['exp_id'] = $ret;
                $data2['nurse_id'] = $id;
                $data2['start_date'] = date_format(date_create($startdate[$key]), "Y/m/d") ;
                $data2['end_date'] = date_format(date_create($enddate[$key]), "Y/m/d") ;
                $data2['organisation'] = html_escape(trim($organisation[$key]));
                $data2['title'] = html_escape(trim($title[$key]));
                $data2['ref'] = html_escape(trim($ref[$key]));
                $data2['duty_description'] = html_escape(trim($duty_description[$key]));
                $data2['training_attended'] = html_escape(trim($training_attended[$key]));
                $data2['add_date'] = date("Y-m-d H:i:s");
                //check experience exist or not
                $exp=$this->nurse_model->select_with_where('*',"nurse_id=".$id,'nurse_experience');
                $expid=$exp[0]['expid'];
                $expdat=count($this->nurse_model->select_with_where('*',"exp_id=".$expid,'nurse_experience_details'));

                //check experience exist or not
                $this->nurse_model->insert('nurse_experience_details',$data2);
                 // $this->nurse_model->update_function('nurse_id',$id,'nurse_experience_details',$data2);
            }

            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'experience');
            $this->session->set_flashdata('msg', 'Update Successful');

        }
        else{
            #insert

            $data['nurse_id'] = $id;
            $data['add_date'] = date("Y-m-d H:i:s");
            $ret = $this->nurse_model->insert_ret('nurse_experience',$data);

            foreach ($title as $key => $value) {
                $data2['exp_id'] = $ret;
                $data2['nurse_id'] = $id;
                $data2['start_date'] = date_format(date_create($startdate[$key]), "Y/m/d") ;
                $data2['end_date'] = date_format(date_create($enddate[$key]), "Y/m/d") ;
                $data2['organisation'] = html_escape(trim($organisation[$key]));
                $data2['title'] = html_escape(trim($title[$key]));
                $data2['duty_description'] = html_escape(trim($duty_description[$key]));
                $data2['training_attended'] = html_escape(trim($training_attended[$key]));
                $data2['add_date'] = date("Y-m-d H:i:s");
                $this->nurse_model->insert('nurse_experience_details',$data2);
            }
            
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'experience');
            $this->session->set_flashdata('msg', 'Insert Successful.');
              
        }
        if ($this->session->userdata('user_role') == 1) {

            redirect('nurse-edit-profile/'.$id); 
        }
        else{
            redirect('nurse-profile'); 
        }

    }

    // ================= NURSE NOTES =================== //
    public function add_notes($id)
    {
        $data['form_title'] = 'Add Notes';
        $this->load->view('nurse_profile/notes/add_notes',$data);  
    }
    public function list_notes($id)
    {
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'List of Notes';
        $this->load->view('nurse_profile/notes/list_notes',$data);  
    }
    public function edit_notes($id)
    {
        $data['form_title'] = 'Update Notes';
        $this->load->view('nurse_profile/notes/edit_notes',$data);  
    }

    // ================= NURSE REVIEWS =================== //
    public function add_reviews($id)
    {
        $data['form_title'] = 'Add Review';
        $this->load->view('nurse_profile/reviews/add_reviews',$data);  
    }
    public function list_reviews($id)
    {   
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'List Review';
        $this->load->view('nurse_profile/reviews/list_reviews',$data);  
    }
    
    public function edit_reviews($id)
    {
        $data['form_title'] = 'Update Review';
        $this->load->view('nurse_profile/reviews/edit_reviews',$data);  
    }

    // ================= NURSE COMPTENCIES =================== //
    public function add_competency($id)
    {
        $data['form_title'] = 'Add Competencies';
        $this->load->view('nurse_profile/competency/add_competency',$data);  
    }
    public function list_competency($id)
    {
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'List Competencies';
        $this->load->view('nurse_profile/competency/list_competency',$data);  
    }
    public function edit_competency($id)
    {
        $data['form_title'] = 'Update Competencies';
        $this->load->view('nurse_profile/competency/edit_competency',$data);  
    }
    public function update_Competencies($id)
    {
        $nursing_competency=$this->input->post('nursing_competency');
        $nursing_competency_desc=$this->input->post('nursing_competency_desc');
        $cnt=count($nursing_competency);
        $res = $this->nurse_model->select_with_where('*',"nurse_id=".$id,'competencies_details');
        if($res==0)
        {
        foreach ($nursing_competency as $key => $value) 
        {
              
                $data2['nurse_id'] = $id;                
                $data2['compe_id'] = html_escape(trim($nursing_competency[$key]));
                $data2['com_value'] = html_escape(trim($nursing_competency_desc[$key]));
                $data2['add_date'] = date("Y-m-d H:i:s");
                //check this competancy id exist or not

                $this->nurse_model->insert_ret('competencies_details',$data2);


       }
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('active', 'Competencies');
            $this->session->set_flashdata('msg', 'Insert Successful.');
       }

       else        
       {
        foreach ($nursing_competency as $key => $value) 
        {
              
                $data2['nurse_id'] = $id;                
                $data2['compe_id'] = html_escape(trim($nursing_competency[$key]));
                $data2['com_value'] = html_escape(trim($nursing_competency_desc[$key]));
                $data2['add_date'] = date("Y-m-d H:i:s");
                //check this competancy id exist or not
        $ch=$this->nurse_model->select_with_where('*',"compe_id=". $data2['compe_id'],'competencies_details');
        $cn=count($ch);
        if($cn==0)
        {
         $this->nurse_model->insert_ret('competencies_details',$data2);   
        }
        else
        {
           $this->nurse_model->update_function('compe_id',$data2['compe_id'],'competencies_details',$data2);  
        }
        


       }

       }
      

        if ($this->session->userdata('user_role') == 1) {

            redirect('nurse-edit-profile/'.$id); 
        }
        else{
            redirect('nurse-profile'); 
        } 
    }
    // ================= NURSE SHEDULE =================== //

    public function add_schedule($id)
    {
        $data['form_title'] = 'Add Schedule';
        $this->load->view('nurse_profile/schedule/add_schedule',$data);  
    }
    public function add_schedule_monthly($id)
    {
        $data['form_title'] = 'Add Schedule Monthly';
        $this->load->view('nurse_profile/schedule/add_schedule_monthly',$data);  
    }
    public function list_schedule($id)
    {
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'List Schedule';
        $this->load->view('nurse_profile/schedule/list_schedule',$data);  
    }
    public function edit_schedule($id)
    {
        $data['form_title'] = 'Update Schedule';
        $this->load->view('nurse_profile/schedule/edit_schedule',$data);  
    }


    // ================= NURSE PATIENT LIST =================== //

    public function nurse_patient_list($id)
    {
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'My Patient List';
        $this->load->view('nurse_profile/booking/list_booking',$data);  
    }

    // ================= NURSE ASSESSMENT =================== //

    public function add_assessment($id)
    {
        $data['form_title'] = 'Add Assessment';
        $this->load->view('nurse_profile/assessment/add_assessment',$data);  
    }
    public function list_assessment($id)
    {
        $data['responsive_table'] = 'true';
        $data['form_title'] = 'List Assessment';
        $this->load->view('nurse_profile/assessment/list_assessment',$data);  
    }
    public function edit_assessment($id)
    {
        $data['form_title'] = 'Update Assessment';
        $this->load->view('nurse_profile/assessment/edit_assessment',$data);  
    }


    //=========================OTHER FUNCTIONS=========================== //

    function encryptIt($string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
        $secret_iv = 'This is my secret iv';
        // hash
        $key = hash('sha256', $secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        $output=str_replace("=", "", $output);
        return $output;
    }

    private function set_upload_options($file_name,$folder_name)
    {   
        //upload an image options
        $url=base_url();

        $config = array();
        $config['file_name'] = $file_name;
        $config['upload_path'] = 'uploads/'.$folder_name;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']      = '0';
        $config['overwrite']     = TRUE;

        return $config;
    }
    //general function start

    public function resize($height, $width, $source, $destination)
    {
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = $source;
        $config['overwrite'] = TRUE;
        $image_config['quality'] = "100%";
        $image_config['maintain_ratio'] = FALSE;
        $config['height'] = $height;
        $config['width'] = $width;
        $config['new_image'] = $destination; //you should have write permission here..
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }


}

?>
