<?php $this->load->view('backend/head_link');?>

	<body class="app sidebar-mini rtl">

		<!--Global-Loader-->
		<!-- <div id="global-loader">
			<img src="back_assets/images/icons/loader.svg" alt="loader">
		</div> -->

		<div class="page">
			<div class="page-main">
				<!--app-header-->
                <?php $this->load->view('backend/header');?>
                <!-- app-content-->
				<div class="container content-patient">
					<div class="side-app">
					    <!-- page-header -->
						<!-- <div class="page-header">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
                            </ol>
							<div class="ml-auto">
								<div class="input-group">
									<a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
										<span>
											<i class="fa fa-calendar"></i> Events Settings
										</span>
										<i class="fa fa-caret-down"></i>
									</a>
									<a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
										<span>
											<i class="fa fa-star"></i>
										</span>
									</a>
								</div>
							</div>
						</div> -->
						<!-- End page-header -->
                        <?php if($this->session->flashdata('msg')){ ?>
                            <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <div class="alert-message">
                                    <span><?=$this->session->flashdata('msg');?></span>
                                </div>
                            </div>
                        <?php } ?> 

                        <div class="row">
							<div class="col-md-12 col-lg-12">
							<div class="card">
								<div class="card-header">
                                    <div class="col-md-10">
                                    <div class="card-title">Update Profile</div>
                                    </div>
                                    <div class="col-md-2">
                                    <!-- <a href="patient/patient_list" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
										<span> Patient List
										</span>
									</a> -->
                                    </div>
								</div>
								<div class="card-body">
                                	<form action="patient/insert_patient_details" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="id" value="<?=$this->session->userdata('loginid')?>">
                            <div class="row">
                                    <div class="col-md-6"><h4 class="" style="line-height:2.7em">Account Information</h4></div>
                                    <div class="col-md-6"><div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                        <div class="">
                                         <!--    <button class="btn btn-primary" type="submit" name="save" id="save_contact">Save</button> -->
                                        </div>
                                    </div></div>
                                    </div>
									                                    <fieldset>
                                       
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="firstname">First Name</label>
                                                </div>
                                                <div class="col-md-9">
                                                <input name="firstname" type="text" value="<?=!empty($patient_details_users[0]['first_name']) ? $patient_details_users[0]['first_name'] : ''?>" class="form-control" required placeholder="First Name" required="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                        <div class="col-md-3 text-right">
                                            <label for="lastname">Last Name</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input name="lastname" value="<?=!empty($patient_details_users[0]['last_name']) ? $patient_details_users[0]['last_name'] : ''?>" type="text" class="form-control" required placeholder="Last Name" required="">
                                        </div>
                                        </div> 
                                    </div>
                                </div>
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="email">E-mail</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="email" value="<?=!empty($patient_details_users[0]['email']) ? $patient_details_users[0]['email'] : ''?>" class="form-control" id="email" name="email" placeholder="Email"required="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="row">
                                            <div class="col-md-3 text-right">
                                                <label for="contact">Contact</label>
                                            </div>
                                            <div class="col-md-9">
                                            <div class="input-group mb-3">
												<input style='width:2rem;text-align:center;background:#ccc;color:#111; outline:0;border:0' value="<?=!empty($patient_details_users[0]['country_code']) ? $patient_details_users[0]['country_code'] : ''?>" type="text" name="country_code" readonly value="+60">
												<input name="contact" value="<?=!empty($patient_details_users[0]['contact_no']) ? $patient_details_users[0]['contact_no'] : ''?>" type="text" class="form-control"  placeholder="Contact no">
											</div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
								                                <div class="row">
                                    <div class="col-md-6">
                                    <div class="form-group">
                                            <div class="row">
                                                    <div class="col-md-3 text-right">
                                                        <label for="id">Gender</label>
                                                    </div>
                                                    <div class="col-md-9">
                                    <select name="gender" class="form-control" >
                                                <option value="1" <?=$patient_details_users[0]['gender'] == 1 ? 'selected' : '' ;?>  >Male</option>
                                                <option value="2" <?=$patient_details_users[0]['gender'] == 2 ? 'selected' : '' ;?> >Female</option>
                                            </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
									                        <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                    <div class="col-md-3 text-left">
                                        <label for="id">Update Profile Photo</label>
                                    </div>
                                    <div class="col-md-6">
<input type="file" class="dropify" data-default-file="uploads/<?=!empty($patient_details_users[0]['profile_pic']) ? $patient_details_users[0]['profile_pic'] : '';?>" 
                                            name="bio_file">
                                    </div>
                            </div>
                        </div>
                    </div>
                                </div>
                                </fieldset>
                                    <div class="row">
                                    <div class="col-md-6"><h4 class="" style="line-height:2.7em">Bio Data</h4></div>
                                    <div class="col-md-6"><div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                        <div class="">
                                         <!--    <button class="btn btn-primary" type="submit" name="save" id="save_contact">Save</button> -->
                                        </div>
                                    </div></div>
                                    </div>
                                    
                                    <fieldset>
                                       
                                    <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="nrc_passport_id">ID</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="nrc_passport_id" name="nrc_passport_id"  placeholder="NRC/PASSPORT" value="<?=!empty($patient_details[0]['nrc_passport_id']) ? $patient_details[0]['nrc_passport_id'] : ''?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                            <div class="col-md-3 text-right">
                                                    <label for="gender">Gender</label>
                                        </div>
                                        <div class="col-md-9">
                                        <select name="gender" class="form-control" id="gender">
                                            <option value="male">Male</option>
                                            <option value="femlae">Female</option>
                                        </select>
                                        </div>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="weight">Weight</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="weight" name="weight"  placeholder="Weight" value="<?=!empty($patient_details[0]['weight']) ? $patient_details[0]['weight'] : ''?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="height">Height</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="height" name="height"  placeholder="Height" value="<?=!empty($patient_details[0]['height']) ? $patient_details[0]['height'] : ''?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                </fieldset>


                                <div class="row">
                                    <div class="col-md-6"><h4 class="" style="line-height:2.7em">Patient Location Details</h4></div>
                                    <div class="col-md-6"><div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                        <div class="">
                                            <!-- <button class="btn btn-primary" type="submit" name="save" id="save_bio">Save</button> -->
                                        </div>
                                    </div></div>
                                    </div>

                                    <fieldset>
                                       
                                    <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="address">Address</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <textarea class="form-control" id="address" name="address" cols="30" rows="1"><?=!empty($patient_details[0]['address']) ? $patient_details[0]['address'] : ''?>
                                                    </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                            <div class="col-md-3 text-right">
                                            <label for="gender">Type of Resident</label>
                                        </div>
                                        <div class="col-md-9">
                                        <select name="resident_type" class="form-control" id="">
                                            <option value="1" <?=!empty($patient_details[0]['resident_type']) && $patient_details[0]['resident_type'] == 1 ? 'selected' : '';?>>Landed House</option>
                                            <option value="2" <?=!empty($patient_details[0]['resident_type']) && $patient_details[0]['resident_type'] == 2 ? 'selected' : '';?>>Multi Storey Residence with lift</option>
                                            <option value="3" <?=!empty($patient_details[0]['resident_type']) &&$patient_details[0]['resident_type'] == 3 ? 'selected' : '';?>>Multi Storey Residence without lift</option>
                                            <option value="4" <?=!empty($patient_details[0]['resident_type']) &&$patient_details[0]['resident_type'] == 4 ? 'selected' : '';?>>Nursing Home/Hostpital</option>
                                        </select>
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                            <div class="col-md-3 text-right">
                                                    <label for="security_type">Type of Scurity</label>
                                        </div>
                                        <div class="col-md-9">
                                            <select name="security_type" class="form-control" id="security_type">
                                                <option value="1" <?=!empty($patient_details[0]['security_type']) && $patient_details[0]['security_type'] == 1 ? 'selected' : '';?>>Gated & Guarded</option>
                                                <option value="2" <?=!empty($patient_details[0]['security_type']) && $patient_details[0]['security_type'] == 2 ? 'selected' : '';?>>Gated</option>
                                                <option value="3" <?=!empty($patient_details[0]['security_type']) && $patient_details[0]['security_type'] == 3 ? 'selected' : '';?>>Restricted Access</option>
                                                <option value="4" <?=!empty($patient_details[0]['security_type']) && $patient_details[0]['security_type'] == 4 ? 'selected' : '';?>>None</option>
                                            </select>
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                            <div class="col-md-3 text-right">
                                                <label for="type_of_security">Living Status</label>
                                            </div>
                                        <div class="col-md-9">
                                            <select name="living_status" class="form-control" id="living_status">
                                                <option value="1" <?=!empty($patient_details[0]['living_status'] ) && $patient_details[0]['living_status'] == 1 ? 'selected' : '';?>>Alone</option>
                                                <option value="2" <?=!empty($patient_details[0]['living_status'] ) && $patient_details[0]['living_status'] == 2 ? 'selected' : '';?>>Family</option>
                                            </select>
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                            <div class="col-md-3 text-right">
                                                    <label for="pets_in_house">Pets In House</label>
                                        </div>
                                        <div class="col-md-9">
                                            <select name="pets_in_house" class="form-control" id="gender">
                                                <option value="1" <?=!empty($patient_details[0]['pets_in_house']) && $patient_details[0]['pets_in_house'] == 1 ? 'selected' : '';?>>Dog</option>
                                                <option value="2" <?=!empty($patient_details[0]['pets_in_house']) && $patient_details[0]['pets_in_house'] == 2 ? 'selected' : '';?>>Cat</option>
                                            </select>
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                            <div class="col-md-3 text-right">
                                                <label for="current_living_condition">Current Living Condition</label>
                                            </div>
                                        <div class="col-md-9">
                                            <select name="current_living_condition" class="form-control" id="current_living_condition">
                                                <option value="1" <?=!empty($patient_details[0]['current_living_condition']) && $patient_details[0]['current_living_condition'] == 1 ? 'selected' : '';?>>Private room with ac with attached bathroom</option>
                                                <option value="2" <?=!empty($patient_details[0]['current_living_condition']) && $patient_details[0]['current_living_condition'] == 2 ? 'selected' : '';?>>Private room without ac with attached bathroom</option>
                                                <option value="3" <?=!empty($patient_details[0]['current_living_condition']) && $patient_details[0]['current_living_condition'] == 3 ? 'selected' : '';?>>Private room with ac with common bathroom</option>
                                                <option value="4" <?=!empty($patient_details[0]['current_living_condition']) && $patient_details[0]['current_living_condition'] == 4 ? 'selected' : '';?>>Private room without ac with common bathroom</option>
                                                <option value="5" <?=!empty($patient_details[0]['current_living_condition']) && $patient_details[0]['current_living_condition'] == 5 ? 'selected' : '';?>>Shared Room</option>
                                                <option value="6" <?=!empty($patient_details[0]['current_living_condition']) && $patient_details[0]['current_living_condition'] == 6 ? 'selected' : '';?>>Communal Space</option>
                                            </select>
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="current_care_status ">Current Care Status</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <select name="current_care_status" class="form-control" id="">
                                                     <option value="1" <?=!empty($patient_details[0]['current_care_status']) && $patient_details[0]['current_care_status'] == 1 ? 'selected' : '';?>>Self</option>
                                                     <option value="2" <?=!empty($patient_details[0]['current_care_status']) && $patient_details[0]['current_care_status'] == 2 ? 'selected' : '';?>>Family</option>
                                                     <option value="3" <?=!empty($patient_details[0]['current_care_status']) && $patient_details[0]['current_care_status'] == 3 ? 'selected' : '';?>>Caregiver/maid</option>
                                                     <option value="4" <?=!empty($patient_details[0]['current_care_status']) && $patient_details[0]['current_care_status'] == 4 ? 'selected' : '';?>>Nurse</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="current_care_status ">Age</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input class="form-control  " type="number" name="age" value="<?=!empty($patient_details[0]['age']) ? $patient_details[0]['age'] : ''?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                </fieldset>
                                            <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                            <div class="">
                                                <button class="btn btn-primary" type="submit" name="submit">Submit</button>
                                            </div>
                                            </div>
                                    </form>
                                </fieldset>
								<!-- table-wrapper -->
							</div>
							<!-- section-wrapper -->
							</div>
						</div>

					</div><!--End side app-->

					<!-- Right-sidebar-->
					<?php $this->load->view('backend/right_sidebar');?>
					<!-- End Rightsidebar-->

                    <?php $this->load->view('backend/footer');?>
				</div>
				<!-- End app-content-->
			</div>
		</div>
		<!-- End Page -->

		<!-- Back to top -->
		<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

	<?php $this->load->view('backend/footer_link');?>
	</body>
</html>