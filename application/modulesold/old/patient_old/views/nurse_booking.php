<?php $this->load->view('backend/head_link');?>

	<body class="app sidebar-mini rtl">

		<div class="page">
			<div class="page-main">
			
                <?php $this->load->view('backend/header');?>
             
				<div class="container content-patient">
					<div class="side-app">

                            <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            
                                
                                
                            
                                    
                                <div class="card Relatedpost nested-media">
                                    <div class="card-header">
                                        <h4 class="card-title">Book Nurse : <?php echo $name?></h4>
                                    </div>
                                    <div class="card-body ">
                                        <?php if ($this->session->flashdata('Successfully')): ?>
                            <script>
                                swal({
                                    title: "Done",
                                    text: "<?php echo $this->session->flashdata('Successfully'); ?>",
                                    timer: 1500,
                                    showConfirmButton: false,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?>
        <!-- <form action="patient/patient_book_post" method="post" class="register-form">  -->
        <form action="patient/patient_book_post_view" method="post" class="register-form" id="checkForm"> 
       <input type="hidden" name="nurse_id" value="<?php echo $nurse_id?>">
       <input type="hidden" name="patient_id" value="<?php echo $loginid?>">
       <input type="hidden" name="charge" value="<?php echo $charge?>">
  
                                                  <div class="row ">      
                                                       <div class="col-md-4 col-sm-12 col-lg-4 col-xs-12">
                                                          <label for="firstName">Time(hours)/Per Day</label>
       <input type="number" name="time_in_hours" class="form-control" min="0" max="8" type="text" required="">    
                                                       </div>            
                                                  </div>
                                                  <div class="row">
                                                       <div class="col-md-4 col-sm-12 col-lg-4 col-xs-12">
                                                           <div class="mt-4">
                                                                <div>Day Period</div>
                                                                <div class="c-datepicker-date-editor  J-datepicker-range-day mt-4">
                                                                 
    <input placeholder="From" id="start" name="fromtime" class="c-datepicker-data-input only-date form-control" required="">
                                                                  <span class="c-datepicker-range-separator">-</span>
<input placeholder="To" name="totime" id="datereg" class="c-datepicker-data-input only-date form-control" required="">
                                                                </div>
                                                              </div>            
                                                       </div>            
                                                  </div>
                        
                                                        <div class="form-label mt-4">Other Options</div>
  

                                                        <div class="custom-controls-stacked">
                                                          <?php 
                                                            foreach ($addons as $key => $value) :?>
                                                       
                                                            
                                                          
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input addon" name="option[]" value="<?=$value['id']?>">
                                                                <span class="custom-control-label"><?=$value['title']?></span>
                                                            </label>

                                                            <input type="hidden" name="addon_title[]"value="<?=$value['title']?>">

                                                            <?php endforeach; ?>
                                                         <!--    <label class="custom-control custom-checkbox">
  <input type="checkbox" class="custom-control-input" name="option2" value="2">
                                                                <span class="custom-control-label">Option 2</span>
                                                            </label>
                                                            <label class="custom-control custom-checkbox">
 <input type="checkbox" class="custom-control-input" name="option3" value="3" >
                                                                <span class="custom-control-label">Option 3</span>
                                                            </label> -->
                                                          
                                                        </div>

                                                  <div class="row ">      
                                                       <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                                          <label for="firstName">Notes</label>

                                      <textarea name="Notes"class="form-control"></textarea>                    
        
                                                       </div>            
                                                  </div>  
                                                                  <div class="form-group form-elements">
                      <div class="form-label">Select Payment Method</div>
                      <div class="custom-controls-stacked">
                        <label class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" name="pmethod" value="cash_on_delivery" checked>
                          <span class="custom-control-label">Cash- After Service</span>
                        </label>
                     <label class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" name="pmethod" value="cash_on_delivery">
                          <span class="custom-control-label">Visa/Master Card</span>
                        </label>
                       
                                     <label class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" name="pmethod"  value="cash_on_delivery">
                          <span class="custom-control-label">Bank Wire</span>
                        </label>       
                      </div>
                    </div>   
                                                  <hr>
                                                  <div class="row">
                                                       <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 ">
                                                       <button class="btn btn-primary" type="submit" name="save" id="">Book</button>  
                                                      
                                                       
                                                      </div>
                                                         
                                                  </div>    
                                                </form>


                                     
                                      
                                    </div>
                                </div>
                                
                            </div>
                               
                        </div>

					</div><!--End side app-->

					<!-- Right-sidebar-->
					<?php $this->load->view('backend/right_sidebar');?>
					<!-- End Rightsidebar-->

                    <?php $this->load->view('backend/footer');?>
				</div>
				<!-- End app-content-->
			</div>
		</div>
		<!-- End Page -->

		<!-- Back to top -->
		<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

	<?php $this->load->view('backend/footer_link');?>

<script>
// $('#datereg').datepicker({uiLibrary: 'bootstrap4'});
// $('#start').datepicker({uiLibrary: 'bootstrap4'});
// $('#end').datepicker({uiLibrary: 'bootstrap4'});

$('#datereg').Zebra_DatePicker();
$('#start').Zebra_DatePicker();
$('#end').Zebra_DatePicker();
</script>


<script>
  document.getElementById('checkForm').addEventListener('submit', (e)=> {
    const options = document.querySelectorAll('.addon:checked');

    if (options.length > 3) {
        e.preventDefault();
        swal("Sorry Cannot select more than 3 addons!!");
        return false;
    }
    else{
        return true;
    }
    
    
  });

</script>
     
	</body>
</html>