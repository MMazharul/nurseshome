<?php $this->load->view('backend/head_link');?>

	<body class="app sidebar-mini rtl">

		<!--Global-Loader-->
		<!-- <div id="global-loader">
			<img src="back_assets/images/icons/loader.svg" alt="loader">
		</div> -->

		<div class="page">
			<div class="page-main">
				<!--app-header-->
                <?php $this->load->view('backend/header');?>
                <!-- app-content-->
				<div class="container content-patient">
					<div class="side-app">
					  
				
                        <?php if($this->session->flashdata('msg')){ ?>
                            <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <div class="alert-message">
                                    <span><?=$this->session->flashdata('msg');?></span>
                                </div>
                            </div>
                        <?php } ?> 

                        <div class="row">
							<div class="col-md-12 col-lg-12">
							<div class="card">
								<div class="card-header">
                                    <div class="col-md-10">
                                    <div class="card-title">Book Nurse Preview</div>
                                    </div>
                                    <div class="col-md-2">
                                    <!-- <a href="patient/patient_list" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
										<span> Patient List
										</span>
									</a> -->
                                    </div>
								</div>
								<div class="card-body">
                                	<form action="patient/patient_book_post" method="post" style="font-size: 15px">
                                      <input type="hidden" name="nurse_id" value="<?php echo $nurse_id?>">
                                       <input type="hidden" name="patient_id" value="<?php echo $loginid?>">
                                       <input type="hidden" name="charge" value="<?php echo $charge?>">
                                    <fieldset>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="nrc_passport_id">Nurse name: </label>
                                                </div>
                                                <div class="col-md-9">
                                                    <strong>
                                                        <?=ucwords($nurse_details[0]['first_name'])." ".ucwords($nurse_details[0]['last_name']);?>
                                                    </strong>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="nrc_passport_id">Nurse Image: </label>
                                                </div>
                                                <div class="col-md-9">
                                                    <img src="uploads/<?=$nurse_details[0]['profile_pic'];?>">
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                       
                                    <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="nrc_passport_id">Time (in hours)</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <strong><?php echo $time_in_hours;?></strong>
                                                    <input type="hidden" class="form-control" id="nrc_passport_id" name="time_in_hours"  placeholder="" value="<?php echo $time_in_hours;?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="date_s">Date Start</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <strong><?php echo $fromtime;?></strong>

                                                    <input type="hidden" class="form-control" id="weight" name="fromtime"  placeholder="" value="<?=$fromtime?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="height">Date End</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <strong><?php echo $totime;?></strong>
                                                    <input type="hidden" class="form-control" id="height" name="totime"  placeholder="Height" value="<?php echo $totime;?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="date_s">Total Days</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <strong><?php echo $days;?></strong>

                                                    <input type="hidden" class="form-control" id="weight" name="days"  placeholder="" value="<?=$days?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="height">Total Charge</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <strong><?php echo "RM ".$charge;?></strong>
                                                    <input type="hidden" class="form-control" id="height" name="charge"  placeholder="Height" value="<?=$charge?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="height">Addons</label>
                                                </div>
                                                <div class="col-md-9">
                                                    
                                                    <strong><?php 
                                                        $i=1;
                                                        // $list = implode(', ', $addon_title);
                                                        // print_r($list);
                                                        foreach ($addon_title as $key => $value) {
                                                            echo " ($i) ".$value."<br>" ;
                                                            $i++;
                                                        }
                                                    ?></strong>
                                                    <?php
                                                        foreach ($addons as $key => $value) {?>
                                                            <input type="hidden" class="form-control" id="" name="option[]"  placeholder="Height" value="<?=$value;?>">
                                                        <?php }
                                                    ?>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="height">Notes</label>
                                                </div>
                                                <div class="col-md-9">
                                                    
                                                    <strong><?php 
                                                        echo $notes;
                                                    ?></strong>
                                                    <input type="hidden" class="form-control" id="height" name="Notes"  placeholder="Height" value="<?=$notes?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
                            
                                </fieldset>


                                
                                            <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                            <div class="">
                                                <a href="patient-book-nurse/<?=$nurse_id?>/<?=$loginid?>" class="btn btn-danger">Back</a>
                                                <button class="btn btn-primary" type="submit" name="submit">Confirm</button>
                                            </div>
                                            </div>
                                    </form>
                                </fieldset>
								<!-- table-wrapper -->
							</div>
							<!-- section-wrapper -->
							</div>
						</div>

					</div><!--End side app-->

					<!-- Right-sidebar-->
					<?php $this->load->view('backend/right_sidebar');?>
					<!-- End Rightsidebar-->

                    <?php $this->load->view('backend/footer');?>
				</div>
				<!-- End app-content-->
			</div>
		</div>
		<!-- End Page -->

		<!-- Back to top -->
		<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

	<?php $this->load->view('backend/footer_link');?>
	</body>
</html>