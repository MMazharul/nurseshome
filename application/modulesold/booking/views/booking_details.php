<?php $this->load->view('backend/head_link');?>

    <body class="app sidebar-mini rtl">

        <!--Global-Loader-->
        <!-- <div id="global-loader">
            <img src="back_assets/images/icons/loader.svg" alt="loader">
        </div> -->

        <div class="page">
            <div class="page-main">
                <!--app-header-->
                
                
       <?php $this->load->view('backend/header');?>



        <!-- app-content-->
                <div class="container content-area">
                    <div class="side-app">

<style type="text/css">
            .c-concierge-card__pulse {
                background: #01a400;
                margin-right: 10px;
            }

            .c-concierge-card__pulse, .u-pulse {
                margin: 4px 20px 0 10px;
                    margin-right: 8px;
                width: 10px;
                height: 10px;
                border-radius: 50%;
                background: #14bef0;
                cursor: pointer;
                -webkit-box-shadow: 0 0 0 rgba(40,190,240,.4);
                box-shadow: 0 0 0 rgba(40,190,240,.4);
                -webkit-animation: pulse 1.2s infinite;
                animation: pulse 1.2s infinite;
                display: inline-block;
            }

                .m-right{
                    margin-right: 7px;
                }
                .u-green-text {
                    color: #01a400;
                }

                .modal_mou{
                    text-decoration: underline!important;
                    font-size: 11px!important;
                }


                /*search box css start here*/
                .search-sec{
                    padding: 2rem;
                }
                .search-slt{
                    display: block;
                    width: 100%;
                    font-size: 11px!important;
                    line-height: 1.5;
                    color: #55595c;
                    background-color: #fff;
                    background-image: none;
                    border: 1px solid #f3ecec;
                    height: calc(3rem + 2px) !important;
                    border-radius:0;
                }
                .wrn-btn{
                    width: 100%;
                    font-size: 16px;
                    font-weight: 400;
                    text-transform: capitalize;
                    height: calc(3rem + 2px) !important;
                    border-radius:0;
                }
                @media (min-width: 992px){
                    .search-sec{
                        position: relative;
                        top: -114px;
                        background: rgba(26, 70, 104, 0.51);
                    }
                }

                @media (max-width: 992px){
                    .search-sec{
                        background: #1A4668;
                    }
                }
    </style>
   
    <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                
                     <?php if($this->session->flashdata('msg')){ ?>
                        <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <div class="alert-message">
                                <span><?=$this->session->flashdata('msg');?></span>
                            </div>
                        </div>
                    <?php } ?> 
                
                    <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">   
                    <div class="card Relatedpost nested-media">
                        <!-- <div class="card-header">
                            <h4 class="card-title">Search for Care</h4>
                        </div> -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <div class="media media-lg mt-0">
                                        <div class="media-body">
                                            <h2 class="mt-0">Booking ID : <?=!empty($com_book_id) ? $com_book_id : '';?>

                                            
                                            </h2>

                                            <div style="margin-top: -10px;margin-bottom: 13px;"><span class="u-green-text"></span><span class="u-green-text" data-reactid="211">

                                            Nurse Name:</span><span>

                                            <?php 
                                            if(!empty($nurse_details[0]['first_name'])){
                                                echo ucwords($nurse_details[0]['first_name']); 
                                            }
                                            echo " ";
                                            if (!empty($nurse_details[0]['last_name'])) {
                                                echo ucwords($nurse_details[0]['last_name']);
                                            }?>
                                            <br>
                                            <span class="u-green-text" data-reactid="211">

                                            Patient Name:</span>
                                            <?php 
                                            if(!empty($booking_details[0]['first_name'])){
                                                echo ucwords($booking_details[0]['first_name']); 
                                            }
                                            echo " ";
                                            if (!empty($booking_details[0]['last_name'])) {
                                                echo ucwords($booking_details[0]['last_name']);
                                            }?>
                                             | <?=$book_day?></span></div>

                                              
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <div class="media media-lg mt-0">
                                        <div class="media-body">
                                                <h3 class="text-right"><span class="u-green-text"> 
                                                 <?=!empty($booking_details[0]['book_charge']) ? ($booking_details[0]['book_charge'] == 0 ? '25.00' : $booking_details[0]['book_charge']) : ''?>
                                                </span></h3>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <hr class="m-0 mb-4">
                            
                            <p><?=$booking_details[0]['notes']?></p>

                            <br>

                            <table class="table-bordered" cellpadding="8">
                                <thead class="bg-light text-dark">
                                    <tr>
                                        <th>Time (Hours)</th>
                                        <th>Days</th>
                                        <th>Duration (Days)</th>
                                        <th class="text-right">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?=!empty($booking_details[0]['time_in_hours']) ? $booking_details[0]['time_in_hours'] : ''?></td>
                                        <td><?=!empty($from_date) ? $from_date : ''?> - <?=!empty($to_date) ? $to_date : ''?></td>
                                        <td><?=!empty($datediff) ? $datediff : ''?></td>
                                        <td class="text-right"><?=!empty($booking_details[0]['book_charge']) ? ($booking_details[0]['book_charge'] == 0 ? '25.00' : $booking_details[0]['book_charge']) : ''?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <p class="text-right"><strong>Total : <?=$total_charge?></strong></p>
                         
                        </div>   
                    </div>
                    </div>
                    </div>

                    <?php if ($booking_details[0]['book_status'] != 1) : ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">   
                    <div class="card">
                        <!-- <div class="card-header">
                            <h4 class="card-title">Search for Care</h4>
                        </div> -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                               
                                        
                                        <div class="text-center d-block">
                                            <i class="fa fa-rocket text-info" style="font-size: 40px"></i>
                                            <!-- <br> -->
                                            <!-- <span style="font-size: 20px; text-transform: uppercase;"><strong>Service Started</strong></span> -->
                                            <br>
                                            <span>
                                            Assessment Done By <strong><?php 
                                            if(!empty($nurse_details[0]['first_name'])){
                                                echo ucwords($nurse_details[0]['first_name']); 
                                            }
                                            echo " ";
                                            if (!empty($nurse_details[0]['last_name'])) {
                                                echo ucwords($nurse_details[0]['last_name']);
                                            }?></strong>
                                            </span>
                                            <hr>
                                        </div>

                
                  
                        <?php 

                        if ($row_count > 0) { ?>

                        <fieldset class="biodata">
                        <!-- <h4>Nutrition</h4> -->
                            <table class="table table-bordered">
                              <tr>
                                <th><strong>Eating Disorder</strong></th>
                                <th><?php 
                                  if (!empty($assessment_details[0]['eating_disorder'])) {
                                    if ($assessment_details[0]['eating_disorder'] == 1) {
                                       echo "Nil";
                                    }elseif ($assessment_details[0]['eating_disorder'] == 2) {
                                      echo "Malnourished";
                                    }
                                    elseif ($assessment_details[0]['eating_disorder'] == 3) {
                                      echo "Obese";
                                    }
                                  };
                                  ?></th>
                              </tr>

                              <tr>
                                <th><strong>Diet Type</strong></th>
                                <th><?php 
                                  if (!empty($assessment_details[0]['diet_type'])) {
                                    if ($assessment_details[0]['diet_type'] == 1) {
                                       echo "Regular";
                                    }elseif ($assessment_details[0]['diet_type'] == 2) {
                                      echo "Theraputic";
                                    }
                                    elseif ($assessment_details[0]['diet_type'] == 3) {
                                      echo "Supplement (Endsure etc)";
                                    }
                                    elseif ($assessment_details[0]['diet_type'] == 4) {
                                      echo "Parental (TPN)";
                                    }
                                  };
                                  ?></th>
                              </tr>

                              <tr>
                                <th><strong>Musculosketel</strong></th>
                                <th><?php 
                                  if (!empty($assessment_details[0]['musculosketel'])) {
                                    if ($assessment_details[0]['musculosketel'] == 1) {
                                       echo "Normal";
                                    }elseif ($assessment_details[0]['musculosketel'] == 2) {
                                      echo "Paralysis-Free hand notes";
                                    }
                                    elseif ($assessment_details[0]['musculosketel'] == 3) {
                                      echo "Joint Swelling-Free hand notes";
                                    }
                                    elseif ($assessment_details[0]['musculosketel'] == 4) {
                                      echo "Pain-Free hand notes";
                                    }
                                    elseif ($assessment_details[0]['musculosketel'] == 5) {
                                      echo "Others-Free hand notes";
                                    }
                                  };
                                  ?></th>
                              </tr>

                               <tr>
                                <th><strong>Genitallia</strong></th>
                                <th><?php 
                                  if (!empty($assessment_details[0]['genitallia'])) {
                                    if ($assessment_details[0]['genitallia'] == 1) {
                                       echo "Normal";
                                    }elseif ($assessment_details[0]['genitallia'] == 2) {
                                      echo "Others-FHN";
                                    }
                                  };
                                  ?></th>
                              </tr>

                               <tr>
                                <th><strong>Vascular Access</strong></th>
                                <th><?php 
                                  if (!empty($assessment_details[0]['vascular_access'])) {
                                    if ($assessment_details[0]['vascular_access'] == 1) {
                                       echo "Normal";
                                    }elseif ($assessment_details[0]['vascular_access'] == 2) {
                                      echo "Others-FHN";
                                    }
                                  };
                                  ?></th>
                              </tr>
                               <tr>
                                <th><strong>Fall Risk</strong></th>
                                <th><?php 
                                  if (!empty($assessment_details[0]['fall_risk'])) {
                                    if ($assessment_details[0]['fall_risk'] == 1) {
                                       echo "No Risk";
                                    }elseif ($assessment_details[0]['fall_risk'] == 2) {
                                      echo "At Risk";
                                    }elseif ($assessment_details[0]['fall_risk'] == 3) {
                                      echo "High Risk";
                                    }
                                  };
                                  ?></th>
                              </tr>

                               <tr>
                                <th><strong>Fall Risk</strong></th>
                                <th><?=!empty($assessment_details[0]['fall_history']) ? $assessment_details[0]['fall_history']  : ''?></th>
                              </tr>


                               <tr>
                                <th><strong>Walking Device</strong></th>
                                <th><?=!empty($assessment_details[0]['walking_device']) ? $assessment_details[0]['walking_device'] : ''?></th>
                              </tr>

                               <tr>
                                <th><strong>Urine Colour</strong></th>
                                <th><?php 
                                  if (!empty($assessment_details[0]['urine_colour'])) {
                                    if ($assessment_details[0]['urine_colour'] == 1) {
                                       echo "Clear";
                                    }elseif ($assessment_details[0]['urine_colour'] == '2_1') {
                                      echo "Cloudy";
                                      if (!empty($assessment_details[0]['cloudy_sub'])) {
                                          if ($assessment_details[0]['cloudy_sub'] ==1) {
                                            echo "<small>\n\r(Dark)</small>";
                                          }elseif ($assessment_details[0]['cloudy_sub'] ==2) {
                                            echo "<small>\n\r(Hemanturia)</small>";
                                            
                                          }
                                      }
                                    }elseif ($assessment_details[0]['urine_colour'] == 3) {
                                      echo "Others (Endsure etc)";
                                    }elseif ($assessment_details[0]['urine_colour'] == 4) {
                                      echo "Normal Voiding";
                                    }
                                  };
                                  ?></th>
                              </tr>

                              <tr>
                                <th><strong>Incontinent/Dysuria/Catheter</strong></th>
                                <th><?php 
                                  if (!empty($assessment_details[0]['incontinent_dysuria_catheter'])) {
                                    if ($assessment_details[0]['incontinent_dysuria_catheter'] == 1) {
                                       echo "FHN";
                                    }elseif ($assessment_details[0]['incontinent_dysuria_catheter'] == 2) {
                                      echo "Dialysis";
                                    }elseif ($assessment_details[0]['incontinent_dysuria_catheter'] == 3) {
                                      echo "Others-FHN";
                                    }
                                  };
                                  ?></th>
                              </tr>

                              <tr>
                                <th><strong>Home Assessment</strong></th>
                                <th><?=!empty($assessment_details[0]['home_assessment'])? trim($assessment_details[0]['home_assessment']): ''?></th>
                              </tr>
                            </table>   
                            <div class="row">
                            <div class="col-md-12">

                                
                                
                                </div>

                            </div>
                     <!--        <h4>Genitourinary</h4> -->
                           

                        </fieldset>


                        <?php } ?>


                            </div>
                            </div>

                           

                            <?php if($booking_details[0]['book_status'] == 3) : ?>
                             <div class="text-center d-block mt-5">
                                  <i class="fa fa-check-square-o text-info" style="font-size: 40px"></i>
                                  <br>
                                  <span style="font-size: 20px; text-transform: uppercase;">
                                      <strong>service has been marked as completed by Nurse</strong>
                                    
                                    </span>

                                  <br>
                              </div>
                           <hr>
                          <?php elseif($booking_details[0]['book_status'] == 5):?>

                           <!--  <div class="text-center d-block mt-5">
                                <i class="fa fa-comment-o text-info" style="font-size: 30px"></i>
                                <br>
                                <span style="font-size: 20px; text-transform: uppercase;"><strong>Review for Nurse</strong></span>
                                <br>
                            </div> -->
                            <hr>

                            <h5>Review By Nurse</h5>
            
                            <?php if(isset($review_by_nurse) && count($review_by_nurse) > 0):?>
                            <div class="row">
                                <div class="col-md-2">
                                    <img class="rounded-circle" width=70% src="uploads/<?=$review_by_nurse[0]['profile_pic']?>" alt="">
                                </div>
                                <div class="col-md-10 pt-4">
                                    <h4><?=$review_by_nurse[0]['first_name']." ".$review_by_nurse[0]['last_name']?>
                                        <span>
                                            <i class="fa fa-star text-warning"></i> <strong class="text-warning"><?=$review_by_nurse[0]['rating']?></strong>
                                        </span> 
                                    </h4>
                                    <p style="font-size: 14px"><?=$review_by_nurse[0]['comment']?></p>
                                </div>
                            </div>
                            <?php else: ?>
    
                            <div class="row">
                                <div class="col-md-2">
                                    <img class="rounded-circle" width=70% src="uploads/<?=$nurse_details[0]['profile_pic']?>" alt="">
                                </div>
                                <div class="col-md-10 pt-4">
                                    <h4><?=$nurse_details[0]['first_name']." ".$nurse_details[0]['last_name']?>
                                    </h4>

                                    <p class="text-danger">-Hasn't Reviewed Yet-</p>
                              
                                </div>
                            </div>



                            <?php endif; ?>
                            <hr>
                            <h5>Review By Patient</h5>

                             <?php if(isset($review_by_patient) && count($review_by_patient) > 0):?>
                            <div class="row">
                                <div class="col-md-2">
                                    <img class="rounded-circle" width=70% src="uploads/<?=$review_by_patient[0]['profile_pic']?>" alt="">
                                </div>
                                <div class="col-md-10 pt-4">
                                    <h4><?=$review_by_patient[0]['first_name']." ".$review_by_patient[0]['last_name']?>
                                        <span>
                                            <i class="fa fa-star text-warning"></i> <strong class="text-warning"><?=$review_by_patient[0]['rating']?></strong>
                                        </span> 
                                    </h4>
                                    <p style="font-size: 14px"><?=$review_by_patient[0]['comment']?></p>
                                </div>
                            </div>
                            <?php else: ?>
    
                            <div class="row">
                                <div class="col-md-2">
                                    <img class="rounded-circle" width=70% src="uploads/<?=$booking_details[0]['profile_pic']?>" alt="">
                                </div>
                                <div class="col-md-10 pt-4">
                                    <h4><?=$booking_details[0]['first_name']." ".$booking_details[0]['last_name']?>
                                    </h4>

                                    <p class="text-danger">-Hasn't Reviewed Yet-</p>
                              
                                </div>
                            </div>



                            <?php endif; ?>

                            


                            <?php endif; ?>
                         
                        </div>   
                    </div>
                    </div>

                    </div>
                        

                    <?php endif; ?>
                </div>
                <div class="col-md-3">
                    
                    <div class="row">

                    <?php 
                        if (isset($nurse_care_note_all) && count($nurse_care_note_all)>0) : ?>
                            <div class="col-md-12">
                        
                        <div class="card">
                            <div class="card-header">
                                   <h5 class="card-title"> Previous notes </h5>
                                </div>
                             
                            <div class="card-body">
                               
                                 <table>
                                    <thead>
                                        <tr>
                                            <td>Sl.</td>
                                            <td>Date</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1; 
                                        foreach($nurse_care_note_all as $values):?>
                                        <tr>   
                                            <td><?=$i ?></td>
                                            <td><a class="fetch_nurse_notes" href="javascript:void(0)" data-id="<?=$values['id']?>" data-toggle="modal" data-target="#exampleModal2">
                                                    <?php  
                                                  $date=date_create($values['date']);
                                                  echo date_format($date,"d-M-Y");?>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php $i++; endforeach; ?>
                                    </tbody>
                                </table> 
                            </div>
                             
                        </div>

                    </div>
                      <?php   endif; ?>

                    

                    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Summary</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-label mb-2 text-center" id="note_date"> </div>        
                                        <div class="form-label d-block row">
                                       
                                        

                                        <div class="col-12">
                                             <h5> Completed Care: </h5>
                                           <table class="table table-bordered">
                                           <thead>
                                               <tr>
                                                    <td>Title</td>
                                                    <td>Note</td>
                                               </tr>
                                           </thead>
                                               <tbody id="care_details">
                                               </tbody>
                                           </table>
                                        
                                        </div>

                                        <div class="col-12">
                                        <h5>Basic Vital Signs</h5>
                                        <table class="table table-bordered">
                                           <tbody id="note_summary">
                                               
                                           </tbody>
                                          </table>
                                        </div>

                                        </div>
                                    
                                </div>
                                   
                                </div>
                                
                            </div>
                        </div>






                    </div>

                

                    </div>


                </div>
            
    </div>

                </div><!--End side app-->

                    <!-- Right-sidebar-->
                <?php $this->load->view('backend/right_sidebar');?>

                    <?php $this->load->view('backend/footer');?>

                </div>
                <!-- End app-content-->
            </div>
        </div>
        <!-- End Page -->

        <!-- Back to top -->
        <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

    <?php $this->load->view('backend/footer_link');?>
<script>
    let allDate = document.querySelectorAll('.fetch_nurse_notes');

    allDate.forEach(function (eachDate) {
        eachDate.addEventListener('click', function(){
            let id = this.dataset.id
            let note_summary = document.getElementById('note_summary');
            let care_details = document.getElementById('care_details');
            let noteDate = document.getElementById('note_date');

            $.ajax({
            url: "<?php echo base_url();?>nurse/fetch_note_summary_by_id",
            type: 'POST',
            datatype: 'JSON',
            data: {id: id},
            success: function(data) {
            var result = JSON.parse(data);
            // console.log(result.nurse_note_details[0].details);
            care_details.innerHTML = '';

                noteDate.innerHTML = `Note Date: ${result.nurse_note.date}`;

                result.nurse_note_details.forEach(function (val){
                care_details.innerHTML += `<tr>
                                            <th width=60%>${val.details}</th> 
                                            <th>${val.care_note}</th>
                                          </tr>`;
                })
                
                note_summary.innerHTML = `<tr> <td>Temp</td><td>${result.nurse_note.temp}</td> </tr> 
                                          <tr> <td>Bp</td> <td>${result.nurse_note.bp}</td> </tr>
                                          <tr> <td>Pulse</td> <td>${result.nurse_note.pulse}</td> </tr>
                                          <tr> <td>Pain Sore</td><td>${result.nurse_note.pain_sore}</td> </tr>
                                          <tr> <td>Respiration</td> <td>${result.nurse_note.respiration}</td> </tr>
                                          <tr> <td>Spo2</td> <td>${result.nurse_note.spo2}</td> </tr>
                                          <tr> <td>Etc</td> <td>${result.nurse_note.etc}</td> </tr>
                                          <tr> <td>Photo Wounds</td> <td> <img src="uploads/${result.nurse_note.photo_wounds}" alt="" /> </td> </tr>
                                        <tr> <td>Summary</td> <td>${result.nurse_note.summary}</td> </tr>
                                          `;


                
            },
            error: function() {
            alert('Something is wrong');
            }
        });
        })
    });

    // fetch.addEventListener('click', (e)=>{

        

    // })


    // $('.fetch_nurse_notes').click(function()
    // {
    //     $.each( this, function( value ) {
    //       alert($(this).data('id'))
    //     });
    //     // alert('clicked');
        

    //    // var class_id = $(this).val();
    //    // html2 = html = '';
  
    // });

</script>
<script>
const cloudy = document.querySelectorAll('.urine_colour');
const cloudySubSelect = document.getElementById('cloudy_sub_select');
// const existCloudySubSelect = document.getElementById('exist_sub_select');

// if (existCloudySubSelect == "2_1") {
//     cloudySubSelect.style.display = 'block';
// }
// else{
//     cloudySubSelect.style.display = 'none';
// }

for (let i = 0; i < cloudy.length; i++) {
    cloudy[i].onchange = () => {
    if(cloudy[i].value == "2_1"){
        cloudySubSelect.style.display = 'block';
    }
    else {
        cloudySubSelect.style.display = 'none';
    }
}
}

</script>
<script type="text/javascript">
        
    const getStarted = document.getElementById('getStarted');
    

    getStarted.addEventListener('click', function(e){
    e.preventDefault();
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        cancelButtonColor: '#F27474',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
        },
         function(isConfirm){
           if (isConfirm){
             // swal("Shortlisted!", "Candidates are successfully shortlisted!", "success");
             window.location.href = getStarted.getAttribute('href');
            } else {
              swal("Cancelled", "Your imaginary file is safe :)", "error");
                 e.preventDefault();
            }
        })
    });

</script>  
<script>
    const serviceAccepted = document.getElementById('serviceAccepted');
    const serviceRejected = document.getElementById('serviceRejected');
    serviceAccepted.addEventListener('click', function(e){
    e.preventDefault();
    swal({
        title: "Are you sure?",
        // text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        cancelButtonColor: '#F27474',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
        },
         function(isConfirm){
           if (isConfirm){
             window.location.href = serviceAccepted.getAttribute('href');
            } else {
              swal("Cancelled", "", "error");
                 e.preventDefault();
            }
        })
    });

    serviceRejected.addEventListener('click', function(e){
    e.preventDefault();
    swal({
        title: "Are you sure?",
        // text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        cancelButtonColor: '#F27474',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
        },
         function(isConfirm){
           if (isConfirm){
             window.location.href = serviceRejected.getAttribute('href');
            } else {
              swal("Cancelled", "", "error");
                 e.preventDefault();
            }
        })
    });

</script>
    </body>
</html>