<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Patient extends MX_Controller {

    //public $counter=0;
    function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->model('patient_model');
        $this->load->model('nurse/nurse_model');
        // $this->load->model('product/product_model');
        // $this->load->helper('text');
        // $this->load->helper(array('form', 'url'));
        // $this->load->helper('inflector');
        //$this->load->library('encrypt');
        // $this->load->model('home_model');
        // $this->load->model('admin/admin_model');
        // $this->load->model('product/product_model');
        $this->login_id = $this->session->userdata('loginid');
    }
    // public function index()
    // {
  
    //     $this->load->view('index');
        
    // }

    //==========================BOOK NURSE=========================== //
    public function book_nurse()
    {
        $id = $this->uri->segment(2);
        $data['loginid'] =$this->uri->segment(3);
        $nurse_det=$this->patient_model->select_with_where('*',"loginid=".$id,'users');
        $data['addons']=$this->patient_model->select_with_where('*','status=1','addons');
        $data['name']=$nurse_det[0]['first_name']." ".$nurse_det[0]['last_name'];
        $data['nurse_id']=$nurse_det[0]['loginid'];
        $data['charge']=$nurse_det[0]['charge'];
        $data['css_to_load'] = array('datepicker.css');
        $data['js_to_load'] = array('datepicker.all.js','datepicker.en.js');
        $this->load->view('nurse_booking', $data);
        
    }
    public function bookings()
    {
        // $data['css_to_load'] = array('datepicker.css');
        // $data['js_to_load'] = array('datepicker.all.js','datepicker.en.js');
        $data['loginid']=$this->session->userdata('loginid');

        $data['booking_details_new'] = $booking_details_new =$this->patient_model->special_join_query_all($data['loginid']);
        

        $data['new_count']=count($data['booking_details_new']);

        $data['booking_details_active']=$this->patient_model->special_join_query_booking($data['loginid'],'2');

        $data['active_count']=count($data['booking_details_active']);
        $data['booking_details_cancel']=$this->patient_model->special_join_query_booking($data['loginid'],'7');
        $data['cancel_count']=count($data['booking_details_cancel']);
        $data['booking_details_completed']=$this->patient_model->special_join_query_booking($data['loginid'],'6');
        $data['completed_count']=count($data['booking_details_cancel']);

        $data['form_title'] = 'My Bookings';
        
        $this->load->view('nurse_booking_list', $data);
        
    }

    public function transaction()
    {
        // $data['css_to_load'] = array('datepicker.css');
        // $data['js_to_load'] = array('datepicker.all.js','datepicker.en.js');
        $data['loginid']=$this->session->userdata('loginid');

        $data['booking_details_new']=$this->patient_model->special_join_query_booking($data['loginid'],'1');



        $data['new_count']=count($data['booking_details_new']);
        $data['booking_details_active']=$this->patient_model->special_join_query_booking($data['loginid'],'2');
        $data['active_count']=count($data['booking_details_active']);
        $data['booking_details_cancel']=$this->patient_model->special_join_query_booking($data['loginid'],'7');
        $data['cancel_count']=count($data['booking_details_cancel']);
        $data['booking_details_completed']=$this->patient_model->special_join_query_booking($data['loginid'],'6');
        $data['completed_count']=count($data['booking_details_cancel']);
        $data['form_title'] = 'My Transaction List';
        
        $this->load->view('nurse_transaction_list', $data);
        
    }

    public function patient_book_post($value='')
    {
    $nurse_id=$this->input->post('nurse_id');
    $time_in_hours=$this->input->post('time_in_hours');
    $fromtime=$this->input->post('fromtime');
    $totime=$this->input->post('totime');
    $options=$this->input->post('option');
    // $option2=$this->input->post('option2');
    // $option3=$this->input->post('option3');
    $patient_id=$this->input->post('patient_id');
    $notes=$this->input->post('Notes');
    $charge=$this->input->post('charge');
    //$s=$this->patient_model->insert_ret('nurse_booking',$book);
    $option = $option2 = $option3 = '';
    
    if (!empty($options[0])) {
       $option =  $options[0];
    }
    if (!empty($options[1])) {
        $option2 = $options[1];
    }
    if (!empty($options[2])) {
        $option3 = $options[2];
    }
    

    $if_exist = $this->patient_model->select_with_where('*',"nurse_id=".$nurse_id." and fromtime='$fromtime' and totime='$totime'",'nurse_booking');

    if (count($if_exist) > 0) {

        $this->session->set_flashdata('type', 'error');
        $this->session->set_flashdata('msg', 'Nurse already been booked for this date!');
       
    }
    else{
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', 'Booking Successfully Done !');

        $book = array('nurse_id' => $nurse_id,'time_in_hours'=>$time_in_hours,'fromtime'=>$fromtime,'totime'=>$totime,'addons_one'=>$option,'addons_two'=>$option2,'addons_three'=>$option3,'patient_id'=>$patient_id,'adddate'=>date("Y-m-d H:i:s"),'notes'=>$notes,'book_charge'=>$charge);
        $book_final=$this->patient_model->insert_ret('nurse_booking',$book);
    }

    // $this->session->set_flashdata('msg',"Booking Confirmation"); 
    // echo $nurse_id;
    // echo "</br>";
    // echo $patient_id;
    //redirect("patient-book-nurse-preview/$nurse_id/$patient_id/$s",'refresh');  
    //redirect("patient-book-nurse/$nurse_id/$patient_id",'refresh');  
  
    redirect("nurse-bookings-for-patient");
    // bookings
    }

    public function patient_book_post_view(){

        $nurse_id=$this->input->post('nurse_id');
        $time_in_hours=$this->input->post('time_in_hours');
        $fromtime=$this->input->post('fromtime');
        $totime=$this->input->post('totime');
        $option=$this->input->post('option');
        // $option2=$this->input->post('option2');
        // $option3=$this->input->post('option3');
        $patient_id=$this->input->post('patient_id');
        $notes=$this->input->post('Notes');
        $charge=$this->input->post('charge');

        $arr = array();
        $addon_sum = 0;

        

        // print_r($data['patient_details']);exit;


        foreach ($option as $key => $value) {
            $addons = $this->patient_model->select_with_where('title,price',"id=".$value,'addons');
            $arr[$key]['title'] = $addons[0]['title'];
            $arr[$key]['price'] = $addons[0]['price'];
            $addon_sum += $arr[$key]['price'];
        }

        



        // echo $addon_sum;
        // exit;

        $st = strtotime($fromtime);
        $end = strtotime($totime);
        $datediff = ($end - $st);
        $total_days = round($datediff / (60 * 60 * 24))+1;

        $total_charge = (($charge*$time_in_hours)*$total_days)+$addon_sum;

        $nurse_details = $this->patient_model->select_with_where('*',"loginid=".$nurse_id,'users');

        $data = array(
            'loginid' => $this->login_id,
            'nurse_id' => $nurse_id,
            'time_in_hours'=>$time_in_hours,
            'fromtime'=>$fromtime,
            'totime'=>$totime,
            'addons'=>$option,
            'patient_id'=>$patient_id,
            'adddate'=>date("Y-m-d H:i:s"),
            'notes'=>$notes,
            'charge'=>$total_charge,
            'addon_title'=> $arr,
            'nurse_details'=>$nurse_details,
            'days' => $total_days
        );

        $data['patient_details'] = $this->patient_model->select_with_where('*',"loginid=".$this->login_id,'users'); 


        $this->load->view('manage_patient/patient_book_post_view', $data);
        // echo $total_charge;
    }   


    public function book_nurse_preview($value='')
    {
        $nurse_id = $this->uri->segment(2);
        $patient_id =$this->uri->segment(3);
        $book_id =$this->uri->segment(4);
        $book_details=$this->patient_model->special_join_query_booking_single($data['loginid'],'7');
        $this->load->view('nurse_booking_list', $data);


    } 
    public function review($id)
    {
        $data['form_title'] = 'Review';

        $booking_id = $this->uri->segment(2); 

        $data['booking_details']=$this->nurse_model->select_where_join('user_role, loginid,patient_bookid,
        first_name,
        last_name,
        contact_no,
        profile_pic,
        email,
        time_in_hours,
        fromtime,
        totime,
        adddate,
        notes,
        book_status,
        book_charge,
        nurse_id,
        patient_id','nurse_booking','users','users.loginid=nurse_booking.patient_id',
        "patient_bookid=".$booking_id);

        $data['assessment_details'] = $this->nurse_model->select_with_where('*','patient_book_id= '.$booking_id  ,' patient_assessment_by_nurse');

        $data['review_by_nurse'] = $this->nurse_model->select_where_left_join('user_id,review.user_role,first_name,review.patient_bookid,
            last_name,profile_pic,comment,rating',
            'users','review','users.loginid=review.user_id',
            'patient_bookid='.$booking_id.' and user_id='.$data['booking_details'][0]['nurse_id']);

        $data['review_by_patient'] = $this->nurse_model->select_where_left_join('user_id,review.user_role,first_name,review.patient_bookid,
            last_name,profile_pic,comment,rating',
            'users','review','users.loginid=review.user_id',
            'patient_bookid='.$booking_id.' and user_id='.$data['booking_details'][0]['patient_id']);

        // $data['patient_details'] = $this->nurse_model->select_with_where('*','loginid= '.$data['booking_details'][0]['patient_id']  ,' users');

        $data['nurse_details'] = $this->nurse_model->select_with_where('*','loginid= '.$data['booking_details'][0]['nurse_id']  ,' users');

        // print_r($data['assessment_details'][0]);
        // exit;
        $date = new DateTime($data['booking_details'][0]['adddate']);
        $from_date = new DateTime($data['booking_details'][0]['fromtime']);
        $to_date = new DateTime($data['booking_details'][0]['totime']);

        $data['total_charge'] = $data['booking_details'][0]['book_charge'];
        // * $data['booking_details'][0]['time_in_hours'];

        $data['row_count'] = count($data['assessment_details']);

        if ($data['total_charge'] == 0) {

            $data['total_charge'] = "25.00";

        }

        $data['book_day'] = $date->format('M j');

        $data['from_date'] = $from_date->format('M j ,Y');

        $data['to_date'] = $to_date->format('M j ,Y');

        $st = strtotime($data['booking_details'][0]['fromtime']);

        $end = strtotime($data['booking_details'][0]['totime']);

        $data['datediff'] = round(($end - $st) / (60 * 60 * 24));

        $data['com_book_id'] = "#".$booking_id."".$data['booking_details'][0]['time_in_hours']."".$date->format('Ymd');

        $data['care_for_patient'] = $this->nurse_model->select_where_left_join('care_for_patient_other_details.id as cof_id, 
             care_for_patient_other_details.details as cof_details',
            'care_for_patient_other_details',
            'care_for_patient_others',
            'care_for_patient_other_details.id=care_for_patient_others.other_type',
            'care_for_patient_others.patient_id='.$data['booking_details'][0]['patient_id']);


        $data['nurse_care_note'] = $this->nurse_model->select_with_where('id,temp,bp,pulse,pain_sore, respiration, spo2, etc, summary, photo_wounds',"patient_book_id=".$data['booking_details'][0]['patient_bookid']." and date=curdate()",'nurse_note_for_patient');


        $data['nurse_care_note_all'] = $this->nurse_model->select_with_where('id,patient_book_id, date',"patient_book_id=".$data['booking_details'][0]['patient_bookid'],'nurse_note_for_patient');


        if (count($data['nurse_care_note']) > 0) {

            $data['nurse_care_note_details'] = $nurse_care_note_details = $this->nurse_model->select_with_where('care_for_patient_id, care_note',"nurse_note_id=".$data['nurse_care_note'][0]['id'],'nurse_note_care_details');

            $data['care_note_id'] = $note_id = array_column($nurse_care_note_details, 'care_for_patient_id');

            $care_note = array_column($nurse_care_note_details, 'care_note');
              $new_arr = array();

            foreach ($note_id as $key => $value)
            {
                $data['care_note'][$value] = $care_note[$key];

            }
        }
        
        $this->load->view('patient/add_review', $data);
    } 

    public function add_review(){

        $user_id = $this->input->post('user_id');
        $user_role = $this->input->post('user_role');
        $patient_bookid = $this->input->post('patient_bookid');
        $rating = $this->input->post('rating');
        $comment = html_escape(trim($this->input->post('comment')));

        $if_exist = $this->nurse_model->select_with_where('*',"patient_bookid=".$patient_bookid." and user_id=".$user_id,'review');

        $data = array(
            "user_id" => $user_id,
            "user_role" => $user_role,
            "patient_bookid" => $patient_bookid,
            "comment" => "$comment",
            "rating" => "$rating"
        );

        if (count($if_exist) > 0) {
           #update
            $condition = array('patient_bookid' => $patient_bookid, 'user_id' => $user_id);

            $this->nurse_model->update_with_multiple_condition($condition,'review',$data);
           // $this->nurse_model->update_function('patient_bookid', $patient_bookid, 'review', $data);
           $this->session->set_flashdata('type', 'success');
           $this->session->set_flashdata('msg', 'Review Added Successfully!!');
        } else {
           #insert
           $this->nurse_model->insert('review',$data);
           $this->session->set_flashdata('type', 'success');
           $this->session->set_flashdata('msg', 'Review Updated!!');
        }


        redirect('add-review/'.$patient_bookid, 'refresh');

    }

    public function service_accepted($id){

        $data['book_status'] = 5;

        $this->nurse_model->update_function('patient_bookid', $id, 'nurse_booking',$data);

        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', 'Service Accepted!');

        redirect('add-review/'.$id, 'refresh');
    }
    public function service_rejected($id){

        $data['book_status'] = 4;

        $this->nurse_model->update_function('patient_bookid', $id, 'nurse_booking',$data);

        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', 'Service Rejected!');

        redirect('add-review/'.$id, 'refresh');
    }

    public function care_for_patient()
    {
        $data['form_title'] = 'Care for patient';

        $data['patient_details'] = $this->patient_model->select_with_where('*',"loginid=".$this->login_id,'users'); 

        // $data['care_for_patient'] = $this->patient_model->select_with_where('*',"patient_id=".$this->login_id,'care_for_patient'); 
        
        $data['care_for_patient_other_details'] = $this->patient_model->select_all('care_for_patient_other_details','*'); 

        $exist_accompany = $this->patient_model->select_with_where('*',"patient_id=".$this->login_id,'care_for_patient_accompany');

        $exist_other = $this->patient_model->select_with_where('other_type',"patient_id=".$this->login_id,'care_for_patient_others'); 

        $data['care_for_patient_others'] =  array_column($exist_other, 'other_type');
        $data['care_for_patient_accompany'] =  array_column($exist_accompany, 'type');
        
        $this->load->view('patient/care_for_patient', $data);
    } 

     public function care_for_patient_update($id)
        {
        // print_r($_POST);
        // exit;

        $accompaniment = $this->input->post('accompaniment');
        $other_care = $this->input->post('care_for_patient');

        $data['patient_id'] = $data2['patient_id'] = $id;

        $data['created_at'] = $data2['created_at'] = date('Y-m-d H:i:s');

        $exist_patient_accompany = $this->patient_model->select_with_where('*',"patient_id=".$this->login_id,'care_for_patient_accompany'); 

        $exist_patient_others = $this->patient_model->select_with_where('*',"patient_id=".$this->login_id,'care_for_patient_others'); 



       if (!empty($accompaniment)) {
           if(count($exist_patient_accompany) > 0) {
            $this->patient_model->delete('care_for_patient_accompany', 'patient_id='.$this->login_id);
            foreach ($accompaniment as $key => $value) {
                $data['type'] = $value;
                $this->patient_model->insert('care_for_patient_accompany', $data);
            }    
            } else {
                foreach ($accompaniment as $key => $value) {
                $data['type'] = $value;
                $this->patient_model->insert('care_for_patient_accompany', $data);
                }
            } 
       }
        
       if (!empty($other_care)) {
         if(count($exist_patient_others) > 0) {
             $this->patient_model->delete('care_for_patient_others', 'patient_id='.$this->login_id);
             foreach ($other_care as $key => $value) {
                $data2['other_type'] = $value;
                $this->patient_model->insert('care_for_patient_others', $data2);
            }
            
        } else {
            foreach ($other_care as $key => $value) {
                $data2['other_type'] = $value;
                $this->patient_model->insert('care_for_patient_others', $data2);
            }
        }
       }
    

            $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', 'Update Successfully!');
      if ($this->session->userdata('user_role') == 1) {

        redirect('patient/patient_profile_update/'.$this->login_id); 

    }
    else{    
         redirect("patient/patient_profile_update/$this->login_id"); 
    }      

        // $this->session->set_flashdata('type', 'success');
        // $this->session->set_flashdata('msg', 'Update Successfully!');
        // redirect('care-patient/'.$this->login_id, 'refresh');
        
    }

    //==========================NURSE LIST=========================== //
    public function patient_list()
    {
        
        $data['patient_details'] = $this->patient_model->select_with_where_desc('*',"user_role>3",'users','loginid','Desc');

        $this->load->view('manage_patient/patient_list', $data);
        
    }

    //==========================PATIENT PROFILE =========================== //
    public function profile()
    {
        $data['form_title'] = 'Profile';
        $id=$this->login_id;
        $data['patient_details_info'] = $this->patient_model->select_with_where('*',"patient_id=".$this->login_id, 'patient_details');

        $data['patient_details'] = $this->patient_model->select_with_where('*',"loginid=".$this->login_id,'users');


        // $data['form_title'] = 'Profile';

        // $data['patient_details'] = $this->patient_model->select_with_where('*',"loginid=".$id,'users');

        // $data['patient_details_info'] = $this->patient_model->select_with_where('*',"patient_id=".$id, 'patient_details');

        // $data['patient_details'] = $this->patient_model->select_with_where('*',"loginid=".$id,'users');

        $data['health_status'] = $this->patient_model->select_with_where('*',"patient_id=".$id,'patient_health_status');

        $data['patient_residence_items_list'] = $this->patient_model->select_all('patient_residence_items_list','*');

        $data['patient_residence_items'] = $exist = $this->patient_model->select_with_where('item',"patient_id=".$id,'patient_residence_items');

        $data['patient_residence_items'] =  array_column($exist, 'item');


        $data['care_for_patient_other_details'] = $this->patient_model->select_all('care_for_patient_other_details','*'); 

        $exist_accompany = $this->patient_model->select_with_where('*',"patient_id=".$id,'care_for_patient_accompany');

        $exist_other = $this->patient_model->select_with_where('other_type',"patient_id=".$id,'care_for_patient_others'); 

        $data['care_for_patient_others'] =  array_column($exist_other, 'other_type');
        $data['care_for_patient_accompany'] =  array_column($exist_accompany, 'type'); 

         $data['booking_details_new']=$this->patient_model->special_join_query_booking($id,'1');
        $this->load->view('manage_patient/profile',$data);
        
    }

    public function patient_profile_update($id)
    {

        $data['form_title'] = 'Profile';
        

       //$data['patient_details'] = $this->patient_model->select_with_where('*',"patient_id=".$id, 'patient_details');

        $data['patient_details_users'] = $this->patient_model->select_with_where('*',"loginid=".$id, 'users');



        $data['patient_details_info'] = $this->patient_model->select_with_where('*',"patient_id=".$id, 'patient_details');

        $data['patient_details'] = $this->patient_model->select_with_where('*',"loginid=".$id,'users');

        //print_r($data['patient_details']);


        // $data['form_title'] = 'Profile';

        // $data['patient_details'] = $this->patient_model->select_with_where('*',"loginid=".$id,'users');

        // $data['patient_details_info'] = $this->patient_model->select_with_where('*',"patient_id=".$id, 'patient_details');

        // $data['patient_details'] = $this->patient_model->select_with_where('*',"loginid=".$id,'users');

        $data['health_status'] = $this->patient_model->select_with_where('*',"patient_id=".$id,'patient_health_status');

        $data['patient_residence_items_list'] = $this->patient_model->select_all('patient_residence_items_list','*');

        $data['patient_residence_items'] = $exist = $this->patient_model->select_with_where('item',"patient_id=".$id,'patient_residence_items');

        $data['patient_residence_items'] =  array_column($exist, 'item');


        $data['care_for_patient_other_details'] = $this->patient_model->select_all('care_for_patient_other_details','*'); 

        $exist_accompany = $this->patient_model->select_with_where('*',"patient_id=".$id,'care_for_patient_accompany');

        $exist_other = $this->patient_model->select_with_where('other_type',"patient_id=".$id,'care_for_patient_others'); 

        $data['care_for_patient_others'] =  array_column($exist_other, 'other_type');
        $data['care_for_patient_accompany'] =  array_column($exist_accompany, 'type'); 

         $data['booking_details_new']=$this->patient_model->special_join_query_booking($id,'1');

        $this->load->view('manage_patient/patient_profile_update',$data);

    }

    //===================ADD NURSE AS USER VIEW=================== //
    public function index()
    {
  
        $this->load->view('manage_patient/register_user_patient');
        
    }
    //===================Condition VIEW=================== //
    public function condition()
    {
        $data['form_title'] = 'My Condition';

        $data['health_status'] = $this->patient_model->select_with_where('*',"patient_id=".$this->login_id,'patient_health_status');

        $data['patient_residence_items_list'] = $this->patient_model->select_all('patient_residence_items_list','*');

        $data['patient_residence_items'] = $exist = $this->patient_model->select_with_where('item',"patient_id=".$this->login_id,'patient_residence_items');

        $data['patient_residence_items'] =  array_column($exist, 'item');
  
        $this->load->view('manage_patient/condition', $data);
        
    }

    //===================Condition Edit view=================== //
    public function edit_condition()
    {
        $data['patient_details'] = $this->patient_model->select_with_where('*',"loginid=".$this->login_id,'users'); 

        $data['health_status'] = $this->patient_model->select_with_where('*',"patient_id=".$this->login_id,'patient_health_status');

        
        $data['patient_residence_items_list'] = $this->patient_model->select_all('patient_residence_items_list','*');

        $data['patient_residence_items'] = $exist = $this->patient_model->select_with_where('item',"patient_id=".$this->login_id,'patient_residence_items');

        $data['patient_residence_items'] =  array_column($exist, 'item');


        $data['patient_residence_items_list'] = $this->patient_model->select_all('patient_residence_items_list','*'); 

        $data['form_title'] = 'Add/Edit Condition';
  
        $this->load->view('manage_patient/edit_condition', $data);
        
    }

    //===================Condition Edit view===================//

    public function update_condition()
    {
        $data['dependency'] = $this->input->post('dependency');
        $data['mental_state'] = $this->input->post('mental_state');
        $data['feeding'] = $this->input->post('feeding');
        $data['allergies']  = $this->input->post('allergies');
        $data['dependency'] = $this->input->post('dependency');

        $new_items =  $this->input->post('patient_res_items');

        $data['patient_id'] = $data2['patient_id'] = $this->login_id;

        $data2['created_at'] = $data['created_at'] = date('Y-m-d H:i:s');

        $result = $this->patient_model->select_with_where('*',"patient_id=".$this->login_id,'patient_health_status'); 

        if(!empty($result)){
            $exist_res_items = $this->patient_model->select_with_where('item',
            "patient_id=".$this->login_id." and health_status_id=".$result[0]['id'] ,'patient_residence_items');
        }

        if (count($result) > 0) {
           #update

            $this->patient_model->update_function('patient_id', $this->login_id, 'patient_health_status', $data);


            $this->patient_model->delete('patient_residence_items', 'health_status_id='.$result[0]['id']);

            $data2['health_status_id'] = $result[0]['id'];

            if (!empty($new_items)) {
              foreach ($new_items as $value) {
               $data2['item'] = $value;
               $this->patient_model->insert_ret('patient_residence_items', $data2);
             }
            }
            

        }else{
           #insert
           $ret = $this->patient_model->insert_ret('patient_health_status', $data);
           $data2['health_status_id'] = $ret;
           foreach ($new_items as $value) {
               $data2['item'] = $value;
               $this->patient_model->insert('patient_residence_items', $data2);
           }
           //echo "data inserted";
        } 

        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', 'Update Successfully!');   
    if ($this->session->userdata('user_role') == 1) {

        redirect('patient/patient_profile_update/'.$id); 

    }
    else{    
        //redirect("patient/patient_profile_update/$this->login_id"); 
        redirect('patient/patient_profile_update/'.$this->login_id); 
    } 
        // $this->session->set_flashdata('type', 'success');
        // $this->session->set_flashdata('msg', 'Update Successfully!');
        // redirect('edit-condition', 'refresh');
        
    }


    //===================Assessment VIEW=================== //
    public function assessment()
    {
        $data['form_title'] = 'Assessment';
        $this->load->view('manage_patient/assessment', $data);
        
    }
    //=================ADD NURSE AS USER POST===================== //
    public function register_user_as_patient(){

        $data['email'] = $email = html_escape(trim($this->input->post('email')));

        $data['first_name'] = html_escape(trim($this->input->post('firstname')));

        $data['last_name'] = html_escape(trim($this->input->post('lastname')));

        $data['country_code'] = html_escape(trim($this->input->post('country_code')));

        $data['contact_no'] = html_escape(trim($this->input->post('contact')));

        $data['gender'] =html_escape(trim($this->input->post('gender')));

        $data['user_role'] = 4;  // role 4 = Patient

        $data['verify_status'] = 1; 

        $data['registered_date'] = date('Y-m-d H:i:s');

        $data['last_login_ip'] = $this->input->ip_address();

        $password = html_escape(trim($this->input->post('password')));

        $passconf = html_escape(trim($this->input->post('passconf')));

        $data['password'] = $this->encryptIt($password);

        //////////////below for upload picture//////////////////
        if($_FILES['bio_file']['name'] != '')
            { 
             $data['profile_pic'] ='';
             $i_ext = explode('.', $_FILES['bio_file']['name']);
             $target_path =mt_rand().date("YmdHis").'patient_photo.'.end($i_ext);
             $_FILES['bio_file']['name'] = $target_path;
 
             $this->upload->initialize($this->set_upload_options($_FILES['bio_file']['name'],'uploads/'));
             $size = getimagesize($_FILES['bio_file']['tmp_name']);
             $this->upload->do_upload();  
             
             if (move_uploaded_file($_FILES['bio_file']['tmp_name'], 'uploads/' . $target_path))
             {
                 if ($size[0] == 150 || $size[1] == 150) 
                 {


                 } 
                else {
                 $imageWidth = 150; //Contains the Width of the Image
                 $imageHeight = 150;
                 $this->resize($imageWidth, $imageHeight, "uploads/" . $target_path, 
                    "uploads/" . $target_path);
                } 
                //$data['bio_file'] = $target_path;
                $data['profile_pic'] = $target_path;
         
             } 
         }

        ///////////////upper for upoad picture//////////////////
        
        if($password == $passconf) {
            $res = $this->patient_model->select_with_where('*',"email='{$email}'",'users');
            if (count($res) > 0) 
            {  
                $this->session->set_flashdata('type', 'danger');
                $this->session->set_flashdata('msg', 'Email already exists!!');
                redirect('patient');
            }
            else{
                $this->patient_model->insert('users',$data);
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', 'Registraion Done Successfully!');
                redirect('patient/patient_list');
            } 
        }else{
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', 'Password not matched!');
                redirect('patient/patient_list');
        } 
    }
    //===================EDIT AUTH NURSE VIEW=================== //
    public function edit_patient_auth_details($id)
    {
        $data['patient_auth_details'] = $this->patient_model->select_with_where('*',"user_role=3",'users');
        $this->load->view('manage_patient/edit_patient_auth_details',$data);
        
    }
    //=================ADD Patient DETAILS VIEW=================== //
    public function add_patient_details($id)
    {
        $data['patient_details'] = $this->patient_model->select_with_where('*',"patient_id=".$id, 'patient_details');

        $data['patient_details_users'] = $this->patient_model->select_with_where('*',"loginid=".$id, 'users');
        $data['pname']=$data['patient_details_users'][0]['first_name']." ".$data['patient_details_users'][0]['last_name'];

        // print_r($data['patient_details_users']); exit;

        $this->load->view('manage_patient/add_patient_details',$data);
        
    }

    public function patient_details()
    {
        $data['patient_details'] = $this->patient_model->select_with_where('*',"patient_id=".$this->login_id, 'patient_details');

        $data['patient_details_users'] = $this->patient_model->select_with_where('*',"loginid=".$this->login_id, 'users');


        //$data['patient_acc_info'] = $this->patient_model->select_with_where('*',"loginid=".$this->login_id, 'users');

        // print_r($data['patient_details_users']); exit;

        $this->load->view('manage_patient/patient_details',$data);
        
    }


    public function insert_patient_details(){
        
    // echo print_r($_POST);

    // exit;
    $data['nrc_passport_id'] = html_escape(trim($this->input->post('nrc_passport_id')));

    $data['weight'] = html_escape(trim($this->input->post('weight')));

    $data['height'] = html_escape(trim($this->input->post('height')));
    
    $data['address'] = html_escape(trim($this->input->post('address')));

    $data['patient_id'] = $id = html_escape(trim($this->input->post('id')));

    $data['resident_type'] = $this->input->post('resident_type');

    $data['security_type'] = $this->input->post('security_type');

    $data['living_status'] = $this->input->post('living_status');

     $data['age'] = html_escape(trim($this->input->post('age')));

    $data['current_living_condition'] = $this->input->post('current_living_condition');

    $data['current_care_status'] = $this->input->post('current_care_status');

    $data['pets_in_house'] = $this->input->post('pets_in_house');

    $data['created_at'] = date('Y-m-d H:i:s');

    //$data['gender']=$this->input->post('gender');

    $res = $this->patient_model->select_with_where('*',"patient_id=".$id,'patient_details');

    ///below for update patient profile pic
    
     $datap['email'] = $email = html_escape(trim($this->input->post('email')));

     $datap['first_name'] = html_escape(trim($this->input->post('firstname')));

     $datap['last_name'] = html_escape(trim($this->input->post('lastname')));

     $datap['country_code'] = html_escape(trim($this->input->post('country_code')));

     $datap['contact_no'] = html_escape(trim($this->input->post('contact')));

     $datap['gender'] = html_escape(trim($this->input->post('gender')));

//echo "gwdwd".$datap['gender'];

        if($_FILES['bio_file']['name'] != '')
            { 
             $datap['profile_pic'] ='';
             $i_ext = explode('.', $_FILES['bio_file']['name']);
             $target_path = $id."_".date("YmdHis").'_patient_photo.'.end($i_ext);
             $_FILES['bio_file']['name'] = $target_path;
 
             $this->upload->initialize($this->set_upload_options($_FILES['bio_file']['name'],'uploads/'));
             $size = getimagesize($_FILES['bio_file']['tmp_name']);
             $this->upload->do_upload();  
             
             if (move_uploaded_file($_FILES['bio_file']['tmp_name'], 'uploads/' . $target_path))
             {
                 if ($size[0] == 150 || $size[1] == 150) 
                 {


                 } 
                else {
                 $imageWidth = 150; //Contains the Width of the Image
                 $imageHeight = 150;
                 $this->resize($imageWidth, $imageHeight, "uploads/" . $target_path, 
                    "uploads/" . $target_path);
                } 
                //$data['bio_file'] = $target_path;
                $datap['profile_pic'] = $target_path;

             } 
         }

    ////update for patient profile pic
 
    $this->patient_model->update_function('loginid',$id,'users',$datap);
    
    if ( count($res) > 0 ) {
    #update
        $this->patient_model->update_function('patient_id', $id, 'patient_details', $data);
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', 'Update Successful.');
    }
    else{
    #insert
        $this->patient_model->insert('patient_details',$data);
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', 'Insert Successful.');

    }

    if ($this->session->userdata('user_role') == 1) {

        redirect('patient/patient_profile_update/'.$id); 

    }
    else{    
        redirect("patient/patient_profile_update/$id"); 
    }

    }

    //=========================OTHER FUNCTIONS=========================== //

    function encryptIt($string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
        $secret_iv = 'This is my secret iv';
        // hash
        $key = hash('sha256', $secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        $output=str_replace("=", "", $output);
        return $output;
    }
// $tableName, $columnName, $columnVal
    public function delete_patient($patient_id)
    {
        $this->patient_model->delete_function('users','loginid',$patient_id);         
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', 'Delete Successful.');
        redirect('patient/patient_list'); 
    }


  ///admin to patient profile view
     public function patient_edit_profile($id)
    {
        // $data['form_title'] = 'Profile';

        // $data['patient_details'] = $this->patient_model->select_with_where('*',"loginid=".$id,'users');

        $data['patient_details_info'] = $this->patient_model->select_with_where('*',"patient_id=".$id, 'patient_details');

        $data['patient_details'] = $this->patient_model->select_with_where('*',"loginid=".$id,'users');

        $data['health_status'] = $this->patient_model->select_with_where('*',"patient_id=".$id,'patient_health_status');

        $data['patient_residence_items_list'] = $this->patient_model->select_all('patient_residence_items_list','*');

        $data['patient_residence_items'] = $exist = $this->patient_model->select_with_where('item',"patient_id=".$id,'patient_residence_items');

        $data['patient_residence_items'] =  array_column($exist, 'item');


        $data['care_for_patient_other_details'] = $this->patient_model->select_all('care_for_patient_other_details','*'); 

        $exist_accompany = $this->patient_model->select_with_where('*',"patient_id=".$id,'care_for_patient_accompany');

        $exist_other = $this->patient_model->select_with_where('other_type',"patient_id=".$id,'care_for_patient_others'); 

        $data['care_for_patient_others'] =  array_column($exist_other, 'other_type');
        $data['care_for_patient_accompany'] =  array_column($exist_accompany, 'type'); 

         $data['booking_details_new']=$this->patient_model->special_join_query_booking($id,'1');
        $this->load->view('patient/patient_profile/index',$data);  
    } 

  ///admin to patient profile view  
    private function set_upload_options($file_name,$folder_name)
    {   
        //upload an image options
        $url=base_url();

        $config = array();
        $config['file_name'] = $file_name;
        $config['upload_path'] = 'uploads/'.$folder_name;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']      = '0';
        $config['overwrite']     = TRUE;

        return $config;
    }
    //general function start

    public function resize($height, $width, $source, $destination)
    {
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = $source;
        $config['overwrite'] = TRUE;
        $image_config['quality'] = "100%";
        $image_config['maintain_ratio'] = FALSE;
        $config['height'] = $height;
        $config['width'] = $width;
        $config['new_image'] = $destination; //you should have write permission here..
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

   
}

 function show($array){
        echo "<pre>";
        print_r($array);
        echo "</pre>";
        exit;
    }
?>
