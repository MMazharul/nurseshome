<?php $this->load->view('backend/head_link');?>
<style type="text/css">
    th,td{text-align: center !important; }

</style>
<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
    <img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
    <div class="page-main">
        <!--app-header-->
        
        
        <?php $this->load->view('backend/header');?>



        <!-- app-content-->
        <div class="container content-nurse">
            <div class="side-app">

                <!-- page-header -->
                <!-- <div class="page-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
                    </ol>
                    <div class="ml-auto">
                        <div class="input-group">
                            <a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
                                <span>
                                    <i class="fa fa-calendar"></i> Events Settings
                                </span>
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                                <span>
                                    <i class="fa fa-star"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div> -->
                <!-- End page-header -->

                <div class="row">
                    <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-10">
                            <div class="card-title">Addons List</div>
                            </div>
                            <div class="col-md-2">
                            <a href="add-addons" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
                                <span> Add Addons
                                </span>
                                
                            </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                            <?php if($this->session->flashdata('msg')){ ?>
                                <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <div class="alert-message">
                                        <span><?=$this->session->flashdata('msg');?></span>
                                    </div>
                                </div>
                            <?php } ?> 
                                <table id="example" class="table table-striped table-bordered text-nowrap w-100">
                                    <thead>
                                        <tr>
                                            <!-- Name | Gender | Date Registered | Status | Rate (RM) | Contact No | Email Address -->
                                        
                                            <th class="wd-15p">#</th>
                                            <th>Addons Title</th>
                                            <th>Price</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                          
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        $i = 1;
                                        foreach ($addons_list as $key => $value) { ?>
                                   
                                        <tr>
                                            <td><?=$i++;?></td>
                                            <td><?=$value['title']?></td>
                                            <td><?=$value['price']?></td>
                                            <td><?=$value['status']== 0?'Inactive':'Active'?></td>
                                            <td>
                                                <a href="view-addon/<?=$value['id']?>" class="btn btn-primary">Edit</a>
                                                <?php if($value['status'] == 0) :?>

                                                    <a href="setting/status_update/<?=$value['id']?>/1" class="btn btn-success addon_status">
                                                    Activate
                                                    </a>
                                                        
                                                <?php elseif($value['status'] == 1): ?>

                                                    <a href="setting/status_update/<?=$value['id']?>/0" class="btn btn-danger addon_status">
                                                    Deactive
                                                    </a>

                                                <?php endif;?>
                                                
                                            </td>
                                        </tr>

                                        <?php $i++; } ?>
                                        
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- table-wrapper -->
                    </div>
                    <!-- section-wrapper -->
                    </div>
                </div>

            </div><!--End side app-->

            <!-- Right-sidebar-->
            <?php $this->load->view('backend/right_sidebar');?>
            <!-- End Rightsidebar-->

            <?php $this->load->view('backend/footer');?>

        </div>
        <!-- End app-content-->
    </div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>

<script>
    let addonStatus = document.querySelectorAll('.addon_status');

    addonStatus.forEach( (each) => {
        
        each.addEventListener('click', (e)=>{

            if (confirm('Are you sure?')) {
                return true;
            }
            else{
                e.preventDefault();
            }

        })

    })
</script>

</body>
</html>