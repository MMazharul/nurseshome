<?php $this->load->view('backend/head_link');?>

<body class="app sidebar-mini rtl">

<!--Global-Loader-->
<!-- <div id="global-loader">
    <img src="back_assets/images/icons/loader.svg" alt="loader">
</div> -->

<div class="page">
    <div class="page-main">
        <!--app-header-->
        <?php $this->load->view('backend/header');?>
        <!-- app-content-->
        <div class="container content-patient">
            <div class="side-app">
   

                <div class="row">
                    <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-10">
                            <div class="card-title">Add New Addon</div>
                            </div>
                            <div class="col-md-2">
                            <a href="list-addons" class="btn btn-primary text-white mr-2" style="width:100%"  id="">
                                <span> Addons List
                                </span>
                            </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="setting/create_addons" method="post" enctype='multipart/form-data'>
                            <div class="row">
                            <?php if($this->session->flashdata('msg')){ ?>
                                <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <div class="alert-message">
                                        <span><?=$this->session->flashdata('msg');?></span>
                                    </div>
                                </div>
                            <?php } ?> 
                            <div id="alert_pass"></div>
                            <div class="col-md-12">
                                <!-- <h4 class="" style="line-height:2.7em">Add Patient</h4> -->
                            </div>
                            </div>
                            <fieldset>
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3 text-right">
                                                    <label for="firstname">Addon Title</label>
                                                </div>
                                                <div class="col-md-9">
                                                <input name="title" type="text" class="form-control" required placeholder="Addon title">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                        <div class="col-md-3 text-right">
                                            <label for="lastname">Addon Price</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input name="price" type="text" class="form-control" required placeholder="Addon Price">
                                        </div>
                                        </div> 
                                    </div>
                            </div>
                            
                            </fieldset>

                            <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                <div class="">
                                    <button class="btn btn-primary" type="submit" name="save" id="save_contact">Create</button>
                                </div>
                            </div>

                            </form>
                        </fieldset>
                        <!-- table-wrapper -->
                    </div>
                    <!-- section-wrapper -->
                    </div>
                </div>

            </div><!--End side app-->

            <!-- Right-sidebar-->
            <?php $this->load->view('backend/right_sidebar');?>
            <!-- End Rightsidebar-->

            <?php $this->load->view('backend/footer');?>
        </div>
        <!-- End app-content-->
    </div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<?php $this->load->view('backend/footer_link');?>


<script>
let pass = document.querySelector('#password');
let pass2 = document.querySelector('#passconf');

pass2.onkeyup = () => {
    if (pass.value != pass2.value ) {
        document.getElementById("alert_pass").innerHTML = 
        `<div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button><div class="alert-message">
            <span>Password Don't Match!</span>
        </div>
        </div>`;
        
        document.getElementById("submitBtn").disabled = true;   
    } 
    else{
        document.getElementById("alert_pass").innerHTML = '';
        document.getElementById("submitBtn").disabled = false;  
    }
}
    // $('.checkbox').change(function(){
    //     alert('hi');
    // });

</script>
</body>
</html>