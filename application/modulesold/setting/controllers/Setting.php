<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Setting extends MX_Controller {

    //public $counter=0;
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('setting_model');
        // $this->load->model('home_model');
        // $this->load->model('nurse/nurse_model');
        // $this->load->helper('text');
        // $this->load->helper(array('form', 'url'));
        // $this->load->helper('inflector');
        //$this->load->library('encrypt');
        // $this->load->model('home_model');
        // $this->load->model('admin/admin_model');
        // $this->load->model('product/product_model');
    }
    public function add_addons()
    {
        $this->load->view('addons/add_addons');
    }

    public function create_addons()
    {
        $data['title']=html_escape(trim($this->input->post('title')));
        $data['price']=$this->input->post('price');
        $data['add_date'] = date('Y-m-d H:i:s');
        $this->setting_model->insert('addons',$data);
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', 'Addons Added Successfully!!');

        redirect('add-addons');
    }

    public function view_addon($id)
    {
        $data['addon_details'] = $this->setting_model->select_with_where('*',"id=".$id,'addons'); 

        $this->load->view('addons/view_addon', $data);


    }

    public function update_addon($id)
    {
        $data['title']=html_escape(trim($this->input->post('title')));
        $data['price']=$this->input->post('price');

        $this->setting_model->update_function('id', $id, 'addons', $data);

        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', 'Addons Updated Successfully!!');

        redirect('view-addon/'.$id);
    }

    public function addons_list()
    {
        $data['addons_list'] = $this->setting_model->select_all('addons');
        $this->load->view('addons/list_addons', $data);
    }

    public function status_update($id, $status){

        $data['status'] = $status;

        $this->setting_model->update_function('id', $id, 'addons', $data);

        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', 'Addons Updated Successfully!!');

        redirect('list-addons');

    }

}

?>
