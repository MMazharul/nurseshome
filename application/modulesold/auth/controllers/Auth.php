<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends MX_Controller {

    //public $counter=0;
    function __construct() {
        parent::__construct();
        $this->load->library("session");
        // $this->load->library('form_validation');
        $this->load->model('auth_model');
        $this->load->library('user_agent');
        // $this->load->model('home_model');
        // $this->load->model('product/product_model');
        // $this->load->helper('text');
        // $this->load->helper(array('form', 'url'));
        // $this->load->helper('inflector');
        //$this->load->library('encrypt');
        // $this->load->model('home_model');
        // $this->load->model('admin/admin_model');
        // $this->load->model('product/product_model');
        $this->login_id = $this->session->userdata('loginid');
    }

    //==============================LOGIN======================================//

    public function index()
    {

        $this->load->view('login');
    }

    public function login_check()
    {

        $email = html_escape(trim($this->input->post('email')));
        $password = $this->encryptIt(html_escape(trim($this->input->post('password'))));

        $res = $this->auth_model->select_with_where("*","email='{$email}' and password='{$password}'", 'users');

        if (count($res) > 0)
        {
            $data['email'] = $res[0]['email'];

            $data['user_role'] = $res[0]['user_role'];

            $log['loginid'] = $res[0]['loginid'];

            $log['logintime'] = date('Y-m-d H:i:s');

            $log['loginip'] = $this->input->ip_address();

            $log['logindevice'] = $this->agent->agent_string();

            $this->auth_model->insert('login_log',$log);
            $this->session->set_userdata($data);
            $this->session->set_userdata($log);

            redirect('/home');
        }
        else{
            $this->session->set_flashdata('type', 'danger');
            $this->session->set_flashdata('msg', 'Login Credentials not matched!');

            redirect('auth');
        }
    }
//============================REGISTRATION======================================//

    public function registration()
    {


        $this->load->view('registration');

    }

    public function register()
    {
        $data['email'] = $email = html_escape(trim($this->input->post('email')));

        $data['first_name'] = html_escape(trim($this->input->post('firstname')));

        $data['last_name'] = html_escape(trim($this->input->post('lastname')));

        $data['country_code'] = html_escape(trim($this->input->post('country_code')));

        $data['contact_no'] = html_escape(trim($this->input->post('contact')));

        $data['user_role'] = html_escape(trim($this->input->post('role')));

        $data['user_type'] = html_escape(trim($this->input->post('user_type')));
        if($data['user_type']==3)
        {
          $data['charge']=25;
        }
        else
        {
          $data['charge']=0;
        }

        $data['verify_status'] = 1;

        $data['registered_date'] = date('Y-m-d H:i:s');

        $data['last_login_ip'] = $this->input->ip_address();

        $password = html_escape(trim($this->input->post('password')));

        $passconf = html_escape(trim($this->input->post('passconf')));

        $data['password'] = $this->encryptIt($password);



        $data['profile_pic'] = "avatar.png";

        if($data['user_role']==0)
        {
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', 'Please Select a User Type!');
                redirect('auth/registration');
        }
        else
        {
        if($password == $passconf) {
            $res = $this->auth_model->select_with_where('*',"email='{$email}'",'users');
            if (count($res) > 0)
            {
                $this->session->set_flashdata('type', 'danger');
                $this->session->set_flashdata('msg', 'Email already exists!!');
                redirect('auth/registration');
            }
            else{
                $this->auth_model->insert('users',$data);
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', 'Registraion Done Successfully!');
                redirect('auth');
            }
        }
        else{
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', 'Password not matched!');
                redirect('auth/registration');
        }
      }

    }

    //==============================LOGOUT======================================//

    public function logout()
    {
        $loginid = ($this->session->userdata('loginid')) ;
        $data['logouttime'] = date('Y-m-d H:i:s');

        $this->auth_model->update_function('loginid', $loginid,'login_log', $data);

		$this->session->unset_userdata('email');
		$this->session->set_userdata('log_scc','Successfully Logged-Out!');
		redirect('auth','refresh');
	}

 //=============================OTHER FUNCTIONS=============================//
    public function change_password_view()
    {
        $data['form_title'] = 'Change Password';
        $this->load->view('change_password',$data);
    }
    public function change_password()
    {
        $password = html_escape(trim($this->input->post('password')));

    	$pre_password = $this->encryptIt(html_escape(trim($this->input->post('pre_password'))));

        $passconf = html_escape(trim($this->input->post('passconf')));

        $res = $this->auth_model->select_with_where('*',
            "loginid=$this->login_id and password='$pre_password'",
            'users');

        if (count($res) > 0) {
          if ($password == $passconf) {
            $data['password'] = $this->encryptIt($password);
            $this->auth_model->update_function('loginid',$this->login_id, 'users', $data);
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', 'Successfully updated!');
          }
          else{
            $this->session->set_flashdata('type', 'danger');
            $this->session->set_flashdata('msg', 'Password not matched');
          }
        }
        else{
            $this->session->set_flashdata('type', 'danger');
            $this->session->set_flashdata('msg', 'Previous password not matched');
        }

        redirect('change-password');
    }



    // public function change_password()
    // {
    //     $email = $this->input->post('email');
    //     $pass = $this->encryptIt($this->input->post('pass_change'));
    //     $data = array('password' => $pass,);
    //     $this->auth_model->password_change($email,$data);
    //     $this->session->set_userdata('log_scc','Successfully changed your password! Login with your "NEW" password.');
    //     redirect('login/index', 'refresh');
    // }
    function decryptIt($string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
        $secret_iv = 'This is my secret iv';
    // hash
        $key = hash('sha256', $secret_key);
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        return $output;
    }

    function encryptIt($string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
        $secret_iv = 'This is my secret iv';
        // hash
        $key = hash('sha256', $secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        $output=str_replace("=", "", $output);
        return $output;
    }


    // public function login_check()
    // {
    //     $ss=$this->encryptIt('123456');
    //     echo $ss;
    // }

    public function forget_password()
    {
        $data['form_title'] = 'Forget Password';
        $this->load->view('forget_pass',$data);
    }
    public function contact_us()
    {
        $data['form_title'] = 'Contact Us';
        $this->load->view('contact_us',$data);
    }
    public function contact_us_post()
    {
        $data['firstname'] = $email = html_escape(trim($this->input->post('firstname')));

        $data['lastname'] = html_escape(trim($this->input->post('lastname')));


        $data['contact_no'] = html_escape(trim($this->input->post('contact')));

        $data['emailaddress'] = html_escape(trim($this->input->post('emailaddress')));

        $data['company'] = html_escape(trim($this->input->post('company')));

        $data['msg_title'] = html_escape(trim($this->input->post('msg_title')));

        $data['message']=html_escape(trim($this->input->post('message')));

         $this->auth_model->insert('contact_us',$data);
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', 'Message Successfully Send !');
        redirect('auth/contact_us');




    }
}


?>
