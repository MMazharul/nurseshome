<?php $this->load->view('backend/head_link');?>
	<body class="bg-account">
	    <!-- page -->
		<div class="page">

			<!-- page-content -->
			<div class="page-content">
				<div class="container text-center text-dark">
					<div class="row">
						<div class="col-lg-4 d-block mx-auto">
							<div class="row">
								<div class="col-xl-12 col-md-12 col-md-12">
									<div class="card">
										<div class="card-body">
											<div class="text-center mb-6">
												<!-- <img src="" class="" alt=""> -->
												<img src="back_assets/images/logo.jpg" class="header-brand-img main-logo" alt="logo">
											</div>
											<hr>
											<!-- <h3>Login</h3> -->
											<p class="">Sign In To Your Account</p>
											<form method="post" action="<?=base_url('index.php/auth/login_check')?>">
											<?php if($this->session->flashdata('msg')){ ?>
												<div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
													<button type="button" class="close" data-dismiss="alert">&times;</button>
													<!-- <div class="alert-icon">
														<i class="icon-info"></i>
													</div> -->
													<div class="alert-message">
														<span><?=$this->session->flashdata('msg');?></span>
													</div>
												</div>
											<?php } ?>
											<div class="input-group mb-3">
												<span class="input-group-addon bg-white"><i class="fa fa-user"></i></span>
												<input name="email" type="text" class="form-control" placeholder="Username">
											</div>
											<div class="input-group mb-4">
												<span class="input-group-addon bg-white"><i class="fa fa-unlock-alt"></i></span>
												<input name="password" type="password" class="form-control" placeholder="Password">
											</div>
											<div class="row">
												<div class="col-12">
													<button type="submit" class="btn btn-primary btn-block">Login</button>
												</div>

											</div>
											</form>
											<br>
											<div class="row">
												<div class="col-xl-6 col-sm-12">
												<a href="auth/registration" class="btn btn-link box-shadow-0 px-0">Sign Up
													</a>
												</div>
												<div class="col-xl-6 col-sm-12">
												<a href="auth/forget_password" class="btn btn-link box-shadow-0 px-0">Forgot Password?
													</a>
												</div>
												</div>

											<div class="mt-6 btn-list">

												<a href="auth/contact_us"><button type="button" class="btn btn-icon b">Contact Us</button></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
			<!-- page-content end -->
		</div>
		<!-- page End-->

		<?php $this->load->view('backend/footer_link');?>

	</body>
</html>
