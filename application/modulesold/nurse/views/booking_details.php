<?php $this->load->view('backend/head_link');?>

    <body class="app sidebar-mini rtl">

        <!--Global-Loader-->
        <!-- <div id="global-loader">
            <img src="back_assets/images/icons/loader.svg" alt="loader">
        </div> -->

        <div class="page">
            <div class="page-main">
                <!--app-header-->
                
                
       <?php $this->load->view('backend/header');?>



        <!-- app-content-->
                <div class="container content-area">
                    <div class="side-app">

<style type="text/css">
            .c-concierge-card__pulse {
                background: #01a400;
                margin-right: 10px;
            }

            .c-concierge-card__pulse, .u-pulse {
                margin: 4px 20px 0 10px;
                    margin-right: 8px;
                width: 10px;
                height: 10px;
                border-radius: 50%;
                background: #14bef0;
                cursor: pointer;
                -webkit-box-shadow: 0 0 0 rgba(40,190,240,.4);
                box-shadow: 0 0 0 rgba(40,190,240,.4);
                -webkit-animation: pulse 1.2s infinite;
                animation: pulse 1.2s infinite;
                display: inline-block;
            }

                .m-right{
                    margin-right: 7px;
                }
                .u-green-text {
                    color: #01a400;
                }

                .modal_mou{
                    text-decoration: underline!important;
                    font-size: 11px!important;
                }


                /*search box css start here*/
                .search-sec{
                    padding: 2rem;
                }
                .search-slt{
                    display: block;
                    width: 100%;
                    font-size: 11px!important;
                    line-height: 1.5;
                    color: #55595c;
                    background-color: #fff;
                    background-image: none;
                    border: 1px solid #f3ecec;
                    height: calc(3rem + 2px) !important;
                    border-radius:0;
                }
                .wrn-btn{
                    width: 100%;
                    font-size: 16px;
                    font-weight: 400;
                    text-transform: capitalize;
                    height: calc(3rem + 2px) !important;
                    border-radius:0;
                }
                @media (min-width: 992px){
                    .search-sec{
                        position: relative;
                        top: -114px;
                        background: rgba(26, 70, 104, 0.51);
                    }
                }

                @media (max-width: 992px){
                    .search-sec{
                        background: #1A4668;
                    }
                }
    </style>
   
    <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                
                     <?php if($this->session->flashdata('msg')){ ?>
                        <div class="alert alert-<?=$this->session->flashdata('type');?> alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <div class="alert-message">
                                <span><?=$this->session->flashdata('msg');?></span>
                            </div>
                        </div>
                    <?php } ?> 
                
                    <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">   
                    <div class="card Relatedpost nested-media">
                        <!-- <div class="card-header">
                            <h4 class="card-title">Search for Care</h4>
                        </div> -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <div class="media media-lg mt-0">
                                        <div class="media-body">
                                            <h2 class="mt-0">Booking ID : <?=!empty($com_book_id) ? $com_book_id : '';?>

                                            
                                            </h2>

                                            <div style="margin-top: -10px;margin-bottom: 13px;"><span class="u-green-text"></span><span class="u-green-text" data-reactid="211">

                                            Patient :</span><span>

                                            <?php 
                                            if(!empty($booking_details[0]['first_name'])){
                                                echo ucwords($booking_details[0]['first_name']); 
                                            }
                                            echo " ";
                                            if (!empty($booking_details[0]['last_name'])) {
                                                echo ucwords($booking_details[0]['last_name']);
                                            }?>
                                             | <?=$book_day?></span></div>
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <div class="media media-lg mt-0">
                                        <div class="media-body">
                                                <h3 class="text-right"><span class="u-green-text"> 
                                                 <?=!empty($booking_details[0]['book_charge']) ? ($booking_details[0]['book_charge'] == 0 ? '25.00' : $booking_details[0]['book_charge']) : ''?>
                                                </span></h3>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <hr class="m-0 mb-4">
                            
                            <p><?=$booking_details[0]['notes']?></p>

                            <br>

                            <table class="table-bordered" cellpadding="8">
                                <thead class="bg-light text-dark">
                                    <tr>
                                        <th>Time (Hours)</th>
                                        <th>Days</th>
                                        <th>Duration (Days)</th>
                                        <th class="text-right">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?=!empty($booking_details[0]['time_in_hours']) ? $booking_details[0]['time_in_hours'] : ''?></td>
                                        <td><?=!empty($from_date) ? $from_date : ''?> - <?=!empty($to_date) ? $to_date : ''?></td>
                                        <td><?=!empty($datediff) ? $datediff : ''?></td>
                                        <td class="text-right"><?=!empty($booking_details[0]['book_charge']) ? ($booking_details[0]['book_charge'] == 0 ? '25.00' : $booking_details[0]['book_charge']) : ''?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <p class="text-right"><strong>Total : <?=$total_charge?></strong></p>
                            <?php
                                if ($booking_details[0]['book_status'] == 1) :
                            ?>
                            <center>
                                <a id="getStarted" href="nurse/get_started_service_by_nurse/<?=$booking_details[0]['patient_bookid']?>" class="btn bg-pink p-4">Get Started</a> 
                            </center>

                            <?php endif;?>
                        </div>   
                    </div>
                    </div>
                    </div>

                    <?php if ($booking_details[0]['book_status'] != 1) : ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">   
                    <div class="card">
                        <!-- <div class="card-header">
                            <h4 class="card-title">Search for Care</h4>
                        </div> -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                               
                                        
                                        <div class="text-center d-block">
                                            <i class="fa fa-rocket text-info" style="font-size: 40px"></i>
                                            <br>
                                            <span style="font-size: 20px; text-transform: uppercase;"><strong>Service Started</strong></span>
                                            <br>
                                            <span>
                                            Please do the patient assessment
                                            </span>
                                            <hr>
                                        </div>

                                        <form action="nurse/patient_assessment_by_nurse" method="post">

                                    <input type="hidden" name="book_id" value="<?=$booking_details[0]['patient_bookid']?>">
                                    <input type="hidden" name="nurse_id" value="<?=$booking_details[0]['nurse_id']?>">
                                    <input type="hidden" name="patient_id" value="<?=$booking_details[0]['patient_id']?>">

                        <?php 

                        if ($row_count > 0) { ?>
                           
                        

                        <fieldset class="biodata">
                        <h4>Nutrition</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-elements">
                                        <div class="form-label">Eating Disorder</div>
                                        <div class="custom-controls-stacked">
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="eating_disorder" value="1"
                                                    <?=!empty($assessment_details[0]['eating_disorder'] && $assessment_details[0]['eating_disorder'] == 1) ? 'checked' : ''?> >
                                                <span class="custom-control-label">Nil</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="eating_disorder" value="2" <?=!empty($assessment_details[0]['eating_disorder'] && $assessment_details[0]['eating_disorder'] == 2) ? 'checked' : ''?>>
                                                <span class="custom-control-label">Malnourished</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="eating_disorder" value="3" <?=!empty($assessment_details[0]['eating_disorder'] && $assessment_details[0]['eating_disorder'] == 3) ? 'checked' : ''?>>
                                                <span class="custom-control-label">Obese</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-elements">
                                    <div class="form-label">Diet Type</div>
                                        <div class="custom-controls-stacked">
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="diet_type" value="1"
                                                    checked="" <?=!empty($assessment_details[0]['eating_disorder'] && $assessment_details[0]['diet_type'] == 1) ? 'checked' : ''?> >
                                                <span class="custom-control-label">Regular</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="diet_type" value="2" <?=!empty($assessment_details[0]['eating_disorder'] && $assessment_details[0]['diet_type'] == 2) ? 'checked' : ''?> >
                                                <span class="custom-control-label">Theraputic</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="diet_type" value="3" <?=!empty($assessment_details[0]['eating_disorder'] && $assessment_details[0]['diet_type'] == 3) ? 'checked' : ''?>  >
                                                <span class="custom-control-label">Supplement (Endsure etc)</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="diet_type" value="4"  <?=!empty($assessment_details[0]['eating_disorder'] && $assessment_details[0]['diet_type'] == 4) ? 'checked' : ''?> >
                                                <span class="custom-control-label">Parental (TPN)</span>
                                            </label>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                            <div class="col-md-6">
                            <h4>Musculosketel</h4>
                                    <div class="form-group form-elements">
                                        <div class="form-label"></div>
                                        <div class="custom-controls-stacked">
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="musculosketel" value="1"
                                                    <?=!empty($assessment_details[0]['musculosketel'] && $assessment_details[0]['musculosketel'] == 1) ? 'checked' : ''?> >
                                                <span class="custom-control-label">Normal</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="musculosketel" value="2" <?=!empty($assessment_details[0]['musculosketel'] && $assessment_details[0]['musculosketel'] == 2) ? 'checked' : ''?>>
                                                <span class="custom-control-label">Paralysis-Free hand notes</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="musculosketel" value="3" <?=!empty($assessment_details[0]['musculosketel'] && $assessment_details[0]['musculosketel'] == 3) ? 'checked' : ''?>>
                                                <span class="custom-control-label">Joint Swelling-Free hand notes</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="musculosketel" value="4" <?=!empty($assessment_details[0]['musculosketel'] && $assessment_details[0]['musculosketel'] == 4) ? 'checked' : ''?>>
                                                <span class="custom-control-label">Pain-Free hand notes</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="musculosketel" value="5" <?=!empty($assessment_details[0]['musculosketel'] && $assessment_details[0]['musculosketel'] == 5) ? 'checked' : ''?>>
                                                <span class="custom-control-label">Others-Free hand notes</span>
                                            </label>
                                        </div>
                                    </div>


                                    <div class="form-group form-elements">
                                        <div class="form-label">Genitallia</div>
                                        <div class="custom-controls-stacked">
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="genitallia" value="1"
                                                    <?=!empty($assessment_details[0]['genitallia'] && $assessment_details[0]['genitallia'] == 1) ? 'checked' : ''?>>
                                                <span class="custom-control-label">Normal</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="genitallia" value="2" <?=!empty($assessment_details[0]['genitallia'] && $assessment_details[0]['genitallia'] == 2) ? 'checked' : ''?>>
                                                <span class="custom-control-label">Others-FHN</span>
                                            </label>
                                            
                                        </div>
                                    </div>

                                    <div class="form-group form-elements">
                                        <div class="form-label">Vascular Access</div>
                                        <div class="custom-controls-stacked">
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="vascular_access" value="1"
                                                    <?=!empty($assessment_details[0]['vascular_access'] && $assessment_details[0]['vascular_access'] == 1) ? 'checked' : ''?>>
                                                <span class="custom-control-label">Normal</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="vascular_access" value="2" <?=!empty($assessment_details[0]['vascular_access'] && $assessment_details[0]['vascular_access'] == 2) ? 'checked' : ''?>>
                                                <span class="custom-control-label">Others-FHN</span>
                                            </label>
                                            
                                        </div>
                                    </div>
                                
                                </div>

                                <div class="col-md-6">
                                <h4>Fall Risk</h4>
                                <div class="custom-controls-stacked">
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input"
                                        name="risk_status" value="1"
                                        <?=!empty($assessment_details[0]['fall_risk'] && $assessment_details[0]['fall_risk'] == 1) ? 'checked' : ''?>>
                                    <span class="custom-control-label">No Risk</span>
                                </label>
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input"
                                        name="risk_status" value="2" <?=!empty($assessment_details[0]['fall_risk'] && $assessment_details[0]['fall_risk'] == 2) ? 'checked' : ''?>>
                                    <span class="custom-control-label">At Risk</span>
                                </label>
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input"
                                        name="risk_status" value="3" <?=!empty($assessment_details[0]['fall_risk'] && $assessment_details[0]['fall_risk'] == 3) ? 'checked' : ''?>>
                                    <span class="custom-control-label">High Risk</span>
                                </label>
                                </div>
                                <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 text-left">
                                        <label for="fall_history">Fall History</label>
                                    </div>
                                    <div class="col-md-9">
                                        <textarea type="text" class="form-control" id="fall_history" name="fall_history" value=""><?=!empty($assessment_details[0]['fall_history']) ? $assessment_details[0]['fall_history']  : ''?></textarea>
                                    </div>
                                </div>
                                </div>
                                <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 text-left">
                                        <label for="walking_device">Walking Device</label>
                                    </div>
                                    <div class="col-md-9">
                                    <textarea type="text" class="form-control" id="walking_device" name="walking_device"><?=!empty($assessment_details[0]['walking_device']) ? $assessment_details[0]['walking_device'] : ''?></textarea>
                                       
                                    </div>
                                </div>
                                </div>

                                </div>
                            </div>
                            <h4>Genitourinary</h4>
                            <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group form-elements">
                                    <div class="form-label">Urine Colour</div>
                                        <div class="custom-controls-stacked">
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="urine_colour custom-control-input"
                                                    name="urine_colour" value="1"
                                                    <?=!empty($assessment_details[0]['urine_colour'] && $assessment_details[0]['urine_colour'] == 1) ? 'checked' : ''?>>
                                                <span class="custom-control-label">Clear</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                    <input type="radio" class="urine_colour custom-control-input"
                                                        name="urine_colour" value="2_1" <?=!empty($assessment_details[0]['urine_colour'] && $assessment_details[0]['urine_colour'] == '2_1') ? 'checked' : ''?>>
                                                    <span class="custom-control-label">Cloudy</span>
                                            
                                      
                                                   

                                                    <select name="cloudy_sub" class="form-control" id="cloudy_sub_select" style="<?=!empty($assessment_details[0]['urine_colour'] && $assessment_details[0]['urine_colour'] == '2_1') ? 'display: block' : 'display: none' ?>">
                                                        <option value="1" <?=!empty($assessment_details[0]['cloudy_sub'] && $assessment_details[0]['cloudy_sub'] == 1) ? 'selected' : '' ?>>Dark</option>
                                                        <option value="2" <?=!empty($assessment_details[0]['cloudy_sub'] && $assessment_details[0]['cloudy_sub'] == 2) ? 'selected' : '' ?>>Hemanturia</option>
                                                    </select>


                                         

                                            </label>
                                            
                                           
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="urine_colour custom-control-input"
                                                    name="urine_colour" value="3" <?=!empty($assessment_details[0]['urine_colour'] && $assessment_details[0]['urine_colour'] == 3) ? 'checked' : '' ?>>
                                                <span class="custom-control-label">Others (Endsure etc)</span>
                                            </label>
                                        </div>

                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="urine_colour custom-control-input"
                                                    name="urine_colour" value="4" <?=!empty($assessment_details[0]['urine_colour'] && $assessment_details[0]['urine_colour'] == 4) ? 'checked' : '' ?> >
                                                <span class="custom-control-label">Normal Voiding</span>
                                            </label>
                                            <strong>Incontinent/Dysuria/Catheter</strong>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="genitourinary custom-control-input"
                                                    name="incontinent_dysuria_catheter" value="1" <?=!empty($assessment_details[0]['incontinent_dysuria_catheter'] && $assessment_details[0]['incontinent_dysuria_catheter'] == 1) ? 'checked' : '' ?>>
                                                <span class="custom-control-label">FHN</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="genitourinary custom-control-input"
                                                    name="incontinent_dysuria_catheter" value="2" <?=!empty($assessment_details[0]['incontinent_dysuria_catheter'] && $assessment_details[0]['incontinent_dysuria_catheter'] == 2) ? 'checked' : '' ?>>
                                                <span class="custom-control-label">Dialysis</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="genitourinary custom-control-input"
                                                    name="incontinent_dysuria_catheter" value="3" <?=!empty($assessment_details[0]['incontinent_dysuria_catheter'] && $assessment_details[0]['incontinent_dysuria_catheter'] == 3) ? 'checked' : '' ?>>
                                                <span class="custom-control-label">Others-FHN</span>
                                            </label>
                                    </div>



                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-elements">
                                        <div class="form-label">Home Assessment</div>
                                        <div class="custom-controls-stacked">
                                        <textarea type="text" class="form-control" id="home_assessment" name="home_assessment"><?=!empty($assessment_details[0]['home_assessment'])? trim($assessment_details[0]['home_assessment']): ''?></textarea>
                                            
                                        </div>
                                    </div>
                                </div>
                               
                                
                            </div>

                        </fieldset>

                    <?php } else { ?>

                        <fieldset class="biodata">
                        <h4>Nutrition</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-elements">
                                        <div class="form-label">Eating Disorder</div>
                                        <div class="custom-controls-stacked">
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="eating_disorder" value="1">
                                                <span class="custom-control-label">Nil</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="eating_disorder" value="2" >
                                                <span class="custom-control-label">Malnourished</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="eating_disorder" value="3" >
                                                <span class="custom-control-label">Obese</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-elements">
                                    <div class="form-label">Diet Type</div>
                                        <div class="custom-controls-stacked">
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="diet_type" value="1"
                                                    checked=""  >
                                                <span class="custom-control-label">Regular</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="diet_type" value="2">
                                                <span class="custom-control-label">Theraputic</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="diet_type" value="3" >
                                                <span class="custom-control-label">Supplement (Endsure etc)</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="diet_type" value="4" >
                                                <span class="custom-control-label">Parental (TPN)</span>
                                            </label>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                            <div class="col-md-6">
                            <h4>Musculosketel</h4>
                                    <div class="form-group form-elements">
                                        <div class="form-label"></div>
                                        <div class="custom-controls-stacked">
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="musculosketel" value="1"
                                                     >
                                                <span class="custom-control-label">Normal</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="musculosketel" value="2">
                                                <span class="custom-control-label">Paralysis-Free hand notes</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="musculosketel" value="3">
                                                <span class="custom-control-label">Joint Swelling-Free hand notes</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="musculosketel" value="4">
                                                <span class="custom-control-label">Pain-Free hand notes</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="musculosketel" value="5">
                                                <span class="custom-control-label">Others-Free hand notes</span>
                                            </label>
                                        </div>
                                    </div>


                                    <div class="form-group form-elements">
                                        <div class="form-label">Genitallia</div>
                                        <div class="custom-controls-stacked">
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="genitallia" value="1">
                                                <span class="custom-control-label">Normal</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="genitallia" value="2" >
                                                <span class="custom-control-label">Others-FHN</span>
                                            </label>
                                            
                                        </div>
                                    </div>

                                    <div class="form-group form-elements">
                                        <div class="form-label">Vascular Access</div>
                                        <div class="custom-controls-stacked">
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="vascular_access" value="1"
                                                    >
                                                <span class="custom-control-label">Normal</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input"
                                                    name="vascular_access" value="2">
                                                <span class="custom-control-label">Others-FHN</span>
                                            </label>
                                            
                                        </div>
                                    </div>
                                
                                </div>

                                <div class="col-md-6">
                                <h4>Fall Risk</h4>
                                <div class="custom-controls-stacked">
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input"
                                        name="risk_status" value="1"
                                       >
                                    <span class="custom-control-label">No Risk</span>
                                </label>
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input"
                                        name="risk_status" value="2">
                                    <span class="custom-control-label">At Risk</span>
                                </label>
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input"
                                        name="risk_status" value="3">
                                    <span class="custom-control-label">High Risk</span>
                                </label>
                                </div>
                                <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 text-left">
                                        <label for="fall_history">Fall History</label>
                                    </div>
                                    <div class="col-md-9">
                                        <textarea type="text" class="form-control" id="fall_history" name="fall_history" value=""></textarea>
                                    </div>
                                </div>
                                </div>
                                <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 text-left">
                                        <label for="walking_device">Walking Device</label>
                                    </div>
                                    <div class="col-md-9">
                                    <textarea type="text" class="form-control" id="walking_device" name="walking_device"></textarea>
                                       
                                    </div>
                                </div>
                                </div>

                                </div>
                            </div>
                            <h4>Genitourinary</h4>
                            <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group form-elements">
                                    <div class="form-label">Urine Colour</div>
                                        <div class="custom-controls-stacked">
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="urine_colour custom-control-input"
                                                    name="urine_colour" value="1" >
                                                <span class="custom-control-label">Clear</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                    <input type="radio" class="urine_colour custom-control-input"
                                                        name="urine_colour" value="2_1">
                                                    <span class="custom-control-label">Cloudy</span>
                                            
                                      
                                                   

                                                    <select name="cloudy_sub" class="form-control" id="cloudy_sub_select" style="display: none">
                                                        <option value="1">Dark</option>
                                                        <option value="2">Hemanturia</option>
                                                    </select>


                                         

                                            </label>
                                            
                                           
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="urine_colour custom-control-input"
                                                    name="urine_colour" value="3">
                                                <span class="custom-control-label">Others (Endsure etc)</span>
                                            </label>
                                        </div>

                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="urine_colour custom-control-input"
                                                    name="urine_colour" value="4" >
                                                <span class="custom-control-label">Normal Voiding</span>
                                            </label>
                                            <strong>Incontinent/Dysuria/Catheter</strong>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="genitourinary custom-control-input"
                                                    name="incontinent_dysuria_catheter" value="1">
                                                <span class="custom-control-label">FHN</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="genitourinary custom-control-input"
                                                    name="incontinent_dysuria_catheter" value="2">
                                                <span class="custom-control-label">Dialysis</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="genitourinary custom-control-input"
                                                    name="incontinent_dysuria_catheter" value="3">
                                                <span class="custom-control-label">Others-FHN</span>
                                            </label>
                                    </div>



                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-elements">
                                        <div class="form-label">Home Assessment</div>
                                        <div class="custom-controls-stacked">
                                        <textarea type="text" class="form-control" id="home_assessment" name="home_assessment"></textarea>
                                            
                                        </div>
                                    </div>
                                </div>
                               
                                
                            </div>

                        </fieldset>

                        <?php } 
                        ?>


                        <?php 
                            if ($booking_details[0]['book_status'] == 2) { ?>
                               <div class="col-md-12 col-xs-12 text-lg-right text-center mt-2">
                                    <div class="">
                                        <button class="btn btn-primary" type="submit" name="save"
                                            id="">Save</button>
                                    </div>
                                </div>
                            <?php }
                        ?>

                            </form>
                                </div>
                            </div>

                            <?php
                                if ($booking_details[0]['book_status'] == 2) :
                            ?>

                            <hr>
        
                            <center>
                                <a id="serviceDone" href="nurse/service_done_by_nurse/<?=$booking_details[0]['patient_bookid']?>" class="btn bg-lime p-4 text-white">Service Done</a> 
                            </center>

                            <?php endif;?>

                            <hr>

                            <?php if($booking_details[0]['book_status'] == 3) { ?>
                                   <div class="text-center d-block mt-5">
                                            <i class="fa fa-check-square-o text-info" style="font-size: 40px"></i>
                                            <br>
                                            <span style="font-size: 20px; text-transform: uppercase;"><strong>Work done by you!</strong></span>
                                            <br>
                                            <span>
                                                Wait for patient Feedback!!
                                            </span>
                                       
                                    </div>
                                    <hr>
                           <?php } ?>
                            


                            <?php if($booking_details[0]['book_status'] == 5) { ?>

                                <div class="text-center d-block mt-5">
                                                <i class="fa fa-check-square-o text-info" style="font-size: 40px"></i>
                                                <br>
                                                <span style="font-size: 20px; text-transform: uppercase;"><strong>Accepted By Patient</strong></span>
                                                <br>
                                           
                                </div>

                                <div class="text-center d-block mt-5">
                                    <i class="fa fa-comment-o text-info" style="font-size: 30px"></i>
                                    <br>
                                    <span style="font-size: 20px; text-transform: uppercase;"><strong>Review for Patient</strong></span>
                                    <br>
                                </div>
                                <hr>
            
                            <?php if(isset($review_by_patient) && count($review_by_patient) > 0){?>
                                <div class="row">
                                    <div class="col-md-2">
                                        <img class="rounded-circle" width=70% src="uploads/<?=$review_by_patient[0]['profile_pic']?>" alt="">
                                    </div>
                                    <div class="col-md-10 pt-4">
                                        <h4><?=$review_by_patient[0]['first_name']." ".$review_by_patient[0]['last_name']?>
                                            <span>
                                                <i class="fa fa-star text-warning"></i> <strong class="text-warning"><?=$review_by_patient[0]['rating']?></strong>
                                            </span> 
                                        </h4>
                                        <p style="font-size: 14px"><?=$review_by_patient[0]['comment']?></p>
                                    </div>
                                </div>
                            <?php }else{ ?>
    
                                <div class="row">
                                    <div class="col-md-2">
                                        <img class="rounded-circle" width=70% src="uploads/<?=$booking_details[0]['profile_pic']?>" alt="">
                                    </div>
                                    <div class="col-md-10 pt-4">
                                        <h4><?=$booking_details[0]['first_name']." ".$booking_details[0]['last_name']?>
                                        </h4>

                                        <p class="text-danger">-Hasn't Reviewed Yet-</p>
                                  
                                    </div>
                                </div>

                                <hr>

                            <?php } ?>
                            

                            <?php if(isset($review_by_nurse) && count($review_by_nurse) > 0){ ?>
                                <div class="row">
                                     <div class="col-md-2">
                                        <img class="rounded-circle" width=70% src="uploads/<?=$review_by_nurse[0]['profile_pic']?>" alt="">
                                    </div>
                                    <div class="col-md-10 pt-4">

                                        <h4> <?=$review_by_nurse[0]['first_name']." ".$review_by_nurse[0]['last_name']?> </h4>

                                        <form action="nurse/review" method="post">
                                            <input type="hidden" name="user_id" value="<?=$review_by_nurse[0]['user_id']?>">
                                            <input type="hidden" name="user_role" value="<?=$review_by_nurse[0]['user_role']?>">
                                            <input type="hidden" name="patient_bookid" value="<?=$review_by_nurse[0]['patient_bookid']?>">

                                            <span>Give your review : 
                                        
                                          <div class="stars stars-example-fontawesome">
                                            <select id="example-fontawesome" name="rating" autocomplete="off">
                                              <option value="1" <?=$review_by_nurse[0]['rating']==1 ? 'selected' : '' ?>>1</option>
                                              <option value="2"<?=$review_by_nurse[0]['rating']==2 ? 'selected' : '' ?>>2</option>
                                              <option value="3"<?=$review_by_nurse[0]['rating']==3 ? 'selected' : '' ?>>3</option>
                                              <option value="4"<?=$review_by_nurse[0]['rating']==4 ? 'selected' : '' ?>>4</option>
                                              <option value="5"<?=$review_by_nurse[0]['rating']==5 ? 'selected' : '' ?>>5</option>
                                            </select>
                                          </div>
                                        
                                        </span>
                                        <textarea name="comment" class="form-control" id="" cols="30" rows="4"><?=$review_by_nurse[0]['comment']?></textarea>

                                        <br>

                                            <button type="submit" class="btn btn-primary">Update Review</button>
                                  

                                        </form>
                                    </div>
                                </div>
                            <?php }else{?>

                                    <div class="row">
                                         <div class="col-md-2">
                                            <img class="rounded-circle" width=70% src="uploads/<?=$nurse_details[0]['profile_pic']?>" alt="">
                                        </div>
                                        <div class="col-md-10 pt-4">

                                            <h4> <?=$nurse_details[0]['first_name']." ".$nurse_details[0]['last_name']?> </h4>

                                            <form action="nurse/review" method="post">
                                                <input type="hidden" name="user_id" value="<?=$nurse_details[0]['loginid']?>">
                                                <input type="hidden" name="user_role" value="<?=$nurse_details[0]['user_role']?>">
                                                <input type="hidden" name="patient_bookid" value="<?=$booking_details[0]['patient_bookid']?>">

                                                <span>Give your review : 
                                            
                                              <div class="stars stars-example-fontawesome">
                                                <select id="example-fontawesome" name="rating" autocomplete="off">
                                                  <option value="1">1</option>
                                                  <option value="2">2</option>
                                                  <option value="3">3</option>
                                                  <option value="4">4</option>
                                                  <option value="5">5</option>
                                                </select>
                                              </div>
                                            
                                            </span>
                                            <textarea name="comment" class="form-control" id="" cols="30" rows="4"></textarea>

                                            <br>
                                               

                                                <button type="submit" class="btn btn-primary">Add Review</button>
                                      
                                             
                                            </form>
                                        </div>

                                    </div> 
                             <?php } ?>

                            

                            <?php } elseif($booking_details[0]['book_status'] == 4){ ?>
                            
                             <div class="text-center d-block mt-5">
                                            <i class="fa fa-check-square-o text-info" style="font-size: 40px"></i>
                                       
                                            <span style="font-size: 20px; text-transform: uppercase;"><strong>Rejected By Patient</strong></span>
                                            <br>
                                       
                                    </div>

                           <?php } ?>
                         
                        </div>   
                    </div>
                    </div>

                    </div>
                        

                    <?php endif; ?>
                </div>
                <div class="col-md-3">
                    
                    <div class="row">
                        
                        <?php if($booking_details[0]['book_status'] != 1) : ?>

                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Private Note</h4>
                                </div>
                                <div class="card-body">
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal" class="btn btn-primary">
                                        <i class="fa fa-plus"></i>
                                        Add Note
                                    </a>
                                    <a href="notes-list/1" class="btn bg-pink">
                                        Note List
                                    </a>
                                </div>
                            </div>
                        </div>

                    <?php endif;?>

                    <?php 
                        if (isset($nurse_care_note_all) && count($nurse_care_note_all)>0) : ?>
                            <div class="col-md-12">
                        
                        <div class="card">
                            <div class="card-header">
                                   <h5 class="card-title"> Previous notes </h5>
                                </div>
                             
                            <div class="card-body">
                               
                                 <table>
                                    <thead>
                                        <tr>
                                            <td>Sl.</td>
                                            <td>Date</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1; 
                                        foreach($nurse_care_note_all as $values):?>
                                        <tr>   
                                            <td><?=$i ?></td>
                                            <td><a class="fetch_nurse_notes" href="javascript:void(0)" data-id="<?=$values['id']?>" data-toggle="modal" data-target="#exampleModal2">
                                                    <?php  
                                                  $date=date_create($values['date']);
                                                  echo date_format($date,"d-M-Y");?>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php $i++; endforeach; ?>
                                    </tbody>
                                </table> 
                            </div>
                             
                        </div>

                    </div>
                      <?php   endif; ?>

                    

                    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Summary For Client</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-label mb-2 text-center" id="note_date"> </div>
                                        
                                   

                                        <div class="form-label d-block row">
                                       
                                        

                                        <div class="col-12">
                                             <h5> Completed Care: </h5>
                                           <table class="table table-bordered">
                                           <thead>
                                               <tr>
                                                    <td>Title</td>
                                                    <td>Note</td>
                                               </tr>
                                           </thead>
                                               <tbody id="care_details">
                                               </tbody>
                                           </table>
                                        
                                        </div>

                                        <div class="col-12">
                                        <h5>Basic Vital Signs</h5>
                                        <table class="table table-bordered">
                                           <tbody id="note_summary">
                                               
                                           </tbody>
                                          </table>
                                        </div>

                                        </div>
                                    
                                </div>
                                   
                                </div>
                                
                            </div>
                        </div>













































                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Note</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                   <form action="nurse/nurse_note_for_patient" method="post" enctype="multipart/form-data">

                                    <input type="hidden" name="patient_book_id" value="<?=$booking_details[0]['patient_bookid']?>">
                                    <input type="hidden" name="nurse_id" value="<?=$booking_details[0]['nurse_id']?>">
                                    <input type="hidden" name="patient_id" value="<?=$booking_details[0]['patient_id']?>">
                                    
                                    <div class="form-label mb-2 text-center">Summary For Client</div>
                                        
                                        <div class="form-group form-elements col-12">

                                        <div class="form-label d-block row">
                                            <span class="">I HAVE COMPLETED THE FOLLOWING CARE:</span>
                                            <span class="">( *Add NOTE if needed* )</span>
                                        </div>

                  
                                        <div class="custom-controls-stacked row">
                                    
                                        <?php 

                                        if (count($care_for_patient) > 0 && !empty($care_note_id) && !empty($care_note)) { ?>
                                        
                                        <?php foreach ($care_for_patient as $key => $value) : ?>

                                        <label class="custom-control custom-checkbox col-md-6">
                                            <input type="checkbox" class="custom-control-input" name="care_done[<?=$value['cof_id']?>][cof_id]" value="<?=$value['cof_id']?>"

                                            <?=in_array($value['cof_id'], $care_note_id) ? 'checked' : ''?>
                                            >
                                            <span class="custom-control-label">
                                                <?=$value['cof_details']?>
                                            </span>          
                                        </label>

                                        <textarea name="care_done[<?=$value['cof_id']?>][note]" class="form-control col-md-6 mb-1" id="" cols="1" rows="1"><?php 
                                                if (in_array($value['cof_id'], $care_note_id)) {
                                                    echo $care_note[$value['cof_id']];
                                                }
                                            ?></textarea>

                                        

                                        <?php endforeach; 
                                        ?> 
                                        
                                        <?php
                                        } 
                                        elseif(count($care_for_patient) > 0 && empty($care_note_id) && empty($care_note)) {

                                        foreach ($care_for_patient as $key => $value) { ?>

                                        <label class="custom-control custom-checkbox col-md-6">
                                        <input type="checkbox" class="custom-control-input" name="care_done[<?=$value['cof_id']?>][cof_id]" value="<?=$value['cof_id']?>">
                                        <span class="custom-control-label">
                                        <?=$value['cof_details']?>
                                        </span>          
                                        </label>

                                        <textarea name="care_done[<?=$value['cof_id']?>][note]" class="form-control col-md-6 mb-1" cols="1" rows="1"></textarea>


                                        <?php }
                                        }

                                        else { 

                                            echo '<div class="text-danger">
                                                     <h3>Sorry no care selected by patient!!</h3>
                                                  </div>';
                                        }
                                        ?>
                                        </div>
                                        </div>

                                    
                                    <div class="form-label mb-2 text-center">Basic Vital Signs</div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                    <label for="note">Temp </label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" value="<?=!empty($nurse_care_note[0]['temp']) ? $nurse_care_note[0]['temp'] : ''?>" class="form-control" name="temp">
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                            <div class="col-md-3">
                                                    <label for="note">BP</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" value="<?=!empty($nurse_care_note[0]['bp']) ? $nurse_care_note[0]['bp'] : ''?>" class="form-control" name="bp">
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                        <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                    <label for="note">Pulse Rate </label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" value="<?=!empty($nurse_care_note[0]['pulse']) ? $nurse_care_note[0]['pulse'] : ''?>" class="form-control" name="pulse">
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                    <label for="note">Respiration </label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" value="<?=!empty($nurse_care_note[0]['respiration']) ? $nurse_care_note[0]['respiration'] : ''?>" class="form-control" name="respiration">
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-6"><div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                        <label for="note">Pain Sore </label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" value="<?=!empty($nurse_care_note[0]['pain_sore']) ? $nurse_care_note[0]['pain_sore'] : ''?>" class="form-control" name="pain_sore">
                                                </div>
                                            </div>
                                        </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                        <label for="note">SPO2 </label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" value="<?=!empty($nurse_care_note[0]['spo2']) ? $nurse_care_note[0]['spo2'] : ''?>" class="form-control" name="spo2">
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6"><div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                        <label for="note">ETC </label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" value="<?=!empty($nurse_care_note[0]['etc']) ? $nurse_care_note[0]['etc'] : ''?>" class="form-control" name="etc">
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
<!-- 
                                    <div class="form-label">
                                        Notes for Care
                                    </div>
                                    <div class="form-group form-elements">
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_done[]" value="1">
                                            <span class="custom-control-label">
                                                BASIC PERSONAL CARE – BASIC CARE FOR CLEANING, FEEDING, MEDICATION ETC
                                            </span>
                                            <textarea class="form-control" name="" id="" cols="30" rows="1"></textarea>          
                                        </label>
                                    </div>
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_done[]" value="1">
                                            <span class="custom-control-label">
                                                BASWOUND DRESSING – BED SORES / DIABETIC WOUNDS /SURGICAL WOUNDS
                                            </span>   
                                            <textarea class="form-control" name="" id="" cols="30" rows="1"></textarea>         
                                        </label>
                                    </div>
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="care_done[]" value="1">
                                            <span class="custom-control-label">
                                                INFANT BABY CARE – 0 TO 12 MONTHS
                                            </span>      
                                            <textarea class="form-control" name="" id="" cols="30" rows="1"></textarea>      
                                        </label>
                                    </div>
                                    </div> -->

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                            <label for="photo_wound">Photo for wounds</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="file" class="dropify"
                                                name="photo_wounds" data-default-file="uploads/<?=!empty($nurse_care_note[0]['photo_wounds']) ? $nurse_care_note[0]['photo_wounds'] : ''?>" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">Summary</div>
                                            <div class="col-md-9">
                                                <textarea class="form-control" name="summary" id="" cols="30" rows="2"><?=!empty($nurse_care_note[0]['summary']) ? $nurse_care_note[0]['summary'] : ''?></textarea> 
                                            </div>
                                        </div>
                                    </div>
                                    

                                        

                                        


                                        <div class="modal-footer">
                                            <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
                                            <button type="submit" class="btn bg-primary">Add</button>
                                        </div>
                                        </form>
                                </div>
                                   
                                </div>
                                
                            </div>
                        </div>
                    </div>

                

                    </div>


                </div>
            
    </div>

                </div><!--End side app-->

                    <!-- Right-sidebar-->
                <?php $this->load->view('backend/right_sidebar');?>

                    <?php $this->load->view('backend/footer');?>

                </div>
                <!-- End app-content-->
            </div>
        </div>
        <!-- End Page -->

        <!-- Back to top -->
        <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

    <?php $this->load->view('backend/footer_link');?>
<script>
    let allDate = document.querySelectorAll('.fetch_nurse_notes');

    allDate.forEach(function (eachDate) {
        eachDate.addEventListener('click', function(){
            let id = this.dataset.id
            let note_summary = document.getElementById('note_summary');
            let care_details = document.getElementById('care_details');
            let noteDate = document.getElementById('note_date');

            $.ajax({
            url: "<?php echo base_url();?>nurse/fetch_note_summary_by_id",
            type: 'POST',
            datatype: 'JSON',
            data: {id: id},
            success: function(data) {
            var result = JSON.parse(data);
            // console.log(result.nurse_note_details[0].details);
            care_details.innerHTML = '';

                noteDate.innerHTML = `Note Date: ${result.nurse_note.date}`;

                result.nurse_note_details.forEach(function (val){
                care_details.innerHTML += `<tr>
                                            <th width=60%>${val.details}</th> 
                                            <th>${val.care_note}</th>
                                          </tr>`;
                })
                
                note_summary.innerHTML = `<tr> <td>Temp</td><td>${result.nurse_note.temp}</td> </tr> 
                                          <tr> <td>Bp</td> <td>${result.nurse_note.bp}</td> </tr>
                                          <tr> <td>Pulse</td> <td>${result.nurse_note.pulse}</td> </tr>
                                          <tr> <td>Pain Sore</td><td>${result.nurse_note.pain_sore}</td> </tr>
                                          <tr> <td>Respiration</td> <td>${result.nurse_note.respiration}</td> </tr>
                                          <tr> <td>Spo2</td> <td>${result.nurse_note.spo2}</td> </tr>
                                          <tr> <td>Etc</td> <td>${result.nurse_note.etc}</td> </tr>
                                          <tr> <td>Photo Wounds</td> <td> <img src="uploads/${result.nurse_note.photo_wounds}" alt="" /> </td> </tr>
                                        <tr> <td>Summary</td> <td>${result.nurse_note.summary}</td> </tr>
                                          `;


                
            },
            error: function() {
            alert('Something is wrong');
            }
        });
        })
    });

    // fetch.addEventListener('click', (e)=>{

        

    // })


    // $('.fetch_nurse_notes').click(function()
    // {
    //     $.each( this, function( value ) {
    //       alert($(this).data('id'))
    //     });
    //     // alert('clicked');
        

    //    // var class_id = $(this).val();
    //    // html2 = html = '';
  
    // });

</script>
<script>
const cloudy = document.querySelectorAll('.urine_colour');
const cloudySubSelect = document.getElementById('cloudy_sub_select');
// const existCloudySubSelect = document.getElementById('exist_sub_select');

// if (existCloudySubSelect == "2_1") {
//     cloudySubSelect.style.display = 'block';
// }
// else{
//     cloudySubSelect.style.display = 'none';
// }

for (let i = 0; i < cloudy.length; i++) {
    cloudy[i].onchange = () => {
    if(cloudy[i].value == "2_1"){
        cloudySubSelect.style.display = 'block';
    }
    else {
        cloudySubSelect.style.display = 'none';
    }
}
}

</script>
<script type="text/javascript">
        
    const getStarted = document.getElementById('getStarted');
    

    getStarted.addEventListener('click', function(e){
    e.preventDefault();
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        cancelButtonColor: '#F27474',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
        },
         function(isConfirm){
           if (isConfirm){
             // swal("Shortlisted!", "Candidates are successfully shortlisted!", "success");
             window.location.href = getStarted.getAttribute('href');
            } else {
              swal("Cancelled", "Your imaginary file is safe :)", "error");
                 e.preventDefault();
            }
        })
    });

</script>  
<script>
    const serviceDone = document.getElementById('serviceDone');
    serviceDone.addEventListener('click', function(e){
    e.preventDefault();
    swal({
        title: "Are you sure?",
        // text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        cancelButtonColor: '#F27474',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
        },
         function(isConfirm){
           if (isConfirm){
             window.location.href = serviceDone.getAttribute('href');
            } else {
              swal("Cancelled", "", "error");
                 e.preventDefault();
            }
        })
    });

</script>
    </body>
</html>