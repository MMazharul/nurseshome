<?php $this->load->view('backend/head_link');?>

<body class="app sidebar-mini rtl">

  <!--Global-Loader-->
  <!-- <div id="global-loader">
			<img src="back_assets/images/icons/loader.svg" alt="loader">
		</div> -->

  <div class="page">
    <div class="page-main">
      <!--app-header-->


      <?php $this->load->view('backend/header');?>



      <!-- app-content-->
      <div class="container content-area">
        <div class="side-app">

          <!-- page-header -->
          <!-- <div class="page-header">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
                            </ol>
							<div class="ml-auto">
								<div class="input-group">
									<a  class="btn btn-primary text-white mr-2"  id="daterange-btn">
										<span>
											<i class="fa fa-calendar"></i> Events Settings
										</span>
										<i class="fa fa-caret-down"></i>
									</a>
									<a href="#" class="btn btn-secondary text-white" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
										<span>
											<i class="fa fa-star"></i>
										</span>
									</a>
								</div>
							</div>
						</div> -->
          <!-- End page-header -->

          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <div class="col-md-10">
                    <div class="card-title"><?=$form_title?></div>
                  </div>
                <!--   <div class="col-md-2">
                    <a class="btn btn-primary text-white mr-2" style="width:100%" id="">
                      <span> 
                      </span>
                    </a>
                  </div> -->
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                   <table class="responsive_table table table-striped table-bordered text-nowrap w-100">
                      <thead>
                      <tr>
                  <!--   <th class="text-center">Pic</th> -->
                        <th class="text-center">Name</th>
                        <th>Contact No</th>
                      
                        <th>Service Hours</th>
                         <th>Charge</th>
                      
                        <th>View Profile</th>
                      </tr>
                    </thead>
                    <tbody>


                                        <?php
foreach ($book_details as $key => $value)
 {
    $patientname=$value['first_name'].' '.$value['last_name'];
    $patientcontact=$value['contact_no'];
    $patientpic=$value['profile_pic'];    
    $bookhour=$value['total_hr'];      
    $book_charge=$value['totalamnt'];
    $patient_id=$value['patient_id'];
    ?>                                        <tr>
                                          
                                           
                                              <td><span><img src="uploads/<?php echo $patientpic?>" width="30px" height="30px"/></span> <?php echo $patientname?></td>
                                            <td><?php echo $patientcontact?></td>
                                            
                                            
                                            <td><?php echo $bookhour?></td>
                                              <td><?php echo $book_charge?></td>
                                            
                                            <td><a href="patient-profile/<?php echo $patient_id?>" class="btn bg-pink">View</a></td>
                                          
                                        </tr>
    <?php
}

                                        ?> 


  
                    </tbody>
                    </table>
                  </div>
                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->
            </div>
          </div>

        </div>
        <!--End side app-->

        <!-- Right-sidebar-->
        <?php $this->load->view('backend/right_sidebar');?>
        <!-- End Rightsidebar-->

        <?php $this->load->view('backend/footer');?>

      </div>
      <!-- End app-content-->
    </div>
  </div>
  <!-- End Page -->

  <!-- Back to top -->
  <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

  <?php $this->load->view('backend/footer_link');?>

</body>

</html>